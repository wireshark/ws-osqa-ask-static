+++
type = "question"
title = "Flow visualization of third parties web sites using wireshark"
description = '''I am currently undertaking a project on computer science using wireshark.I want to know if is possible to create a plugin that allows me visualize the third parties websites(google ads and more) ???'''
date = "2014-01-18T06:46:00Z"
lastmod = "2014-01-21T05:06:00Z"
weight = 29007
keywords = [ "using", "wireshar", "view" ]
aliases = [ "/questions/29007" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Flow visualization of third parties web sites using wireshark](/questions/29007/flow-visualization-of-third-parties-web-sites-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29007-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29007-score" class="post-score" title="current number of votes">0</div><span id="post-29007-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am currently undertaking a project on computer science using wireshark.I want to know if is possible to create a plugin that allows me visualize the third parties websites(google ads and more) ???</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-using" rel="tag" title="see questions tagged &#39;using&#39;">using</span> <span class="post-tag tag-link-wireshar" rel="tag" title="see questions tagged &#39;wireshar&#39;">wireshar</span> <span class="post-tag tag-link-view" rel="tag" title="see questions tagged &#39;view&#39;">view</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jan '14, 06:46</strong></p><img src="https://secure.gravatar.com/avatar/b4a3c5f539a5eef7cbe1041291114f8f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Astrokilla&#39;s gravatar image" /><p><span>Astrokilla</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Astrokilla has no accepted answers">0%</span></p></div></div><div id="comments-container-29007" class="comments-container"></div><div id="comment-tools-29007" class="comment-tools"></div><div class="clear"></div><div id="comment-29007-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29054"></span>

<div id="answer-container-29054" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29054-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29054-score" class="post-score" title="current number of votes">0</div><span id="post-29054-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Astrokilla has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>want to know if is possible to create a plugin that allows me visualize the third parties websites</p></blockquote><p>sure, you can write a Listener/Tap for Wireshark, either in C or in Lua (<a href="http://wiki.wireshark.org/Lua/Taps">http://wiki.wireshark.org/Lua/Taps</a> ). Within that Listener you can aggregate data and do whatever you need, including the visualization of 'third parties websites'.</p><p>See the code of <a href="http://anonsvn.wireshark.org/wireshark/trunk-1.10/epan/dissectors/packet-http.c">packet-http.c</a> (look for: register_tap("http")). The HTTP dissector registers a TAP (Listener) to be able to Export HTTP objects (File -&gt; Export Objects -&gt; HTTP).</p><p>See also: <a href="http://anonsvn.wireshark.org/wireshark/trunk-1.10/doc/README.developer">README.developer</a> and <a href="http://anonsvn.wireshark.org/wireshark/trunk-1.10/doc/README.tapping">README.tapping</a>.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Jan '14, 04:54</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-29054" class="comments-container"><span id="29055"></span><div id="comment-29055" class="comment"><div id="post-29055-score" class="comment-score"></div><div class="comment-text"><p>Thank so much for the help Kurt !!!</p></div><div id="comment-29055-info" class="comment-info"><span class="comment-age">(21 Jan '14, 05:01)</span> <span class="comment-user userinfo">Astrokilla</span></div></div><span id="29056"></span><div id="comment-29056" class="comment"><div id="post-29056-score" class="comment-score"></div><div class="comment-text"><p>You're welcome.</p><p>Hint: If a supplied answer resolves your question can you please "accept" it by clicking the checkmark icon next to it. This highlights good answers for the benefit of subsequent users with the same or similar questions. For extra points you can up vote the answer (thumb up).</p></div><div id="comment-29056-info" class="comment-info"><span class="comment-age">(21 Jan '14, 05:06)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-29054" class="comment-tools"></div><div class="clear"></div><div id="comment-29054-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

