+++
type = "question"
title = "How to get IP level logs from agilent test set E703E version using wireshark tool? please share the setup required for this?"
description = '''My test phone is connected via RF cable to agilent test set. wireshark is installed in another PC. agilent test set has one LAN port and one DATA port. how to make a setup where I can trace my test phone IP level information using wireshark?'''
date = "2014-01-30T01:12:00Z"
lastmod = "2014-01-30T01:18:00Z"
weight = 29296
keywords = [ "namrata" ]
aliases = [ "/questions/29296" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to get IP level logs from agilent test set E703E version using wireshark tool? please share the setup required for this?](/questions/29296/how-to-get-ip-level-logs-from-agilent-test-set-e703e-version-using-wireshark-tool-please-share-the-setup-required-for-this)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29296-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29296-score" class="post-score" title="current number of votes">0</div><span id="post-29296-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My test phone is connected via RF cable to agilent test set. wireshark is installed in another PC. agilent test set has one LAN port and one DATA port. how to make a setup where I can trace my test phone IP level information using wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-namrata" rel="tag" title="see questions tagged &#39;namrata&#39;">namrata</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jan '14, 01:12</strong></p><img src="https://secure.gravatar.com/avatar/bd97c1d4ec6f1122b816835825456090?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Namrata&#39;s gravatar image" /><p><span>Namrata</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Namrata has no accepted answers">0%</span></p></div></div><div id="comments-container-29296" class="comments-container"><span id="29297"></span><div id="comment-29297" class="comment"><div id="post-29297-score" class="comment-score"></div><div class="comment-text"><p>Well, isn't that something you should ask agilent support?</p></div><div id="comment-29297-info" class="comment-info"><span class="comment-age">(30 Jan '14, 01:18)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-29296" class="comment-tools"></div><div class="clear"></div><div id="comment-29296-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

