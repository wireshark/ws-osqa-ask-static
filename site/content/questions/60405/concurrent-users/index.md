+++
type = "question"
title = "Concurrent Users"
description = '''I would like to install Wireshark on a HP Blade server that has one interface. How can I set it up so that up to 10 users can connect to the server and take separate traces?'''
date = "2017-03-29T06:53:00Z"
lastmod = "2017-03-29T14:12:00Z"
weight = 60405
keywords = [ "about_wireshark" ]
aliases = [ "/questions/60405" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Concurrent Users](/questions/60405/concurrent-users)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60405-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60405-score" class="post-score" title="current number of votes">0</div><span id="post-60405-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to install Wireshark on a HP Blade server that has one interface. How can I set it up so that up to 10 users can connect to the server and take separate traces?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-about_wireshark" rel="tag" title="see questions tagged &#39;about_wireshark&#39;">about_wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Mar '17, 06:53</strong></p><img src="https://secure.gravatar.com/avatar/09e8b441e138570eb4a800c44592811d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Blackers&#39;s gravatar image" /><p><span>Blackers</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Blackers has no accepted answers">0%</span></p></div></div><div id="comments-container-60405" class="comments-container"></div><div id="comment-tools-60405" class="comment-tools"></div><div class="clear"></div><div id="comment-60405-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60431"></span>

<div id="answer-container-60431" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60431-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60431-score" class="post-score" title="current number of votes">1</div><span id="post-60431-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Run six instances of dumpcap with an appropriate capture filter set in each - say one for each user IP address if that's possible.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Mar '17, 14:12</strong></p><img src="https://secure.gravatar.com/avatar/2e1b4057f2ff59fe059b23cc6571abaf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="PaulOfford&#39;s gravatar image" /><p><span>PaulOfford</span><br />
<span class="score" title="131 reputation points">131</span><span title="28 badges"><span class="badge1">●</span><span class="badgecount">28</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="PaulOfford has 5 accepted answers">11%</span></p></div></div><div id="comments-container-60431" class="comments-container"></div><div id="comment-tools-60431" class="comment-tools"></div><div class="clear"></div><div id="comment-60431-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

