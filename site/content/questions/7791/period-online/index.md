+++
type = "question"
title = "period online"
description = '''I want to use Wireshark to check how much time my kids spend in front of the computer by seeing how long they were online. Is there a simple command or filter to see that?'''
date = "2011-12-06T01:33:00Z"
lastmod = "2011-12-11T20:55:00Z"
weight = 7791
keywords = [ "time" ]
aliases = [ "/questions/7791" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [period online](/questions/7791/period-online)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7791-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7791-score" class="post-score" title="current number of votes">0</div><span id="post-7791-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to use Wireshark to check how much time my kids spend in front of the computer by seeing how long they were online. Is there a simple command or filter to see that?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Dec '11, 01:33</strong></p><img src="https://secure.gravatar.com/avatar/99795f1edf66c46eebbd262d3a615cc1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="schwar42&#39;s gravatar image" /><p><span>schwar42</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="schwar42 has no accepted answers">0%</span></p></div></div><div id="comments-container-7791" class="comments-container"><span id="7795"></span><div id="comment-7795" class="comment"><div id="post-7795-score" class="comment-score"></div><div class="comment-text"><p>There are much easier ways to realize that e.g. buy a router with monitoring support and/or "child safety" features</p></div><div id="comment-7795-info" class="comment-info"><span class="comment-age">(06 Dec '11, 02:33)</span> <span class="comment-user userinfo">Landi</span></div></div><span id="7903"></span><div id="comment-7903" class="comment"><div id="post-7903-score" class="comment-score"></div><div class="comment-text"><p>I could say so much here... but I've been advised to refrain....</p></div><div id="comment-7903-info" class="comment-info"><span class="comment-age">(11 Dec '11, 20:55)</span> <span class="comment-user userinfo">lchappell ♦</span></div></div></div><div id="comment-tools-7791" class="comment-tools"></div><div class="clear"></div><div id="comment-7791-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

