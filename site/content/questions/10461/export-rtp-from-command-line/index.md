+++
type = "question"
title = "Export RTP from command line"
description = '''In Wireshark GUI, I can export a .raw / .au file by using   Telephony &amp;gt; RTP &amp;gt; Stream Analysis ...&amp;gt; Save Payload  But how can I export the .raw / .au by using command line ?  Thanks a lot!'''
date = "2012-04-26T08:08:00Z"
lastmod = "2012-06-29T19:07:00Z"
weight = 10461
keywords = [ "rtp", "command-line" ]
aliases = [ "/questions/10461" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Export RTP from command line](/questions/10461/export-rtp-from-command-line)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10461-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10461-score" class="post-score" title="current number of votes">0</div><span id="post-10461-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In Wireshark GUI, I can export a .raw / .au file by using</p><blockquote><p>Telephony &gt; RTP &gt; Stream Analysis ...&gt; Save Payload</p></blockquote><p>But how can I export the .raw / .au by using command line ?</p><p>Thanks a lot!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-command-line" rel="tag" title="see questions tagged &#39;command-line&#39;">command-line</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Apr '12, 08:08</strong></p><img src="https://secure.gravatar.com/avatar/37b682ab09006e00bd4f53d761690338?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="manfree&#39;s gravatar image" /><p><span>manfree</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="manfree has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>27 Apr '12, 04:12</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-10461" class="comments-container"></div><div id="comment-tools-10461" class="comment-tools"></div><div class="clear"></div><div id="comment-10461-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12341"></span>

<div id="answer-container-12341" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12341-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12341-score" class="post-score" title="current number of votes">0</div><span id="post-12341-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can use oreka to do this.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Jun '12, 19:07</strong></p><img src="https://secure.gravatar.com/avatar/cdbb36db3a1bc78b87865c0e5c0e2e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grinob&#39;s gravatar image" /><p><span>grinob</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grinob has no accepted answers">0%</span></p></div></div><div id="comments-container-12341" class="comments-container"></div><div id="comment-tools-12341" class="comment-tools"></div><div class="clear"></div><div id="comment-12341-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

