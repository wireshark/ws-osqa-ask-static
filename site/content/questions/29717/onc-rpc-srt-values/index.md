+++
type = "question"
title = "ONC-RPC SRT values"
description = '''I am running Wireshark version 1.10.0 on Mac OS X 10.9.1. Displaying ONC-RPC Service Response Time statistics for NFS version 3. What are the displayed values? Microseconds? Seconds? 10 Microseconds? Thank you very much. An amazing tool. Jim'''
date = "2014-02-11T13:46:00Z"
lastmod = "2014-02-12T12:26:00Z"
weight = 29717
keywords = [ "srt", "onc-rpc" ]
aliases = [ "/questions/29717" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [ONC-RPC SRT values](/questions/29717/onc-rpc-srt-values)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29717-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29717-score" class="post-score" title="current number of votes">0</div><span id="post-29717-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am running Wireshark version 1.10.0 on Mac OS X 10.9.1.</p><p>Displaying ONC-RPC Service Response Time statistics for NFS version 3.</p><p>What are the displayed values? Microseconds? Seconds? 10 Microseconds?</p><p>Thank you very much.</p><p>An amazing tool. Jim</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-srt" rel="tag" title="see questions tagged &#39;srt&#39;">srt</span> <span class="post-tag tag-link-onc-rpc" rel="tag" title="see questions tagged &#39;onc-rpc&#39;">onc-rpc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Feb '14, 13:46</strong></p><img src="https://secure.gravatar.com/avatar/3f8bd3eb59c9b5fd97c5757704d374cf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mauroj&#39;s gravatar image" /><p><span>mauroj</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mauroj has no accepted answers">0%</span></p></div></div><div id="comments-container-29717" class="comments-container"></div><div id="comment-tools-29717" class="comment-tools"></div><div class="clear"></div><div id="comment-29717-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29789"></span>

<div id="answer-container-29789" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29789-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29789-score" class="post-score" title="current number of votes">0</div><span id="post-29789-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I determined that the values are in seconds.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Feb '14, 12:12</strong></p><img src="https://secure.gravatar.com/avatar/3f8bd3eb59c9b5fd97c5757704d374cf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mauroj&#39;s gravatar image" /><p><span>mauroj</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mauroj has no accepted answers">0%</span></p></div></div><div id="comments-container-29789" class="comments-container"><span id="29790"></span><div id="comment-29790" class="comment"><div id="post-29790-score" class="comment-score"></div><div class="comment-text"><p>On a related note, it would be a good idea I think to specifically state in the documentation:</p><p><a href="http://www.wireshark.org/docs/wsug_html_chunked/ChStatSRT.html">http://www.wireshark.org/docs/wsug_html_chunked/ChStatSRT.html</a></p><p>What the time values are.</p></div><div id="comment-29790-info" class="comment-info"><span class="comment-age">(12 Feb '14, 12:26)</span> <span class="comment-user userinfo">mauroj</span></div></div></div><div id="comment-tools-29789" class="comment-tools"></div><div class="clear"></div><div id="comment-29789-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

