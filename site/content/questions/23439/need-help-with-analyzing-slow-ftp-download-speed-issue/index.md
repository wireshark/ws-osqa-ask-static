+++
type = "question"
title = "Need help with Analyzing slow FTP download speed issue"
description = '''We are facing slow FTP download issue , could someone please assist with things we need to look at that may be causing issue, internet utilization is normal , no B.W hogging is there.'''
date = "2013-07-30T00:53:00Z"
lastmod = "2013-07-30T01:06:00Z"
weight = 23439
keywords = [ "ftp", "slow" ]
aliases = [ "/questions/23439" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Need help with Analyzing slow FTP download speed issue](/questions/23439/need-help-with-analyzing-slow-ftp-download-speed-issue)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23439-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23439-score" class="post-score" title="current number of votes">0</div><span id="post-23439-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We are facing slow FTP download issue , could someone please assist with things we need to look at that may be causing issue, internet utilization is normal , no B.W hogging is there.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ftp" rel="tag" title="see questions tagged &#39;ftp&#39;">ftp</span> <span class="post-tag tag-link-slow" rel="tag" title="see questions tagged &#39;slow&#39;">slow</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jul '13, 00:53</strong></p><img src="https://secure.gravatar.com/avatar/6615a61d69b703d89076bb0f18342bbf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="m_1607&#39;s gravatar image" /><p><span>m_1607</span><br />
<span class="score" title="35 reputation points">35</span><span title="12 badges"><span class="badge1">●</span><span class="badgecount">12</span></span><span title="13 badges"><span class="silver">●</span><span class="badgecount">13</span></span><span title="16 badges"><span class="bronze">●</span><span class="badgecount">16</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="m_1607 has no accepted answers">0%</span></p></div></div><div id="comments-container-23439" class="comments-container"><span id="23442"></span><div id="comment-23442" class="comment"><div id="post-23442-score" class="comment-score"></div><div class="comment-text"><p>As said before, this is not a place to seek free consultancy on solving your network issues. This site is here to assist you in using wireshark to solve your network issues on your own.</p><p>Please ask specific questions on how to use wireshark or interpret its output. If you need more help, than you might want to hire a network consultant to solve your issues.</p></div><div id="comment-23442-info" class="comment-info"><span class="comment-age">(30 Jul '13, 01:06)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-23439" class="comment-tools"></div><div class="clear"></div><div id="comment-23439-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

