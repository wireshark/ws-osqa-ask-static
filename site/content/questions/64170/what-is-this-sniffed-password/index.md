+++
type = "question"
title = "what is this sniffed password ?"
description = ''''''
date = "2017-10-24T14:35:00Z"
lastmod = "2017-10-25T05:18:00Z"
weight = 64170
keywords = [ "sniffing", "text", "password", "hex" ]
aliases = [ "/questions/64170" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [what is this sniffed password ?](/questions/64170/what-is-this-sniffed-password)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64170-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64170-score" class="post-score" title="current number of votes">0</div><span id="post-64170-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/ask.JPG" alt="dear all, can you please interpret the password in the attached photo, i.e from hex to text so i can use it for login" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-text" rel="tag" title="see questions tagged &#39;text&#39;">text</span> <span class="post-tag tag-link-password" rel="tag" title="see questions tagged &#39;password&#39;">password</span> <span class="post-tag tag-link-hex" rel="tag" title="see questions tagged &#39;hex&#39;">hex</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Oct '17, 14:35</strong></p><img src="https://secure.gravatar.com/avatar/3b4cf656d865077dc101cb18417c9438?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mnewiraq&#39;s gravatar image" /><p><span>mnewiraq</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mnewiraq has no accepted answers">0%</span></p></img></div></div><div id="comments-container-64170" class="comments-container"></div><div id="comment-tools-64170" class="comment-tools"></div><div class="clear"></div><div id="comment-64170-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="64171"></span>

<div id="answer-container-64171" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64171-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64171-score" class="post-score" title="current number of votes">0</div><span id="post-64171-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Looks almost like it is hex encoded but there are some parts that aren't, e.g. %C3K, %219 and %0F0. A common way to find out how this works is to visit the website with a known password and check the cookie value to find out how the encoding works.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Oct '17, 14:42</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-64171" class="comments-container"><span id="64173"></span><div id="comment-64173" class="comment"><div id="post-64173-score" class="comment-score"></div><div class="comment-text"><p>thanks dude</p><p>can i get your skype or whatsapp please</p><p>i need your help for more details, kindly.</p></div><div id="comment-64173-info" class="comment-info"><span class="comment-age">(24 Oct '17, 14:49)</span> <span class="comment-user userinfo">mnewiraq</span></div></div><span id="64181"></span><div id="comment-64181" class="comment"><div id="post-64181-score" class="comment-score"></div><div class="comment-text"><p>You're welcome. I use neither Sykpe nor WhatsApp, or any other application of that kind, sorry.</p></div><div id="comment-64181-info" class="comment-info"><span class="comment-age">(25 Oct '17, 05:18)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-64171" class="comment-tools"></div><div class="clear"></div><div id="comment-64171-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

