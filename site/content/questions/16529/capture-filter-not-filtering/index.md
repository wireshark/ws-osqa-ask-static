+++
type = "question"
title = "Capture filter not filtering"
description = '''I have 1.8.3 version of Wireshark and I can&#x27;t apply capture filters. I typed &quot;port 80&quot; (w/o quotes) and then I start capture, but all traffic is capturing. What is strange is that if I reopen capture filters window filter string input is empty.'''
date = "2012-12-04T03:18:00Z"
lastmod = "2012-12-04T03:18:00Z"
weight = 16529
keywords = [ "filter", "capture" ]
aliases = [ "/questions/16529" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capture filter not filtering](/questions/16529/capture-filter-not-filtering)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16529-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16529-score" class="post-score" title="current number of votes">0</div><span id="post-16529-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have 1.8.3 version of Wireshark and I can't apply capture filters. I typed "port 80" (w/o quotes) and then I start capture, but all traffic is capturing. What is strange is that if I reopen capture filters window filter string input is empty.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Dec '12, 03:18</strong></p><img src="https://secure.gravatar.com/avatar/3eb4331cfde5927ff4d4e9ca10510473?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Coolmax&#39;s gravatar image" /><p><span>Coolmax</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Coolmax has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>04 Dec '12, 10:42</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-16529" class="comments-container"></div><div id="comment-tools-16529" class="comment-tools"></div><div class="clear"></div><div id="comment-16529-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

