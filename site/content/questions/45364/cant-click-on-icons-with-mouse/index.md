+++
type = "question"
title = "Can&#x27;t click on icons with mouse"
description = '''Hi, When i start Wireshark i can click on everything on the start page, but not on any of the icons above? File, Edit, View and so on doesn&#x27;t work. Nothing happens. I&#x27;m using Windows 7 x64 and dual monitor. Wireshark 1.12.7 What can be wrong? Best regards Johnny'''
date = "2015-08-26T03:33:00Z"
lastmod = "2015-08-26T03:33:00Z"
weight = 45364
keywords = [ "menu", "mouse", "icons" ]
aliases = [ "/questions/45364" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can't click on icons with mouse](/questions/45364/cant-click-on-icons-with-mouse)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45364-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45364-score" class="post-score" title="current number of votes">0</div><span id="post-45364-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>When i start Wireshark i can click on everything on the start page, but not on any of the icons above? File, Edit, View and so on doesn't work. Nothing happens.</p><p>I'm using Windows 7 x64 and dual monitor. Wireshark 1.12.7</p><p>What can be wrong?</p><p>Best regards Johnny</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-menu" rel="tag" title="see questions tagged &#39;menu&#39;">menu</span> <span class="post-tag tag-link-mouse" rel="tag" title="see questions tagged &#39;mouse&#39;">mouse</span> <span class="post-tag tag-link-icons" rel="tag" title="see questions tagged &#39;icons&#39;">icons</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Aug '15, 03:33</strong></p><img src="https://secure.gravatar.com/avatar/9eb975d590e75f0943149a56f54de905?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Wempa&#39;s gravatar image" /><p><span>Wempa</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Wempa has no accepted answers">0%</span></p></div></div><div id="comments-container-45364" class="comments-container"></div><div id="comment-tools-45364" class="comment-tools"></div><div class="clear"></div><div id="comment-45364-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

