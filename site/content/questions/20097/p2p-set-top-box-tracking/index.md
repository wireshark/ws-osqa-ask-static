+++
type = "question"
title = "p2p set top box tracking"
description = '''I was shown a demonstration of wireshark a few months ago. We were trying to trace the ip addresses and country of origin from a p2p iptv set top box. I&#x27;m not sure if he had any add ons to the wireshark software. I want to set up the same scenario. The stb was connected to the internet and to a lapt...'''
date = "2013-04-04T22:37:00Z"
lastmod = "2013-04-05T00:14:00Z"
weight = 20097
keywords = [ "box", "top", "set", "p2p", "tracking" ]
aliases = [ "/questions/20097" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [p2p set top box tracking](/questions/20097/p2p-set-top-box-tracking)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20097-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20097-score" class="post-score" title="current number of votes">0</div><span id="post-20097-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I was shown a demonstration of wireshark a few months ago. We were trying to trace the ip addresses and country of origin from a p2p iptv set top box. I'm not sure if he had any add ons to the wireshark software. I want to set up the same scenario. The stb was connected to the internet and to a laptop via a small 3 way switch. I've tried to set it up again but with my desktop but it's just giving me the ip addresses of my PC &amp; the ip address of the stb. Can you point me in the right direction with which tutorials I have to read / see please. And whether I need to install any add-ons????</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-box" rel="tag" title="see questions tagged &#39;box&#39;">box</span> <span class="post-tag tag-link-top" rel="tag" title="see questions tagged &#39;top&#39;">top</span> <span class="post-tag tag-link-set" rel="tag" title="see questions tagged &#39;set&#39;">set</span> <span class="post-tag tag-link-p2p" rel="tag" title="see questions tagged &#39;p2p&#39;">p2p</span> <span class="post-tag tag-link-tracking" rel="tag" title="see questions tagged &#39;tracking&#39;">tracking</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Apr '13, 22:37</strong></p><img src="https://secure.gravatar.com/avatar/fe667e85346029aa1fc01b9e152ebb36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jkang13&#39;s gravatar image" /><p><span>jkang13</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jkang13 has no accepted answers">0%</span></p></div></div><div id="comments-container-20097" class="comments-container"></div><div id="comment-tools-20097" class="comment-tools"></div><div class="clear"></div><div id="comment-20097-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20102"></span>

<div id="answer-container-20102" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20102-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20102-score" class="post-score" title="current number of votes">1</div><span id="post-20102-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have a look at <a href="http://wiki.wireshark.org/HowToUseGeoIP">http://wiki.wireshark.org/HowToUseGeoIP</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Apr '13, 00:14</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-20102" class="comments-container"></div><div id="comment-tools-20102" class="comment-tools"></div><div class="clear"></div><div id="comment-20102-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

