+++
type = "question"
title = "Device driver disabling and showing yellow mark in Device manager."
description = '''Hi, I am using tshark.exe file to capture USB trace, some time i am facing driver disabling and showing yellow mark in device manager and USBPcapCMD.exe is not able to detect drives. Please any one Help me to solve this issue. Thanks in Advance!!!! '''
date = "2017-06-09T06:47:00Z"
lastmod = "2017-06-09T06:47:00Z"
weight = 61909
keywords = [ "usbpcapcmd", "tshark" ]
aliases = [ "/questions/61909" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Device driver disabling and showing yellow mark in Device manager.](/questions/61909/device-driver-disabling-and-showing-yellow-mark-in-device-manager)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61909-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61909-score" class="post-score" title="current number of votes">0</div><span id="post-61909-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am using <strong>tshark.exe</strong> file to capture USB trace, some time i am facing driver disabling and showing yellow mark in device manager and <strong>USBPcapCMD.exe</strong> is not able to detect drives.</p><p>Please any one Help me to solve this issue.</p><p>Thanks in Advance!!!!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-usbpcapcmd" rel="tag" title="see questions tagged &#39;usbpcapcmd&#39;">usbpcapcmd</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jun '17, 06:47</strong></p><img src="https://secure.gravatar.com/avatar/d2c9789a43b411fb047ce641badacaf5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pramod&#39;s gravatar image" /><p><span>Pramod</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pramod has no accepted answers">0%</span></p></div></div><div id="comments-container-61909" class="comments-container"></div><div id="comment-tools-61909" class="comment-tools"></div><div class="clear"></div><div id="comment-61909-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

