+++
type = "question"
title = "Is there any dissector available for GTPv2 (29.274) - context request/response,Forward relocation request/response ?"
description = '''I am using Wireshark Version 1.4.2 (SVN Rev 34959 from /trunk-1.4) I found that the below GTPv2 messages are not dissected completely(IE data not dissected).  Is there any dissector available for GTPv2 29.274 V8.4.0(2009-12) for complete decoding of  1.context request/response, 2.Forward relocation ...'''
date = "2010-12-30T02:21:00Z"
lastmod = "2010-12-30T02:57:00Z"
weight = 1534
keywords = [ "gtpv2", "s10", "messages", "decoding" ]
aliases = [ "/questions/1534" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Is there any dissector available for GTPv2 (29.274) - context request/response,Forward relocation request/response ?](/questions/1534/is-there-any-dissector-available-for-gtpv2-29274-context-requestresponseforward-relocation-requestresponse)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1534-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1534-score" class="post-score" title="current number of votes">0</div><span id="post-1534-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using Wireshark Version 1.4.2 (SVN Rev 34959 from /trunk-1.4) I found that the below GTPv2 messages are not dissected completely(IE data not dissected). Is there any dissector available for GTPv2 29.274 V8.4.0(2009-12) for complete decoding of 1.context request/response, 2.Forward relocation request/response ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gtpv2" rel="tag" title="see questions tagged &#39;gtpv2&#39;">gtpv2</span> <span class="post-tag tag-link-s10" rel="tag" title="see questions tagged &#39;s10&#39;">s10</span> <span class="post-tag tag-link-messages" rel="tag" title="see questions tagged &#39;messages&#39;">messages</span> <span class="post-tag tag-link-decoding" rel="tag" title="see questions tagged &#39;decoding&#39;">decoding</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Dec '10, 02:21</strong></p><img src="https://secure.gravatar.com/avatar/a25e304c88d25af731b808c7f362c96c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="surendar&#39;s gravatar image" /><p><span>surendar</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="surendar has no accepted answers">0%</span></p></div></div><div id="comments-container-1534" class="comments-container"></div><div id="comment-tools-1534" class="comment-tools"></div><div class="clear"></div><div id="comment-1534-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1537"></span>

<div id="answer-container-1537" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1537-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1537-score" class="post-score" title="current number of votes">0</div><span id="post-1537-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You should file a bug report on <a href="https://bugs.wireshark.org">bugs.wireshark.org</a>, providing a sample capture and description.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Dec '10, 02:57</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-1537" class="comments-container"></div><div id="comment-tools-1537" class="comment-tools"></div><div class="clear"></div><div id="comment-1537-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

