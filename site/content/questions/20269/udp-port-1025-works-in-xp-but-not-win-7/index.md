+++
type = "question"
title = "UDP port 1025 works in XP, but not Win 7"
description = '''Hi,  I&#x27;m trying to capture packets from my embedded board (IP 192.168.66.190, port 1025) using UDP. It&#x27;s working fine in Windows XP, but it doesn&#x27;t work in Windows 7. Wireshark is still able to detect the packets coming in, but there is nothing when a UDP listener is started on the same port.'''
date = "2013-04-10T03:31:00Z"
lastmod = "2013-04-10T06:06:00Z"
weight = 20269
keywords = [ "1025", "windows", "udp", "port", "7" ]
aliases = [ "/questions/20269" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [UDP port 1025 works in XP, but not Win 7](/questions/20269/udp-port-1025-works-in-xp-but-not-win-7)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20269-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20269-score" class="post-score" title="current number of votes">0</div><span id="post-20269-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I'm trying to capture packets from my embedded board (IP 192.168.66.190, port 1025) using UDP. It's working fine in Windows XP, but it doesn't work in Windows 7. Wireshark is still able to detect the packets coming in, but there is nothing when a UDP listener is started on the same port.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-1025" rel="tag" title="see questions tagged &#39;1025&#39;">1025</span> <span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span> <span class="post-tag tag-link-port" rel="tag" title="see questions tagged &#39;port&#39;">port</span> <span class="post-tag tag-link-7" rel="tag" title="see questions tagged &#39;7&#39;">7</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Apr '13, 03:31</strong></p><img src="https://secure.gravatar.com/avatar/5f96cece918c3779f9fdf0839bd2bec3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ktchiam&#39;s gravatar image" /><p><span>ktchiam</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ktchiam has no accepted answers">0%</span></p></div></div><div id="comments-container-20269" class="comments-container"></div><div id="comment-tools-20269" class="comment-tools"></div><div class="clear"></div><div id="comment-20269-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="20270"></span>

<div id="answer-container-20270" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20270-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20270-score" class="post-score" title="current number of votes">1</div><span id="post-20270-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's a Windows question really, since Wireshark is able to capture them.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Apr '13, 03:54</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-20270" class="comments-container"></div><div id="comment-tools-20270" class="comment-tools"></div><div class="clear"></div><div id="comment-20270-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="20271"></span>

<div id="answer-container-20271" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20271-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20271-score" class="post-score" title="current number of votes">1</div><span id="post-20271-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try turning the Windows Firewall off. If that fixes the issue and you want the firewall back on (as you should do generally) then you'll need to add an exception to the firewall for your app to allow the UDP traffic in.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Apr '13, 04:19</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-20271" class="comments-container"><span id="20272"></span><div id="comment-20272" class="comment"><div id="post-20272-score" class="comment-score"></div><div class="comment-text"><p>Sorry that it's kind of Windows related, but since Wireshark can capture the packets, may be someone would know why.</p><p>I've turned off the firewall, but it still doesn't work. I've also opened up another client to send UDP packets to 1025 and it can be received. It just won't receive packets coming from my ethernet port.</p></div><div id="comment-20272-info" class="comment-info"><span class="comment-age">(10 Apr '13, 04:23)</span> <span class="comment-user userinfo">ktchiam</span></div></div><span id="20273"></span><div id="comment-20273" class="comment"><div id="post-20273-score" class="comment-score"></div><div class="comment-text"><p>Wireshark, or to be technically accurate WinPCap, captures traffic in a very different manner than a normal app would receive it.</p><p>When you say you've opened up another client to send UDP packets and they are received, was that client on the same host as the listener app, or another host?</p><p>Does your app bind it's listener to the required IP address or does it listen on all?</p></div><div id="comment-20273-info" class="comment-info"><span class="comment-age">(10 Apr '13, 04:40)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-20271" class="comment-tools"></div><div class="clear"></div><div id="comment-20271-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="20278"></span>

<div id="answer-container-20278" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20278-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20278-score" class="post-score" title="current number of votes">0</div><span id="post-20278-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Sorry that it's kind of Windows related, but since Wireshark can capture the packets, may be someone would know why.</p></blockquote><p>Probably because the packets are somehow 'damaged' and windows drops them. Take a look at the IP stats of windows. Do the counters for 'errors' increase while you are seeing the packets in Wireshark, but <strong>not</strong> in your application?</p><pre><code>netstat -s

IPv4 Statistics

  Packets Received                   = 1070088
  Received Header Errors             = 0
  Received Address Errors            = 271
  Datagrams Forwarded                = 0
  Unknown Protocols Received         = 0
  Received Packets Discarded         = 67898
  Received Packets Delivered         = 1108133
  Output Requests                    = 1979878
  Routing Discards                   = 0
  Discarded Output Packets           = 5809
  Output Packet No Route             = 1
  Reassembly Required                = 0
  Reassembly Successful              = 0
  Reassembly Failures                = 0
  Datagrams Successfully Fragmented  = 0
  Datagrams Failing Fragmentation    = 0
  Fragments Created                  = 0

UDP Statistics for IPv4

  Datagrams Received    = 40722
  No Ports              = 352
  Receive Errors        = 67544
  Datagrams Sent        = 61341
</code></pre><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Apr '13, 06:06</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Apr '13, 06:08</strong> </span></p></div></div><div id="comments-container-20278" class="comments-container"></div><div id="comment-tools-20278" class="comment-tools"></div><div class="clear"></div><div id="comment-20278-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

