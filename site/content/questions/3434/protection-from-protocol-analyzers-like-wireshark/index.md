+++
type = "question"
title = "Protection from protocol analyzers like wireshark"
description = '''How can i protect my computer from people who try to capture or sniff my computer from protocol analyzers? Just wondering..'''
date = "2011-04-10T17:27:00Z"
lastmod = "2012-08-02T02:17:00Z"
weight = 3434
keywords = [ "protect" ]
aliases = [ "/questions/3434" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [Protection from protocol analyzers like wireshark](/questions/3434/protection-from-protocol-analyzers-like-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3434-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3434-score" class="post-score" title="current number of votes">0</div><span id="post-3434-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can i protect my computer from people who try to capture or sniff my computer from protocol analyzers? Just wondering..</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-protect" rel="tag" title="see questions tagged &#39;protect&#39;">protect</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Apr '11, 17:27</strong></p><img src="https://secure.gravatar.com/avatar/837927f1c9adfe4589641435c5fa0577?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kv2004&#39;s gravatar image" /><p><span>kv2004</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kv2004 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Feb '12, 22:28</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-3434" class="comments-container"></div><div id="comment-tools-3434" class="comment-tools"></div><div class="clear"></div><div id="comment-3434-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="3435"></span>

<div id="answer-container-3435" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3435-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3435-score" class="post-score" title="current number of votes">1</div><span id="post-3435-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Keep them away from your Switches and routers. If they don't have physical access to your infrastructure and do not know the administrative passwords to configure monitor ports they're not going to sniff your computer (except maybe broadcasts, which aren't usually that exciting to capture).</p><p>Regarding Wireless: it might be difficult to prevent anyone from getting into range, but that's what WPA2 with a strong password (16+ random chars) is for - let them have all that garbled bits and bytes ;-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Apr '11, 23:10</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-3435" class="comments-container"></div><div id="comment-tools-3435" class="comment-tools"></div><div class="clear"></div><div id="comment-3435-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="3457"></span>

<div id="answer-container-3457" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3457-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3457-score" class="post-score" title="current number of votes">1</div><span id="post-3457-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Use encryption to transmit sensitive data. For example:</p><ul><li>Use HTTPS, not HTTP</li><li>Use SSH, not TELNET</li><li>Use FTP server and client that support SSL</li><li>Use IPsec</li><li>...</li></ul></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Apr '11, 16:11</strong></p><img src="https://secure.gravatar.com/avatar/3b60e92020a427bb24332efc0b560943?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="packethunter&#39;s gravatar image" /><p><span>packethunter</span><br />
<span class="score" title="2137 reputation points"><span>2.1k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="packethunter has 8 accepted answers">8%</span></p></div></div><div id="comments-container-3457" class="comments-container"></div><div id="comment-tools-3457" class="comment-tools"></div><div class="clear"></div><div id="comment-3457-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="13309"></span>

<div id="answer-container-13309" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13309-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13309-score" class="post-score" title="current number of votes">-1</div><span id="post-13309-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>WIRESHARK is unstoppable sir. i am expert on that.</p><p>https means crypted but md5 algo is non reversable...</p><p>just a small hint : use secret 7 instead of password 5 in router switch and linux platforms....caus esecret 7 is md5 hashed.....wireshark can only see whatever u see in cli...md5ed secret....</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Aug '12, 02:17</strong></p><img src="https://secure.gravatar.com/avatar/0afe1c5c0811d8ccfcd7f651e4d3ca98?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="emrah&#39;s gravatar image" /><p><span>emrah</span><br />
<span class="score" title="0 reputation points">0</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="emrah has no accepted answers">0%</span></p></div></div><div id="comments-container-13309" class="comments-container"></div><div id="comment-tools-13309" class="comment-tools"></div><div class="clear"></div><div id="comment-13309-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

