+++
type = "question"
title = "Unable to write to standard output: The pipe is being closed"
description = '''Hi, when I try to capture traffic in EVE software and It opens the wireshark, In CMD I receive bellow error and nothing capture. &quot;Connecting to &quot;root&quot;@192.168.125.25...&quot; tcpdump: listening on vunl0_2_0, link-type EN10MB (Ethernet), capture size 262144 bytes Unable to write to standard output: The pi...'''
date = "2017-04-07T11:32:00Z"
lastmod = "2017-04-09T12:03:00Z"
weight = 60661
keywords = [ "capture", "cannot" ]
aliases = [ "/questions/60661" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to write to standard output: The pipe is being closed](/questions/60661/unable-to-write-to-standard-output-the-pipe-is-being-closed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60661-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60661-score" class="post-score" title="current number of votes">0</div><span id="post-60661-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>when I try to capture traffic in EVE software and It opens the wireshark, In CMD I receive bellow error and nothing capture.</p><pre><code>&quot;Connecting to &quot;root&quot;@192.168.125.25...&quot;
tcpdump: listening on vunl0_2_0, link-type EN10MB (Ethernet), capture size 262144 bytes
Unable to write to standard output: The pipe is being closed.</code></pre><p>Please Help me. thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-cannot" rel="tag" title="see questions tagged &#39;cannot&#39;">cannot</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Apr '17, 11:32</strong></p><img src="https://secure.gravatar.com/avatar/09ebd18b20708b9d89b66f5765fe90c6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hrahere&#39;s gravatar image" /><p><span>hrahere</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hrahere has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Apr '17, 12:01</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-60661" class="comments-container"><span id="60674"></span><div id="comment-60674" class="comment"><div id="post-60674-score" class="comment-score"></div><div class="comment-text"><p>What is this "EVE software" that opens Wireshark?</p></div><div id="comment-60674-info" class="comment-info"><span class="comment-age">(09 Apr '17, 12:03)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-60661" class="comment-tools"></div><div class="clear"></div><div id="comment-60661-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

