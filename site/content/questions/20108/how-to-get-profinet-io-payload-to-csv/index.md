+++
type = "question"
title = "How to get Profinet IO payload to .csv"
description = '''Hello What are the magic words with tshark (or some other way) to get Profinet IO log as csv format with IO payload? I can transform the log to csv and I&#x27;ve tried all the display filter fields for pn_io http://www.wireshark.org/docs/dfref/p/pn_io.html and others, but I can&#x27;t get the payload. Some fi...'''
date = "2013-04-05T04:19:00Z"
lastmod = "2013-04-05T04:19:00Z"
weight = 20108
keywords = [ "tshark", "payload", "profinet" ]
aliases = [ "/questions/20108" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to get Profinet IO payload to .csv](/questions/20108/how-to-get-profinet-io-payload-to-csv)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20108-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20108-score" class="post-score" title="current number of votes">0</div><span id="post-20108-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>What are the magic words with tshark (or some other way) to get Profinet IO log as csv format with IO payload?</p><p>I can transform the log to csv and I've tried all the display filter fields for pn_io <a href="http://www.wireshark.org/docs/dfref/p/pn_io.html">http://www.wireshark.org/docs/dfref/p/pn_io.html</a> and others, but I can't get the payload. Some field which seemed to contain payload (by field name) prints just word 'data'.</p><p>br, Esa</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-payload" rel="tag" title="see questions tagged &#39;payload&#39;">payload</span> <span class="post-tag tag-link-profinet" rel="tag" title="see questions tagged &#39;profinet&#39;">profinet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Apr '13, 04:19</strong></p><img src="https://secure.gravatar.com/avatar/d3e303267cb9ebe045d8c641813e30fd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Esa&#39;s gravatar image" /><p><span>Esa</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Esa has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Apr '13, 12:34</strong> </span></p><img src="https://secure.gravatar.com/avatar/fac200552b0c24be2bc93a740bd54d0d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joke&#39;s gravatar image" /><p><span>joke</span><br />
<span class="score" title="1278 reputation points"><span>1.3k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span></p></div></div><div id="comments-container-20108" class="comments-container"></div><div id="comment-tools-20108" class="comment-tools"></div><div class="clear"></div><div id="comment-20108-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

