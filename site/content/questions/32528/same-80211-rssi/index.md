+++
type = "question"
title = "same 802.11 RSSI"
description = '''Hi, I used GNURadio with USRP to capture the real-time 802.11 RSSI value in the wireshark. But the value in the screen is always 42dBm. What&#x27;s the problem? Thanks.'''
date = "2014-05-05T03:16:00Z"
lastmod = "2014-05-05T03:16:00Z"
weight = 32528
keywords = [ "rssi" ]
aliases = [ "/questions/32528" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [same 802.11 RSSI](/questions/32528/same-80211-rssi)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32528-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32528-score" class="post-score" title="current number of votes">0</div><span id="post-32528-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I used GNURadio with USRP to capture the real-time 802.11 RSSI value in the wireshark. But the value in the screen is always 42dBm. What's the problem?</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rssi" rel="tag" title="see questions tagged &#39;rssi&#39;">rssi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 May '14, 03:16</strong></p><img src="https://secure.gravatar.com/avatar/b195c8cedbec55fa1e900ec6f5fb4130?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bonnie&#39;s gravatar image" /><p><span>Bonnie</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bonnie has no accepted answers">0%</span></p></div></div><div id="comments-container-32528" class="comments-container"></div><div id="comment-tools-32528" class="comment-tools"></div><div class="clear"></div><div id="comment-32528-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

