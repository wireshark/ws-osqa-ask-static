+++
type = "question"
title = "Newbie Question: How to See the Contents of a Packet via Wireshark"
description = '''Hello, I am very new to Wireshark and have not been able to find this exact info online so I thought I&#x27;d ask here. How can someone see the contents of a packet with Wireshark? I am doing an activity for school and being brand new to the program I could use some help figuring that out. Thanks a lot!'''
date = "2012-11-08T06:49:00Z"
lastmod = "2012-11-08T06:56:00Z"
weight = 15719
keywords = [ "contents" ]
aliases = [ "/questions/15719" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Newbie Question: How to See the Contents of a Packet via Wireshark](/questions/15719/newbie-question-how-to-see-the-contents-of-a-packet-via-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15719-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15719-score" class="post-score" title="current number of votes">0</div><span id="post-15719-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I am very new to Wireshark and have not been able to find this exact info online so I thought I'd ask here.</p><p>How can someone see the contents of a packet with Wireshark? I am doing an activity for school and being brand new to the program I could use some help figuring that out.</p><p>Thanks a lot!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-contents" rel="tag" title="see questions tagged &#39;contents&#39;">contents</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Nov '12, 06:49</strong></p><img src="https://secure.gravatar.com/avatar/8fc943f40b592a312837f3e99f059ef8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="EmoJosh&#39;s gravatar image" /><p><span>EmoJosh</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="EmoJosh has no accepted answers">0%</span></p></div></div><div id="comments-container-15719" class="comments-container"></div><div id="comment-tools-15719" class="comment-tools"></div><div class="clear"></div><div id="comment-15719-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15721"></span>

<div id="answer-container-15721" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15721-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15721-score" class="post-score" title="current number of votes">1</div><span id="post-15721-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Well, it's strange that you did not find any useful information online, as there are really a lot of tutorials that explain the basics of Wireshark.</p><p>Please watch the following videos and you will be enlighted ;-)</p><pre><code>http://www.youtube.com/watch?v=pk4OfsxxB4g&amp;feature=related
http://www.youtube.com/watch?v=NHLTa29iovU
http://wiresharkdownloads.riverbed.com/video/wireshark/introduction-to-wireshark/</code></pre><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Nov '12, 06:56</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Nov '12, 06:56</strong> </span></p></div></div><div id="comments-container-15721" class="comments-container"></div><div id="comment-tools-15721" class="comment-tools"></div><div class="clear"></div><div id="comment-15721-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

