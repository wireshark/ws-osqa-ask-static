+++
type = "question"
title = "Cannot see packets from other devices on my network"
description = '''I have read the FAQ: When I use Wireshark to capture packets, why do I see only packets to and from my machine, or not see all the traffic I&#x27;m expecting to see from or to the machine I&#x27;m trying to monitor? I have read the explanation for that question a couple times, and am not sure I really underst...'''
date = "2011-01-12T20:12:00Z"
lastmod = "2012-01-16T16:05:00Z"
weight = 1721
keywords = [ "on", "other", "network", "devices" ]
aliases = [ "/questions/1721" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Cannot see packets from other devices on my network](/questions/1721/cannot-see-packets-from-other-devices-on-my-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1721-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1721-score" class="post-score" title="current number of votes">0</div><span id="post-1721-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have read the FAQ: When I use Wireshark to capture packets, why do I see only packets to and from my machine, or not see all the traffic I'm expecting to see from or to the machine I'm trying to monitor?</p><p>I have read the explanation for that question a couple times, and am not sure I really understand what's going on. What exactly is a switched network, and how do I know if I have one?</p><p>I am using a broadcom network card. Is this one of the cards that does not support promiscuous mode?</p><p>Thanks for any response.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-on" rel="tag" title="see questions tagged &#39;on&#39;">on</span> <span class="post-tag tag-link-other" rel="tag" title="see questions tagged &#39;other&#39;">other</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-devices" rel="tag" title="see questions tagged &#39;devices&#39;">devices</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jan '11, 20:12</strong></p><img src="https://secure.gravatar.com/avatar/b39a95018bec8cc6f1e02948b4468e79?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Desh&#39;s gravatar image" /><p><span>Desh</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Desh has no accepted answers">0%</span></p></div></div><div id="comments-container-1721" class="comments-container"></div><div id="comment-tools-1721" class="comment-tools"></div><div class="clear"></div><div id="comment-1721-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="1724"></span>

<div id="answer-container-1724" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1724-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1724-score" class="post-score" title="current number of votes">1</div><span id="post-1724-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">This Wireshark Wiki article</a> discusses this in detail.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Jan '11, 22:41</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-1724" class="comments-container"></div><div id="comment-tools-1724" class="comment-tools"></div><div class="clear"></div><div id="comment-1724-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="8389"></span>

<div id="answer-container-8389" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8389-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8389-score" class="post-score" title="current number of votes">0</div><span id="post-8389-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>hey try using cain and abel's sniffer to capture packets with the APR enabled, it captures even on switched networks :) -swampfox out</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Jan '12, 18:10</strong></p><img src="https://secure.gravatar.com/avatar/3daa8590b3dd2774e19954feac1f7df3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="swampfox&#39;s gravatar image" /><p><span>swampfox</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="swampfox has no accepted answers">0%</span></p></div></div><div id="comments-container-8389" class="comments-container"><span id="8395"></span><div id="comment-8395" class="comment"><div id="post-8395-score" class="comment-score"></div><div class="comment-text"><p>Using Cain&amp;Abel to sniff packets is an "unfriendly" way to capture data in most cases (eavesdropping). You should be very careful before doing something like this in a work environment or there might be trouble.</p></div><div id="comment-8395-info" class="comment-info"><span class="comment-age">(15 Jan '12, 17:52)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="8401"></span><div id="comment-8401" class="comment"><div id="post-8401-score" class="comment-score"></div><div class="comment-text"><p>if your in a work area it shouldn't make a difference. using cain's APR feature Captures the same packets as wireshark just uses a differnent method of capture.</p><p>http://www.oxid.it/downloads/ca_setup.exe (cain and abel download link)</p></div><div id="comment-8401-info" class="comment-info"><span class="comment-age">(15 Jan '12, 22:02)</span> <span class="comment-user userinfo">swampfox</span></div></div><span id="8415"></span><div id="comment-8415" class="comment"><div id="post-8415-score" class="comment-score"></div><div class="comment-text"><p>btw if your half routing a computer the source in wireshark may say the source name with a .local behind it. im not quite sure what this means but i suspect its only capturing a half of the normal ammount of packets. anyone got some ideas?</p></div><div id="comment-8415-info" class="comment-info"><span class="comment-age">(16 Jan '12, 11:19)</span> <span class="comment-user userinfo">swampfox</span></div></div><span id="8416"></span><div id="comment-8416" class="comment"><div id="post-8416-score" class="comment-score"></div><div class="comment-text"><p>If you're in a work area, the organization's IT department might give you some trouble if you do <a href="http://www.oxid.it/ca_um/topics/apr.htm">ARP poisoning</a> on your network. Wireshark does not include ARP poisoning capabilities, so it does not attempt to fool a switch into sending traffic to it; Cain does, and thus can capture traffic that Wireshark can't see.</p></div><div id="comment-8416-info" class="comment-info"><span class="comment-age">(16 Jan '12, 11:24)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="8420"></span><div id="comment-8420" class="comment"><div id="post-8420-score" class="comment-score">1</div><div class="comment-text"><p>If you were in my work area, we'd shut down your network port(s) until we could figure out who you were. Then we'd shut down your user account until your supervisor could have a chat with you. After re-enabling your account, we'd subject you to extra scrutiny for about a month.</p></div><div id="comment-8420-info" class="comment-info"><span class="comment-age">(16 Jan '12, 16:05)</span> <span class="comment-user userinfo">Jim Aragon</span></div></div></div><div id="comment-tools-8389" class="comment-tools"></div><div class="clear"></div><div id="comment-8389-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

