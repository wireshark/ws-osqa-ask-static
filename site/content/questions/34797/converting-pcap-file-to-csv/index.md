+++
type = "question"
title = "Converting pcap file to CSV."
description = '''Hi everyone. Can someone let me know how to convert pcap file to CSV file using wireshark in windows7? Thanks. Maigana.'''
date = "2014-07-21T03:41:00Z"
lastmod = "2014-07-25T09:53:00Z"
weight = 34797
keywords = [ "windows7" ]
aliases = [ "/questions/34797" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Converting pcap file to CSV.](/questions/34797/converting-pcap-file-to-csv)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34797-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34797-score" class="post-score" title="current number of votes">0</div><span id="post-34797-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everyone. Can someone let me know how to convert pcap file to CSV file using wireshark in windows7? Thanks.</p><p>Maigana.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jul '14, 03:41</strong></p><img src="https://secure.gravatar.com/avatar/e3778ae8a621767b52cdf5a8052a93c4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Maigana&#39;s gravatar image" /><p><span>Maigana</span><br />
<span class="score" title="11 reputation points">11</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Maigana has no accepted answers">0%</span></p></div></div><div id="comments-container-34797" class="comments-container"></div><div id="comment-tools-34797" class="comment-tools"></div><div class="clear"></div><div id="comment-34797-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34798"></span>

<div id="answer-container-34798" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34798-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34798-score" class="post-score" title="current number of votes">2</div><span id="post-34798-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>From the Menu; File | Export Packet Dissections | As "CSV" ...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Jul '14, 04:25</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-34798" class="comments-container"><span id="34911"></span><div id="comment-34911" class="comment"><div id="post-34911-score" class="comment-score"></div><div class="comment-text"><p>Thanks for your response. I have done that and send the file. But i've tried openning it as i in SPSS as I use to open CSV files but can not. Any way out. Thanks.</p></div><div id="comment-34911-info" class="comment-info"><span class="comment-age">(25 Jul '14, 09:50)</span> <span class="comment-user userinfo">Maigana</span></div></div><span id="34912"></span><div id="comment-34912" class="comment"><div id="post-34912-score" class="comment-score"></div><div class="comment-text"><p>Have you looked at the file in a text editor? Does it look OK? Maybe SPSS is expecting some special format.</p></div><div id="comment-34912-info" class="comment-info"><span class="comment-age">(25 Jul '14, 09:53)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-34798" class="comment-tools"></div><div class="clear"></div><div id="comment-34798-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

