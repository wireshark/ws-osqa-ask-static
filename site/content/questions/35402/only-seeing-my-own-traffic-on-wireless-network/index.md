+++
type = "question"
title = "Only seeing my own traffic on wireless network"
description = '''I have few routers that I am playing with. The one with dd-wrt, works fine and I can see all the network traffic on it. The capture interface is my wireless network adapter on my Mac. for the other network, a linksys, running the stock firmware, I can only see the traffic on my own pc and maybe the ...'''
date = "2014-08-11T05:00:00Z"
lastmod = "2014-08-12T13:07:00Z"
weight = 35402
keywords = [ "wireless", "ddwrt", "network" ]
aliases = [ "/questions/35402" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Only seeing my own traffic on wireless network](/questions/35402/only-seeing-my-own-traffic-on-wireless-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35402-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35402-score" class="post-score" title="current number of votes">0</div><span id="post-35402-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have few routers that I am playing with. The one with dd-wrt, works fine and I can see all the network traffic on it.<br />
The capture interface is my wireless network adapter on my Mac.</p><p>for the other network, a linksys, running the stock firmware, I can only see the traffic on my own pc and maybe the router information. But not seeing the other clients on there. very odd. Should I use another interface?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-ddwrt" rel="tag" title="see questions tagged &#39;ddwrt&#39;">ddwrt</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Aug '14, 05:00</strong></p><img src="https://secure.gravatar.com/avatar/14e8ff606c6f737f476604034a184baa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="catcurio&#39;s gravatar image" /><p><span>catcurio</span><br />
<span class="score" title="1 reputation points">1</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="catcurio has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-35402" class="comments-container"></div><div id="comment-tools-35402" class="comment-tools"></div><div class="clear"></div><div id="comment-35402-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35442"></span>

<div id="answer-container-35442" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35442-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35442-score" class="post-score" title="current number of votes">0</div><span id="post-35442-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>when doing captures, you will only be able to see devices involved with the transactions, so if you are capturing on your wireless adapter, you will only see traffic leaving and entering that adapter. If you want a good network overview capture at a point where the majority of traffic must travel, such as a router or midpoint proxy in order to see what is going on network wide. If you were in an enterprise setup you could capture at the domain controller to get a good idea of general HTTP requests.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Aug '14, 13:07</strong></p><img src="https://secure.gravatar.com/avatar/5115a34ce66b0260069e51c489e521ac?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jmadsen&#39;s gravatar image" /><p><span>jmadsen</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jmadsen has no accepted answers">0%</span></p></div></div><div id="comments-container-35442" class="comments-container"></div><div id="comment-tools-35442" class="comment-tools"></div><div class="clear"></div><div id="comment-35442-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

