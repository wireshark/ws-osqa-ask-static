+++
type = "question"
title = "user interface, fourth window"
description = '''I&#x27;ve got a simple question about wireshark&#x27;s layout. I would like to know if is it possible to insert a fourth window in the user interface. We would like to add a topology of a sensor net and i dont found any reference in internet about it.'''
date = "2014-02-14T04:06:00Z"
lastmod = "2014-02-15T15:00:00Z"
weight = 29865
keywords = [ "interface", "window", "add", "user" ]
aliases = [ "/questions/29865" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [user interface, fourth window](/questions/29865/user-interface-fourth-window)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29865-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29865-score" class="post-score" title="current number of votes">0</div><span id="post-29865-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've got a simple question about wireshark's layout. I would like to know if is it possible to insert a fourth window in the user interface. We would like to add a topology of a sensor net and i dont found any reference in internet about it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-window" rel="tag" title="see questions tagged &#39;window&#39;">window</span> <span class="post-tag tag-link-add" rel="tag" title="see questions tagged &#39;add&#39;">add</span> <span class="post-tag tag-link-user" rel="tag" title="see questions tagged &#39;user&#39;">user</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Feb '14, 04:06</strong></p><img src="https://secure.gravatar.com/avatar/347a473627a1c8b558663368d033097c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lina&#39;s gravatar image" /><p><span>lina</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lina has no accepted answers">0%</span></p></div></div><div id="comments-container-29865" class="comments-container"></div><div id="comment-tools-29865" class="comment-tools"></div><div class="clear"></div><div id="comment-29865-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29866"></span>

<div id="answer-container-29866" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29866-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29866-score" class="post-score" title="current number of votes">1</div><span id="post-29866-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You'll need to do a fair amount of code rewrite for that. It may be easier to add it as a dialog in a similar manner to the stats and graph dialogs.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Feb '14, 04:11</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-29866" class="comments-container"><span id="29897"></span><div id="comment-29897" class="comment"><div id="post-29897-score" class="comment-score"></div><div class="comment-text"><p>I would also vote, from a UI perspective, for adding it as a dialog. I consider the results of some form of processing a capture (stats, graphs, etc.) to be separate documents from the presentation of the raw packets, and prefer to see them in separate windows.</p></div><div id="comment-29897-info" class="comment-info"><span class="comment-age">(15 Feb '14, 15:00)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-29866" class="comment-tools"></div><div class="clear"></div><div id="comment-29866-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

