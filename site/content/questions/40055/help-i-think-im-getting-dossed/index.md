+++
type = "question"
title = "Help! i think im getting dossed"
description = '''so last night my internet just started going down at random times i used wireshark to look and see what was happening and this pulled up http://gyazo.com/4af295e61a549b0979a230c62733dec2 when it went down. idk whats going on i just need help.'''
date = "2015-02-24T14:37:00Z"
lastmod = "2015-02-25T01:27:00Z"
weight = 40055
keywords = [ "help" ]
aliases = [ "/questions/40055" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Help! i think im getting dossed](/questions/40055/help-i-think-im-getting-dossed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40055-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40055-score" class="post-score" title="current number of votes">0</div><span id="post-40055-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>so last night my internet just started going down at random times i used wireshark to look and see what was happening and this pulled up <a href="http://gyazo.com/4af295e61a549b0979a230c62733dec2">http://gyazo.com/4af295e61a549b0979a230c62733dec2</a> when it went down. idk whats going on i just need help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Feb '15, 14:37</strong></p><img src="https://secure.gravatar.com/avatar/9e5a966e32b657952ffa27e314bdd7d4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sworthia123&#39;s gravatar image" /><p><span>sworthia123</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sworthia123 has no accepted answers">0%</span></p></div></div><div id="comments-container-40055" class="comments-container"></div><div id="comment-tools-40055" class="comment-tools"></div><div class="clear"></div><div id="comment-40055-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40063"></span>

<div id="answer-container-40063" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40063-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40063-score" class="post-score" title="current number of votes">0</div><span id="post-40063-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No evidence of that all from the screenshot. You haven't indicated where the capture was made, but I assume on the host with IP 25.9.150.1, and that host is attempting to open a tcp connection to the address 25.75.193.166 on port 25565 and failing as there is no response from the remote end.</p><p>If you can provide more info about your setup then we might be able to explain things a little better, but on the evidence shown there's nothing much to say.</p><p>What happens when your "internet goes down". i.e. what stops working? Have you tried basic tests such as browsing to Google, or pinging Google?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Feb '15, 01:27</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-40063" class="comments-container"></div><div id="comment-tools-40063" class="comment-tools"></div><div class="clear"></div><div id="comment-40063-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

