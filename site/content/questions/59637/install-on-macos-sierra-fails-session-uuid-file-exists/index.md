+++
type = "question"
title = "Install on macOS Sierra fails &quot;Session UUID file exists&quot;"
description = '''Wireshark installed once; then, once I removed it, the installation appears to complete but really doesn&#x27;t: Feb 23 11:30:38 wahoo installd[303]: PackageKit: ----- Begin install ----- Feb 23 11:30:38 wahoo installd[303]: PackageKit: Session UUID file exists - will not overwrite /var/folders/zz/zyxvpx...'''
date = "2017-02-23T11:32:00Z"
lastmod = "2017-02-23T11:32:00Z"
weight = 59637
keywords = [ "sierra", "problem", "installation", "uuid" ]
aliases = [ "/questions/59637" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Install on macOS Sierra fails "Session UUID file exists"](/questions/59637/install-on-macos-sierra-fails-session-uuid-file-exists)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59637-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59637-score" class="post-score" title="current number of votes">0</div><span id="post-59637-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark installed once; then, once I removed it, the installation appears to complete but really doesn't:</p><pre><code>Feb 23 11:30:38 wahoo installd[303]: PackageKit: ----- Begin install -----
Feb 23 11:30:38 wahoo installd[303]: PackageKit: Session UUID file exists - will not overwrite /var/folders/zz/zyxvpxvq6csfxvn_n0000000000000/C/PKInstallSandboxManager/5CE691EA-4E77-4D41-B64B-E5A20ACD779C.activeSandbox
Feb 23 11:30:40 wahoo installd[303]: PackageKit: ----- End install -----</code></pre><p>This shows up in the Installer log no matter which version I try to install. Help!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sierra" rel="tag" title="see questions tagged &#39;sierra&#39;">sierra</span> <span class="post-tag tag-link-problem" rel="tag" title="see questions tagged &#39;problem&#39;">problem</span> <span class="post-tag tag-link-installation" rel="tag" title="see questions tagged &#39;installation&#39;">installation</span> <span class="post-tag tag-link-uuid" rel="tag" title="see questions tagged &#39;uuid&#39;">uuid</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Feb '17, 11:32</strong></p><img src="https://secure.gravatar.com/avatar/84bad1f4d78e3bd9ca36923a6e97ce3b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="emmayche&#39;s gravatar image" /><p><span>emmayche</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="emmayche has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Feb '17, 15:27</strong> </span></p></div></div><div id="comments-container-59637" class="comments-container"></div><div id="comment-tools-59637" class="comment-tools"></div><div class="clear"></div><div id="comment-59637-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

