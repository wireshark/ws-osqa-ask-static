+++
type = "question"
title = "installation is a pain"
description = '''I find Wireshark really a pain, because when it starts it immediately hangs because it is loading user preferences which I dont have because I just installed it. I can&#x27;t even close the program, task manage doesn&#x27;t even start. Uninstall is impossible because some associated program needs to close and...'''
date = "2016-06-24T13:57:00Z"
lastmod = "2016-06-27T06:11:00Z"
weight = 53643
keywords = [ "installation" ]
aliases = [ "/questions/53643" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [installation is a pain](/questions/53643/installation-is-a-pain)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53643-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53643-score" class="post-score" title="current number of votes">-3</div><span id="post-53643-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I find Wireshark really a pain, because when it starts it immediately hangs because it is loading user preferences which I dont have because I just installed it. I can't even close the program, task manage doesn't even start. Uninstall is impossible because some associated program needs to close and I need to guess which one... After a reboot, I am able to uninstall wireshark, but then WinPcap unstall hangs, the progress bar is not making progress, and it says output folder c:\Windows\system32, not sure what the program wants to output there, I think it should remove files instead. It keeps me busy for more than an hour now. By the way I tried to post a message here two times but it failed because first time I needed to validate my email, second time because the website stopped responding. This is a pain, people.</p><p>Is there a version of wireshark that just works?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-installation" rel="tag" title="see questions tagged &#39;installation&#39;">installation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jun '16, 13:57</strong></p><img src="https://secure.gravatar.com/avatar/f929d8cf04203c9a1c7f9f30793b0642?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper%20Sikken&#39;s gravatar image" /><p><span>Jasper Sikken</span><br />
<span class="score" title="3 reputation points">3</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper Sikken has no accepted answers">0%</span></p></div></div><div id="comments-container-53643" class="comments-container"><span id="53644"></span><div id="comment-53644" class="comment"><div id="post-53644-score" class="comment-score"></div><div class="comment-text"><p>By the way how come the old old stable version of wireshark and the new version are both dated on June 7th 2016? Why is it such a mess?</p></div><div id="comment-53644-info" class="comment-info"><span class="comment-age">(24 Jun '16, 14:00)</span> <span class="comment-user userinfo">Jasper Sikken</span></div></div><span id="53645"></span><div id="comment-53645" class="comment"><div id="post-53645-score" class="comment-score"></div><div class="comment-text"><p>During installation, why do I need to decide whether or not I want to install WinPcap when it is needed anyway? Then dont ask the question. Has anyone heard of user experience?</p></div><div id="comment-53645-info" class="comment-info"><span class="comment-age">(24 Jun '16, 14:03)</span> <span class="comment-user userinfo">Jasper Sikken</span></div></div><span id="53646"></span><div id="comment-53646" class="comment"><div id="post-53646-score" class="comment-score"></div><div class="comment-text"><p>Ow man, even the old stable version is hanging, I see a progress bar that is 100%, but still it is loading module preferences. I dont have preferences because I just installed it.</p><p>Why is it hanging on preference that I dont have?</p><p>Why is the old stable version not stable?</p><p>Why does the progress bar say 100% when it is not done?</p></div><div id="comment-53646-info" class="comment-info"><span class="comment-age">(24 Jun '16, 14:07)</span> <span class="comment-user userinfo">Jasper Sikken</span></div></div></div><div id="comment-tools-53643" class="comment-tools"></div><div class="clear"></div><div id="comment-53643-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="53649"></span>

<div id="answer-container-53649" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53649-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53649-score" class="post-score" title="current number of votes">0</div><span id="post-53649-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Urm sounds to me like you may have an underlying issue with your computer, perhaps an incompatible program or service running in the background I would start your machine in clean boot mode to see if it works then, after this I would run a clean up so make sure you have hard drive space and a reasonable amount of ram, then run malwarebytes to see if you have any infections.</p><p>There is nothing wrong with wireshark it is a great program and tons of people use it.<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Jun '16, 01:39</strong></p><img src="https://secure.gravatar.com/avatar/1d67795c1484b0652b735a3827dcb9b4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="m0rph&#39;s gravatar image" /><p><span>m0rph</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="m0rph has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-53649" class="comments-container"></div><div id="comment-tools-53649" class="comment-tools"></div><div class="clear"></div><div id="comment-53649-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="53651"></span>

<div id="answer-container-53651" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53651-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53651-score" class="post-score" title="current number of votes">0</div><span id="post-53651-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>What you describe is a know issue with WinPcap on newer systems (so not related to Wireshark itself) and has been discussed numerous times. Have a look for example to this thread: <a href="https://ask.wireshark.org/questions/26361/loading-configuration-files">https://ask.wireshark.org/questions/26361/loading-configuration-files</a></p><p>If the suggestions given in previous threads do not help you, you could eventually give a try to <a href="https://github.com/nmap/npcap/">Npcap</a> (installed in WinPcap mode) as a replacement for legacy WinPcap.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Jun '16, 06:55</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-53651" class="comments-container"><span id="53670"></span><div id="comment-53670" class="comment"><div id="post-53670-score" class="comment-score"></div><div class="comment-text"><p>Just explicitly answer one of your questions: we ask if you want to install WinPcap because not everyone wants to install WinPcap. This could be because they're not interested in doing live captures (I'm generally not--the capture files I get come in email or from web servers) or they're not <em>allowed</em> to capture [think: corporate security policies] or, in recent times, they'd prefer to use Npcap.</p></div><div id="comment-53670-info" class="comment-info"><span class="comment-age">(27 Jun '16, 06:11)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div></div><div id="comment-tools-53651" class="comment-tools"></div><div class="clear"></div><div id="comment-53651-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

