+++
type = "question"
title = "what version to install on Snow Leopard but 32-bit processor?"
description = '''I have a MacBookPro with these specs:  Model Name: MacBook Pro  Model Identifier: MacBookPro1,2  Processor Name: Intel Core Duo  Processor Speed: 2.16 GHz  Number Of Processors: 1  Total Number Of Cores: 2  L2 Cache: 2 MB  Memory: 2 GB  Bus Speed: 667 MHz What version of Wireshark do I need to insta...'''
date = "2012-04-13T10:17:00Z"
lastmod = "2012-04-13T19:19:00Z"
weight = 10132
keywords = [ "osx", "mac" ]
aliases = [ "/questions/10132" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [what version to install on Snow Leopard but 32-bit processor?](/questions/10132/what-version-to-install-on-snow-leopard-but-32-bit-processor)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10132-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10132-score" class="post-score" title="current number of votes">0</div><span id="post-10132-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a MacBookPro with these specs: Model Name: MacBook Pro Model Identifier: MacBookPro1,2 Processor Name: Intel Core Duo Processor Speed: 2.16 GHz Number Of Processors: 1 Total Number Of Cores: 2 L2 Cache: 2 MB Memory: 2 GB Bus Speed: 667 MHz</p><p>What version of Wireshark do I need to install? The download page says OS X 10.6 (Snow Leopard) Intel 64-bit .dmg OS X 10.5 (Leopard) Intel 32-bit .dmg with the first one selected... but my processor is 32-bit</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Apr '12, 10:17</strong></p><img src="https://secure.gravatar.com/avatar/67a2d2fc229079b65fe2a65f125a581b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="canon273&#39;s gravatar image" /><p><span>canon273</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="canon273 has no accepted answers">0%</span></p></div></div><div id="comments-container-10132" class="comments-container"></div><div id="comment-tools-10132" class="comment-tools"></div><div class="clear"></div><div id="comment-10132-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="10136"></span>

<div id="answer-container-10136" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10136-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10136-score" class="post-score" title="current number of votes">0</div><span id="post-10136-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try the Leopard build; the Leopard build is done on a machine running Leopard, but <em>might</em> work on Snow Leopard (assuming some annoying shared-library version number issue doesn't occur with one of the X11 shared libraries).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Apr '12, 11:27</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-10136" class="comments-container"></div><div id="comment-tools-10136" class="comment-tools"></div><div class="clear"></div><div id="comment-10136-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="10143"></span>

<div id="answer-container-10143" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10143-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10143-score" class="post-score" title="current number of votes">0</div><span id="post-10143-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If your processor is 32 bits, then use the one most current for 32 bits. If it's a leopard version instead of snow leopard, use it. If it doesn't work, get rid of it.</p><p><strong><em>* You have confirmed it is 32 bits, right?</em></strong> * Nothing on my Mac told me. I asked elsewhere. Even Intel doesn't make it clear.</p><p>Why in both windoze and mac the tools that tell you what processor you have don't tell you how many bits it is, is beyond me... especially because so many downloads require you to know this!</p><p>At work I deal with an Intel 4-core machine that's 32 bits. Go figger.</p><p>I have a MacBook: Model Name: MacBook Model Identifier: MacBook5,1 Processor Name: Intel Core 2 Duo Processor Speed: 2 GHz Number of Processors: 1 Total Number of Cores: 2 L2 Cache: 3 MB Memory: 2 GB Bus Speed: 1.07 GHz</p><p>Mine (above) is 64 bits.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Apr '12, 19:19</strong></p><img src="https://secure.gravatar.com/avatar/c609362c709623fe3591a5da33a4937b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="PReinie&#39;s gravatar image" /><p><span>PReinie</span><br />
<span class="score" title="15 reputation points">15</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="PReinie has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Apr '12, 19:29</strong> </span></p></div></div><div id="comments-container-10143" class="comments-container"></div><div id="comment-tools-10143" class="comment-tools"></div><div class="clear"></div><div id="comment-10143-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

