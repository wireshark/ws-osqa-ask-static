+++
type = "question"
title = ".rf5 file opening error"
description = '''When I try to open error a file *.rf5 it throw up error, its ATM catpure. HLAYER &quot;MTP-2&quot; &quot;../../protocols/base/base.upd*&quot; LOADED &quot;K12xx decoder base - english version&quot; LAYER &quot;SSCOP&quot; &quot;../../protocols/bisup/sscop.upd1&quot; LOADED &quot;SSCOP ITU Q.2100, 07/1994 [06.05.02]&quot; LAYER &quot;AAL2L3&quot; &quot;../../protocols/umts_...'''
date = "2011-07-19T02:51:00Z"
lastmod = "2013-11-20T13:27:00Z"
weight = 5117
keywords = [ "rf5" ]
aliases = [ "/questions/5117" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [.rf5 file opening error](/questions/5117/rf5-file-opening-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5117-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5117-score" class="post-score" title="current number of votes">0</div><span id="post-5117-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I try to open error a file *.rf5 it throw up error, its ATM catpure.</p><pre><code>HLAYER      &quot;MTP-2&quot;     &quot;../../protocols/base/base.upd*&quot;    LOADED  &quot;K12xx decoder base - english version&quot;
LAYER       &quot;SSCOP&quot;     &quot;../../protocols/bisup/sscop.upd1&quot;  LOADED  &quot;SSCOP ITU Q.2100, 07/1994 [06.05.02]&quot;
LAYER       &quot;AAL2L3&quot;    &quot;../../protocols/umts_common/aal2l3.upd1&quot;   LOADED  &quot;ITU-T Q.2630.1/2 AAL2 Signalling CS1/2&quot;
RELATION    &quot;BASE&quot;      &quot;SSCOP&quot; UNCOND  POSITION 290 416 290 390
RELATION    &quot;SSCOP&quot;     &quot;AAL2L3&quot;    UNCOND  POSITION 290 350 290 303
DECKRNL     &quot;MTP-2&quot;     LSBF    UPD_DK
DECKRNL     &quot;SSCOP&quot;     MSBF    UPD_DK
DECKRNL     &quot;AAL2L3&quot;    LSBF    UPD_DK
LAYPOS      &quot;BASE&quot;      150 416 430 486
LAYPOS      &quot;MTP-2&quot;     10  10  70  50
LAYPOS      &quot;SSCOP&quot;     260 350 320 390
LAYPOS      &quot;AAL2L3&quot;    256 263 324 303</code></pre><p>Format : error opening file.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rf5" rel="tag" title="see questions tagged &#39;rf5&#39;">rf5</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jul '11, 02:51</strong></p><img src="https://secure.gravatar.com/avatar/c24bc9a87b34a1b067dd183a4c797c85?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="deessing&#39;s gravatar image" /><p><span>deessing</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="deessing has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Nov '13, 13:28</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-5117" class="comments-container"></div><div id="comment-tools-5117" class="comment-tools"></div><div class="clear"></div><div id="comment-5117-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="10843"></span>

<div id="answer-container-10843" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10843-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10843-score" class="post-score" title="current number of votes">0</div><span id="post-10843-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It appears that Wireshark does not currently support the K18 file format; K12 and K15 are listed as supported, but not K18. Also see this <a href="http://ask.wireshark.org/questions/10820/how-to-read-k18-rf5-traces-in-wireshark">question and answer</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 May '12, 09:43</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-10843" class="comments-container"></div><div id="comment-tools-10843" class="comment-tools"></div><div class="clear"></div><div id="comment-10843-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="27182"></span>

<div id="answer-container-27182" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27182-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27182-score" class="post-score" title="current number of votes">0</div><span id="post-27182-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try one of the <a href="http://www.wireshark.org/download/automated/">automated builds</a> with a version number of "SVN-53452" or later; somebody finally filed <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=9455">a bug</a> for a failure to read a k18 capture, and attached a capture file that showed the problem, so it was finally possible for some reverse-engineering to be done to handle that file, at least.</p><p>If that still doesn't handle it, file another bug, and attach the capture file, so that more reverse-engineering can be done.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Nov '13, 13:27</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-27182" class="comments-container"></div><div id="comment-tools-27182" class="comment-tools"></div><div class="clear"></div><div id="comment-27182-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

