+++
type = "question"
title = "OSX Wireshark WIFI not capturing HTTP/JSON request/responses"
description = '''Hello, Just started using OSX and I need to capture some JSON response/requests. I&#x27;m using WIFI. I start wireshark , select the WIFI adapter to capture, but any traffic I get is all too low down the stack - e.g. TCP. I don&#x27;t get any http/json stuff I was to capture higher stack protocols like HTTP &amp;...'''
date = "2016-02-05T12:53:00Z"
lastmod = "2016-02-05T15:33:00Z"
weight = 49902
keywords = [ "macosx", "monitor", "monitor-mode" ]
aliases = [ "/questions/49902" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [OSX Wireshark WIFI not capturing HTTP/JSON request/responses](/questions/49902/osx-wireshark-wifi-not-capturing-httpjson-requestresponses)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49902-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49902-score" class="post-score" title="current number of votes">0</div><span id="post-49902-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>Just started using OSX and I need to capture some JSON response/requests. I'm using WIFI. I start wireshark , select the WIFI adapter to capture, but any traffic I get is all too low down the stack - e.g. TCP. I don't get any http/json stuff I was to capture higher stack protocols like HTTP &amp; JSON.</p><p>Any ideas?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span> <span class="post-tag tag-link-monitor-mode" rel="tag" title="see questions tagged &#39;monitor-mode&#39;">monitor-mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Feb '16, 12:53</strong></p><img src="https://secure.gravatar.com/avatar/c42fcb16287da364889b2429adbc18bb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ThomasM&#39;s gravatar image" /><p><span>ThomasM</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ThomasM has no accepted answers">0%</span></p></div></div><div id="comments-container-49902" class="comments-container"><span id="49914"></span><div id="comment-49914" class="comment"><div id="post-49914-score" class="comment-score"></div><div class="comment-text"><p>What are the TCP port numbers for that traffic? Perhaps non-standard ports are being used and Wireshark isn't recognizing them as HTTP.</p></div><div id="comment-49914-info" class="comment-info"><span class="comment-age">(05 Feb '16, 15:33)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-49902" class="comment-tools"></div><div class="clear"></div><div id="comment-49902-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

