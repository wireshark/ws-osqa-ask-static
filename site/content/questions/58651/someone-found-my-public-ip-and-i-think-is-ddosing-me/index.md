+++
type = "question"
title = "Someone found my public IP, and I think is DDOSing me?"
description = '''Super noob here, so I apologize for any confusing statements. So pretty much I was livestreaming yesterday on Twitch.tv, and a guy came in the chat saying some pretty rude stuff, so naturally I banned him. Not too long after I had banned the first user, someone else came in and threatened to take my...'''
date = "2017-01-10T18:34:00Z"
lastmod = "2017-01-10T18:34:00Z"
weight = 58651
keywords = [ "ip", "ddos", "address", "internet" ]
aliases = [ "/questions/58651" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Someone found my public IP, and I think is DDOSing me?](/questions/58651/someone-found-my-public-ip-and-i-think-is-ddosing-me)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58651-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58651-score" class="post-score" title="current number of votes">0</div><span id="post-58651-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Super noob here, so I apologize for any confusing statements.</p><p>So pretty much I was livestreaming yesterday on Twitch.tv, and a guy came in the chat saying some pretty rude stuff, so naturally I banned him. Not too long after I had banned the first user, someone else came in and threatened to take my internet down unless I unbanned his friend. I obviously said no, but low and behold my internet goes down. SO, after it came back up I kept playing games and livestreaming for a little, and then within probably 30 minutes my internet goes back down. They pasted my public IP address in the twitch chat as well. Fast forward to today (1/10/17) there was someone in my Twitch chat that said "I heard that someone was going around ddosing small streamers, I really hope it doesn't happen to you." Internet goes down again. I called Comcast asking if there was something that I could do, but they couldn't help a whole lot. Are there any recommendations that anyone might have to help?</p><p>Again, I apologize if any of this is really confusing, so please feel free to ask whatever you need. I really appreciate any help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-ddos" rel="tag" title="see questions tagged &#39;ddos&#39;">ddos</span> <span class="post-tag tag-link-address" rel="tag" title="see questions tagged &#39;address&#39;">address</span> <span class="post-tag tag-link-internet" rel="tag" title="see questions tagged &#39;internet&#39;">internet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jan '17, 18:34</strong></p><img src="https://secure.gravatar.com/avatar/7f723408695065f8565d56401ac8f57d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="farstruck&#39;s gravatar image" /><p><span>farstruck</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="farstruck has no accepted answers">0%</span></p></div></div><div id="comments-container-58651" class="comments-container"></div><div id="comment-tools-58651" class="comment-tools"></div><div class="clear"></div><div id="comment-58651-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

