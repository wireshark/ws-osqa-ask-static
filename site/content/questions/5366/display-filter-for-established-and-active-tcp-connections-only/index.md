+++
type = "question"
title = "display filter for established and active tcp connections only"
description = '''display filter for established and active tcp connections only'''
date = "2011-07-30T00:57:00Z"
lastmod = "2011-08-01T19:46:00Z"
weight = 5366
keywords = [ "display-filter" ]
aliases = [ "/questions/5366" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [display filter for established and active tcp connections only](/questions/5366/display-filter-for-established-and-active-tcp-connections-only)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5366-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5366-score" class="post-score" title="current number of votes">0</div><span id="post-5366-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>display filter for established and active tcp connections only</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-display-filter" rel="tag" title="see questions tagged &#39;display-filter&#39;">display-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jul '11, 00:57</strong></p><img src="https://secure.gravatar.com/avatar/d0c4ecc2ee6bf339beace37652e1824f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ams&#39;s gravatar image" /><p><span>ams</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ams has no accepted answers">0%</span></p></div></div><div id="comments-container-5366" class="comments-container"><span id="5384"></span><div id="comment-5384" class="comment"><div id="post-5384-score" class="comment-score">2</div><div class="comment-text"><p>I can guess what your question is, but it's better if you ask it.</p></div><div id="comment-5384-info" class="comment-info"><span class="comment-age">(01 Aug '11, 16:13)</span> <span class="comment-user userinfo">helloworld</span></div></div></div><div id="comment-tools-5366" class="comment-tools"></div><div class="clear"></div><div id="comment-5366-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5386"></span>

<div id="answer-container-5386" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5386-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5386-score" class="post-score" title="current number of votes">1</div><span id="post-5386-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=1184">bug 1184</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Aug '11, 18:06</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-5386" class="comments-container"><span id="5389"></span><div id="comment-5389" class="comment"><div id="post-5389-score" class="comment-score"></div><div class="comment-text"><p>Um, ok.....</p><p>From the looks of that bug, it seems you're proposing that a solution (to a problem we don't yet really know) is to modify Wireshark to display the process associated with each packet.</p></div><div id="comment-5389-info" class="comment-info"><span class="comment-age">(01 Aug '11, 19:39)</span> <span class="comment-user userinfo">helloworld</span></div></div><span id="5391"></span><div id="comment-5391" class="comment"><div id="post-5391-score" class="comment-score"></div><div class="comment-text"><p>Yes, I believe that is the essence of the enhancement bug report. Gerald has done some experimentation with it (on Linux). Someone, (perhaps ams), may have an interest in it and want to fully implement it. Once that enhancement bug is resolved, I believe it would satisfy what I <em>think</em> ams desires, although it's hard to know for sure since you have to read between the lines to formulate what his question actually is.</p></div><div id="comment-5391-info" class="comment-info"><span class="comment-age">(01 Aug '11, 19:46)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-5386" class="comment-tools"></div><div class="clear"></div><div id="comment-5386-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

