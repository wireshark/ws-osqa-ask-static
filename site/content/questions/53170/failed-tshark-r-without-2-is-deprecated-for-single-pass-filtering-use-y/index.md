+++
type = "question"
title = "Failed: tshark: -R without -2 is deprecated. For single-pass filtering use -Y."
description = '''Hi, I have updated wireshark from 1.10.5 to 2.0.3 facing error &quot;Failed: tshark: -R without -2 is deprecated. For single-pass filtering use -Y.&quot; while executing the &quot;query filter set &amp;lt;filter_name&amp;gt;&quot; in tshark. If any one faced this issue please help me in resolve.'''
date = "2016-06-03T03:32:00Z"
lastmod = "2016-06-03T03:36:00Z"
weight = 53170
keywords = [ "tshark" ]
aliases = [ "/questions/53170" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Failed: tshark: -R without -2 is deprecated. For single-pass filtering use -Y.](/questions/53170/failed-tshark-r-without-2-is-deprecated-for-single-pass-filtering-use-y)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53170-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53170-score" class="post-score" title="current number of votes">0</div><span id="post-53170-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I have updated wireshark from 1.10.5 to 2.0.3 facing error "Failed: tshark: -R without -2 is deprecated. For single-pass filtering use -Y." while executing the "query filter set &lt;filter_name&gt;" in tshark. If any one faced this issue please help me in resolve.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jun '16, 03:32</strong></p><img src="https://secure.gravatar.com/avatar/3f786c3a4950ba2a5460668117f3e7b2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Naresh&#39;s gravatar image" /><p><span>Naresh</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Naresh has no accepted answers">0%</span></p></div></div><div id="comments-container-53170" class="comments-container"></div><div id="comment-tools-53170" class="comment-tools"></div><div class="clear"></div><div id="comment-53170-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53171"></span>

<div id="answer-container-53171" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53171-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53171-score" class="post-score" title="current number of votes">0</div><span id="post-53171-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Use the "-Y" parameter instead of the "-R" you used previously.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Jun '16, 03:36</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-53171" class="comments-container"></div><div id="comment-tools-53171" class="comment-tools"></div><div class="clear"></div><div id="comment-53171-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

