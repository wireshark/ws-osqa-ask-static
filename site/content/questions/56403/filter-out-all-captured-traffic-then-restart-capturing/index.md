+++
type = "question"
title = "Filter Out ALL Captured Traffic then Restart Capturing"
description = '''I want to remove ALL traffic scanned except using all the IP addresses or something like that (while keeping my own IP) then restart the capture removing all the previously captured traffic. This way I can scan for a few minutes, apply the filter, everything scanned will be removed then continue my ...'''
date = "2016-10-15T04:20:00Z"
lastmod = "2016-10-16T03:40:00Z"
weight = 56403
keywords = [ "filter", "capture-filter", "traffic" ]
aliases = [ "/questions/56403" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Filter Out ALL Captured Traffic then Restart Capturing](/questions/56403/filter-out-all-captured-traffic-then-restart-capturing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56403-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56403-score" class="post-score" title="current number of votes">0</div><span id="post-56403-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to remove ALL traffic scanned except using all the IP addresses or something like that (while keeping my own IP) then restart the capture removing all the previously captured traffic.</p><p>This way I can scan for a few minutes, apply the filter, everything scanned will be removed then continue my action to easily find the wanted traffic.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Oct '16, 04:20</strong></p><img src="https://secure.gravatar.com/avatar/4091efcbae0a50b6d3a7b3e2091985ac?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Acidic&#39;s gravatar image" /><p><span>Acidic</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Acidic has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Oct '16, 04:20</strong> </span></p></div></div><div id="comments-container-56403" class="comments-container"><span id="56421"></span><div id="comment-56421" class="comment"><div id="post-56421-score" class="comment-score"></div><div class="comment-text"><p>I'm trying to understand what your ultimate goal is. Do you experience memory issues when capturing continuously? What kind of issue are you trying to identify?</p></div><div id="comment-56421-info" class="comment-info"><span class="comment-age">(16 Oct '16, 03:40)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-56403" class="comment-tools"></div><div class="clear"></div><div id="comment-56403-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

