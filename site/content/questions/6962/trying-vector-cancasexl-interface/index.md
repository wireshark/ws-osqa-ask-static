+++
type = "question"
title = "Trying Vector CANcaseXL interface"
description = '''Has anyone had success using the Vector CANcaseXL USB interface with Wireshark? The interface does not show up in the interface list. Thanks in advance. Norman'''
date = "2011-10-18T11:19:00Z"
lastmod = "2011-10-18T11:19:00Z"
weight = 6962
keywords = [ "vector", "cancasexl" ]
aliases = [ "/questions/6962" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Trying Vector CANcaseXL interface](/questions/6962/trying-vector-cancasexl-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6962-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6962-score" class="post-score" title="current number of votes">0</div><span id="post-6962-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Has anyone had success using the Vector CANcaseXL USB interface with Wireshark? The interface does not show up in the interface list.</p><p>Thanks in advance.</p><p>Norman</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vector" rel="tag" title="see questions tagged &#39;vector&#39;">vector</span> <span class="post-tag tag-link-cancasexl" rel="tag" title="see questions tagged &#39;cancasexl&#39;">cancasexl</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Oct '11, 11:19</strong></p><img src="https://secure.gravatar.com/avatar/e3feee4beed9b89dad6123be4d35a56c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="normanterry&#39;s gravatar image" /><p><span>normanterry</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="normanterry has no accepted answers">0%</span></p></div></div><div id="comments-container-6962" class="comments-container"></div><div id="comment-tools-6962" class="comment-tools"></div><div class="clear"></div><div id="comment-6962-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

