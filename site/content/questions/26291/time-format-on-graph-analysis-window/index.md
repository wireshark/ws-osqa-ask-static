+++
type = "question"
title = "Time format on Graph Analysis window"
description = '''Hi Can you explain to me how do I can change the time format on the window &quot;Graph Analysis&quot; to absolute time or something like that? This window appears when I choose &quot;Flow&quot; in the VoIP Calls window. Thank you. Juan David.'''
date = "2013-10-22T09:49:00Z"
lastmod = "2013-10-28T07:45:00Z"
weight = 26291
keywords = [ "absolute_time", "format", "flow", "voip", "time" ]
aliases = [ "/questions/26291" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Time format on Graph Analysis window](/questions/26291/time-format-on-graph-analysis-window)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26291-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26291-score" class="post-score" title="current number of votes">0</div><span id="post-26291-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Can you explain to me how do I can change the time format on the window "Graph Analysis" to absolute time or something like that? This window appears when I choose "Flow" in the VoIP Calls window.</p><p>Thank you.</p><p>Juan David.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-absolute_time" rel="tag" title="see questions tagged &#39;absolute_time&#39;">absolute_time</span> <span class="post-tag tag-link-format" rel="tag" title="see questions tagged &#39;format&#39;">format</span> <span class="post-tag tag-link-flow" rel="tag" title="see questions tagged &#39;flow&#39;">flow</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Oct '13, 09:49</strong></p><img src="https://secure.gravatar.com/avatar/f705219593a0c00e329adb7bfb84dfcf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jdtami&#39;s gravatar image" /><p><span>jdtami</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jdtami has no accepted answers">0%</span></p></div></div><div id="comments-container-26291" class="comments-container"></div><div id="comment-tools-26291" class="comment-tools"></div><div class="clear"></div><div id="comment-26291-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26464"></span>

<div id="answer-container-26464" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26464-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26464-score" class="post-score" title="current number of votes">0</div><span id="post-26464-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's the same format as the selected time format in Wireshark. So, if you want to have absolute time change that in Wireshark</p><blockquote><p>View -&gt; Time Display Format -&gt; Date and Time of Day</p></blockquote><p>and then run the <code>Graph Analysis</code> again and you will see the same date/time format in the <code>Graph Analysis</code>.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Oct '13, 07:45</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Oct '13, 07:46</strong> </span></p></div></div><div id="comments-container-26464" class="comments-container"></div><div id="comment-tools-26464" class="comment-tools"></div><div class="clear"></div><div id="comment-26464-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

