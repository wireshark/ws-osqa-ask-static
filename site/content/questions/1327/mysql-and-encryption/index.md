+++
type = "question"
title = "Mysql and encryption"
description = '''Hi, I need to know whether or not my Mysql connection is actually being encrypted. Any suggestions as to how I would approach this. So far, I&#x27;ve been filtering on port 3306 and also filtering on my ip.addr but I&#x27;m not seeing anything that would indicate that my remote connection to Mysql is actually...'''
date = "2010-12-13T07:47:00Z"
lastmod = "2010-12-15T05:15:00Z"
weight = 1327
keywords = [ "ssl", "mysql" ]
aliases = [ "/questions/1327" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Mysql and encryption](/questions/1327/mysql-and-encryption)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1327-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1327-score" class="post-score" title="current number of votes">0</div><span id="post-1327-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I need to know whether or not my Mysql connection is actually being encrypted. Any suggestions as to how I would approach this. So far, I've been filtering on port 3306 and also filtering on my ip.addr but I'm not seeing anything that would indicate that my remote connection to Mysql is actually getting encrypted. Mysql is setup to use ssl. Thanks for any help. Mike</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-mysql" rel="tag" title="see questions tagged &#39;mysql&#39;">mysql</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Dec '10, 07:47</strong></p><img src="https://secure.gravatar.com/avatar/41787bd6cfcd9ef6e4fe7eaacd79b3b3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mikeg&#39;s gravatar image" /><p><span>mikeg</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mikeg has no accepted answers">0%</span></p></div></div><div id="comments-container-1327" class="comments-container"><span id="1337"></span><div id="comment-1337" class="comment"><div id="post-1337-score" class="comment-score"></div><div class="comment-text"><p>I converted the extra answers to comments, please have a look at :</p><p>http://ask.wireshark.org/questions/292/example-of-how-to-use-askwiresharkorg-and-how-not-to</p><p>to know why :-)</p></div><div id="comment-1337-info" class="comment-info"><span class="comment-age">(14 Dec '10, 01:21)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div><span id="1356"></span><div id="comment-1356" class="comment"><div id="post-1356-score" class="comment-score"></div><div class="comment-text"><p>Thanks. I was unaware.</p></div><div id="comment-1356-info" class="comment-info"><span class="comment-age">(15 Dec '10, 05:15)</span> <span class="comment-user userinfo">mikeg</span></div></div></div><div id="comment-tools-1327" class="comment-tools"></div><div class="clear"></div><div id="comment-1327-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1332"></span>

<div id="answer-container-1332" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1332-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1332-score" class="post-score" title="current number of votes">0</div><span id="post-1332-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can verify if your mysql server supports ssl connection using the following query</p><p>SHOW VARIABLES LIKE 'have_ssl';</p><p>You should see something like this : +---------------+-------+ | Variable_name | Value | +---------------+-------+ | have_ssl | YES | +---------------+-------+</p><p>To validate whether your mysql server is working with ssl create a user that only accept ssl connections</p><p>CREATE USER johndoe;</p><p>grant select ON <em>.</em> to <span class="__cf_email__" data-cfemail="19737176777d767c59">[email protected]</span>'yourhost' identified by 'somepassword' REQUIRE SSL;</p><p>By succesfully connecting and quering data from the specified server</p><blockquote><p>database &gt; table 0 you would have validated that your ssl is correctly working</p></blockquote><p>Hope this helps</p><p>Victor</p><p>Gazzang http://gazzang.com</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Dec '10, 15:44</strong></p><img src="https://secure.gravatar.com/avatar/917fe532f172a7e5ce45bd97afdde5e0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Victor&#39;s gravatar image" /><p><span>Victor</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Victor has no accepted answers">0%</span></p></div></div><div id="comments-container-1332" class="comments-container"><span id="1328"></span><div id="comment-1328" class="comment"><div id="post-1328-score" class="comment-score"></div><div class="comment-text"><p>I can see the query under the mysql protocol but maybe that is the wrong level to be looking at. I don't know. Mike</p></div><div id="comment-1328-info" class="comment-info"><span class="comment-age">(13 Dec '10, 08:20)</span> <span class="comment-user userinfo">mikeg</span></div></div><span id="1335"></span><div id="comment-1335" class="comment"><div id="post-1335-score" class="comment-score"></div><div class="comment-text"><p>Victor, The database does support ssl and it is enabled. It doesn't work. No error messages, nothing to go on. One thing I'm not doing in this case is providing a client cert. I need to find a method that doesn't require a client cert. because I need a remote db connection for a webapp. The database server does not reside on the application server.</p><p>So, I'm trying to modify the Mysql connection string by adding the pair: encrypt=true. Using this method I can connect but don't know if the data is actually being encrypted. That's why I'm using Wireshark.</p><p>Last year, I setup MS Sqlserver with ssl and verified that the data was being encrypted using Wireshark.</p><p>Thanks for your response. Once I figure this out, I'll post my findings.</p><p>Mike</p></div><div id="comment-1335-info" class="comment-info"><span class="comment-age">(13 Dec '10, 19:31)</span> <span class="comment-user userinfo">mikeg</span></div></div></div><div id="comment-tools-1332" class="comment-tools"></div><div class="clear"></div><div id="comment-1332-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

