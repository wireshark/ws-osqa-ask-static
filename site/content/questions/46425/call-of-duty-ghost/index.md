+++
type = "question"
title = "Call of Duty Ghost?"
description = '''Hello I&#x27;m in need of assistance on the topic Call of Duty Ghost and I was wondering is there a way I can pull packets? We all know there are Modders/Hackers in certain games and this one has a few and we need to clean up alittle. But when I launch Wireshark it does pull some packets of everyone in t...'''
date = "2015-10-08T18:06:00Z"
lastmod = "2015-10-11T05:29:00Z"
weight = 46425
keywords = [ "ghost", "cod" ]
aliases = [ "/questions/46425" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Call of Duty Ghost?](/questions/46425/call-of-duty-ghost)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46425-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46425-score" class="post-score" title="current number of votes">0</div><span id="post-46425-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello I'm in need of assistance on the topic Call of Duty Ghost and I was wondering is there a way I can pull packets? We all know there are Modders/Hackers in certain games and this one has a few and we need to clean up alittle. But when I launch Wireshark it does pull some packets of everyone in the lobby but I can't filter them for the Modder/Hacker.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ghost" rel="tag" title="see questions tagged &#39;ghost&#39;">ghost</span> <span class="post-tag tag-link-cod" rel="tag" title="see questions tagged &#39;cod&#39;">cod</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Oct '15, 18:06</strong></p><img src="https://secure.gravatar.com/avatar/0d1d12015f01dbd3ad86a66a0d8b9507?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Junktio&#39;s gravatar image" /><p><span>Junktio</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Junktio has no accepted answers">0%</span></p></div></div><div id="comments-container-46425" class="comments-container"></div><div id="comment-tools-46425" class="comment-tools"></div><div class="clear"></div><div id="comment-46425-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="46455"></span>

<div id="answer-container-46455" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46455-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46455-score" class="post-score" title="current number of votes">0</div><span id="post-46455-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't believe there is any easy way to detect cheaters/modders/you_name_them via Wireshark. Most cheating tricks involve changing the "game logic" within the client. This will obviously result in network traffic as well, but you won't be able to see the difference between 'regular' traffic and 'cheater' traffic.</p><p>See here: <a href="http://gaming.stackexchange.com/questions/12534/how-do-i-determine-if-someone-is-cheating-in-call-of-duty-black-ops">http://gaming.stackexchange.com/questions/12534/how-do-i-determine-if-someone-is-cheating-in-call-of-duty-black-ops</a><br />
</p><p>So, the short answer to your question is: There is no (easy) way to detect a cheater with Wireshark!</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Oct '15, 05:29</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-46455" class="comments-container"></div><div id="comment-tools-46455" class="comment-tools"></div><div class="clear"></div><div id="comment-46455-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

