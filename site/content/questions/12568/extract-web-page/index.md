+++
type = "question"
title = "extract web page"
description = '''How do you extract a web page and a graphc file from wire shark?'''
date = "2012-07-10T12:15:00Z"
lastmod = "2012-07-10T12:15:00Z"
weight = 12568
keywords = [ "extracttop" ]
aliases = [ "/questions/12568" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [extract web page](/questions/12568/extract-web-page)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12568-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12568-score" class="post-score" title="current number of votes">0</div><span id="post-12568-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do you extract a web page and a graphc file from wire shark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-extracttop" rel="tag" title="see questions tagged &#39;extracttop&#39;">extracttop</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jul '12, 12:15</strong></p><img src="https://secure.gravatar.com/avatar/49fa20257072052df246d3c91e7c4355?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jennifer26m&#39;s gravatar image" /><p><span>jennifer26m</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jennifer26m has no accepted answers">0%</span></p></div></div><div id="comments-container-12568" class="comments-container"></div><div id="comment-tools-12568" class="comment-tools"></div><div class="clear"></div><div id="comment-12568-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

