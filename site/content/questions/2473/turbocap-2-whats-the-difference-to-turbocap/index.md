+++
type = "question"
title = "TurboCap 2 - whats the difference to TurboCap?"
description = '''I tried to find out what the difference is between the TurboCap and TurboCap 2 card - or did they just relabel the &quot;old&quot; TurboCap card to indicate that it has 2 ports while there now also is a 4 port card?  And, as far as I remember I bought a TurboCap card for about $895 a while ago , but now it&#x27;s ...'''
date = "2011-02-22T05:16:00Z"
lastmod = "2011-03-01T11:33:00Z"
weight = 2473
keywords = [ "hardware", "capture", "turbocap" ]
aliases = [ "/questions/2473" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [TurboCap 2 - whats the difference to TurboCap?](/questions/2473/turbocap-2-whats-the-difference-to-turbocap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2473-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2473-score" class="post-score" title="current number of votes">0</div><span id="post-2473-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I tried to find out what the difference is between the TurboCap and TurboCap 2 card - or did they just relabel the "old" TurboCap card to indicate that it has 2 ports while there now also is a 4 port card?</p><p>And, as far as I remember I bought a TurboCap card for about $895 a while ago , but now it's more close to $2000 - is there any reason for that (or is the reason called "Riverbed" :-))?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hardware" rel="tag" title="see questions tagged &#39;hardware&#39;">hardware</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-turbocap" rel="tag" title="see questions tagged &#39;turbocap&#39;">turbocap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Feb '11, 05:16</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-2473" class="comments-container"><span id="2570"></span><div id="comment-2570" class="comment"><div id="post-2570-score" class="comment-score">1</div><div class="comment-text"><p>I'm not an employee of Cace/Riverbed, so I don't know for sure, but as far as I can tell, your intuition seems to be correct. TurboCap2 probably just distinguishes it from TurboCap4.</p><p>As for the price difference, did you perhaps purchase your TurboCap card at one of the SHARKFEST's at a promotional price? Or perhaps at an "early offer" price, or maybe you got a discount for purchasing other products like AirPcap???</p></div><div id="comment-2570-info" class="comment-info"><span class="comment-age">(25 Feb '11, 18:49)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="2572"></span><div id="comment-2572" class="comment"><div id="post-2572-score" class="comment-score"></div><div class="comment-text"><p>cmaynard, thank you for your comment. No, I did not buy TurboCap at a discount price, it was just a regular sale. Maybe it was an "early offer" but I don't remember it that way. Not to worry, I was just a bit confused :-)</p></div><div id="comment-2572-info" class="comment-info"><span class="comment-age">(26 Feb '11, 00:47)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-2473" class="comment-tools"></div><div class="clear"></div><div id="comment-2473-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2612"></span>

<div id="answer-container-2612" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2612-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2612-score" class="post-score" title="current number of votes">1</div><span id="post-2612-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Jasper has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You and Chris are correct. The original 2-port card was named <em>TurboCap</em>. When the 4-port card came out we had to change its name to <em>TurboCap 2</em> to distinguish it from <em>TurboCap 4</em>.</p><p>I tend to duck my head and keep coding when pricing is discussed so you'll have to ask the sales team about that.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Mar '11, 09:44</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-2612" class="comments-container"><span id="2616"></span><div id="comment-2616" class="comment"><div id="post-2616-score" class="comment-score"></div><div class="comment-text"><p>Thanks, Gerald! About the price - no worries, sales people do funny things sometimes :-)</p></div><div id="comment-2616-info" class="comment-info"><span class="comment-age">(01 Mar '11, 11:33)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-2612" class="comment-tools"></div><div class="clear"></div><div id="comment-2612-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

