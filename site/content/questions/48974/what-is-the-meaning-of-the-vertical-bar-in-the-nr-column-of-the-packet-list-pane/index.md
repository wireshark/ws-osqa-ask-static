+++
type = "question"
title = "What is the meaning of the vertical bar in the Nr. column of the &quot;Packet List&quot; pane?"
description = '''With the version 2 of Wireshark I recognize an enhancement in the Nr. column of the &quot;Packet List&quot; pane. Sometimes when I click onthis bar I get a dot, sometimes there are 2 dots marking different lines. I think there is some meaning behind. Does anyone know bout that?'''
date = "2016-01-08T08:09:00Z"
lastmod = "2016-01-08T09:06:00Z"
weight = 48974
keywords = [ "column", "nr." ]
aliases = [ "/questions/48974" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [What is the meaning of the vertical bar in the Nr. column of the "Packet List" pane?](/questions/48974/what-is-the-meaning-of-the-vertical-bar-in-the-nr-column-of-the-packet-list-pane)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48974-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48974-score" class="post-score" title="current number of votes">0</div><span id="post-48974-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>With the version 2 of Wireshark I recognize an enhancement in the Nr. column of the "Packet List" pane. Sometimes when I click onthis bar I get a dot, sometimes there are 2 dots marking different lines. I think there is some meaning behind.</p><p>Does anyone know bout that?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-column" rel="tag" title="see questions tagged &#39;column&#39;">column</span> <span class="post-tag tag-link-nr." rel="tag" title="see questions tagged &#39;nr.&#39;">nr.</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jan '16, 08:09</strong></p><img src="https://secure.gravatar.com/avatar/f76e1057a16ab7d81c0981c956f34ae1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="efranz&#39;s gravatar image" /><p><span>efranz</span><br />
<span class="score" title="21 reputation points">21</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="efranz has no accepted answers">0%</span></p></div></div><div id="comments-container-48974" class="comments-container"></div><div id="comment-tools-48974" class="comment-tools"></div><div class="clear"></div><div id="comment-48974-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48976"></span>

<div id="answer-container-48976" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48976-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48976-score" class="post-score" title="current number of votes">2</div><span id="post-48976-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="efranz has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This allows you to quickly identify related packets. For example a continuous line corresponds to packets belonging to the same conversation as the currently selected packet, a dashed line packets not related to the conversation.</p><p>See <a href="https://blog.wireshark.org/2015/11/let-me-tell-you-about-wireshark-2-0/">https://blog.wireshark.org/2015/11/let-me-tell-you-about-wireshark-2-0/</a> and <a href="https://www.youtube.com/watch?v=rLfYuO6pdVA">https://www.youtube.com/watch?v=rLfYuO6pdVA</a> for more details.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Jan '16, 08:25</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-48976" class="comments-container"><span id="48977"></span><div id="comment-48977" class="comment"><div id="post-48977-score" class="comment-score"></div><div class="comment-text"><p>The Users Guide <a href="https://www.wireshark.org/docs/wsug_html/#ChUsePacketListPaneSection">section</a> for the packet list could use an update.</p></div><div id="comment-48977-info" class="comment-info"><span class="comment-age">(08 Jan '16, 08:40)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="48979"></span><div id="comment-48979" class="comment"><div id="post-48979-score" class="comment-score"></div><div class="comment-text"><p>Thanks a lot, and also for the short summary of enhancements in Wireshark 2 and the youtube-video.</p></div><div id="comment-48979-info" class="comment-info"><span class="comment-age">(08 Jan '16, 08:58)</span> <span class="comment-user userinfo">efranz</span></div></div><span id="48980"></span><div id="comment-48980" class="comment"><div id="post-48980-score" class="comment-score"></div><div class="comment-text"><p>You also can have a look here: <a href="http://crnetpackets.com/2015/06/09/the-great-new-features-of-wireshark-1-99/">http://crnetpackets.com/2015/06/09/the-great-new-features-of-wireshark-1-99/</a> There I have explained a few different symbols of this grey ribbon.</p></div><div id="comment-48980-info" class="comment-info"><span class="comment-age">(08 Jan '16, 08:58)</span> <span class="comment-user userinfo">Christian_R</span></div></div><span id="48981"></span><div id="comment-48981" class="comment"><div id="post-48981-score" class="comment-score"></div><div class="comment-text"><p>More specificially:</p><p><a href="https://www.youtube.com/watch?v=rLfYuO6pdVA&amp;t=19m56s">https://www.youtube.com/watch?v=rLfYuO6pdVA&amp;t=19m56s</a></p></div><div id="comment-48981-info" class="comment-info"><span class="comment-age">(08 Jan '16, 09:03)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="48982"></span><div id="comment-48982" class="comment"><div id="post-48982-score" class="comment-score"></div><div class="comment-text"><p>Excellent. Thanks for all your answers and hints.</p></div><div id="comment-48982-info" class="comment-info"><span class="comment-age">(08 Jan '16, 09:06)</span> <span class="comment-user userinfo">efranz</span></div></div></div><div id="comment-tools-48976" class="comment-tools"></div><div class="clear"></div><div id="comment-48976-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

