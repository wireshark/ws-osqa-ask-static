+++
type = "question"
title = "tshark follow,udp,raw write file(pipe)"
description = '''Hello, what syntax to use? tshark -r mon0.pcap -q -z &quot;follow,udp,raw,192.168.1.102:52166,239.0.0.1:1234&quot; -w test.ts  write eq mon0.pcap-dump udp stream. In wireshark -analyze-&amp;gt;follow udp stream- its ok.Make .ts file. Thanks.'''
date = "2016-06-20T03:55:00Z"
lastmod = "2016-06-20T03:55:00Z"
weight = 53571
keywords = [ "tshark.follow.udp" ]
aliases = [ "/questions/53571" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [tshark follow,udp,raw write file(pipe)](/questions/53571/tshark-followudpraw-write-filepipe)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53571-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53571-score" class="post-score" title="current number of votes">0</div><span id="post-53571-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>what syntax to use?</p><pre><code>tshark -r mon0.pcap -q -z &quot;follow,udp,raw,192.168.1.102:52166,239.0.0.1:1234&quot; -w test.ts</code></pre><p>write eq mon0.pcap-dump udp stream.</p><p>In wireshark -analyze-&gt;follow udp stream- its ok.Make .ts file.</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tshark.follow.udp" rel="tag" title="see questions tagged &#39;tshark.follow.udp&#39;">tshark.follow.udp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jun '16, 03:55</strong></p><img src="https://secure.gravatar.com/avatar/28536a246615cd52064fc2ef8fc48078?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="marian34&#39;s gravatar image" /><p><span>marian34</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="marian34 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Jun '16, 08:35</strong> </span></p></div></div><div id="comments-container-53571" class="comments-container"></div><div id="comment-tools-53571" class="comment-tools"></div><div class="clear"></div><div id="comment-53571-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

