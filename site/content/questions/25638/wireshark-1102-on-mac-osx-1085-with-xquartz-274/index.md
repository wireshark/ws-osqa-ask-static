+++
type = "question"
title = "wireshark 1.10.2 on mac osx 10.8.5 with xquartz 2.7.4"
description = '''Dear community, when I install wireshark 1.10.2 on mac osx 10.8.5 with xquartz 2.7.4, wireshark crash at boot and wont work. How can I solve this trouble. Thanks. Gabriele.'''
date = "2013-10-04T05:02:00Z"
lastmod = "2013-10-10T05:26:00Z"
weight = 25638
keywords = [ "wireshark" ]
aliases = [ "/questions/25638" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark 1.10.2 on mac osx 10.8.5 with xquartz 2.7.4](/questions/25638/wireshark-1102-on-mac-osx-1085-with-xquartz-274)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25638-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25638-score" class="post-score" title="current number of votes">0</div><span id="post-25638-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear community, when I install wireshark 1.10.2 on mac osx 10.8.5 with xquartz 2.7.4, wireshark crash at boot and wont work.</p><p>How can I solve this trouble.</p><p>Thanks.</p><p>Gabriele.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Oct '13, 05:02</strong></p><img src="https://secure.gravatar.com/avatar/4a07f4773a439d8245bcf03433ea796f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gabriele%20Ellena&#39;s gravatar image" /><p><span>Gabriele Ellena</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gabriele Ellena has no accepted answers">0%</span></p></div></div><div id="comments-container-25638" class="comments-container"><span id="25656"></span><div id="comment-25656" class="comment"><div id="post-25656-score" class="comment-score"></div><div class="comment-text"><p>What does the crash report for it in the Console application (look under "User Diagnostic Reports") say?</p></div><div id="comment-25656-info" class="comment-info"><span class="comment-age">(04 Oct '13, 14:14)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="25867"></span><div id="comment-25867" class="comment"><div id="post-25867-score" class="comment-score"></div><div class="comment-text"><p>hi, this is what UDR say :</p><p>10/10/13 14:23:37,597 defaults[459]: The domain/default pair of (kCFPreferencesAnyApplication, AppleAquaColorVariant) does not exist 10/10/13 14:23:37,615 defaults[460]: The domain/default pair of (kCFPreferencesAnyApplication, AppleHighlightColor) does not exist 10/10/13 14:23:37,659 defaults[473]: The domain/default pair of (.GlobalPreferences, AppleCollationOrder) does not exist</p><p>thanks</p></div><div id="comment-25867-info" class="comment-info"><span class="comment-age">(10 Oct '13, 05:26)</span> <span class="comment-user userinfo">Gabriele Ellena</span></div></div></div><div id="comment-tools-25638" class="comment-tools"></div><div class="clear"></div><div id="comment-25638-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

