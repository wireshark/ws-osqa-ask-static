+++
type = "question"
title = "Truncated cookies"
description = '''Hi I&#x27;m new to wireshark and trying to get more information on how to get the full cookies that isn&#x27;t &quot;truncated&quot;? The snaplen is not enabled. example: [truncated] Cookie: OAX=VDdXBE21tLMAAsVW; member_type=0; __utma=70287432xxxxxxxxxxxxxxxxxxxxxxxxx etc In advance thank you!  Regards C'''
date = "2011-05-02T14:28:00Z"
lastmod = "2012-01-03T22:00:00Z"
weight = 3885
keywords = [ "cookie", "truncated" ]
aliases = [ "/questions/3885" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Truncated cookies](/questions/3885/truncated-cookies)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3885-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3885-score" class="post-score" title="current number of votes">1</div><span id="post-3885-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I'm new to wireshark and trying to get more information on how to get the full cookies that isn't "truncated"? The snaplen is not enabled.</p><p>example: [truncated] Cookie: OAX=VDdXBE21tLMAAsVW; member_type=0; __utma=70287432xxxxxxxxxxxxxxxxxxxxxxxxx etc</p><p>In advance thank you!<br />
</p><p>Regards C</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cookie" rel="tag" title="see questions tagged &#39;cookie&#39;">cookie</span> <span class="post-tag tag-link-truncated" rel="tag" title="see questions tagged &#39;truncated&#39;">truncated</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 May '11, 14:28</strong></p><img src="https://secure.gravatar.com/avatar/278242a0681e902d33c609f345ee169c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mr%20C&#39;s gravatar image" /><p><span>Mr C</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mr C has no accepted answers">0%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 May '11, 19:22</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-3885" class="comments-container"></div><div id="comment-tools-3885" class="comment-tools"></div><div class="clear"></div><div id="comment-3885-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="3888"></span>

<div id="answer-container-3888" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3888-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3888-score" class="post-score" title="current number of votes">4</div><span id="post-3888-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi, Here [truncated] just means that the display of the field is trunkated, as in limited to 240 characters. The complete field is present and you should see it highlighted in the bytes pane. Regards Anders</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 May '11, 21:30</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 May '11, 07:16</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-3888" class="comments-container"></div><div id="comment-tools-3888" class="comment-tools"></div><div class="clear"></div><div id="comment-3888-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="8215"></span>

<div id="answer-container-8215" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8215-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8215-score" class="post-score" title="current number of votes">0</div><span id="post-8215-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Right click on the field and copy-value will copy the non-truncated value.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Jan '12, 22:00</strong></p><img src="https://secure.gravatar.com/avatar/f52e4b433ac1a3dd397a26939f847faa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="realdreams&#39;s gravatar image" /><p><span>realdreams</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="realdreams has no accepted answers">0%</span></p></div></div><div id="comments-container-8215" class="comments-container"></div><div id="comment-tools-8215" class="comment-tools"></div><div class="clear"></div><div id="comment-8215-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

