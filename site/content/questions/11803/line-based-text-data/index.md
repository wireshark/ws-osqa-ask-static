+++
type = "question"
title = "line based text data"
description = '''Im new to wireshark, but from my understanding line based texta data of POST methods is where usernames and passwords are displayed, however sometimes this field appears to be encrypted, is there anyway to see the text form of it? Thanks for the help!'''
date = "2012-06-10T16:09:00Z"
lastmod = "2012-06-10T16:09:00Z"
weight = 11803
keywords = [ "help" ]
aliases = [ "/questions/11803" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [line based text data](/questions/11803/line-based-text-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11803-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11803-score" class="post-score" title="current number of votes">0</div><span id="post-11803-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Im new to wireshark, but from my understanding line based texta data of POST methods is where usernames and passwords are displayed, however sometimes this field appears to be encrypted, is there anyway to see the text form of it? Thanks for the help!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jun '12, 16:09</strong></p><img src="https://secure.gravatar.com/avatar/415c50c2687e10f633a8a328661111fb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gumby67&#39;s gravatar image" /><p><span>gumby67</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gumby67 has no accepted answers">0%</span></p></div></div><div id="comments-container-11803" class="comments-container"></div><div id="comment-tools-11803" class="comment-tools"></div><div class="clear"></div><div id="comment-11803-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

