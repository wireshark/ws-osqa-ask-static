+++
type = "question"
title = "wireshark can capture traffice of switches configured in port mirroring?"
description = '''Below is the scenario: Linksys switches are configured in port-mirroring mode. there is lot of broadcast from the user machines. Can we capture the logs of those machines who are broadcasting?? Pls reply asap. thanks fro help.'''
date = "2011-11-17T23:41:00Z"
lastmod = "2011-11-18T15:20:00Z"
weight = 7499
keywords = [ "on", "port", "mirroring", "wireshark" ]
aliases = [ "/questions/7499" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark can capture traffice of switches configured in port mirroring?](/questions/7499/wireshark-can-capture-traffice-of-switches-configured-in-port-mirroring)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7499-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7499-score" class="post-score" title="current number of votes">0</div><span id="post-7499-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Below is the scenario: Linksys switches are configured in port-mirroring mode. there is lot of broadcast from the user machines.</p><p>Can we capture the logs of those machines who are broadcasting??</p><p>Pls reply asap.</p><p>thanks fro help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-on" rel="tag" title="see questions tagged &#39;on&#39;">on</span> <span class="post-tag tag-link-port" rel="tag" title="see questions tagged &#39;port&#39;">port</span> <span class="post-tag tag-link-mirroring" rel="tag" title="see questions tagged &#39;mirroring&#39;">mirroring</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Nov '11, 23:41</strong></p><img src="https://secure.gravatar.com/avatar/77a02490db824bbfd3a5b690143f4e4d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Manmohan&#39;s gravatar image" /><p><span>Manmohan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Manmohan has no accepted answers">0%</span></p></div></div><div id="comments-container-7499" class="comments-container"></div><div id="comment-tools-7499" class="comment-tools"></div><div class="clear"></div><div id="comment-7499-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7510"></span>

<div id="answer-container-7510" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7510-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7510-score" class="post-score" title="current number of votes">0</div><span id="post-7510-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can capture the traffic from the monitor port(s) and run statistics on the traffic exposing the top talkers.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Nov '11, 15:20</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-7510" class="comments-container"></div><div id="comment-tools-7510" class="comment-tools"></div><div class="clear"></div><div id="comment-7510-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

