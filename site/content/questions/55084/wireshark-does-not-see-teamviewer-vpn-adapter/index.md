+++
type = "question"
title = "Wireshark does not see Teamviewer VPN Adapter"
description = '''I am using TeamViewer VPN on a Windows 8.1 host. The VPN driver uses a virtual network adapter and I would like to capture the traffic on this adapter. I have wireshark (v2.0.5) installed on the same host but wireshark does not see the TeamViewer VPN Adapter. It sees all the other adapters on the ho...'''
date = "2016-08-23T15:45:00Z"
lastmod = "2016-08-23T15:45:00Z"
weight = 55084
keywords = [ "vpn", "teamviewer", "adapter", "virtual" ]
aliases = [ "/questions/55084" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark does not see Teamviewer VPN Adapter](/questions/55084/wireshark-does-not-see-teamviewer-vpn-adapter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55084-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55084-score" class="post-score" title="current number of votes">0</div><span id="post-55084-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using TeamViewer VPN on a Windows 8.1 host. The VPN driver uses a virtual network adapter and I would like to capture the traffic on this adapter.</p><p>I have wireshark (v2.0.5) installed on the same host but wireshark does not see the TeamViewer VPN Adapter. It sees all the other adapters on the host (virtual and real). Is there any particular reason why it does not see the TeamViewer VPN adapter?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vpn" rel="tag" title="see questions tagged &#39;vpn&#39;">vpn</span> <span class="post-tag tag-link-teamviewer" rel="tag" title="see questions tagged &#39;teamviewer&#39;">teamviewer</span> <span class="post-tag tag-link-adapter" rel="tag" title="see questions tagged &#39;adapter&#39;">adapter</span> <span class="post-tag tag-link-virtual" rel="tag" title="see questions tagged &#39;virtual&#39;">virtual</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Aug '16, 15:45</strong></p><img src="https://secure.gravatar.com/avatar/14ccb8cde6ee9aa4d5d110fbbcbe9db8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="iamthebull&#39;s gravatar image" /><p><span>iamthebull</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="iamthebull has no accepted answers">0%</span></p></div></div><div id="comments-container-55084" class="comments-container"></div><div id="comment-tools-55084" class="comment-tools"></div><div class="clear"></div><div id="comment-55084-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

