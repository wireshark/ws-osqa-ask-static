+++
type = "question"
title = "meaning of wlsscid?"
description = '''88 10.665353 SIP client A-SBC SIP Request: ACK sip:&quot;mobile_number_in_local_format&quot;@&quot;A-SBC_IP_address&quot;:5060;transport=udp;wlsscid=45d67fb8e542f2b9 What is wlsscid?'''
date = "2011-05-08T06:53:00Z"
lastmod = "2011-05-08T08:45:00Z"
weight = 4002
keywords = [ "identification", "wlsscid" ]
aliases = [ "/questions/4002" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [meaning of wlsscid?](/questions/4002/meaning-of-wlsscid)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4002-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4002-score" class="post-score" title="current number of votes">0</div><span id="post-4002-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>88 10.665353 SIP client A-SBC SIP Request: ACK sip:"mobile_number_in_local_format"@"A-SBC_IP_address":5060;transport=udp;wlsscid=45d67fb8e542f2b9</p><p>What is wlsscid?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-identification" rel="tag" title="see questions tagged &#39;identification&#39;">identification</span> <span class="post-tag tag-link-wlsscid" rel="tag" title="see questions tagged &#39;wlsscid&#39;">wlsscid</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 May '11, 06:53</strong></p><img src="https://secure.gravatar.com/avatar/13231e33ab17a93476f7b98c9d5b272a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wired&#39;s gravatar image" /><p><span>wired</span><br />
<span class="score" title="44 reputation points">44</span><span title="13 badges"><span class="badge1">●</span><span class="badgecount">13</span></span><span title="14 badges"><span class="silver">●</span><span class="badgecount">14</span></span><span title="17 badges"><span class="bronze">●</span><span class="badgecount">17</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wired has one accepted answer">9%</span></p></div></div><div id="comments-container-4002" class="comments-container"></div><div id="comment-tools-4002" class="comment-tools"></div><div class="clear"></div><div id="comment-4002-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4003"></span>

<div id="answer-container-4003" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4003-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4003-score" class="post-score" title="current number of votes">1</div><span id="post-4003-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="wired has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://forums.oracle.com/forums/thread.jspa?threadID=936253&amp;tstart=-3">wlsscid</a> is a Weblogic SIP server parameter<br />
<br />
<a href="http://download.oracle.com/docs/cd/E13209_01/wlcp/wlss31/notes/new.html">Table 1-2 WebLogic SIP Server Parameters</a><br />
wlsscid identifies the cluster ID of the cluster that originated the SIP message during a software upgrade. The sideways forwarding mechanism uses this attribute to ensure that messages are delivered to a compatible cluster.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 May '11, 08:07</strong></p><img src="https://secure.gravatar.com/avatar/fac200552b0c24be2bc93a740bd54d0d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joke&#39;s gravatar image" /><p><span>joke</span><br />
<span class="score" title="1278 reputation points"><span>1.3k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joke has 6 accepted answers">9%</span> </br></br></p></div></div><div id="comments-container-4003" class="comments-container"><span id="4004"></span><div id="comment-4004" class="comment"><div id="post-4004-score" class="comment-score"></div><div class="comment-text"><p>Thank you.</p></div><div id="comment-4004-info" class="comment-info"><span class="comment-age">(08 May '11, 08:45)</span> <span class="comment-user userinfo">wired</span></div></div></div><div id="comment-tools-4003" class="comment-tools"></div><div class="clear"></div><div id="comment-4003-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

