+++
type = "question"
title = "Prepopulate Filters in IO Graphs?"
description = '''I use the same IO Graphs filters every time when I begin my analysis of WLAN traces. Is there a way to pre-populate the filters and save them so I don&#x27;t have to fill them in every time? '''
date = "2012-06-26T23:35:00Z"
lastmod = "2012-06-27T03:45:00Z"
weight = 12226
keywords = [ "graph" ]
aliases = [ "/questions/12226" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Prepopulate Filters in IO Graphs?](/questions/12226/prepopulate-filters-in-io-graphs)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12226-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12226-score" class="post-score" title="current number of votes">0</div><span id="post-12226-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I use the same IO Graphs filters every time when I begin my analysis of WLAN traces. Is there a way to pre-populate the filters and save them so I don't have to fill them in every time?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jun '12, 23:35</strong></p><img src="https://secure.gravatar.com/avatar/8f02f7e3f905ddb31e65e05c9588cbda?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="robin5u&#39;s gravatar image" /><p><span>robin5u</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="robin5u has no accepted answers">0%</span></p></div></div><div id="comments-container-12226" class="comments-container"></div><div id="comment-tools-12226" class="comment-tools"></div><div class="clear"></div><div id="comment-12226-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12231"></span>

<div id="answer-container-12231" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12231-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12231-score" class="post-score" title="current number of votes">0</div><span id="post-12231-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Go to:</p><blockquote><p><code>Analyze -&gt; Dsiplay Filters -&gt; New</code><br />
</p></blockquote><p>Define your Filter. Click 'Save'.</p><p>Then in the IO Graphs click on the Button <code>Filter:</code> and select your display filter from the list.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jun '12, 03:45</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-12231" class="comments-container"></div><div id="comment-tools-12231" class="comment-tools"></div><div class="clear"></div><div id="comment-12231-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

