+++
type = "question"
title = "Wireshark for Ubuntu Linux on a stand alone machine"
description = '''I need to install Wireshark on a stand-alone Ubuntu machine (without Internet access). The answers from here all points to getting packages via wget and so forth. Is there a complete package somewhere that I can download and copy over to this machine (via flash drive)? Thanks,'''
date = "2013-02-19T09:53:00Z"
lastmod = "2013-02-22T10:53:00Z"
weight = 18742
keywords = [ "alone", "stand", "ubuntu" ]
aliases = [ "/questions/18742" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark for Ubuntu Linux on a stand alone machine](/questions/18742/wireshark-for-ubuntu-linux-on-a-stand-alone-machine)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18742-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18742-score" class="post-score" title="current number of votes">0</div><span id="post-18742-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need to install Wireshark on a stand-alone Ubuntu machine (without Internet access). The answers from here all points to getting packages via wget and so forth. Is there a complete package somewhere that I can download and copy over to this machine (via flash drive)?</p><p>Thanks,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-alone" rel="tag" title="see questions tagged &#39;alone&#39;">alone</span> <span class="post-tag tag-link-stand" rel="tag" title="see questions tagged &#39;stand&#39;">stand</span> <span class="post-tag tag-link-ubuntu" rel="tag" title="see questions tagged &#39;ubuntu&#39;">ubuntu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Feb '13, 09:53</strong></p><img src="https://secure.gravatar.com/avatar/4025240b8c0475c260d9cb7529e827c9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ecs1749&#39;s gravatar image" /><p><span>ecs1749</span><br />
<span class="score" title="21 reputation points">21</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="11 badges"><span class="bronze">●</span><span class="badgecount">11</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ecs1749 has no accepted answers">0%</span></p></div></div><div id="comments-container-18742" class="comments-container"></div><div id="comment-tools-18742" class="comment-tools"></div><div class="clear"></div><div id="comment-18742-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="18745"></span>

<div id="answer-container-18745" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18745-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18745-score" class="post-score" title="current number of votes">0</div><span id="post-18745-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://packages.ubuntu.com/">http://packages.ubuntu.com/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Feb '13, 10:42</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-18745" class="comments-container"><span id="18780"></span><div id="comment-18780" class="comment"><div id="post-18780-score" class="comment-score"></div><div class="comment-text"><p>This method is a non-starter. Trying to hunt down all the dependencies is too daunting.</p></div><div id="comment-18780-info" class="comment-info"><span class="comment-age">(20 Feb '13, 10:16)</span> <span class="comment-user userinfo">ecs1749</span></div></div></div><div id="comment-tools-18745" class="comment-tools"></div><div class="clear"></div><div id="comment-18745-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="18752"></span>

<div id="answer-container-18752" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18752-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18752-score" class="post-score" title="current number of votes">0</div><span id="post-18752-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Is there a complete package somewhere that I can download and copy over to this machine (via flash drive)?</p></blockquote><p>There are some dependencies for the wireshark package, so just downloading that single package does not really help and there is no 'super-package' that contains everything you need.</p><p>So, what are your options?</p><p>I suggest to follow one of the tutorials on the internet for offline package installation</p><p>Easiest way:<br />
</p><blockquote><p><a href="http://techspalace.blogspot.de/2009/04/offline-update-ubuntu.html">http://techspalace.blogspot.de/2009/04/offline-update-ubuntu.html</a><br />
</p></blockquote><p>Other options:<br />
</p><blockquote><p><a href="http://ubuntuforums.org/showthread.php?t=1100816">http://ubuntuforums.org/showthread.php?t=1100816</a><br />
<a href="http://askubuntu.com/questions/974/how-can-i-install-software-or-packages-without-internet-offline">http://askubuntu.com/questions/974/how-can-i-install-software-or-packages-without-internet-offline</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Feb '13, 13:32</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-18752" class="comments-container"><span id="18757"></span><div id="comment-18757" class="comment"><div id="post-18757-score" class="comment-score"></div><div class="comment-text"><p>Thanks, Kurt. With the easiest way, do I need a Linux machine for Internet or can it be Windows (the instruction simply said "run it")?</p><p>...oh, keryxproject<br />
</p><p>Never mind...I am trying it now.</p></div><div id="comment-18757-info" class="comment-info"><span class="comment-age">(19 Feb '13, 15:46)</span> <span class="comment-user userinfo">ecs1749</span></div></div><span id="18765"></span><div id="comment-18765" class="comment"><div id="post-18765-score" class="comment-score"></div><div class="comment-text"><p>you need a system that has <strong>wget</strong> installed (available on Linux and Windows), as the generated script simply contains wget commands.</p><p>Hint: You can also just download the *.deb files (URL in the script) with a browser.</p><p>Example (not related to Wireshark):</p><pre><code>#!/bin/sh
wget -c http://de.archive.ubuntu.com/ubuntu/pool/main/f/fonts-sil-gentium/fonts-sil-gentium_1.02-12_all.deb
wget -c http://de.archive.ubuntu.com/ubuntu/pool/main/f/fonts-sil-gentium-basic/fonts-sil-gentium-basic_1.1-5_all.deb
wget -c http://de.archive.ubuntu.com/ubuntu/pool/main/t/tomcat6/libservlet2.5-java_6.0.35-1ubuntu3.2_all.deb</code></pre><p>On Windows, you would download <a href="http://gnuwin32.sourceforge.net/packages/wget.htm">wget for Windows</a> and then manually run the commands in the script, one by one (or use a browser). Then you would transfer all *.deb files to your Linux system and follow the tutorial.</p></div><div id="comment-18765-info" class="comment-info"><span class="comment-age">(20 Feb '13, 02:58)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="18781"></span><div id="comment-18781" class="comment"><div id="post-18781-score" class="comment-score"></div><div class="comment-text"><p>"Easiest way:</p><p><a href="http://techspalace.blogspot.de/2009/04/offline-update-ubuntu.html">http://techspalace.blogspot.de/2009/04/offline-update-ubuntu.html</a></p><p>Other options:</p><p><a href="http://ubuntuforums.org/showthread.php?t=1100816">http://ubuntuforums.org/showthread.php?t=1100816</a> <a href="http://askubuntu.com/questions/974/how-can-i-install-software-or-packages-without-internet-offline">http://askubuntu.com/questions/974/how-can-i-install-software-or-packages-without-internet-offline"</a></p><p>Can't get easiest method to work. Synaptic Package Manager not installed. Tried to use Software Center, found Wireshark but then there is no option to "Generate Package Download Script"</p><p>With the "Other options", the script file created by grabpackages.py is basically blank (contain only 1 line wget -c).</p></div><div id="comment-18781-info" class="comment-info"><span class="comment-age">(20 Feb '13, 10:28)</span> <span class="comment-user userinfo">ecs1749</span></div></div><span id="18782"></span><div id="comment-18782" class="comment"><div id="post-18782-score" class="comment-score"></div><div class="comment-text"><p>With the Keryx method, when I run Keryx on a machine with Internet access, clicked Manage, and it just sit there "Downloading package lists..." for a long long time and nothing happens.</p></div><div id="comment-18782-info" class="comment-info"><span class="comment-age">(20 Feb '13, 10:54)</span> <span class="comment-user userinfo">ecs1749</span></div></div><span id="18786"></span><div id="comment-18786" class="comment"><div id="post-18786-score" class="comment-score"></div><div class="comment-text"><blockquote><p>Can't get easiest method to work. Synaptic Package Manager not installed.</p></blockquote><p>well, then it's kind of 'hard'. You could install a 'dummy' Ubuntu system (same version, only base packages), run Synaptic there, download all packages, then install them on the standalone system. If additional packages are required, apt-get will tell you.</p><p>You could also use a proxy to get internet access for the standalone system</p><blockquote><p><code>export http_proxy=http://yourproxyaddress:proxyport</code><br />
<code>apt-get install wireshark</code></p></blockquote></div><div id="comment-18786-info" class="comment-info"><span class="comment-age">(20 Feb '13, 13:48)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="18819"></span><div id="comment-18819" class="comment not_top_scorer"><div id="post-18819-score" class="comment-score"></div><div class="comment-text"><p>I ended up installing Linux as a client OS under VirtualBox on the isolated machine, then zip up the virtual Linux and copied over to a Windows machine that has Internet access. After that, I was able to get Wireshark installed and works good up to this point. Then I zipped the virtual machine up and copy it back to the isolated machine.<br />
</p><p>In theory, I should be able to boot up the virtual Linux and Wireshark should work from then on. Right now, it won't boot. This is not a Wireshark issue - just a Virtual Box issue. But just to comment to close the loop on this one for future reference.</p></div><div id="comment-18819-info" class="comment-info"><span class="comment-age">(22 Feb '13, 10:53)</span> <span class="comment-user userinfo">ecs1749</span></div></div></div><div id="comment-tools-18752" class="comment-tools"><span class="comments-showing"> showing 5 of 6 </span> <a href="#" class="show-all-comments-link">show 1 more comments</a></div><div class="clear"></div><div id="comment-18752-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

