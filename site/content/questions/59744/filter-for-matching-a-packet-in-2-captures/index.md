+++
type = "question"
title = "Filter for matching a packet in 2 captures"
description = '''I have a iSCSI machine which connects to a Target machine. I&#x27;m running a wireshark on both source and the destination. I want to find a packet from the source where i have the segment id from the destination. Reason for the test: The storage team is saying the packet that Destination receives has a ...'''
date = "2017-02-28T12:59:00Z"
lastmod = "2017-02-28T13:04:00Z"
weight = 59744
keywords = [ "filter" ]
aliases = [ "/questions/59744" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Filter for matching a packet in 2 captures](/questions/59744/filter-for-matching-a-packet-in-2-captures)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59744-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59744-score" class="post-score" title="current number of votes">0</div><span id="post-59744-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a iSCSI machine which connects to a Target machine. I'm running a wireshark on both source and the destination. I want to find a packet from the source where i have the segment id from the destination.</p><p>Reason for the test: The storage team is saying the packet that Destination receives has a small packet length.so the application fails. I want to see the same packet in the source to see the size of the packet length,However I'm not able to find the filter to check the segment id to search.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Feb '17, 12:59</strong></p><img src="https://secure.gravatar.com/avatar/80dcf18fee554aecce7da5d671e6b67f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="batcha_malick&#39;s gravatar image" /><p><span>batcha_malick</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="batcha_malick has no accepted answers">0%</span></p></div></div><div id="comments-container-59744" class="comments-container"></div><div id="comment-tools-59744" class="comment-tools"></div><div class="clear"></div><div id="comment-59744-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59745"></span>

<div id="answer-container-59745" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59745-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59745-score" class="post-score" title="current number of votes">0</div><span id="post-59745-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please have a look here: <a href="https://blog.packet-foo.com/2015/02/working-with-multi-point-captures/">https://blog.packet-foo.com/2015/02/working-with-multi-point-captures/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Feb '17, 13:04</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></div></div><div id="comments-container-59745" class="comments-container"></div><div id="comment-tools-59745" class="comment-tools"></div><div class="clear"></div><div id="comment-59745-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

