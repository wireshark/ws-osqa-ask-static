+++
type = "question"
title = "how do I capture activity on the router"
description = '''My wireless router does not show up in my list. How do I make it show up as a device that I want to monitor?'''
date = "2013-02-04T12:41:00Z"
lastmod = "2013-02-04T12:46:00Z"
weight = 18294
keywords = [ "router" ]
aliases = [ "/questions/18294" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how do I capture activity on the router](/questions/18294/how-do-i-capture-activity-on-the-router)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18294-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18294-score" class="post-score" title="current number of votes">0</div><span id="post-18294-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My wireless router does not show up in my list. How do I make it show up as a device that I want to monitor?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Feb '13, 12:41</strong></p><img src="https://secure.gravatar.com/avatar/f8d429e50559d9708949f05807724dc7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="appups785&#39;s gravatar image" /><p><span>appups785</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="appups785 has no accepted answers">0%</span></p></div></div><div id="comments-container-18294" class="comments-container"><span id="18295"></span><div id="comment-18295" class="comment"><div id="post-18295-score" class="comment-score"></div><div class="comment-text"><p>What is this list you mention? The interface list shown in Wireshark?</p></div><div id="comment-18295-info" class="comment-info"><span class="comment-age">(04 Feb '13, 12:46)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-18294" class="comment-tools"></div><div class="clear"></div><div id="comment-18294-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

