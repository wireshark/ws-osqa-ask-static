+++
type = "question"
title = "Can any one help me how can i extract the EXE file in HTTP pcap?"
description = '''Please help me'''
date = "2011-03-22T05:11:00Z"
lastmod = "2011-03-22T07:15:00Z"
weight = 3017
keywords = [ "extractexe" ]
aliases = [ "/questions/3017" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can any one help me how can i extract the EXE file in HTTP pcap?](/questions/3017/can-any-one-help-me-how-can-i-extract-the-exe-file-in-http-pcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3017-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3017-score" class="post-score" title="current number of votes">0</div><span id="post-3017-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Please help me</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-extractexe" rel="tag" title="see questions tagged &#39;extractexe&#39;">extractexe</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Mar '11, 05:11</strong></p><img src="https://secure.gravatar.com/avatar/4382893663cabf8a4a0e2ca272e7bdc0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sethaliasathanar&#39;s gravatar image" /><p><span>sethaliasath...</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sethaliasathanar has no accepted answers">0%</span></p></div></div><div id="comments-container-3017" class="comments-container"></div><div id="comment-tools-3017" class="comment-tools"></div><div class="clear"></div><div id="comment-3017-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3022"></span>

<div id="answer-container-3022" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3022-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3022-score" class="post-score" title="current number of votes">0</div><span id="post-3022-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you checked the <a href="http://www.wireshark.org/docs/wsug_html_chunked/ChIOExportSection.html#ChIOExportObjectsDialog">Users Guide</a>?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Mar '11, 07:15</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-3022" class="comments-container"></div><div id="comment-tools-3022" class="comment-tools"></div><div class="clear"></div><div id="comment-3022-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

