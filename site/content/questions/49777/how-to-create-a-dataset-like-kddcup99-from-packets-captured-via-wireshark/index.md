+++
type = "question"
title = "[closed] How to create a dataset like KDDCUP99 from packets captured via Wireshark?"
description = '''I am doing a project in Network Attack Detection.I needed to create a dataset similar to KDDCUP99 but with lesser attributes. Please guide me on how to create the dataset from the packets captured via Wireshark or tshark. '''
date = "2016-02-03T07:30:00Z"
lastmod = "2016-02-03T08:39:00Z"
weight = 49777
keywords = [ "network", "dissection", "packet-capture", "tshark", "wireshark" ]
aliases = [ "/questions/49777" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] How to create a dataset like KDDCUP99 from packets captured via Wireshark?](/questions/49777/how-to-create-a-dataset-like-kddcup99-from-packets-captured-via-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49777-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49777-score" class="post-score" title="current number of votes">0</div><span id="post-49777-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am doing a project in Network Attack Detection.I needed to create a dataset similar to KDDCUP99 but with lesser attributes. Please guide me on how to create the dataset from the packets captured via Wireshark or tshark.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-dissection" rel="tag" title="see questions tagged &#39;dissection&#39;">dissection</span> <span class="post-tag tag-link-packet-capture" rel="tag" title="see questions tagged &#39;packet-capture&#39;">packet-capture</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Feb '16, 07:30</strong></p><img src="https://secure.gravatar.com/avatar/f0439f43a20564c4f0b85e123cc538ac?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Niya&#39;s gravatar image" /><p><span>Niya</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Niya has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>03 Feb '16, 08:39</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-49777" class="comments-container"><span id="49785"></span><div id="comment-49785" class="comment"><div id="post-49785-score" class="comment-score"></div><div class="comment-text"><p><a href="https://ask.wireshark.org/questions/14655">Duplicate of this.</a></p></div><div id="comment-49785-info" class="comment-info"><span class="comment-age">(03 Feb '16, 08:39)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-49777" class="comment-tools"></div><div class="clear"></div><div id="comment-49777-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by Jaap 03 Feb '16, 08:39

</div>

</div>

</div>

