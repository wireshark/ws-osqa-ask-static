+++
type = "question"
title = "dos attack ip"
description = '''Hello recently my game server is getting dos attacks everyday, so would it be possible to track real IP address of attacker with wireshark? Or it is not possible? Discovered wireshark few days ago :-))'''
date = "2014-02-19T07:30:00Z"
lastmod = "2014-02-19T09:55:00Z"
weight = 30014
keywords = [ "dos" ]
aliases = [ "/questions/30014" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [dos attack ip](/questions/30014/dos-attack-ip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30014-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30014-score" class="post-score" title="current number of votes">0</div><span id="post-30014-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello recently my game server is getting dos attacks everyday, so would it be possible to track real IP address of attacker with wireshark? Or it is not possible? Discovered wireshark few days ago :-))</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dos" rel="tag" title="see questions tagged &#39;dos&#39;">dos</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Feb '14, 07:30</strong></p><img src="https://secure.gravatar.com/avatar/20fff803457483f17bbc0241a2a9e9d2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ignas&#39;s gravatar image" /><p><span>Ignas</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ignas has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Feb '14, 07:39</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-30014" class="comments-container"><span id="30015"></span><div id="comment-30015" class="comment"><div id="post-30015-score" class="comment-score"></div><div class="comment-text"><p>fixed smiley, as discovering Wireshark should make you smile ;-)</p></div><div id="comment-30015-info" class="comment-info"><span class="comment-age">(19 Feb '14, 07:39)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="30016"></span><div id="comment-30016" class="comment"><div id="post-30016-score" class="comment-score"></div><div class="comment-text"><p>Ye, but it is very complicated. so is it possible to do so?</p></div><div id="comment-30016-info" class="comment-info"><span class="comment-age">(19 Feb '14, 07:40)</span> <span class="comment-user userinfo">Ignas</span></div></div><span id="30021"></span><div id="comment-30021" class="comment"><div id="post-30021-score" class="comment-score"></div><div class="comment-text"><p>I was just dosed again. Router has many logs with ip addr about dos but in wireshark couldnt find any of them... how is that possible? Here is small log: <a href="https://mega.co.nz/#!cIVwAJjR!_rwA2dBVvKkKJyzYE6VllVGggLE3A8PviqXSWZXIvio">wireshark file</a> also this dos was kinda weak cause i was able to login to servers and start wireshark. would be awsome if someone could atleast tell me the port they are attacking</p></div><div id="comment-30021-info" class="comment-info"><span class="comment-age">(19 Feb '14, 09:55)</span> <span class="comment-user userinfo">Ignas</span></div></div></div><div id="comment-tools-30014" class="comment-tools"></div><div class="clear"></div><div id="comment-30014-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

