+++
type = "question"
title = "How To setup wireshark to scan all address etc. details inside"
description = '''Hi, I Would like to ask what could be the best setup to work with wireshark. I have install wireshark and connect it to the core switch  Problem that I have encountered was I can&#x27;t see all the address on my network. Thanks '''
date = "2015-05-13T20:21:00Z"
lastmod = "2015-05-14T04:28:00Z"
weight = 42383
keywords = [ "wireshark" ]
aliases = [ "/questions/42383" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How To setup wireshark to scan all address etc. details inside](/questions/42383/how-to-setup-wireshark-to-scan-all-address-etc-details-inside)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42383-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42383-score" class="post-score" title="current number of votes">0</div><span id="post-42383-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I Would like to ask what could be the best setup to work with wireshark. I have install wireshark and connect it to the core switch Problem that I have encountered was I can't see all the address on my network.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 May '15, 20:21</strong></p><img src="https://secure.gravatar.com/avatar/439ed4f0db81f1ee8227393638ea2aee?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="searching123&#39;s gravatar image" /><p><span>searching123</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="searching123 has no accepted answers">0%</span></p></div></div><div id="comments-container-42383" class="comments-container"></div><div id="comment-tools-42383" class="comment-tools"></div><div class="clear"></div><div id="comment-42383-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42387"></span>

<div id="answer-container-42387" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42387-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42387-score" class="post-score" title="current number of votes">0</div><span id="post-42387-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's how switches work - they only give you what you need, and that also means you can only record traffic that is sent to your PC.</p><p>Check this Wiki page for more information on how to get the rest:</p><p><a href="https://wiki.wireshark.org/CaptureSetup/Ethernet">https://wiki.wireshark.org/CaptureSetup/Ethernet</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 May '15, 04:28</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-42387" class="comments-container"></div><div id="comment-tools-42387" class="comment-tools"></div><div class="clear"></div><div id="comment-42387-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

