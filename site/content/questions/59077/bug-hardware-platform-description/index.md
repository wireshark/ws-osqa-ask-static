+++
type = "question"
title = "bug hardware platform description"
description = '''Hi, when describing a bug on wireshark bug database, what would &quot;Hardware: x86-x64 Ubuntu&quot; mean? would it mean that the affected OS is Ubuntu only? The tooltip says it is the hardware platform on which the bug was observe, but does &quot;observe&quot; mean that Ubuntu (as in this case) is the only platform af...'''
date = "2017-01-26T02:58:00Z"
lastmod = "2017-01-26T04:42:00Z"
weight = 59077
keywords = [ "bug" ]
aliases = [ "/questions/59077" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [bug hardware platform description](/questions/59077/bug-hardware-platform-description)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59077-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59077-score" class="post-score" title="current number of votes">0</div><span id="post-59077-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, when describing a bug on wireshark bug database, what would "Hardware: x86-x64 Ubuntu" mean? would it mean that the affected OS is Ubuntu only? The tooltip says it is the hardware platform on which the bug was observe, but does "observe" mean that Ubuntu (as in this case) is the only platform affected? that the issue does not affect other platforms?</p><p>thanks</p><p>OS</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bug" rel="tag" title="see questions tagged &#39;bug&#39;">bug</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jan '17, 02:58</strong></p><img src="https://secure.gravatar.com/avatar/a425221df9fbd0d4139091944f943e9c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="osariel&#39;s gravatar image" /><p><span>osariel</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="osariel has no accepted answers">0%</span></p></div></div><div id="comments-container-59077" class="comments-container"></div><div id="comment-tools-59077" class="comment-tools"></div><div class="clear"></div><div id="comment-59077-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59078"></span>

<div id="answer-container-59078" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59078-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59078-score" class="post-score" title="current number of votes">2</div><span id="post-59078-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It means that the one who reported the bug encountered it on that hardware platform, so the bug should be reproducible on that platform at least. It most likely also affects other platforms, but if a developer can't reproduce it on his own platform, the bug report helps to check if it's maybe a case where only one platform is affected.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jan '17, 04:42</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-59078" class="comments-container"></div><div id="comment-tools-59078" class="comment-tools"></div><div class="clear"></div><div id="comment-59078-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

