+++
type = "question"
title = "How to convert an AirMagnet Pro multi channel .amm file into .pcap?"
description = '''Does anyone has such a converting tool? With AirMagnet Pro 10.7 it is not possible to save the trace in .pcap format.'''
date = "2017-06-09T03:05:00Z"
lastmod = "2017-06-09T19:03:00Z"
weight = 61896
keywords = [ "convert", "airmagnet" ]
aliases = [ "/questions/61896" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to convert an AirMagnet Pro multi channel .amm file into .pcap?](/questions/61896/how-to-convert-an-airmagnet-pro-multi-channel-amm-file-into-pcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61896-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61896-score" class="post-score" title="current number of votes">0</div><span id="post-61896-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does anyone has such a converting tool? With AirMagnet Pro 10.7 it is not possible to save the trace in .pcap format.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-convert" rel="tag" title="see questions tagged &#39;convert&#39;">convert</span> <span class="post-tag tag-link-airmagnet" rel="tag" title="see questions tagged &#39;airmagnet&#39;">airmagnet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jun '17, 03:05</strong></p><img src="https://secure.gravatar.com/avatar/20e72efc48167d9b8f8bc232bd64d95e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="R_H_O&#39;s gravatar image" /><p><span>R_H_O</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="R_H_O has no accepted answers">0%</span></p></div></div><div id="comments-container-61896" class="comments-container"></div><div id="comment-tools-61896" class="comment-tools"></div><div class="clear"></div><div id="comment-61896-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61924"></span>

<div id="answer-container-61924" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61924-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61924-score" class="post-score" title="current number of votes">1</div><span id="post-61924-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you can provide us with one or more .amm files, <em>and</em> with AirMagnet's dissection of them, we <em>might</em> be able to reverse-engineer the format and allow Wireshark to read the files.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Jun '17, 19:03</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-61924" class="comments-container"></div><div id="comment-tools-61924" class="comment-tools"></div><div class="clear"></div><div id="comment-61924-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

