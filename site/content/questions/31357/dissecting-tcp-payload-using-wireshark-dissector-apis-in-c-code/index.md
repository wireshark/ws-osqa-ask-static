+++
type = "question"
title = "[closed] dissecting tcp payload using wireshark dissector APIs in C code"
description = '''hello all, i have just the tcp payload data,i know the protocol message in the payload. what i am trying to do is calling dissector API on this packet,in my c code, expecting that it would form the proto tree that i will use in print_proto_tree function to get payload details. i have populated the a...'''
date = "2014-04-01T11:50:00Z"
lastmod = "2014-04-04T08:18:00Z"
weight = 31357
keywords = [ "libwireshark", "dissector", "programming", "diameter" ]
aliases = [ "/questions/31357" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] dissecting tcp payload using wireshark dissector APIs in C code](/questions/31357/dissecting-tcp-payload-using-wireshark-dissector-apis-in-c-code)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31357-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31357-score" class="post-score" title="current number of votes">0</div><span id="post-31357-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello all,</p><p>i have just the tcp payload data,i know the protocol message in the payload. what i am trying to do is calling dissector API on this packet,in my c code, expecting that it would form the proto tree that i will use in print_proto_tree function to get payload details.</p><p>i have populated the argument with required values.(except the eth,ip, related values)</p><p>but everytime i am getting this segmentation fault.</p><p>is this the correct approach.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-libwireshark" rel="tag" title="see questions tagged &#39;libwireshark&#39;">libwireshark</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-programming" rel="tag" title="see questions tagged &#39;programming&#39;">programming</span> <span class="post-tag tag-link-diameter" rel="tag" title="see questions tagged &#39;diameter&#39;">diameter</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Apr '14, 11:50</strong></p><img src="https://secure.gravatar.com/avatar/425d250364423a7595a3eb9dea779cb2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sanny_D&#39;s gravatar image" /><p><span>Sanny_D</span><br />
<span class="score" title="0 reputation points">0</span><span title="18 badges"><span class="badge1">●</span><span class="badgecount">18</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="21 badges"><span class="bronze">●</span><span class="badgecount">21</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sanny_D has 3 accepted answers">50%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Apr '14, 08:23</strong> </span></p></div></div><div id="comments-container-31357" class="comments-container"><span id="31512"></span><div id="comment-31512" class="comment"><div id="post-31512-score" class="comment-score"></div><div class="comment-text"><p>needed to populate correctly. it is dissecting properly.</p></div><div id="comment-31512-info" class="comment-info"><span class="comment-age">(04 Apr '14, 08:18)</span> <span class="comment-user userinfo">Sanny_D</span></div></div></div><div id="comment-tools-31357" class="comment-tools"></div><div class="clear"></div><div id="comment-31357-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question: You have already asked a very similar question and received answers. If you are not happy with those answers, please continue to ask questions in your original request: http://ask.wireshark.org/questions/31149/how-to-dissect-bytearray-of-tcp-payload" by Kurt Knochner 02 Apr '14, 13:30

</div>

</div>

</div>

