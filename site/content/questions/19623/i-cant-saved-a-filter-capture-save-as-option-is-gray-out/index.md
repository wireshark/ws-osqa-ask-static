+++
type = "question"
title = "I can&#x27;t saved a filter capture; save as option is gray out?"
description = '''Hello  I&#x27;m having troubleshoot saving this capture, I applied the filter wlan.addr == 30:F7:0D:4C:FB:9F and then save as to save only the displayed packets, but the option is gray out. I tried editcap, but I wasn&#x27;t able to save it either. Any help is very much appreciated.  Thanks. '''
date = "2013-03-18T12:35:00Z"
lastmod = "2013-03-18T13:14:00Z"
weight = 19623
keywords = [ "filesave", "capture-filter" ]
aliases = [ "/questions/19623" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [I can't saved a filter capture; save as option is gray out?](/questions/19623/i-cant-saved-a-filter-capture-save-as-option-is-gray-out)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19623-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19623-score" class="post-score" title="current number of votes">0</div><span id="post-19623-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>I'm having troubleshoot saving this capture, I applied the filter wlan.addr == 30:F7:0D:4C:FB:9F and then save as to save only the displayed packets, but the option is gray out. I tried editcap, but I wasn't able to save it either.</p><p>Any help is very much appreciated.</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filesave" rel="tag" title="see questions tagged &#39;filesave&#39;">filesave</span> <span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Mar '13, 12:35</strong></p><img src="https://secure.gravatar.com/avatar/3ec9add3f6ceb792756b1227b1266919?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wil1323&#39;s gravatar image" /><p><span>wil1323</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wil1323 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Mar '13, 12:41</strong> </span></p></div></div><div id="comments-container-19623" class="comments-container"></div><div id="comment-tools-19623" class="comment-tools"></div><div class="clear"></div><div id="comment-19623-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19624"></span>

<div id="answer-container-19624" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19624-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19624-score" class="post-score" title="current number of votes">3</div><span id="post-19624-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><pre><code>Go to 
• File
• Export Specified Packets...
• check if &quot;Displayed&quot; is selected
• hit Save</code></pre><p>You can find more information in the <a href="http://www.wireshark.org/docs/relnotes/wireshark-1.8.0.html">Wireshark 1.8.0 Release Notes</a> and the <a href="http://www.wireshark.org/lists/wireshark-users/201211/msg00029.html">Wireshark User's List</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Mar '13, 13:14</strong></p><img src="https://secure.gravatar.com/avatar/fac200552b0c24be2bc93a740bd54d0d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joke&#39;s gravatar image" /><p><span>joke</span><br />
<span class="score" title="1278 reputation points"><span>1.3k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joke has 6 accepted answers">9%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Mar '13, 13:36</strong> </span></p></div></div><div id="comments-container-19624" class="comments-container"></div><div id="comment-tools-19624" class="comment-tools"></div><div class="clear"></div><div id="comment-19624-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

