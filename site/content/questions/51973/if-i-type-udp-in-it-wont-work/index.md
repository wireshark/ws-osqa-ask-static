+++
type = "question"
title = "if i type UDP in it won&#x27;t work??"
description = '''I&quot;m really confuse with this... Once again I&#x27;m new so please don&#x27;t judge me or anything, thank you'''
date = "2016-04-26T15:08:00Z"
lastmod = "2016-04-27T02:12:00Z"
weight = 51973
keywords = [ "udp", "stream" ]
aliases = [ "/questions/51973" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [if i type UDP in it won't work??](/questions/51973/if-i-type-udp-in-it-wont-work)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51973-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51973-score" class="post-score" title="current number of votes">0</div><span id="post-51973-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I"m really confuse with this... Once again I'm new so <em>please don't judge me or anything,</em> thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Apr '16, 15:08</strong></p><img src="https://secure.gravatar.com/avatar/15b318b7f54d8731b02306b376dc9188?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Serenity%20Adams&#39;s gravatar image" /><p><span>Serenity Adams</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Serenity Adams has no accepted answers">0%</span></p></div></div><div id="comments-container-51973" class="comments-container"><span id="51977"></span><div id="comment-51977" class="comment"><div id="post-51977-score" class="comment-score"></div><div class="comment-text"><p>If you type <strong>UDP</strong> in <em>what</em> exactly?</p></div><div id="comment-51977-info" class="comment-info"><span class="comment-age">(26 Apr '16, 16:30)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="51989"></span><div id="comment-51989" class="comment"><div id="post-51989-score" class="comment-score"></div><div class="comment-text"><p>Agree with cmaynard you should specify where tyou put it</p></div><div id="comment-51989-info" class="comment-info"><span class="comment-age">(27 Apr '16, 02:12)</span> <span class="comment-user userinfo">AlexAu</span></div></div></div><div id="comment-tools-51973" class="comment-tools"></div><div class="clear"></div><div id="comment-51973-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51974"></span>

<div id="answer-container-51974" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51974-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51974-score" class="post-score" title="current number of votes">0</div><span id="post-51974-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try lowercase <strong>udp</strong> instead of <strong>UDP</strong>!</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Apr '16, 15:15</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-51974" class="comments-container"><span id="51975"></span><div id="comment-51975" class="comment"><div id="post-51975-score" class="comment-score"></div><div class="comment-text"><p>I've tried that, it still won't work.</p></div><div id="comment-51975-info" class="comment-info"><span class="comment-age">(26 Apr '16, 15:44)</span> <span class="comment-user userinfo">Serenity Adams</span></div></div><span id="51985"></span><div id="comment-51985" class="comment"><div id="post-51985-score" class="comment-score"></div><div class="comment-text"><p><span>@Serenity Adams</span>, please provide the context (your goal and circumstances) when asking Questions. It will significantly raise your chances to get an answer.</p><p>In this particular case, the information I am missing is whether you enter <code>udp</code> as a capture filter before capturing, as a display filter when the capture is running or already taken or loaded from a file, ...</p></div><div id="comment-51985-info" class="comment-info"><span class="comment-age">(26 Apr '16, 23:56)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-51974" class="comment-tools"></div><div class="clear"></div><div id="comment-51974-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

