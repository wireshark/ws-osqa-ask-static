+++
type = "question"
title = "How to capture picassa video link"
description = '''Hi all, Im trying to use wireshark to capture and reveal a link in this video but in the past couple day i not able to find it. If someone could help i would really appricate. Here is the video link: http://phimtt.com/xem-phim/nu-trang-tai-danh-word-twisters-adventures-2008 The format im looking for...'''
date = "2013-08-27T14:21:00Z"
lastmod = "2013-08-29T03:55:00Z"
weight = 24114
keywords = [ "picasaweb", "decode" ]
aliases = [ "/questions/24114" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture picassa video link](/questions/24114/how-to-capture-picassa-video-link)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24114-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24114-score" class="post-score" title="current number of votes">0</div><span id="post-24114-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>Im trying to use wireshark to capture and reveal a link in this video but in the past couple day i not able to find it. If someone could help i would really appricate.</p><p>Here is the video link:</p><p><a href="http://phimtt.com/xem-phim/nu-trang-tai-danh-word-twisters-adventures-2008">http://phimtt.com/xem-phim/nu-trang-tai-danh-word-twisters-adventures-2008</a></p><p>The format im looking for are:</p><p><a href="https://picasaweb.google.com/lh/photo/oLb1wikeW75BUCYthty5JGdsRkDU9D-GZvm2P4XELMM">https://picasaweb.google.com/lh/photo/oLb1wikeW75BUCYthty5JGdsRkDU9D-GZvm2P4XELMM</a> <a href="https://picasaweb.google.com/lh/photo/oLb1wikeW75BUCYthty5JGdsRkDU9D-GZvm2P4XELMM?start=15https://picasaweb.google.com/106164294256489669464/Image?authkey=Gv1sRgCKm7hsnPk5aRZQ#5657122599477586178">https://picasaweb.google.com/lh/photo/oLb1wikeW75BUCYthty5JGdsRkDU9D-GZvm2P4XELMM?start=15https://picasaweb.google.com/106164294256489669464/Image?authkey=Gv1sRgCKm7hsnPk5aRZQ#5657122599477586178</a> <a href="https://picasaweb.google.com/lh/photo/EXbYzXpUnlELm0FHsEcmbE9c4LVWd6J4g-EJTl_M9Bo">https://picasaweb.google.com/lh/photo/EXbYzXpUnlELm0FHsEcmbE9c4LVWd6J4g-EJTl_M9Bo</a></p><p>The video is host on picasaweb.google.com</p><p>From the video link above: i was able to find this with chrome network tool, but is not valid:</p><p><a href="https://picasaweb.google.com/lh/photo/sUWS1rSBmbd5opCoiZ7pJ5AXDnHPiDqJi8iGjh-qmp4nU7TeaO8OKjTI9ctVHJyhkqVUdmibvSJeXTAMcOv2nC2VgAA8r-uPGh1Rj55HDDKWZZnRZ27mYsELo8UdTpTt">https://picasaweb.google.com/lh/photo/sUWS1rSBmbd5opCoiZ7pJ5AXDnHPiDqJi8iGjh-qmp4nU7TeaO8OKjTI9ctVHJyhkqVUdmibvSJeXTAMcOv2nC2VgAA8r-uPGh1Rj55HDDKWZZnRZ27mYsELo8UdTpTt</a></p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-picasaweb" rel="tag" title="see questions tagged &#39;picasaweb&#39;">picasaweb</span> <span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Aug '13, 14:21</strong></p><img src="https://secure.gravatar.com/avatar/1f40080225cfa65cf2f5b86da62a0328?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tdx3000&#39;s gravatar image" /><p><span>tdx3000</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tdx3000 has no accepted answers">0%</span></p></div></div><div id="comments-container-24114" class="comments-container"></div><div id="comment-tools-24114" class="comment-tools"></div><div class="clear"></div><div id="comment-24114-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="24124"></span>

<div id="answer-container-24124" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24124-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24124-score" class="post-score" title="current number of votes">0</div><span id="post-24124-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The video link is this:</p><blockquote><p><a href="http://r19---sn-4g57lnlz.googlevideo.com/videoplayback?id=8c6602ad47600920&amp;itag=34&amp;source=picasa&amp;ip=88.217.140.185&amp;ipbits=0&amp;expire=1380286154&amp;sparams=expire,id,ip,ipbits,itag,source&amp;signature=18D167E4C9F5D16823AC59842FD47526C5896354.7FC5177924498400A4E041F5BBEA4E0FB4A8A413&amp;key=cms1&amp;begin=0&amp;cm2=0&amp;cms_redirect=yes&amp;ms=nxu&amp;mt=1377694101&amp;mv=m">http://r19---sn-4g57lnlz.googlevideo.com/videoplayback?id=8c6602ad47600920&amp;itag=34&amp;source=picasa&amp;ip=88.217.140.185&amp;ipbits=0&amp;expire=1380286154&amp;sparams=expire,id,ip,ipbits,itag,source&amp;signature=18D167E4C9F5D16823AC59842FD47526C5896354.7FC5177924498400A4E041F5BBEA4E0FB4A8A413&amp;key=cms1&amp;begin=0&amp;cm2=0&amp;cms_redirect=yes&amp;ms=nxu&amp;mt=1377694101&amp;mv=m</a></p></blockquote><p>If you click on it, it will at least offer the download of a 220 MByte flash file. I'm not sure how long this link will be valid, as most of the video hosters tend to generate dynamic URLs that are only valid for defined period of time.</p><p>Here is how I found the link:</p><blockquote><p>Statistics -&gt; Conversations -&gt; TCP</p></blockquote><p>Pick the conversation with the most bytes (after the movie is playing for a couple of seconds), as that is most certainly the video, <strong>if</strong> there was no other download in parallel. Then click "Follow Stream" and extract the URL from the HTTP request header.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Aug '13, 05:54</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-24124" class="comments-container"><span id="24155"></span><div id="comment-24155" class="comment"><div id="post-24155-score" class="comment-score"></div><div class="comment-text"><p>Hello,</p><p>Thank you helping.</p><p>The link you got is the genereate link i can get that with in chrome network tool no problem.</p><p>The link im looking to find is picasaweb.google.com/lh/photo/xxxxxx</p><p>This seem to be the share link <a href="https://picasaweb.google.com/lh/photo/sUWS1rSBmbd5opCoiZ7pJ5AXDnHPiDqJi8iGjh-qmp4nU7TeaO8OKjTI9ctVHJyhkqVUdmibvSJeXTAMcOv2nC2VgAA8r-uPGh1Rj55HDDKWZZnRZ27mYsELo8UdTpTt">https://picasaweb.google.com/lh/photo/sUWS1rSBmbd5opCoiZ7pJ5AXDnHPiDqJi8iGjh-qmp4nU7TeaO8OKjTI9ctVHJyhkqVUdmibvSJeXTAMcOv2nC2VgAA8r-uPGh1Rj55HDDKWZZnRZ27mYsELo8UdTpTt</a></p><p>But it too long to be a valid share link, a valid share link should be like one of 3 below</p><p><a href="https://picasaweb.google.com/lh/photo/oLb1wikeW75BUCYthty5JGdsRkDU9D-GZvm2P4XELMM">https://picasaweb.google.com/lh/photo/oLb1wikeW75BUCYthty5JGdsRkDU9D-GZvm2P4XELMM</a> <a href="https://picasaweb.google.com/lh/photo/oLb1wikeW75BUCYthty5JGdsRkDU9D-GZvm2P4XELMM?start=15https://picasaweb.google.com/106164294256489669464/Image?authkey=Gv1sRgCKm7hsnPk5aRZQ#5657122599477586178">https://picasaweb.google.com/lh/photo/oLb1wikeW75BUCYthty5JGdsRkDU9D-GZvm2P4XELMM?start=15https://picasaweb.google.com/106164294256489669464/Image?authkey=Gv1sRgCKm7hsnPk5aRZQ#5657122599477586178</a></p><p>sUWS1rSBmbd5opCoiZ7pJ5AXDnHPiDqJi8iGjh-qmp4nU7TeaO8OKjTI9ctVHJyhkqVUdmibvSJeXTAMcOv2nC2VgAA8r-uPGh1Rj55HDDKWZZnRZ27mYsELo8UdTpTt I wonder if this part is encoded or encrypt it.</p><p>On the server they use a proxy to get the link from database and pass it to the player. The player much decrypt it and request with picasaweb to generate the video link.</p><p>If that is the case then the truth link im looking should be somewhere in the package that must be capture by wireshark ?</p></div><div id="comment-24155-info" class="comment-info"><span class="comment-age">(28 Aug '13, 18:12)</span> <span class="comment-user userinfo">tdx3000</span></div></div><span id="24159"></span><div id="comment-24159" class="comment"><div id="post-24159-score" class="comment-score"></div><div class="comment-text"><blockquote><p>The link im looking to find is picasaweb.google.com/lh/photo/xxxxxx</p></blockquote><p>I don't think there is a way to get the 'reverese' picasaweb link, as that is most certainly only stored in the databases of google.</p><p>At least you cannot get that information by looking at capture files while the video plays.</p><p>I suggest to ask this question in a google picasa forum or ask the guy how posted that page on <a href="http://phimtt.com">http://phimtt.com</a> where he got the video from.</p></div><div id="comment-24159-info" class="comment-info"><span class="comment-age">(29 Aug '13, 03:55)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-24124" class="comment-tools"></div><div class="clear"></div><div id="comment-24124-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

