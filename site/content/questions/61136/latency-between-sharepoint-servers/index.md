+++
type = "question"
title = "latency between Sharepoint servers."
description = '''Hi All, Hope doing well. We need to detect the source of delays in a farm sharepoint. Communication is very slow from a front-end server to a database server. The two severs are separated by three zones and by firewalls. How can I know that the slowness is at the firewall or application level.'''
date = "2017-05-01T04:14:00Z"
lastmod = "2017-05-02T03:36:00Z"
weight = 61136
keywords = [ "hight" ]
aliases = [ "/questions/61136" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [latency between Sharepoint servers.](/questions/61136/latency-between-sharepoint-servers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61136-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61136-score" class="post-score" title="current number of votes">0</div><span id="post-61136-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All, Hope doing well.</p><p>We need to detect the source of delays in a farm sharepoint. Communication is very slow from a front-end server to a database server. The two severs are separated by three zones and by firewalls. How can I know that the slowness is at the firewall or application level.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hight" rel="tag" title="see questions tagged &#39;hight&#39;">hight</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 May '17, 04:14</strong></p><img src="https://secure.gravatar.com/avatar/8ac552808f07d0ee0076a9b20a907953?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="happyman&#39;s gravatar image" /><p><span>happyman</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="happyman has no accepted answers">0%</span></p></div></div><div id="comments-container-61136" class="comments-container"><span id="61152"></span><div id="comment-61152" class="comment"><div id="post-61152-score" class="comment-score"></div><div class="comment-text"><p>Hi guys, any update, i need help please.</p></div><div id="comment-61152-info" class="comment-info"><span class="comment-age">(02 May '17, 01:55)</span> <span class="comment-user userinfo">happyman</span></div></div><span id="61154"></span><div id="comment-61154" class="comment"><div id="post-61154-score" class="comment-score"></div><div class="comment-text"><p>Your "answer" has been converted to a comment as that's how this site works. Please read the FAQ for more information.</p></div><div id="comment-61154-info" class="comment-info"><span class="comment-age">(02 May '17, 03:36)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-61136" class="comment-tools"></div><div class="clear"></div><div id="comment-61136-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

