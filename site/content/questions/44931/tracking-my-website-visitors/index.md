+++
type = "question"
title = "Tracking my website visitors"
description = '''So, my girlfriend has website through Godaddy.com. There is a contact portion of the website that allows individuals to send a message that goes directly to her email. Somebody has started harassing her via this contact page. Is there a way to use Wireshark or anything else to find what the MAC addr...'''
date = "2015-08-08T13:13:00Z"
lastmod = "2015-08-08T15:14:00Z"
weight = 44931
keywords = [ "website" ]
aliases = [ "/questions/44931" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Tracking my website visitors](/questions/44931/tracking-my-website-visitors)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44931-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44931-score" class="post-score" title="current number of votes">0</div><span id="post-44931-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>So, my girlfriend has website through Godaddy.com. There is a contact portion of the website that allows individuals to send a message that goes directly to her email. Somebody has started harassing her via this contact page. Is there a way to use Wireshark or anything else to find what the MAC address is or even what the IP address is? Please help. Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-website" rel="tag" title="see questions tagged &#39;website&#39;">website</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Aug '15, 13:13</strong></p><img src="https://secure.gravatar.com/avatar/026648c9f00b9a71b371bec8bde0654a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="stockfball11&#39;s gravatar image" /><p><span>stockfball11</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="stockfball11 has no accepted answers">0%</span></p></div></div><div id="comments-container-44931" class="comments-container"></div><div id="comment-tools-44931" class="comment-tools"></div><div class="clear"></div><div id="comment-44931-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="44932"></span>

<div id="answer-container-44932" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44932-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44932-score" class="post-score" title="current number of votes">0</div><span id="post-44932-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>So, my girlfriend has website through Godaddy.com.</p></blockquote><p>I'm assuming that means that Godaddy not only provides the domain name but also provides the hosting - i.e., the Web server runs on a server machine or machines at a Godaddy site. If so, then:</p><blockquote><p>Is there a way to use Wireshark or anything else to find what the MAC address is or even what the IP address is?</p></blockquote><p>...at least for Wireshark or other packet sniffers, the answer is "not from your machine or hers" - the traffic is between the harasser and Godaddy, and you don't have access to it from your machines.</p><p>You should ask Godaddy whether they provide any form of logging of the IP addresses from which contact messages are posted. (The MAC address is unavailable; it's only available to the machine to which the harasser's computer immediately sends the packet, which is probably a cable modem or DSL modem for a home user and a corporate router for a corporate user.) Given the IP address, it should be possible to find out to what organization that IP address belongs, and given the IP address and the time the contact message was posted, that organization may be able to say to which user that IP address was assigned at that time.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Aug '15, 15:14</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-44932" class="comments-container"></div><div id="comment-tools-44932" class="comment-tools"></div><div class="clear"></div><div id="comment-44932-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

