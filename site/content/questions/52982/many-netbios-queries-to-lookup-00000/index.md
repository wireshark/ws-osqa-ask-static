+++
type = "question"
title = "Many netbios queries to lookup 0.0.0.0/0"
description = '''Hello, I have many nbns queries in my network from all hosts looking up for 0.0.0.0/0 here is a sample capture : https://www.cloudshark.org/captures/c268dfd3e600  Any ideas on what&#x27;s the origin ? The process sending this lookups is windows system process (pid 4)'''
date = "2016-05-27T00:39:00Z"
lastmod = "2016-05-27T02:25:00Z"
weight = 52982
keywords = [ "broadcast", "query", "name", "netbios" ]
aliases = [ "/questions/52982" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Many netbios queries to lookup 0.0.0.0/0](/questions/52982/many-netbios-queries-to-lookup-00000)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52982-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52982-score" class="post-score" title="current number of votes">0</div><span id="post-52982-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I have many nbns queries in my network from all hosts looking up for 0.0.0.0/0</p><p>here is a sample capture : <a href="https://www.cloudshark.org/captures/c268dfd3e600">https://www.cloudshark.org/captures/c268dfd3e600</a> <img src="http://img15.hostingpics.net/pics/415956nbns.png" alt="alt text" /></p><p>Any ideas on what's the origin ? The process sending this lookups is windows system process (pid 4)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-broadcast" rel="tag" title="see questions tagged &#39;broadcast&#39;">broadcast</span> <span class="post-tag tag-link-query" rel="tag" title="see questions tagged &#39;query&#39;">query</span> <span class="post-tag tag-link-name" rel="tag" title="see questions tagged &#39;name&#39;">name</span> <span class="post-tag tag-link-netbios" rel="tag" title="see questions tagged &#39;netbios&#39;">netbios</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 May '16, 00:39</strong></p><img src="https://secure.gravatar.com/avatar/edcf9e32163ee8688ce7aee4a65c4020?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nicodonald&#39;s gravatar image" /><p><span>Nicodonald</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nicodonald has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 May '16, 02:41</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-52982" class="comments-container"><span id="52983"></span><div id="comment-52983" class="comment"><div id="post-52983-score" class="comment-score"></div><div class="comment-text"><p>Not really an Ask Wireshark question as Wireshark can't tell you anything about processes. I'll leave it open though as someone might have seen it before. You might get more help on a Windows networking forum.</p></div><div id="comment-52983-info" class="comment-info"><span class="comment-age">(27 May '16, 02:25)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-52982" class="comment-tools"></div><div class="clear"></div><div id="comment-52982-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

