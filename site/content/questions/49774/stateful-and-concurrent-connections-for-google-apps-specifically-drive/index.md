+++
type = "question"
title = "Stateful and Concurrent Connections for Google Apps (specifically Drive)"
description = '''Hello, I&#x27;m fairly new to the networking scene so hopefully my questions makes sense. I have a web application that displays content from Google Drive. I am interested in understanding two primary things: 1) How do I measure / determine stateful connections from my app &amp;amp; Drive? 2) How do I measur...'''
date = "2016-02-03T07:05:00Z"
lastmod = "2016-02-03T07:05:00Z"
weight = 49774
keywords = [ "connections", "concurrent", "stateful", "googledrive", "drive" ]
aliases = [ "/questions/49774" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Stateful and Concurrent Connections for Google Apps (specifically Drive)](/questions/49774/stateful-and-concurrent-connections-for-google-apps-specifically-drive)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49774-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49774-score" class="post-score" title="current number of votes">0</div><span id="post-49774-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I'm fairly new to the networking scene so hopefully my questions makes sense. I have a web application that displays content from Google Drive. I am interested in understanding two primary things:</p><p>1) How do I measure / determine stateful connections from my app &amp; Drive?</p><p>2) How do I measure / determine the number of concurrent connections when my app is in use?</p><ul><li>More specifically, I hope to find out how Wireshark can help me get this data. If Wireshark is not capable, what's my best alternative?</li></ul><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-connections" rel="tag" title="see questions tagged &#39;connections&#39;">connections</span> <span class="post-tag tag-link-concurrent" rel="tag" title="see questions tagged &#39;concurrent&#39;">concurrent</span> <span class="post-tag tag-link-stateful" rel="tag" title="see questions tagged &#39;stateful&#39;">stateful</span> <span class="post-tag tag-link-googledrive" rel="tag" title="see questions tagged &#39;googledrive&#39;">googledrive</span> <span class="post-tag tag-link-drive" rel="tag" title="see questions tagged &#39;drive&#39;">drive</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Feb '16, 07:05</strong></p><img src="https://secure.gravatar.com/avatar/b43869de3dac51938f6d6f07c10e9d06?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="BenB16&#39;s gravatar image" /><p><span>BenB16</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="BenB16 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Jul '16, 15:42</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-49774" class="comments-container"></div><div id="comment-tools-49774" class="comment-tools"></div><div class="clear"></div><div id="comment-49774-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

