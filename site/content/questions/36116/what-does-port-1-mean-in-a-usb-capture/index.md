+++
type = "question"
title = "What does [Port 1] mean in a USB capture?"
description = '''Hi there, so given the following part of a Wireshark packet, taken from a USB capture on Linux while pluging in a USB drive: 1 0.000000000 host 1.0 USBHUB 64 GET_STATUS Request [Port 1]  Frame 1: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0 Endpoint: 0x80, Direction: IN  ...'''
date = "2014-09-09T10:55:00Z"
lastmod = "2014-09-09T10:55:00Z"
weight = 36116
keywords = [ "wireshark", "usb", "usbmon", "linux" ]
aliases = [ "/questions/36116" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [What does \[Port 1\] mean in a USB capture?](/questions/36116/what-does-port-1-mean-in-a-usb-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36116-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36116-score" class="post-score" title="current number of votes">0</div><span id="post-36116-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi there,</p><p>so given the following part of a Wireshark packet, taken from a USB capture on Linux while pluging in a USB drive:</p><pre><code>1   0.000000000 host    1.0 USBHUB  64 GET_STATUS Request     [Port 1] 
Frame 1: 64 bytes on wire (512 bits), 64 bytes captured (512 bits) on interface 0 Endpoint: 0x80, Direction: IN</code></pre><p>What does "Port 1" mean? Does it indicate the physical port number? Since it does not change, when changing the port, this is unlikely...but what does it mean then?</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-usb" rel="tag" title="see questions tagged &#39;usb&#39;">usb</span> <span class="post-tag tag-link-usbmon" rel="tag" title="see questions tagged &#39;usbmon&#39;">usbmon</span> <span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Sep '14, 10:55</strong></p><img src="https://secure.gravatar.com/avatar/ed42d94d476f543682fea8aab051d515?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rolf&#39;s gravatar image" /><p><span>Rolf</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rolf has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Sep '14, 14:01</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-36116" class="comments-container"></div><div id="comment-tools-36116" class="comment-tools"></div><div class="clear"></div><div id="comment-36116-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

