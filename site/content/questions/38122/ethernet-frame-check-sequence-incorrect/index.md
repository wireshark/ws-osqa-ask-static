+++
type = "question"
title = "Ethernet Frame check sequence incorrect"
description = '''Hello everyone. A few weeks ago we bought a HP V1910-24G switch - and now we tried to centralize all priority connections on it. The problem is, since this last change remote desktop are very slow and gets discconected like if a loop were causing delays. Switch is manageable and has RSTP enabled, we...'''
date = "2014-11-25T07:46:00Z"
lastmod = "2014-11-25T07:46:00Z"
weight = 38122
keywords = [ "frame", "stp", "tcpflags" ]
aliases = [ "/questions/38122" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Ethernet Frame check sequence incorrect](/questions/38122/ethernet-frame-check-sequence-incorrect)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38122-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38122-score" class="post-score" title="current number of votes">0</div><span id="post-38122-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello everyone.</p><p>A few weeks ago we bought a HP V1910-24G switch - and now we tried to centralize all priority connections on it.</p><p>The problem is, since this last change remote desktop are very slow and gets discconected like if a loop were causing delays.</p><p>Switch is manageable and has RSTP enabled, we check for possible double wire on the rest of the fast-eth but everything seems to be ok Layer1.</p><p>Can this bad frame be caused by loops? I see all bits of FCS are 1, all flags are like if the frame were corrupt, and the spanning tree proto is acusing something that I don't understand.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/networka.jpg" alt="alt text" /></p><p>Switches A, B, C, D are not manageable. Just for extension. And mikrotik has RSTP enabled working as a bridge.</p><p>Can someone please help me analize this?</p><p>Link with wireshark fragment: <a href="https://onedrive.live.com/redir?resid=EB8881F9E3D4CA5A%21112">https://onedrive.live.com/redir?resid=EB8881F9E3D4CA5A%21112</a></p><p>Best regards,</p><p>Miguel</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-frame" rel="tag" title="see questions tagged &#39;frame&#39;">frame</span> <span class="post-tag tag-link-stp" rel="tag" title="see questions tagged &#39;stp&#39;">stp</span> <span class="post-tag tag-link-tcpflags" rel="tag" title="see questions tagged &#39;tcpflags&#39;">tcpflags</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Nov '14, 07:46</strong></p><img src="https://secure.gravatar.com/avatar/5aa82180c727c08459a5e20661099e48?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hexulon&#39;s gravatar image" /><p><span>Hexulon</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hexulon has no accepted answers">0%</span></p></img></div></div><div id="comments-container-38122" class="comments-container"></div><div id="comment-tools-38122" class="comment-tools"></div><div class="clear"></div><div id="comment-38122-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

