+++
type = "question"
title = "which MAC-address is sending packets to the MAC of my network-card?"
description = '''Hello Forum I suspect my neighbor, an IT-geek, is mdk3-ing my AP(Beacon Flood Mode)probably with a command like that:  mdk3 wlan0 b -n 02wireless636661 -g -s 40 -t -c 11 While the attack is working I an not able to establish a connection to my wifi network :-( At the beginning it was somehow funny b...'''
date = "2014-04-29T10:31:00Z"
lastmod = "2014-04-29T10:31:00Z"
weight = 32294
keywords = [ "mac-address" ]
aliases = [ "/questions/32294" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [which MAC-address is sending packets to the MAC of my network-card?](/questions/32294/which-mac-address-is-sending-packets-to-the-mac-of-my-network-card)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32294-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32294-score" class="post-score" title="current number of votes">0</div><span id="post-32294-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Forum</p><p>I suspect my neighbor, an IT-geek, is mdk3-ing my AP(Beacon Flood Mode)probably with a command like that:<br />
</p><h1 id="mdk3-wlan0-b--n-02wireless636661--g--s-40--t--c-11">mdk3 wlan0 b -n 02wireless636661 -g -s 40 -t -c 11</h1><p>While the attack is working I an not able to establish a connection to my wifi network :-(</p><p>At the beginning it was somehow funny but not I have to think about possible countermesurments.</p><p>Question: How do I find out with Wireshark which MAC-address is sending packets to the MAC of my network-card?</p><p>Any help is appriciated very much. Thank you!</p><p>Joseph</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac-address" rel="tag" title="see questions tagged &#39;mac-address&#39;">mac-address</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Apr '14, 10:31</strong></p><img src="https://secure.gravatar.com/avatar/c08acf577aad3b14e932ee8f48cf7d20?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joseph123&#39;s gravatar image" /><p><span>joseph123</span><br />
<span class="score" title="11 reputation points">11</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joseph123 has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-32294" class="comments-container"></div><div id="comment-tools-32294" class="comment-tools"></div><div class="clear"></div><div id="comment-32294-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

