+++
type = "question"
title = "Best NIC for PCAP Capture"
description = '''Wireshark Team, what is a reliable, stable, low packet loss Gigabit Copper NIC that is good to use with Wireshark. 1,2,4 port, doesnt matter. Thank you.'''
date = "2014-09-08T08:20:00Z"
lastmod = "2014-09-08T08:20:00Z"
weight = 36077
keywords = [ "pcap" ]
aliases = [ "/questions/36077" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Best NIC for PCAP Capture](/questions/36077/best-nic-for-pcap-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36077-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36077-score" class="post-score" title="current number of votes">0</div><span id="post-36077-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark Team, what is a reliable, stable, low packet loss Gigabit Copper NIC that is good to use with Wireshark. 1,2,4 port, doesnt matter. Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Sep '14, 08:20</strong></p><img src="https://secure.gravatar.com/avatar/19e79b2c96f0c36649e73e79c84c8fc6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bryanbent&#39;s gravatar image" /><p><span>bryanbent</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bryanbent has no accepted answers">0%</span></p></div></div><div id="comments-container-36077" class="comments-container"></div><div id="comment-tools-36077" class="comment-tools"></div><div class="clear"></div><div id="comment-36077-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

