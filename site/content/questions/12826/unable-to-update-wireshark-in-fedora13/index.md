+++
type = "question"
title = "Unable to update wireshark in Fedora13"
description = '''Hello Team I need your help in upgrading the wireshark in my machine which has Fedora 13 installed on it. I tried the following ways and was not able to proceed further. [root@pc2 sbin]# yum update wireshark Loaded plugins: presto, refresh-packagekit Setting up Update Process No Packages marked for ...'''
date = "2012-07-18T06:32:00Z"
lastmod = "2012-07-18T06:50:00Z"
weight = 12826
keywords = [ "fedora13" ]
aliases = [ "/questions/12826" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to update wireshark in Fedora13](/questions/12826/unable-to-update-wireshark-in-fedora13)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12826-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12826-score" class="post-score" title="current number of votes">0</div><span id="post-12826-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Team</p><p>I need your help in upgrading the wireshark in my machine which has Fedora 13 installed on it. I tried the following ways and was not able to proceed further.</p><p>[<span class="__cf_email__" data-cfemail="a3d1ccccd7e3d3c091">[email protected]</span> sbin]# yum update wireshark Loaded plugins: presto, refresh-packagekit Setting up Update Process No Packages marked for Update [<span class="__cf_email__" data-cfemail="93e1fcfce7d3e3f0a1">[email protected]</span> sbin]#</p><p>I am a new user to Fedora. Could you please help/suggest me how to proceed further, as the 1.2.17 version is older an many IE data could not be dissected . Please revert if any further information is required. -- Regards Ranjith</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fedora13" rel="tag" title="see questions tagged &#39;fedora13&#39;">fedora13</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jul '12, 06:32</strong></p><img src="https://secure.gravatar.com/avatar/a0d88bacac764f708025829f75bb929d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ranjith&#39;s gravatar image" /><p><span>Ranjith</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ranjith has no accepted answers">0%</span></p></div></div><div id="comments-container-12826" class="comments-container"></div><div id="comment-tools-12826" class="comment-tools"></div><div class="clear"></div><div id="comment-12826-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12828"></span>

<div id="answer-container-12828" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12828-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12828-score" class="post-score" title="current number of votes">1</div><span id="post-12828-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Fedora13 is pretty old and, as far as I know, no longer supported.</p><p>Among other things I believe this means that the Fedora13 repository is no longer being updated and thus will not have a recent version of Wireshark..</p><p>One way to proceed would be to (install) (update to) a recent supported version of Fedora.</p><p>Another way would be to set up yum on your system to be able to access a current (F16/F17) Fedora repository; this requires some finagling which, as a new Fedora user, you may not want to try.</p><p>(There may be other ways to get a recent version of Wireshark on your Fedora 13 that I'm not familiar with).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jul '12, 06:50</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Jul '12, 06:51</strong> </span></p></div></div><div id="comments-container-12828" class="comments-container"></div><div id="comment-tools-12828" class="comment-tools"></div><div class="clear"></div><div id="comment-12828-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

