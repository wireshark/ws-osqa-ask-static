+++
type = "question"
title = "Bluetooth capturing"
description = '''How do I configure Wireshark to capture Bluetooth traffic?'''
date = "2010-09-24T06:14:00Z"
lastmod = "2013-04-17T01:23:00Z"
weight = 312
keywords = [ "bluetooth" ]
aliases = [ "/questions/312" ]
osqa_answers = 4
osqa_accepted = false
+++

<div class="headNormal">

# [Bluetooth capturing](/questions/312/bluetooth-capturing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-312-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-312-score" class="post-score" title="current number of votes">1</div><span id="post-312-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>How do I configure Wireshark to capture Bluetooth traffic?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bluetooth" rel="tag" title="see questions tagged &#39;bluetooth&#39;">bluetooth</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Sep '10, 06:14</strong></p><img src="https://secure.gravatar.com/avatar/2538bb5c08afc9f09dd6b8fdd64963a3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="julianast&#39;s gravatar image" /><p><span>julianast</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="julianast has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Mar '11, 18:11</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-312" class="comments-container"></div><div id="comment-tools-312" class="comment-tools"></div><div class="clear"></div><div id="comment-312-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

4 Answers:

</div>

</div>

<span id="17709"></span>

<div id="answer-container-17709" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17709-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17709-score" class="post-score" title="current number of votes">1</div><span id="post-17709-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I can capture the bluetooth traffic in ubuntu, as long as you establish a PAN using blueman. Then there will be an interface named pan0 in your wireshark. but it is in ethernet header format</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jan '13, 15:29</strong></p><img src="https://secure.gravatar.com/avatar/5d4b0e49653f79a63f4024c3f377f6bc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="geneopenflow&#39;s gravatar image" /><p><span>geneopenflow</span><br />
<span class="score" title="16 reputation points">16</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="geneopenflow has no accepted answers">0%</span></p></div></div><div id="comments-container-17709" class="comments-container"></div><div id="comment-tools-17709" class="comment-tools"></div><div class="clear"></div><div id="comment-17709-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="3179"></span>

<div id="answer-container-3179" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3179-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3179-score" class="post-score" title="current number of votes">0</div><span id="post-3179-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, Wireshark can not currently capture Bluetooth traffic. See <a href="http://wiki.wireshark.org/CaptureSetup/Bluetooth">http://wiki.wireshark.org/CaptureSetup/Bluetooth</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Mar '11, 11:22</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-3179" class="comments-container"></div><div id="comment-tools-3179" class="comment-tools"></div><div class="clear"></div><div id="comment-3179-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="3186"></span>

<div id="answer-container-3186" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3186-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3186-score" class="post-score" title="current number of votes">0</div><span id="post-3186-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>On what operating system is this? The Wiki page mentioned above is out-of-date - recent versions of libpcap can, on Linux, capture Bluetooth traffic. On other operating systems, capturing on Bluetooth is not supported.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Mar '11, 18:09</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-3186" class="comments-container"><span id="3236"></span><div id="comment-3236" class="comment"><div id="post-3236-score" class="comment-score"></div><div class="comment-text"><p>Guy, would you mind updating the Bluetooth wiki page? Looking at the libpcap CHANGES file, it's not clear to me which version of libpcap first supported capturing Bluetooth on Linux. The release notes for libpcap v0.9.6 indicates, "Add Bluetooth support", but it's unclear if that support was for capturing Bluetooth traffic on Linux or for only reading Bluetooth capture files created from the hcidump utility as currently mentioned on the wiki? I suspect the latter since that wiki page was last updated on 4-12-2008 and 0.9.6 was released on 4-27-2007.</p></div><div id="comment-3236-info" class="comment-info"><span class="comment-age">(30 Mar '11, 14:20)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-3186" class="comment-tools"></div><div class="clear"></div><div id="comment-3186-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="20504"></span>

<div id="answer-container-20504" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20504-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20504-score" class="post-score" title="current number of votes">0</div><span id="post-20504-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Whole Blueooth are now supported. There is also special Bluetooth interface named Bluetooth0 (1, 2, etc.) Also you can sniffing Bluetooth by USB (Bluetooth USB dongle).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Apr '13, 01:23</strong></p><img src="https://secure.gravatar.com/avatar/45a69e9e5c0af55bfef57c8c6b637a95?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Michal%20Labedzki&#39;s gravatar image" /><p><span>Michal Labedzki</span><br />
<span class="score" title="31 reputation points">31</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Michal Labedzki has no accepted answers">0%</span></p></div></div><div id="comments-container-20504" class="comments-container"></div><div id="comment-tools-20504" class="comment-tools"></div><div class="clear"></div><div id="comment-20504-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

