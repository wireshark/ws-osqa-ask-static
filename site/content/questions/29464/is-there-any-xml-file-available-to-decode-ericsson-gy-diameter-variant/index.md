+++
type = "question"
title = "Is there any xml file available to decode Ericsson Gy+ Diameter variant?"
description = '''Hello, Is there any xml file available to decode Ericsson Gy+ Diameter variant? I have some traces but can&#x27;t decode them. Best regards'''
date = "2014-02-05T08:45:00Z"
lastmod = "2016-07-20T05:20:00Z"
weight = 29464
keywords = [ "ericsson", "gy+" ]
aliases = [ "/questions/29464" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Is there any xml file available to decode Ericsson Gy+ Diameter variant?](/questions/29464/is-there-any-xml-file-available-to-decode-ericsson-gy-diameter-variant)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29464-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29464-score" class="post-score" title="current number of votes">0</div><span id="post-29464-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>Is there any xml file available to decode Ericsson Gy+ Diameter variant?</p><p>I have some traces but can't decode them.</p><p>Best regards</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ericsson" rel="tag" title="see questions tagged &#39;ericsson&#39;">ericsson</span> <span class="post-tag tag-link-gy+" rel="tag" title="see questions tagged &#39;gy+&#39;">gy+</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Feb '14, 08:45</strong></p><img src="https://secure.gravatar.com/avatar/2f10cefc49a08b1998cb951e72599793?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lolodub&#39;s gravatar image" /><p><span>lolodub</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lolodub has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Jul '16, 08:06</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-29464" class="comments-container"><span id="54180"></span><div id="comment-54180" class="comment"><div id="post-54180-score" class="comment-score"></div><div class="comment-text"><p>I am also looking for custom dictionaries to read custom AVPs for Gx+, Gy+, ESy developed by Ericsson. Would very much appreciate if you can share what you have learnt so far on this.</p></div><div id="comment-54180-info" class="comment-info"><span class="comment-age">(20 Jul '16, 05:09)</span> <span class="comment-user userinfo">GameOfPackets</span></div></div><span id="54182"></span><div id="comment-54182" class="comment"><div id="post-54182-score" class="comment-score"></div><div class="comment-text"><p>Your answer has been converted to a comment as that's how this site works. Please read the FAQ for more information.</p></div><div id="comment-54182-info" class="comment-info"><span class="comment-age">(20 Jul '16, 05:20)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-29464" class="comment-tools"></div><div class="clear"></div><div id="comment-29464-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="29502"></span>

<div id="answer-container-29502" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29502-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29502-score" class="post-score" title="current number of votes">1</div><span id="post-29502-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>In Wireshark's source code, in the diameter/Ericsson.xml file you'll find all the AVP definitions for Gy+.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Feb '14, 15:00</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div></div><div id="comments-container-29502" class="comments-container"></div><div id="comment-tools-29502" class="comment-tools"></div><div class="clear"></div><div id="comment-29502-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="29465"></span>

<div id="answer-container-29465" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29465-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29465-score" class="post-score" title="current number of votes">0</div><span id="post-29465-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi,</p><p>Gy is diameter based inteface, and it should be readable in CCA/CCR messages between the GGSN and prepaid platform without any encoding, also you may look into the logs on the GGSN messages /var/log.</p><p>BR, Yousef Abssi</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Feb '14, 09:51</strong></p><img src="https://secure.gravatar.com/avatar/23dc781c5a8f34496c3629b0d5dbf258?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yabssi&#39;s gravatar image" /><p><span>yabssi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yabssi has no accepted answers">0%</span></p></div></div><div id="comments-container-29465" class="comments-container"><span id="29481"></span><div id="comment-29481" class="comment"><div id="post-29481-score" class="comment-score"></div><div class="comment-text"><p>Hi Yousef,</p><p>Ericsson Gy+ has vendor specific AVPs that are not dissected by wireshark. This is not that simple.</p><p>Best regards</p></div><div id="comment-29481-info" class="comment-info"><span class="comment-age">(06 Feb '14, 00:51)</span> <span class="comment-user userinfo">lolodub</span></div></div></div><div id="comment-tools-29465" class="comment-tools"></div><div class="clear"></div><div id="comment-29465-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

