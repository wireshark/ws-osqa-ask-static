+++
type = "question"
title = "Wireshark 1.99 Monitor Mode"
description = '''Relatively new to Wireshark, running 1.99.7 on a MacBook Pro (2015), OS X Yosemite 10.10.3. I have no problem with basic capture via wifi, but have been unable to figure out how to turn on monitor mode. I&#x27;ve read the various guides and posts, most of which reference an Edit Interface Settings window...'''
date = "2015-07-14T10:03:00Z"
lastmod = "2015-07-14T12:37:00Z"
weight = 44139
keywords = [ "wireshark1.99", "osx", "monitoring", "monitor", "monitor-mode" ]
aliases = [ "/questions/44139" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark 1.99 Monitor Mode](/questions/44139/wireshark-199-monitor-mode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44139-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44139-score" class="post-score" title="current number of votes">0</div><span id="post-44139-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Relatively new to Wireshark, running 1.99.7 on a MacBook Pro (2015), OS X Yosemite 10.10.3. I have no problem with basic capture via wifi, but have been unable to figure out how to turn on monitor mode.</p><p>I've read the various guides and posts, most of which reference an Edit Interface Settings window that doesn't seem to exist in 1.99. The Capture Options window seems similar, and there is a Capture section in the preferences page, but neither of those have anything regarding monitoring.</p><p>The only thing I've found that has potential is a line under Preferences &gt; Advanced that's called capture.devices_monitor_mode. If it were a Boolean, I would just change it to True, but it's a String, so I'm not sure how to configure the string to show that I want the wifi chip to be in monitor mode. I've tried cutting and pasting the same interface string from the promiscuous mode line into it, but nothing changed.</p><p>Any and all help appreciated!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark1.99" rel="tag" title="see questions tagged &#39;wireshark1.99&#39;">wireshark1.99</span> <span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-monitoring" rel="tag" title="see questions tagged &#39;monitoring&#39;">monitoring</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span> <span class="post-tag tag-link-monitor-mode" rel="tag" title="see questions tagged &#39;monitor-mode&#39;">monitor-mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jul '15, 10:03</strong></p><img src="https://secure.gravatar.com/avatar/ea042666aaf8a153bc872c6c37cc6573?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RganarD&#39;s gravatar image" /><p><span>RganarD</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RganarD has no accepted answers">0%</span></p></div></div><div id="comments-container-44139" class="comments-container"></div><div id="comment-tools-44139" class="comment-tools"></div><div class="clear"></div><div id="comment-44139-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="44149"></span>

<div id="answer-container-44149" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44149-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44149-score" class="post-score" title="current number of votes">0</div><span id="post-44149-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There has been a similar question here: <a href="https://ask.wireshark.org/questions/42897/how-do-i-turn-on-monitor-mode-in-mac-os-x-with-wireshark-v199">how-do-i-turn-on-monitor-mode-in-mac-os-x-with-wireshark-v199</a> I think the answer there, should answer your question.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Jul '15, 12:37</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></div></div><div id="comments-container-44149" class="comments-container"></div><div id="comment-tools-44149" class="comment-tools"></div><div class="clear"></div><div id="comment-44149-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

