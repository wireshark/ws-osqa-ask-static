+++
type = "question"
title = "how do you capture HTTP_VIA response header ?"
description = '''I used wireshark to capture http headers to detect proxy, but I am not able to see HTTP_VIA header ,so can somebody help me how to see it ? Thanks in advance !! '''
date = "2013-07-01T08:29:00Z"
lastmod = "2013-07-01T13:44:00Z"
weight = 22516
keywords = [ "http" ]
aliases = [ "/questions/22516" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how do you capture HTTP\_VIA response header ?](/questions/22516/how-do-you-capture-http_via-response-header)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22516-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22516-score" class="post-score" title="current number of votes">0</div><span id="post-22516-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I used wireshark to capture http headers to detect proxy, but I am not able to see HTTP_VIA header ,so can somebody help me how to see it ? Thanks in advance !!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Jul '13, 08:29</strong></p><img src="https://secure.gravatar.com/avatar/4cd6a0feedf713bad8dda248622df38c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mg12&#39;s gravatar image" /><p><span>mg12</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mg12 has no accepted answers">0%</span></p></div></div><div id="comments-container-22516" class="comments-container"></div><div id="comment-tools-22516" class="comment-tools"></div><div class="clear"></div><div id="comment-22516-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22523"></span>

<div id="answer-container-22523" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22523-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22523-score" class="post-score" title="current number of votes">1</div><span id="post-22523-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If the VIA header is not there, then the request was either not being proxied or the proxy did not add the VIA header (the VIA header is optional).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Jul '13, 13:44</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-22523" class="comments-container"></div><div id="comment-tools-22523" class="comment-tools"></div><div class="clear"></div><div id="comment-22523-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

