+++
type = "question"
title = "How to capture data sent from specific internal ip addresses."
description = '''I only need to capture data from the internal ip address: 192.168.0.13 How can I configure Wireshark to capture only from that internal ip?'''
date = "2012-12-08T12:58:00Z"
lastmod = "2012-12-08T13:22:00Z"
weight = 16726
keywords = [ "internet" ]
aliases = [ "/questions/16726" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture data sent from specific internal ip addresses.](/questions/16726/how-to-capture-data-sent-from-specific-internal-ip-addresses)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16726-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16726-score" class="post-score" title="current number of votes">0</div><span id="post-16726-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I only need to capture data from the internal ip address: 192.168.0.13 How can I configure Wireshark to capture only from that internal ip?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-internet" rel="tag" title="see questions tagged &#39;internet&#39;">internet</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Dec '12, 12:58</strong></p><img src="https://secure.gravatar.com/avatar/8406cc68c4717d9fbaf7e93ffe8af976?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SNLThe_Office&#39;s gravatar image" /><p><span>SNLThe_Office</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SNLThe_Office has no accepted answers">0%</span></p></div></div><div id="comments-container-16726" class="comments-container"></div><div id="comment-tools-16726" class="comment-tools"></div><div class="clear"></div><div id="comment-16726-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16728"></span>

<div id="answer-container-16728" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16728-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16728-score" class="post-score" title="current number of votes">1</div><span id="post-16728-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Go to the capture options dialog and double click on the network card entry of the card you want to capture on. Set a capture filter to "host 192.168.0.13" and you're good to go.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Dec '12, 13:22</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-16728" class="comments-container"></div><div id="comment-tools-16728" class="comment-tools"></div><div class="clear"></div><div id="comment-16728-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

