+++
type = "question"
title = "Is it possible to trace the attacker through a DDoS?"
description = '''I was recently hit offline by multiple IP&#x27;s confirmed but I need to figure out if its possible to trace them to the actual attacker.. There were only 23 IP&#x27;s with TCP connections most ranging in the US, and Singapore, and a few in Germany. Is it possible to trace it to the attacker?'''
date = "2013-06-14T00:16:00Z"
lastmod = "2013-06-14T00:21:00Z"
weight = 22040
keywords = [ "tracinganip" ]
aliases = [ "/questions/22040" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Is it possible to trace the attacker through a DDoS?](/questions/22040/is-it-possible-to-trace-the-attacker-through-a-ddos)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22040-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22040-score" class="post-score" title="current number of votes">0</div><span id="post-22040-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I was recently hit offline by multiple IP's confirmed but I need to figure out if its possible to trace them to the actual attacker.. There were only 23 IP's with TCP connections most ranging in the US, and Singapore, and a few in Germany. Is it possible to trace it to the attacker?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tracinganip" rel="tag" title="see questions tagged &#39;tracinganip&#39;">tracinganip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jun '13, 00:16</strong></p><img src="https://secure.gravatar.com/avatar/96043869d68b82772ff0253ab214e483?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SnailTrail&#39;s gravatar image" /><p><span>SnailTrail</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SnailTrail has no accepted answers">0%</span></p></div></div><div id="comments-container-22040" class="comments-container"></div><div id="comment-tools-22040" class="comment-tools"></div><div class="clear"></div><div id="comment-22040-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22041"></span>

<div id="answer-container-22041" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22041-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22041-score" class="post-score" title="current number of votes">0</div><span id="post-22041-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>In general, no. They are either spoofed packets or sessions from bots (computers that were taken over by exploiting a vulnerability). Either way, the source IP is usually not related to the attacker.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Jun '13, 00:21</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-22041" class="comments-container"></div><div id="comment-tools-22041" class="comment-tools"></div><div class="clear"></div><div id="comment-22041-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

