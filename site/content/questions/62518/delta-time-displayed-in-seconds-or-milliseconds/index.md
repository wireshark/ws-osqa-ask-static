+++
type = "question"
title = "Delta Time displayed in seconds or milliseconds?"
description = '''Hi, I want to make sure I&#x27;m interpreting output correctly. View &#92; Time Display Format is set to &#x27;Time of Day&#x27; and &#x27;milliseconds&#x27; I also have a &#x27;Delta Time&#x27; column with the field set for &#x27;frame.time_delta&#x27; Reviewing the trace, there are &#x27;Delta Times&#x27; showing 0.205284770, 0.206425460 which I interpret...'''
date = "2017-07-05T05:39:00Z"
lastmod = "2017-07-05T09:56:00Z"
weight = 62518
keywords = [ "frame.time.delta" ]
aliases = [ "/questions/62518" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Delta Time displayed in seconds or milliseconds?](/questions/62518/delta-time-displayed-in-seconds-or-milliseconds)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62518-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62518-score" class="post-score" title="current number of votes">0</div><span id="post-62518-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I want to make sure I'm interpreting output correctly.</p><p>View \ Time Display Format is set to 'Time of Day' and 'milliseconds'</p><p>I also have a 'Delta Time' column with the field set for 'frame.time_delta'</p><p>Reviewing the trace, there are 'Delta Times' showing 0.205284770, 0.206425460 which I interpret as 205ms &amp; 206ms.</p><p>There are large entries such as 21.546111330 &amp; 30.960526450 which I interpret as 21,000ms &amp; 30,000ms?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-frame.time.delta" rel="tag" title="see questions tagged &#39;frame.time.delta&#39;">frame.time.delta</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jul '17, 05:39</strong></p><img src="https://secure.gravatar.com/avatar/621f3e207df82fb1251d2b2d86007be4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="drs&#39;s gravatar image" /><p><span>drs</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="drs has no accepted answers">0%</span></p></div></div><div id="comments-container-62518" class="comments-container"></div><div id="comment-tools-62518" class="comment-tools"></div><div class="clear"></div><div id="comment-62518-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="62521"></span>

<div id="answer-container-62521" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62521-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62521-score" class="post-score" title="current number of votes">1</div><span id="post-62521-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The decimal point always remains fixed between seconds and tenths of seconds. So 0.205xxx is 205.xxx milliseconds, and 21.546xxx is 21 seconds and 546.xxx milliseconds.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Jul '17, 07:31</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-62521" class="comments-container"><span id="62537"></span><div id="comment-62537" class="comment"><div id="post-62537-score" class="comment-score"></div><div class="comment-text"><p>Sindy,</p><p>Thank you for confirming.</p></div><div id="comment-62537-info" class="comment-info"><span class="comment-age">(05 Jul '17, 09:38)</span> <span class="comment-user userinfo">drs</span></div></div><span id="62538"></span><div id="comment-62538" class="comment"><div id="post-62538-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-62538-info" class="comment-info"><span class="comment-age">(05 Jul '17, 09:56)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-62521" class="comment-tools"></div><div class="clear"></div><div id="comment-62521-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

