+++
type = "question"
title = "Help analyzing ftp connection problem"
description = '''I have 2 PCs on a network one Win7 192.168.1.64 and the other XP 192.168.1.69. I am running Filezilla server on both. Typing ftp 192.168.69 on the win7 machine is detected by Filezilla on the XP PC, but typing ftp 192.168.1.64 on the XP PC causes no response whatever from filezilla on the win7 PC. M...'''
date = "2013-04-23T15:28:00Z"
lastmod = "2013-04-23T15:33:00Z"
weight = 20751
keywords = [ "ftp" ]
aliases = [ "/questions/20751" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Help analyzing ftp connection problem](/questions/20751/help-analyzing-ftp-connection-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20751-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20751-score" class="post-score" title="current number of votes">0</div><span id="post-20751-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have 2 PCs on a network one Win7 192.168.1.64 and the other XP 192.168.1.69. I am running Filezilla server on both. Typing ftp 192.168.69 on the win7 machine is detected by Filezilla on the XP PC, but typing ftp 192.168.1.64 on the XP PC causes no response whatever from filezilla on the win7 PC. Moreover Wireshark does not appear to detect any packets arriving on the Win 7 PC. I know the XP PC is working fine as I can sync an app from my iphone on it, but I can get no response from the Win7 PC Can you give me any help on how I can find the problem Thanks Wheelyjon.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ftp" rel="tag" title="see questions tagged &#39;ftp&#39;">ftp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Apr '13, 15:28</strong></p><img src="https://secure.gravatar.com/avatar/552eec2c7eb127a4027cddb9fdc4b8d9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Wheelyjon&#39;s gravatar image" /><p><span>Wheelyjon</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Wheelyjon has no accepted answers">0%</span></p></div></div><div id="comments-container-20751" class="comments-container"></div><div id="comment-tools-20751" class="comment-tools"></div><div class="clear"></div><div id="comment-20751-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20752"></span>

<div id="answer-container-20752" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20752-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20752-score" class="post-score" title="current number of votes">0</div><span id="post-20752-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sounds like the windows firewall is in your way. You can add an exception rule or just (temporarily) disable the windows firewall altogether.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Apr '13, 15:33</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-20752" class="comments-container"></div><div id="comment-tools-20752" class="comment-tools"></div><div class="clear"></div><div id="comment-20752-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

