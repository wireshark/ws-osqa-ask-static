+++
type = "question"
title = "Capture Options filter String Error for &quot;unknown&quot; link-type"
description = '''Hi RPCAPD Version: winpcap 4.1.3 Source Code Wireshark Version: 1.12.6 ,Windows 7 64 Bit I am unable to use capture filter in remote capture (Linux Interface on Windows)for only &quot;tcp&quot; packets and analyze. where as the same I am able to do in Wireshark 1.6.1. Request you to help me out, why the lates...'''
date = "2015-06-30T03:36:00Z"
lastmod = "2015-06-30T03:36:00Z"
weight = 43716
keywords = [ "capture-options", "remote-capture", "interfaces", "capture-filter", "wireshark" ]
aliases = [ "/questions/43716" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capture Options filter String Error for "unknown" link-type](/questions/43716/capture-options-filter-string-error-for-unknown-link-type)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43716-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43716-score" class="post-score" title="current number of votes">0</div><span id="post-43716-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>RPCAPD Version: winpcap 4.1.3 Source Code Wireshark Version: 1.12.6 ,Windows 7 64 Bit</p><p>I am unable to use capture filter in remote capture (Linux Interface on Windows)for only "tcp" packets and analyze. where as the same I am able to do in Wireshark 1.6.1.</p><p>Request you to help me out, why the latest Wireshark 1.12.6 or 1.12.4 is unable to perform it.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Capture_Capture_Filter_Error.jpg" alt="alt text" /></p><p>Thank you</p><p>Dinesh</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture-options" rel="tag" title="see questions tagged &#39;capture-options&#39;">capture-options</span> <span class="post-tag tag-link-remote-capture" rel="tag" title="see questions tagged &#39;remote-capture&#39;">remote-capture</span> <span class="post-tag tag-link-interfaces" rel="tag" title="see questions tagged &#39;interfaces&#39;">interfaces</span> <span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jun '15, 03:36</strong></p><img src="https://secure.gravatar.com/avatar/04334c27cb629065a13d61a61b611038?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dinesh%20Babu%20Sadu&#39;s gravatar image" /><p><span>Dinesh Babu ...</span><br />
<span class="score" title="16 reputation points">16</span><span title="13 badges"><span class="badge1">●</span><span class="badgecount">13</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="17 badges"><span class="bronze">●</span><span class="badgecount">17</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dinesh Babu Sadu has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Jul '15, 02:14</strong> </span></p></div></div><div id="comments-container-43716" class="comments-container"></div><div id="comment-tools-43716" class="comment-tools"></div><div class="clear"></div><div id="comment-43716-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

