+++
type = "question"
title = "We are a call center using VoIP and SIP.. FC protocal"
description = '''We ran Wireshark and found goups of packets that had all 0&#x27;s for destination and source. protocal is FC and in brackets to the right it states [malformed packets]. I am trying to figure out why outbound voice quailty is poor. Does anyone suspect anything about the malformed packets described? Is thi...'''
date = "2011-09-01T12:09:00Z"
lastmod = "2011-09-01T12:09:00Z"
weight = 6049
keywords = [ "wireshark" ]
aliases = [ "/questions/6049" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [We are a call center using VoIP and SIP.. FC protocal](/questions/6049/we-are-a-call-center-using-voip-and-sip-fc-protocal)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6049-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6049-score" class="post-score" title="current number of votes">0</div><span id="post-6049-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We ran Wireshark and found goups of packets that had all 0's for destination and source. protocal is FC and in brackets to the right it states [malformed packets]. I am trying to figure out why outbound voice quailty is poor.</p><p>Does anyone suspect anything about the malformed packets described? Is this normal stuff?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Sep '11, 12:09</strong></p><img src="https://secure.gravatar.com/avatar/3c79bc8ff287a204307e2497cd9d99cc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pete2k&#39;s gravatar image" /><p><span>pete2k</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pete2k has no accepted answers">0%</span></p></div></div><div id="comments-container-6049" class="comments-container"></div><div id="comment-tools-6049" class="comment-tools"></div><div class="clear"></div><div id="comment-6049-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

