+++
type = "question"
title = "Can dfilter_apply() over an edt tree be undone?"
description = '''I am writing a dissector, and I am applying a read filter using dfilter_apply() on edt tree on the packets during a live capture. Is there any way to undo the effect of dfilter_apply(); on edt tree, since during live capture the tree gets modified after applying a read filter?'''
date = "2012-02-06T08:40:00Z"
lastmod = "2012-02-06T08:40:00Z"
weight = 8850
keywords = [ "development", "dissector", "dfilter", "wireshark" ]
aliases = [ "/questions/8850" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can dfilter\_apply() over an edt tree be undone?](/questions/8850/can-dfilter_apply-over-an-edt-tree-be-undone)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8850-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8850-score" class="post-score" title="current number of votes">0</div><span id="post-8850-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am writing a dissector, and I am applying a read filter using <code>dfilter_apply()</code> on edt tree on the packets during a live capture.<br />
Is there any way to undo the effect of <code>dfilter_apply();</code> on edt tree, since during live capture the tree gets modified after applying a read filter?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-development" rel="tag" title="see questions tagged &#39;development&#39;">development</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-dfilter" rel="tag" title="see questions tagged &#39;dfilter&#39;">dfilter</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Feb '12, 08:40</strong></p><img src="https://secure.gravatar.com/avatar/425d250364423a7595a3eb9dea779cb2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sanny_D&#39;s gravatar image" /><p><span>Sanny_D</span><br />
<span class="score" title="0 reputation points">0</span><span title="18 badges"><span class="badge1">●</span><span class="badgecount">18</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="21 badges"><span class="bronze">●</span><span class="badgecount">21</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sanny_D has 3 accepted answers">50%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Feb '12, 13:22</strong> </span></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span></p></div></div><div id="comments-container-8850" class="comments-container"></div><div id="comment-tools-8850" class="comment-tools"></div><div class="clear"></div><div id="comment-8850-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

