+++
type = "question"
title = "Telegram not working properly."
description = '''Hello. I want to use Telegram Desktop on my Windows and my Telegram is the last version. When I set Passcode lock on my telegram then the telegram can&#x27;t connect to the internet and without passcode lock it work very nice. I captured traffic via wireshark and can anyone advice me? Uploaded file: http...'''
date = "2016-10-30T04:29:00Z"
lastmod = "2016-10-30T04:29:00Z"
weight = 56829
keywords = [ "telegram" ]
aliases = [ "/questions/56829" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Telegram not working properly.](/questions/56829/telegram-not-working-properly)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56829-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56829-score" class="post-score" title="current number of votes">0</div><span id="post-56829-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello. I want to use Telegram Desktop on my Windows and my Telegram is the last version. When I set Passcode lock on my telegram then the telegram can't connect to the internet and without passcode lock it work very nice. I captured traffic via wireshark and can anyone advice me? Uploaded file: <a href="http://s000.tinyupload.com/?file_id=05909947278734371590">http://s000.tinyupload.com/?file_id=05909947278734371590</a></p><p>Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-telegram" rel="tag" title="see questions tagged &#39;telegram&#39;">telegram</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Oct '16, 04:29</strong></p><img src="https://secure.gravatar.com/avatar/1f1d393403ea997213960ee852d8f897?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hack3rcon&#39;s gravatar image" /><p><span>hack3rcon</span><br />
<span class="score" title="11 reputation points">11</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hack3rcon has no accepted answers">0%</span></p></div></div><div id="comments-container-56829" class="comments-container"></div><div id="comment-tools-56829" class="comment-tools"></div><div class="clear"></div><div id="comment-56829-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

