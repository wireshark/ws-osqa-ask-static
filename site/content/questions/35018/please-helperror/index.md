+++
type = "question"
title = "please help...error"
description = '''Please check that &quot;&#92;Device&#92;NPF_{E84FBE38-F0F7-4298-90B2-72BD7CF496ED}&quot; is the proper interface. Please help with this terror this is the message that I get on the screen  me_: ****CommView D-Link DWA-520 Wireless 108G Desktop Adapter'''
date = "2014-07-30T15:40:00Z"
lastmod = "2014-08-01T00:21:00Z"
weight = 35018
keywords = [ "capture-filter", "help" ]
aliases = [ "/questions/35018" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [please help...error](/questions/35018/please-helperror)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35018-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35018-score" class="post-score" title="current number of votes">0</div><span id="post-35018-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Please check that "\Device\NPF_{E84FBE38-F0F7-4298-90B2-72BD7CF496ED}" is the proper interface. Please help with this terror this is the message that I get on the screen me_: ****CommView D-Link DWA-520 Wireless 108G Desktop Adapter</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jul '14, 15:40</strong></p><img src="https://secure.gravatar.com/avatar/de99f620d4da7d7f9ce885f6b3903f39?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Alejandro%20MontoYah%20Rivasplata&#39;s gravatar image" /><p><span>Alejandro Mo...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Alejandro MontoYah Rivasplata has no accepted answers">0%</span></p></div></div><div id="comments-container-35018" class="comments-container"></div><div id="comment-tools-35018" class="comment-tools"></div><div class="clear"></div><div id="comment-35018-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35045"></span>

<div id="answer-container-35045" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35045-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35045-score" class="post-score" title="current number of votes">0</div><span id="post-35045-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p><strong>CommView</strong> D-Link DWA-520 Wireless 108G Desktop</p></blockquote><p>CommView? Did you use Wireshark or <strong>CommView for Wifi</strong>?</p><p>If <strong>Wireshark</strong>: Please add (much) more details what you did before that error message appeared.<br />
If <strong>CommView for Wifi</strong>: This is the wrong site for your question. Please contact <a href="http://www.tamos.com/support/">TamoSoft Support</a>.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Aug '14, 00:21</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Aug '14, 00:21</strong> </span></p></div></div><div id="comments-container-35045" class="comments-container"></div><div id="comment-tools-35045" class="comment-tools"></div><div class="clear"></div><div id="comment-35045-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

