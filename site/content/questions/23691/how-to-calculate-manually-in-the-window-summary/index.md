+++
type = "question"
title = "how to calculate manually in the window summary?"
description = '''Hallo all, I began to confusion when reading the results capture the data contained in the summary window. The data have been listed on the summary window when it has completed the capture. Can someone tell me, how the data is obtained?, and how to calculate it manually packet, between lass and firs...'''
date = "2013-08-10T09:39:00Z"
lastmod = "2013-08-10T09:39:00Z"
weight = 23691
keywords = [ "qos", "manually" ]
aliases = [ "/questions/23691" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to calculate manually in the window summary?](/questions/23691/how-to-calculate-manually-in-the-window-summary)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23691-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23691-score" class="post-score" title="current number of votes">0</div><span id="post-23691-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hallo all,</p><p>I began to confusion when reading the results capture the data contained in the summary window. The data have been listed on the summary window when it has completed the capture.</p><p>Can someone tell me, how the data is obtained?, and how to calculate it manually packet, between lass and first packet, avg. packet / sec, avg. packet size, byte, avg. byte / sec, avg. MBit / sec?</p><p>Moreover, how to calculate it manually in the menu Telephony particular delay, jitter, bandwidth, etc.?.</p><p>thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-qos" rel="tag" title="see questions tagged &#39;qos&#39;">qos</span> <span class="post-tag tag-link-manually" rel="tag" title="see questions tagged &#39;manually&#39;">manually</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Aug '13, 09:39</strong></p><img src="https://secure.gravatar.com/avatar/3ad5d9d930c81f5371566cb3f2dbea09?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="apinds&#39;s gravatar image" /><p><span>apinds</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="apinds has no accepted answers">0%</span></p></div></div><div id="comments-container-23691" class="comments-container"></div><div id="comment-tools-23691" class="comment-tools"></div><div class="clear"></div><div id="comment-23691-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

