+++
type = "question"
title = "display destination"
description = '''capturing displays only the ip destination '''
date = "2014-06-28T13:36:00Z"
lastmod = "2014-06-28T15:16:00Z"
weight = 34261
keywords = [ "capture", "error" ]
aliases = [ "/questions/34261" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [display destination](/questions/34261/display-destination)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34261-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34261-score" class="post-score" title="current number of votes">0</div><span id="post-34261-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>capturing displays only the ip destination</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jun '14, 13:36</strong></p><img src="https://secure.gravatar.com/avatar/e5148f2bef5731033c7329b366ce7862?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ghassan&#39;s gravatar image" /><p><span>ghassan</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ghassan has no accepted answers">0%</span></p></div></div><div id="comments-container-34261" class="comments-container"><span id="34263"></span><div id="comment-34263" class="comment"><div id="post-34263-score" class="comment-score">1</div><div class="comment-text"><p>thanks for that information. But what is your question?</p></div><div id="comment-34263-info" class="comment-info"><span class="comment-age">(28 Jun '14, 15:16)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-34261" class="comment-tools"></div><div class="clear"></div><div id="comment-34261-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

