+++
type = "question"
title = "How to delete display-filter buttons in the main user interface."
description = '''I can add display-filter buttons as many as I want in the main user interface. But I cannot delete display-filter buttons in the main user interface. As a result, the number of display-filter buttons is increasing which I don&#x27;t want.'''
date = "2017-01-17T00:28:00Z"
lastmod = "2017-01-17T23:08:00Z"
weight = 58829
keywords = [ "display-filter" ]
aliases = [ "/questions/58829" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [How to delete display-filter buttons in the main user interface.](/questions/58829/how-to-delete-display-filter-buttons-in-the-main-user-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58829-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58829-score" class="post-score" title="current number of votes">0</div><span id="post-58829-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can add display-filter buttons as many as I want in the main user interface. But I cannot delete display-filter buttons in the main user interface. As a result, the number of display-filter buttons is increasing which I don't want.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-display-filter" rel="tag" title="see questions tagged &#39;display-filter&#39;">display-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Jan '17, 00:28</strong></p><img src="https://secure.gravatar.com/avatar/b4deb019a300d67e83a17e98fa84a2ad?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="q3yoon&#39;s gravatar image" /><p><span>q3yoon</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="q3yoon has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>17 Jan '17, 00:30</strong> </span></p></div></div><div id="comments-container-58829" class="comments-container"></div><div id="comment-tools-58829" class="comment-tools"></div><div class="clear"></div><div id="comment-58829-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58830"></span>

<div id="answer-container-58830" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58830-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58830-score" class="post-score" title="current number of votes">1</div><span id="post-58830-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="cmaynard has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can do that in the preferences at the "Filter Expressions" pages.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Jan '17, 01:20</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-58830" class="comments-container"><span id="58860"></span><div id="comment-58860" class="comment"><div id="post-58860-score" class="comment-score"></div><div class="comment-text"><p>Got it - thanks!</p></div><div id="comment-58860-info" class="comment-info"><span class="comment-age">(17 Jan '17, 16:53)</span> <span class="comment-user userinfo">q3yoon</span></div></div><span id="58865"></span><div id="comment-58865" class="comment"><div id="post-58865-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-58865-info" class="comment-info"><span class="comment-age">(17 Jan '17, 23:08)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-58830" class="comment-tools"></div><div class="clear"></div><div id="comment-58830-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

