+++
type = "question"
title = "Statistics Options"
description = '''I downloaded the free version of Wireshark, but when I select Statistics, then there is no option for ports, among other things. How do I get these options? Are they free? More specifically, I am running Wireshark version 1.4.0 running in windows XP and I got it from the wireshark webpage. I cannot ...'''
date = "2010-10-25T13:06:00Z"
lastmod = "2010-10-25T19:03:00Z"
weight = 627
keywords = [ "statistics" ]
aliases = [ "/questions/627" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Statistics Options](/questions/627/statistics-options)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-627-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-627-score" class="post-score" title="current number of votes">0</div><span id="post-627-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I downloaded the free version of Wireshark, but when I select Statistics, then there is no option for ports, among other things. How do I get these options? Are they free?</p><p>More specifically, I am running Wireshark version 1.4.0 running in windows XP and I got it from the wireshark webpage. I cannot see the "Port Type" option in the statistics pull down menu that I saw as an option in one of the educational videos by Laura Chappell. I have statistics options such as Summary, Protocol Hierarchy, Conversations etc. I don't have the specific option "Port Type". As I said before, I noticed it in the educational video "Wireshark Functionality and Fundamentals" under section 6 "Examining basic Trace File Statistics." I also don't have the option "VoIP Calls," "SIP," among other features. Forgive my lack of specificity, I did not think I would get a response so quickly or at all really. :) Thanks for your time.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Oct '10, 13:06</strong></p><img src="https://secure.gravatar.com/avatar/a6eb7ac970d7ed0e9f01b5b7f7145304?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="schwei&#39;s gravatar image" /><p><span>schwei</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="schwei has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Oct '10, 14:44</strong> </span></p></div></div><div id="comments-container-627" class="comments-container"><span id="633"></span><div id="comment-633" class="comment"><div id="post-633-score" class="comment-score">1</div><div class="comment-text"><p>You see nothing when you select Statistics from the menubar?</p><p>Where did you get your copy of Wireshark? What OS? What version?</p><p>You should see Summary, Protocol Hierarchy, Conversations, Endpoints, Packet Lengths, IO Graphs, Conversation List, etc.</p></div><div id="comment-633-info" class="comment-info"><span class="comment-age">(25 Oct '10, 14:18)</span> <span class="comment-user userinfo">lchappell ♦</span></div></div><span id="642"></span><div id="comment-642" class="comment"><div id="post-642-score" class="comment-score">1</div><div class="comment-text"><p><a href="http://www.wireshark.org/faq.html#q1.6">The <em>only</em> version of Wireshark that's available is the free version</a>; there isn't any non-free version. What sort of "ports" statistic are you looking for?</p></div><div id="comment-642-info" class="comment-info"><span class="comment-age">(25 Oct '10, 19:03)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-627" class="comment-tools"></div><div class="clear"></div><div id="comment-627-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

