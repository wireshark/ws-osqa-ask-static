+++
type = "question"
title = "finding Streaming URLs for Radio Station"
description = '''I am trying to get the Streaming URL for 101.3 Virgin Radio out of Halifax http://www.iheartradio.ca/virginradio/halifax I followed an old post on here but it did not work and the other stuff they said to try was above my head so I come here looking for some help please thanks.'''
date = "2017-07-15T11:14:00Z"
lastmod = "2017-07-15T11:14:00Z"
weight = 62809
keywords = [ "url", "raido", "stream" ]
aliases = [ "/questions/62809" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [finding Streaming URLs for Radio Station](/questions/62809/finding-streaming-urls-for-radio-station)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62809-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62809-score" class="post-score" title="current number of votes">0</div><span id="post-62809-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to get the Streaming URL for 101.3 Virgin Radio out of Halifax <a href="http://www.iheartradio.ca/virginradio/halifax">http://www.iheartradio.ca/virginradio/halifax</a></p><p>I followed an old post on here but it did not work and the other stuff they said to try was above my head so I come here looking for some help please thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-url" rel="tag" title="see questions tagged &#39;url&#39;">url</span> <span class="post-tag tag-link-raido" rel="tag" title="see questions tagged &#39;raido&#39;">raido</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jul '17, 11:14</strong></p><img src="https://secure.gravatar.com/avatar/587d5ca54a97a25ad0a42f2ca3b965a5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Helix512&#39;s gravatar image" /><p><span>Helix512</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Helix512 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Jul '17, 11:47</strong> </span></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span></p></div></div><div id="comments-container-62809" class="comments-container"></div><div id="comment-tools-62809" class="comment-tools"></div><div class="clear"></div><div id="comment-62809-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

