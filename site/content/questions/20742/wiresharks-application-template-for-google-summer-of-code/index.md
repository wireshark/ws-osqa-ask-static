+++
type = "question"
title = "Wireshark&#x27;s Application template for Google Summer Of Code"
description = '''Hello, (I&#x27;m french, so I excuse for my possible mistakes :)) I want to participate of the GSoC 2013, and I&#x27;m really interested by several Wireshark projets. But, I don&#x27;t find the &quot;Application Template&quot; who give the informations that wireshark want for study our proposal. For exemple the &quot;Application...'''
date = "2013-04-23T12:17:00Z"
lastmod = "2013-04-24T09:49:00Z"
weight = 20742
keywords = [ "applicationtemplate", "gsoc2013" ]
aliases = [ "/questions/20742" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark's Application template for Google Summer Of Code](/questions/20742/wiresharks-application-template-for-google-summer-of-code)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20742-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20742-score" class="post-score" title="current number of votes">0</div><span id="post-20742-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>(I'm french, so I excuse for my possible mistakes :))</p><p>I want to participate of the GSoC 2013, and I'm really interested by several Wireshark projets.</p><p>But, I don't find the "Application Template" who give the informations that wireshark want for study our proposal.</p><p>For exemple the "Application Template" of Nmap : <a href="http://www.google-melange.com/gsoc/org/google/gsoc2013/nmap">http://www.google-melange.com/gsoc/org/google/gsoc2013/nmap</a> or OWASP : <a href="https://www.owasp.org/index.php/GSoC_SAT">https://www.owasp.org/index.php/GSoC_SAT</a></p><p>Or maybe Wireshark don't want a special "Application Template"</p><p>Sincerely. Simon.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-applicationtemplate" rel="tag" title="see questions tagged &#39;applicationtemplate&#39;">applicationtemplate</span> <span class="post-tag tag-link-gsoc2013" rel="tag" title="see questions tagged &#39;gsoc2013&#39;">gsoc2013</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Apr '13, 12:17</strong></p><img src="https://secure.gravatar.com/avatar/142d88b44d8db1bdc850fc439658c5e1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="simonCoSG&#39;s gravatar image" /><p><span>simonCoSG</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="simonCoSG has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Apr '13, 14:15</strong> </span></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span></p></div></div><div id="comments-container-20742" class="comments-container"></div><div id="comment-tools-20742" class="comment-tools"></div><div class="clear"></div><div id="comment-20742-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20744"></span>

<div id="answer-container-20744" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20744-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20744-score" class="post-score" title="current number of votes">0</div><span id="post-20744-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I filled in the application template. Please let me know if you have any questions.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Apr '13, 14:16</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-20744" class="comments-container"><span id="20770"></span><div id="comment-20770" class="comment"><div id="post-20770-score" class="comment-score"></div><div class="comment-text"><p>Hello,</p><p>Thanks you :)</p><p>I have some questions :</p><ul><li>How we send the application template ? By email ? And under which format (pdf ..) ?</li><li>When you say "Willingness to work on other projects :" You speak about the others wireshark's projects ? Or about the others projects that we have chosen for others organisations ?</li><li>Here : "If you are applying for other GSoC projects this year please list them here" You speak about the others organisations's projects ?</li></ul><p>And do you know a good web site to host my CV and severals codes ?</p><p>Thanks for your help :) Simon.</p></div><div id="comment-20770-info" class="comment-info"><span class="comment-age">(24 Apr '13, 09:49)</span> <span class="comment-user userinfo">simonCoSG</span></div></div></div><div id="comment-tools-20744" class="comment-tools"></div><div class="clear"></div><div id="comment-20744-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

