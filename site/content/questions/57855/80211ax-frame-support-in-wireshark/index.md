+++
type = "question"
title = "802.11ax Frame support in wireshark"
description = '''Hi,  when do you expect to have Wireshark support for 802.11ax framing including HE Capabilities, HE Operational, Trigger Frame... Draft P802.11ax_D0.5 already released.'''
date = "2016-12-05T02:30:00Z"
lastmod = "2016-12-05T03:05:00Z"
weight = 57855
keywords = [ "802.11ax", "he" ]
aliases = [ "/questions/57855" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [802.11ax Frame support in wireshark](/questions/57855/80211ax-frame-support-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57855-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57855-score" class="post-score" title="current number of votes">0</div><span id="post-57855-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>when do you expect to have Wireshark support for 802.11ax framing including HE Capabilities, HE Operational, Trigger Frame...</p><p>Draft P802.11ax_D0.5 already released.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-802.11ax" rel="tag" title="see questions tagged &#39;802.11ax&#39;">802.11ax</span> <span class="post-tag tag-link-he" rel="tag" title="see questions tagged &#39;he&#39;">he</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Dec '16, 02:30</strong></p><img src="https://secure.gravatar.com/avatar/abe7d517887530c6c7cd52e90f298b5a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lzur&#39;s gravatar image" /><p><span>lzur</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lzur has no accepted answers">0%</span></p></div></div><div id="comments-container-57855" class="comments-container"></div><div id="comment-tools-57855" class="comment-tools"></div><div class="clear"></div><div id="comment-57855-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57857"></span>

<div id="answer-container-57857" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57857-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57857-score" class="post-score" title="current number of votes">0</div><span id="post-57857-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>When someone submits a patch to add that functionality.</p><p>Wireshark is maintained by a team of volunteers, there is no schedule or plan as such, apart from the ideas listed on the wiki <a href="https://wiki.wireshark.org/Development">development page</a>.</p><p>You can raise an entry on the <a href="https://bugs.wireshark.org">Wireshark Bugzilla</a> requesting an enhancement. Attaching a capture and links to the documentation will help immensely.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Dec '16, 03:05</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-57857" class="comments-container"></div><div id="comment-tools-57857" class="comment-tools"></div><div class="clear"></div><div id="comment-57857-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

