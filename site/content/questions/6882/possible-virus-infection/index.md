+++
type = "question"
title = "possible virus infection"
description = '''I think my computer and Blackberry are being controlled by my competitor.   Why can I run a Wireshark live capture in any other house using their wireless connection, but in my house, it doesn’t capture anything?   My Blackberry &quot;black key work of my industry&quot; (???). When I send emails to my custome...'''
date = "2011-10-13T20:18:00Z"
lastmod = "2011-10-13T20:18:00Z"
weight = 6882
keywords = [ "troubleshooting" ]
aliases = [ "/questions/6882" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [possible virus infection](/questions/6882/possible-virus-infection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6882-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6882-score" class="post-score" title="current number of votes">0</div><span id="post-6882-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I think my computer and Blackberry are being controlled by my competitor.</p><ol><li><p>Why can I run a Wireshark live capture in any other house using their wireless connection, but in my house, it doesn’t capture anything?</p></li><li><p>My Blackberry "black key work of my industry" (???). When I send emails to my customers with the subject of key words from the industry, I get a red X near the email, which means blocked from sending.</p></li><li>Does it mean that someone is controlling my Blackberry?</li><li>What should I do to find out who he is? Which USA authority can help me?</li><li>Do I have to replace the device? or SIM card? Or maybe my Blackberry is infected with spyware.</li></ol></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-troubleshooting" rel="tag" title="see questions tagged &#39;troubleshooting&#39;">troubleshooting</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Oct '11, 20:18</strong></p><img src="https://secure.gravatar.com/avatar/10b549ad7dbd8d77de70c0f0fcd7e22d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smith&#39;s gravatar image" /><p><span>smith</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smith has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Oct '11, 11:24</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-6882" class="comments-container"></div><div id="comment-tools-6882" class="comment-tools"></div><div class="clear"></div><div id="comment-6882-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

