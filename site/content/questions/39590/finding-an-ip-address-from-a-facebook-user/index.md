+++
type = "question"
title = "finding an ip address from a facebook user"
description = '''Hello all, I am new to wireshark and i would like to find an ip address from a facebook user (fake profile). I have at first a question, do i and the other profile need to chat online or it is possible even if only i send a message to him/her?And second, i tried to and i got a number of IP addresses...'''
date = "2015-02-03T00:36:00Z"
lastmod = "2015-02-03T02:41:00Z"
weight = 39590
keywords = [ "findipaddress" ]
aliases = [ "/questions/39590" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [finding an ip address from a facebook user](/questions/39590/finding-an-ip-address-from-a-facebook-user)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39590-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39590-score" class="post-score" title="current number of votes">0</div><span id="post-39590-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello all, I am new to wireshark and i would like to find an ip address from a facebook user (fake profile). I have at first a question, do i and the other profile need to chat online or it is possible even if only i send a message to him/her?And second, i tried to and i got a number of IP addresses how do i know which one to select?? Thank you in advance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-findipaddress" rel="tag" title="see questions tagged &#39;findipaddress&#39;">findipaddress</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Feb '15, 00:36</strong></p><img src="https://secure.gravatar.com/avatar/bb1a8cc611074ed5d9fcfb8f13de31aa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="panpan&#39;s gravatar image" /><p><span>panpan</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="panpan has no accepted answers">0%</span></p></div></div><div id="comments-container-39590" class="comments-container"></div><div id="comment-tools-39590" class="comment-tools"></div><div class="clear"></div><div id="comment-39590-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39595"></span>

<div id="answer-container-39595" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39595-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39595-score" class="post-score" title="current number of votes">0</div><span id="post-39595-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can't do that since all this flows through Facebook servers.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Feb '15, 02:41</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-39595" class="comments-container"></div><div id="comment-tools-39595" class="comment-tools"></div><div class="clear"></div><div id="comment-39595-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

