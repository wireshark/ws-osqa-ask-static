+++
type = "question"
title = "decrypt SSL trafic for 993"
description = '''Hi, I was wondering if this would be possible, Currently have my yahoo account working on my iphone but its been a while since I put in the password, now that i want to check my email on my computer, I completely forgot it, and i never put in the recover email account :(. Been looking everywhere int...'''
date = "2017-02-21T07:04:00Z"
lastmod = "2017-02-21T15:17:00Z"
weight = 59583
keywords = [ "ssl_decrypt" ]
aliases = [ "/questions/59583" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [decrypt SSL trafic for 993](/questions/59583/decrypt-ssl-trafic-for-993)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59583-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59583-score" class="post-score" title="current number of votes">0</div><span id="post-59583-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I was wondering if this would be possible, Currently have my yahoo account working on my iphone but its been a while since I put in the password, now that i want to check my email on my computer, I completely forgot it, and i never put in the recover email account :(. Been looking everywhere into jailbreaking tweaks to show the black dots, to calling customer support yahoo (which they do not have) to now my last resort on sniffing the password. Would there be a way to uncrypt the password? Even if i have it working on the iphone?</p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl_decrypt" rel="tag" title="see questions tagged &#39;ssl_decrypt&#39;">ssl_decrypt</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Feb '17, 07:04</strong></p><img src="https://secure.gravatar.com/avatar/dd2630227be6d715406847ade75c3d27?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="killmasta93&#39;s gravatar image" /><p><span>killmasta93</span><br />
<span class="score" title="-1 reputation points">-1</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="killmasta93 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Feb '17, 04:37</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-59583" class="comments-container"></div><div id="comment-tools-59583" class="comment-tools"></div><div class="clear"></div><div id="comment-59583-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59584"></span>

<div id="answer-container-59584" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59584-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59584-score" class="post-score" title="current number of votes">0</div><span id="post-59584-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Refer to this article</p><p><a href="https://blog.packet-foo.com/2016/07/how-to-use-wireshark-to-steal-passwords/">https://blog.packet-foo.com/2016/07/how-to-use-wireshark-to-steal-passwords/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Feb '17, 07:10</strong></p><img src="https://secure.gravatar.com/avatar/1e22670f8d643ca08d658b80a6782932?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Packet_vlad&#39;s gravatar image" /><p><span>Packet_vlad</span><br />
<span class="score" title="436 reputation points">436</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Packet_vlad has 5 accepted answers">20%</span></p></div></div><div id="comments-container-59584" class="comments-container"><span id="59592"></span><div id="comment-59592" class="comment"><div id="post-59592-score" class="comment-score"></div><div class="comment-text"><p>Thanks for the reply, so I was reading the article practically means its impossible?</p></div><div id="comment-59592-info" class="comment-info"><span class="comment-age">(21 Feb '17, 15:17)</span> <span class="comment-user userinfo">killmasta93</span></div></div></div><div id="comment-tools-59584" class="comment-tools"></div><div class="clear"></div><div id="comment-59584-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

