+++
type = "question"
title = "Netlink message decoding"
description = '''Hi, I&#x27;ve used this document to dump netlink communications to a pcap: http://lists.sandelman.ca/pipermail/tcpdump-workers/2014-October/000027.html I then open this pcap with wireshark 1.10.6 . It is recognized as a SLL protocol (&quot;Encapsulation type: Linux cooked-mode capture (25)&quot;) but the data is n...'''
date = "2015-05-26T14:57:00Z"
lastmod = "2015-05-26T14:57:00Z"
weight = 42679
keywords = [ "netlink" ]
aliases = [ "/questions/42679" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Netlink message decoding](/questions/42679/netlink-message-decoding)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42679-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42679-score" class="post-score" title="current number of votes">0</div><span id="post-42679-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I've used this document to dump netlink communications to a pcap: <a href="http://lists.sandelman.ca/pipermail/tcpdump-workers/2014-October/000027.html">http://lists.sandelman.ca/pipermail/tcpdump-workers/2014-October/000027.html</a> I then open this pcap with wireshark 1.10.6 . It is recognized as a SLL protocol ("Encapsulation type: Linux cooked-mode capture (25)") but the data is not parsed, I thought that wireshark would be able to tell me if the packet was a request/control message, from which family etc... I was wondering if it was possible to do that ?</p><p>Cheers</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-netlink" rel="tag" title="see questions tagged &#39;netlink&#39;">netlink</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 May '15, 14:57</strong></p><img src="https://secure.gravatar.com/avatar/e2e55c6d8b33c6f22b441b0f39cfa209?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="teto&#39;s gravatar image" /><p><span>teto</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="teto has no accepted answers">0%</span></p></div></div><div id="comments-container-42679" class="comments-container"></div><div id="comment-tools-42679" class="comment-tools"></div><div class="clear"></div><div id="comment-42679-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

