+++
type = "question"
title = "text2pcap issue"
description = '''How to convert below hexdump into readable pcap format  c364f21d7e098003101b0d120600710419895812049733070b1207001204198915 9309024462424804e95404eb6b1e281c060700118605010101a011600f80020780 a109060704000001000e036c1aa1180201000201383010800804142341703973f8 020104830100&#x27;H  '''
date = "2012-06-29T01:58:00Z"
lastmod = "2012-06-29T06:29:00Z"
weight = 12302
keywords = [ "text2pcap" ]
aliases = [ "/questions/12302" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [text2pcap issue](/questions/12302/text2pcap-issue)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12302-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12302-score" class="post-score" title="current number of votes">1</div><span id="post-12302-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How to convert below hexdump into readable pcap format</p><blockquote><pre><code>c364f21d7e098003101b0d120600710419895812049733070b1207001204198915
9309024462424804e95404eb6b1e281c060700118605010101a011600f80020780
a109060704000001000e036c1aa1180201000201383010800804142341703973f8
020104830100&#39;H</code></pre></blockquote></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-text2pcap" rel="tag" title="see questions tagged &#39;text2pcap&#39;">text2pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jun '12, 01:58</strong></p><img src="https://secure.gravatar.com/avatar/5b9e159e5e85ffdb11bccdf239a9f8fc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rks2k122&#39;s gravatar image" /><p><span>rks2k122</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rks2k122 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Jun '12, 02:09</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-12302" class="comments-container"></div><div id="comment-tools-12302" class="comment-tools"></div><div class="clear"></div><div id="comment-12302-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12305"></span>

<div id="answer-container-12305" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12305-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12305-score" class="post-score" title="current number of votes">1</div><span id="post-12305-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>that format is not supported by text2pcap.</p><blockquote><p><code>http://www.wireshark.org/docs/wsug_html_chunked/AppToolstext2pcap.html</code><br />
</p><p><code>Text2pcap understands a hexdump of the form generated by od -Ax -tx1 -v. In other words,</code> <strong><code>each byte is individually displayed and surrounded with a space. Each line begins with an offset describing the position in the file. The offset is a hex number (can also be octal or decimal - see -o), of more than two hex digits.</code></strong> Here is a sample dump that text2pcap can recognize:`</p></blockquote><p>Please convert it to a format like this:</p><blockquote><p><code> 000000 c3 64 f2 1d 7e 09 80 03 ........ 000008 10 1b 0d 12 06 00 71 04 ........ ...</code></p></blockquote><p>How can you do it?<br />
</p><blockquote><pre><code>#include &quot;what_we_need.h&quot;

#define GOOD 1
#define BAD 0

int able_to_program;
int want_to_use_editor;
int found_victim;
int mood;

able_to_program = check_programming_skills();
want_to_use_editor = check_editor_and_mood();

if (want_to_use_editor) {
   mood = GOOD; 
   use_editor_to_format_string(mood);
} else {
   if (able_to_program) {
      write_a_script_to_format_string();
   } else {
      found_victim = find_someone_who_can_do_it_for_you();
      if (found_victim) {
         let_him_do_it();
      } else {
         mood = BAD;
         swear_a_few_seconds();
         use_editor_to_format_string(mood);
      }
   }
}
</code></pre></blockquote><p>;-)</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Jun '12, 03:02</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Jun '12, 07:21</strong> </span></p></div></div><div id="comments-container-12305" class="comments-container"><span id="12310"></span><div id="comment-12310" class="comment"><div id="post-12310-score" class="comment-score"></div><div class="comment-text"><p>Is that code GPL'd? :)</p></div><div id="comment-12310-info" class="comment-info"><span class="comment-age">(29 Jun '12, 06:04)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="12312"></span><div id="comment-12312" class="comment"><div id="post-12312-score" class="comment-score"></div><div class="comment-text"><p>Sorry, I forgot to mention it. Yep, it's GPL. Feel free to extend it ;-))</p></div><div id="comment-12312-info" class="comment-info"><span class="comment-age">(29 Jun '12, 06:29)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-12305" class="comment-tools"></div><div class="clear"></div><div id="comment-12305-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

