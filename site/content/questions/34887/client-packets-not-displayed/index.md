+++
type = "question"
title = "Client packets not displayed"
description = '''I&#x27;ve turned off all capture and display filters but still only get the server side of a client-server session. This even happens with simple pings. I can see the server responding but the ping request is missing. I can also see the client IP address but only for overhead type packets.'''
date = "2014-07-24T09:50:00Z"
lastmod = "2014-07-24T12:34:00Z"
weight = 34887
keywords = [ "filter", "ping" ]
aliases = [ "/questions/34887" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Client packets not displayed](/questions/34887/client-packets-not-displayed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34887-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34887-score" class="post-score" title="current number of votes">0</div><span id="post-34887-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've turned off all capture and display filters but still only get the server side of a client-server session. This even happens with simple pings. I can see the server responding but the ping request is missing. I can also see the client IP address but only for overhead type packets.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-ping" rel="tag" title="see questions tagged &#39;ping&#39;">ping</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jul '14, 09:50</strong></p><img src="https://secure.gravatar.com/avatar/97759e5fd4a2956092f8a884eba6658d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nutman&#39;s gravatar image" /><p><span>nutman</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nutman has no accepted answers">0%</span></p></div></div><div id="comments-container-34887" class="comments-container"></div><div id="comment-tools-34887" class="comment-tools"></div><div class="clear"></div><div id="comment-34887-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34889"></span>

<div id="answer-container-34889" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34889-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34889-score" class="post-score" title="current number of votes">1</div><span id="post-34889-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Fairly frequent question here, see the answer by <span>@Kurt Knochner</span> to this question: <a href="http://ask.wireshark.org/questions/33597/wireshark-on-win7-machine-not-capturing-all-traffic">http://ask.wireshark.org/questions/33597/wireshark-on-win7-machine-not-capturing-all-traffic</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Jul '14, 10:05</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-34889" class="comments-container"><span id="34896"></span><div id="comment-34896" class="comment"><div id="post-34896-score" class="comment-score"></div><div class="comment-text"><p>The solution was to turn off my firewall. Seems strange to me but I tried it several times. With the firewall on - no client packets. Firewall off - all there. I have Windows 7 - 64bit and Bit Defender security. I am uses a control application with a direct connection between my computer and the server.</p></div><div id="comment-34896-info" class="comment-info"><span class="comment-age">(24 Jul '14, 12:34)</span> <span class="comment-user userinfo">nutman</span></div></div></div><div id="comment-tools-34889" class="comment-tools"></div><div class="clear"></div><div id="comment-34889-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

