+++
type = "question"
title = "Color rules and expert info in newest build"
description = '''Just loaded the 1.11.4 build to test it out. Love that it does not require X11, however 2 things seem to not be functioning so far. 1 The expert system does not launch from the bottom left hand corder &quot;LED&quot; button how do you launch it. 2. I don&#x27;t see anywhere to edit my color rules, my existing ones...'''
date = "2014-04-22T08:00:00Z"
lastmod = "2014-04-22T08:04:00Z"
weight = 32060
keywords = [ "color-rules", "expert-info", "coloring" ]
aliases = [ "/questions/32060" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Color rules and expert info in newest build](/questions/32060/color-rules-and-expert-info-in-newest-build)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32060-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32060-score" class="post-score" title="current number of votes">0</div><span id="post-32060-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Just loaded the 1.11.4 build to test it out. Love that it does not require X11, however 2 things seem to not be functioning so far. 1 The expert system does not launch from the bottom left hand corder "LED" button how do you launch it. 2. I don't see anywhere to edit my color rules, my existing ones work from different profiles but I can't see how to edit them or make new ones.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-color-rules" rel="tag" title="see questions tagged &#39;color-rules&#39;">color-rules</span> <span class="post-tag tag-link-expert-info" rel="tag" title="see questions tagged &#39;expert-info&#39;">expert-info</span> <span class="post-tag tag-link-coloring" rel="tag" title="see questions tagged &#39;coloring&#39;">coloring</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Apr '14, 08:00</strong></p><img src="https://secure.gravatar.com/avatar/6d5aa4db1ea17608e48019d9b4c80a85?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wookieboy&#39;s gravatar image" /><p><span>wookieboy</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wookieboy has no accepted answers">0%</span></p></div></div><div id="comments-container-32060" class="comments-container"></div><div id="comment-tools-32060" class="comment-tools"></div><div class="clear"></div><div id="comment-32060-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32061"></span>

<div id="answer-container-32061" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32061-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32061-score" class="post-score" title="current number of votes">1</div><span id="post-32061-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="wookieboy has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I guess those dialogs and functions have not yet been ported to the QT version. Keep in mind that the devs try to improve Wireshark while moving to QT, so it's not just a "simple port". So it will take some time until Wireshark on QT will have all functionality available.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Apr '14, 08:04</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-32061" class="comments-container"></div><div id="comment-tools-32061" class="comment-tools"></div><div class="clear"></div><div id="comment-32061-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

