+++
type = "question"
title = "IO graph - File Transaction Behavior"
description = '''I&#x27;ve never seen anything like this...seems like some pretty strange file transaction behavior.  Seen this type thing before?? Thanks in advance, Fritz  '''
date = "2016-10-24T16:52:00Z"
lastmod = "2016-10-25T08:50:00Z"
weight = 56625
keywords = [ "transfer", "smb", "file", "behavior" ]
aliases = [ "/questions/56625" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [IO graph - File Transaction Behavior](/questions/56625/io-graph-file-transaction-behavior)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56625-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56625-score" class="post-score" title="current number of votes">0</div><span id="post-56625-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've never seen anything like this...seems like some pretty strange file transaction behavior.</p><p>Seen this type thing before?? Thanks in advance, Fritz</p><p><img src="https://osqa-ask.wireshark.org/upfiles/ONRA_StreamIndex_11_ONRONFILER_NMCI_ISF.png" alt="alt text" /></p><p><img src="https://osqa-ask.wireshark.org/upfiles/File_transacrion_behavior.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-transfer" rel="tag" title="see questions tagged &#39;transfer&#39;">transfer</span> <span class="post-tag tag-link-smb" rel="tag" title="see questions tagged &#39;smb&#39;">smb</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span> <span class="post-tag tag-link-behavior" rel="tag" title="see questions tagged &#39;behavior&#39;">behavior</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Oct '16, 16:52</strong></p><img src="https://secure.gravatar.com/avatar/6e0fda2a5c8d02515d88f004b33a9998?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="fritzbied&#39;s gravatar image" /><p><span>fritzbied</span><br />
<span class="score" title="6 reputation points">6</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="fritzbied has no accepted answers">0%</span></p></img></div></div><div id="comments-container-56625" class="comments-container"><span id="56629"></span><div id="comment-56629" class="comment"><div id="post-56629-score" class="comment-score"></div><div class="comment-text"><p>Can you provide us a trace? You can anomyze it with tracewrangler, if you want!</p></div><div id="comment-56629-info" class="comment-info"><span class="comment-age">(24 Oct '16, 22:37)</span> <span class="comment-user userinfo">Christian_R</span></div></div><span id="56644"></span><div id="comment-56644" class="comment"><div id="post-56644-score" class="comment-score"></div><div class="comment-text"><p>Should be able to but will have to wait until this evening...I can send an excel summary view dump...the pattern in the image persists for the complete trace...don't have the start up. Not sure how to attach it.</p><p>Thanks in advance...R/Fritz</p></div><div id="comment-56644-info" class="comment-info"><span class="comment-age">(25 Oct '16, 05:38)</span> <span class="comment-user userinfo">fritzbied</span></div></div><span id="56647"></span><div id="comment-56647" class="comment"><div id="post-56647-score" class="comment-score"></div><div class="comment-text"><p>Capture files cannot be uploaded to this site directly, you have to upload them to cloudshark.org or to any plain file sharing service and edit your question with a login-free link to the uploaded file.</p></div><div id="comment-56647-info" class="comment-info"><span class="comment-age">(25 Oct '16, 08:50)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-56625" class="comment-tools"></div><div class="clear"></div><div id="comment-56625-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

