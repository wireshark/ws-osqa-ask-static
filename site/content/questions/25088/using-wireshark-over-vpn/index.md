+++
type = "question"
title = "Using WireShark over VPN"
description = '''Hi Guys, I&#x27;m new... Really new. As in, my day job is at 20th Century Fox and this is becoming my night job. I&#x27;m testing out Meraki gear for a hotel I&#x27;m building (yes, by night). My test site is in Sherman Oaks and I&#x27;m located in Beverly Hills. If I tunnel in using VPN, can I run wireshark on my mach...'''
date = "2013-09-22T18:54:00Z"
lastmod = "2013-09-23T01:09:00Z"
weight = 25088
keywords = [ "vpn", "remote", "wireshark" ]
aliases = [ "/questions/25088" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Using WireShark over VPN](/questions/25088/using-wireshark-over-vpn)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25088-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25088-score" class="post-score" title="current number of votes">0</div><span id="post-25088-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Guys,</p><p>I'm new... Really new. As in, my day job is at 20th Century Fox and this is becoming my night job. I'm testing out Meraki gear for a hotel I'm building (yes, by night). My test site is in Sherman Oaks and I'm located in Beverly Hills. If I tunnel in using VPN, can I run wireshark on my machine in Beverly Hills to assess the 'test network' in Sherman Oaks? Or, should I screenshaire with a machine on the local network in Sherman Oaks and run WireShark from there?</p><p>Thanks,</p><p>Alex Kirkwood</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vpn" rel="tag" title="see questions tagged &#39;vpn&#39;">vpn</span> <span class="post-tag tag-link-remote" rel="tag" title="see questions tagged &#39;remote&#39;">remote</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Sep '13, 18:54</strong></p><img src="https://secure.gravatar.com/avatar/779f6e84b70759bf1d8f2b2ae8e04bc3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DieTrying&#39;s gravatar image" /><p><span>DieTrying</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DieTrying has no accepted answers">0%</span></p></div></div><div id="comments-container-25088" class="comments-container"></div><div id="comment-tools-25088" class="comment-tools"></div><div class="clear"></div><div id="comment-25088-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="25102"></span>

<div id="answer-container-25102" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25102-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25102-score" class="post-score" title="current number of votes">0</div><span id="post-25102-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>If I tunnel in using VPN, **can I run wireshark on my machine in Beverly Hills to assess the 'test network' in Sherman Oaks?</p></blockquote><p>No.</p><p>Reason: How are the packets form Sherman Oaks supposed to get into the VPN tunnel to your Wireshark system in Beverly Hills?</p><blockquote><p>Or, should I screenshare with a machine on the local network in Sherman Oaks</p></blockquote><p>Yes.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Sep '13, 01:09</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-25102" class="comments-container"></div><div id="comment-tools-25102" class="comment-tools"></div><div class="clear"></div><div id="comment-25102-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

