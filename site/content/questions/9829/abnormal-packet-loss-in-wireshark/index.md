+++
type = "question"
title = "abnormal packet loss in wireshark"
description = '''I am testing the packETH 1.7 packet genrator and used wireshark 1.2.7 for same. I use two ubuntu 10.04 machine with 100Mb NIC and later added 1Gb NIC.Both machine are connected PtoP with CAT6 straight cable. Now i transmit 200000 udp packet at rate from 125packet/sec to 1000000packet/sec.I used wire...'''
date = "2012-03-28T17:28:00Z"
lastmod = "2012-03-28T17:28:00Z"
weight = 9829
keywords = [ "packetdrop", "packetloss" ]
aliases = [ "/questions/9829" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [abnormal packet loss in wireshark](/questions/9829/abnormal-packet-loss-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9829-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9829-score" class="post-score" title="current number of votes">0</div><span id="post-9829-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am testing the packETH 1.7 packet genrator and used wireshark 1.2.7 for same. I use two ubuntu 10.04 machine with 100Mb NIC and later added 1Gb NIC.Both machine are connected PtoP with CAT6 straight cable. Now i transmit 200000 udp packet at rate from 125packet/sec to 1000000packet/sec.I used wireshark on both machine to capture packet and measure performance of packet generator. The observation is that in some readings source machine wireshark capture less packets than destination machine wireshark. Why such abnormal packet loss observed on source side when destination machine get same lost packets?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packetdrop" rel="tag" title="see questions tagged &#39;packetdrop&#39;">packetdrop</span> <span class="post-tag tag-link-packetloss" rel="tag" title="see questions tagged &#39;packetloss&#39;">packetloss</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Mar '12, 17:28</strong></p><img src="https://secure.gravatar.com/avatar/4293f9055055778dd50bb7528761470f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Upendra%20Pathrikar&#39;s gravatar image" /><p><span>Upendra Path...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Upendra Pathrikar has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Mar '12, 17:36</strong> </span></p></div></div><div id="comments-container-9829" class="comments-container"></div><div id="comment-tools-9829" class="comment-tools"></div><div class="clear"></div><div id="comment-9829-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

