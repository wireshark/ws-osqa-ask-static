+++
type = "question"
title = "ethers file with v2.2.7-0-g1861a96"
description = '''v2.2.7-0-g1861a96 is not using ethers file from either the Personal configuration Location or System Location. Under Preferences | Appearance | Name Resolution | Resolve MAC addresses is selected. Is this a known issue or am I missing something?'''
date = "2017-06-08T07:40:00Z"
lastmod = "2017-10-07T14:16:00Z"
weight = 61867
keywords = [ "nameresolution", "ethers" ]
aliases = [ "/questions/61867" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [ethers file with v2.2.7-0-g1861a96](/questions/61867/ethers-file-with-v227-0-g1861a96)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61867-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61867-score" class="post-score" title="current number of votes">0</div><span id="post-61867-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>v2.2.7-0-g1861a96 is not using ethers file from either the Personal configuration Location or System Location. Under Preferences | Appearance | Name Resolution | Resolve MAC addresses is selected. Is this a known issue or am I missing something?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nameresolution" rel="tag" title="see questions tagged &#39;nameresolution&#39;">nameresolution</span> <span class="post-tag tag-link-ethers" rel="tag" title="see questions tagged &#39;ethers&#39;">ethers</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jun '17, 07:40</strong></p><img src="https://secure.gravatar.com/avatar/49b3bcf398ba78109e5a4a984b3dbe5c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="GaryRoger&#39;s gravatar image" /><p><span>GaryRoger</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="GaryRoger has no accepted answers">0%</span></p></div></div><div id="comments-container-61867" class="comments-container"><span id="61879"></span><div id="comment-61879" class="comment"><div id="post-61879-score" class="comment-score"></div><div class="comment-text"><p>Got mine in ~/.wireshark/ethers working just fine. Is that what you have?</p></div><div id="comment-61879-info" class="comment-info"><span class="comment-age">(08 Jun '17, 14:07)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="61907"></span><div id="comment-61907" class="comment"><div id="post-61907-score" class="comment-score"></div><div class="comment-text"><p>Personal configuration C:\Users\339472\AppData\Roaming\Wireshark\</p><p>System C:\Program Files\Wireshark</p><p>This is where the "About Wireshark" Folders tab shows that the ethers file should exist.</p></div><div id="comment-61907-info" class="comment-info"><span class="comment-age">(09 Jun '17, 06:15)</span> <span class="comment-user userinfo">GaryRoger</span></div></div><span id="63728"></span><div id="comment-63728" class="comment"><div id="post-63728-score" class="comment-score"></div><div class="comment-text"><p>I have it in ~/.wireshark/ethers, but it's not working for me? Reloading wireshark didn't work either</p></div><div id="comment-63728-info" class="comment-info"><span class="comment-age">(07 Oct '17, 12:44)</span> <span class="comment-user userinfo">naisanza</span></div></div><span id="63731"></span><div id="comment-63731" class="comment"><div id="post-63731-score" class="comment-score"></div><div class="comment-text"><p>so I assume you're <strong>not running Wireshark as root</strong>, and put the ethers file in your user home directory?</p></div><div id="comment-63731-info" class="comment-info"><span class="comment-age">(07 Oct '17, 14:16)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-61867" class="comment-tools"></div><div class="clear"></div><div id="comment-61867-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

