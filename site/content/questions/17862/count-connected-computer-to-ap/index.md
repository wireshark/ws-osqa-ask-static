+++
type = "question"
title = "count connected computer to ap"
description = '''I sysadmin of high school In my school I have about 9 ap with the same ssid and password ,also roaming enable  the ap connect to central switch. The ap not config as router  Who can I know which computer connect to spiffily ap or the number computer that connect to spiffily ap'''
date = "2013-01-22T11:03:00Z"
lastmod = "2013-01-22T12:45:00Z"
weight = 17862
keywords = [ "capture-filter" ]
aliases = [ "/questions/17862" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [count connected computer to ap](/questions/17862/count-connected-computer-to-ap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17862-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17862-score" class="post-score" title="current number of votes">0</div><span id="post-17862-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I sysadmin of high school In my school I have about 9 ap with the same ssid and password ,also roaming enable the ap connect to central switch. The ap not config as router Who can I know which computer connect to spiffily ap or the number computer that connect to spiffily ap</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jan '13, 11:03</strong></p><img src="https://secure.gravatar.com/avatar/f874a6fc34a6203cf4cf2678b7cec96a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Miki&#39;s gravatar image" /><p><span>Miki</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Miki has no accepted answers">0%</span></p></div></div><div id="comments-container-17862" class="comments-container"></div><div id="comment-tools-17862" class="comment-tools"></div><div class="clear"></div><div id="comment-17862-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17867"></span>

<div id="answer-container-17867" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17867-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17867-score" class="post-score" title="current number of votes">0</div><span id="post-17867-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You could try a tool like Kismet which will show you what clients are associated with which AP, but you'd need to be in range. Or you could do a Wireshark capture and find out yourself, but it would be more work than using a WLAN scanner like Kismet - and still you'd need to be in range.</p><p>Best solution would be to have APs running that have some kind of reporting functionality. If yours do not have that, you'll have to monitor access like I just described.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Jan '13, 12:45</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-17867" class="comments-container"></div><div id="comment-tools-17867" class="comment-tools"></div><div class="clear"></div><div id="comment-17867-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

