+++
type = "question"
title = "tshark defragmentation with http"
description = '''I want to get the complete POST request from tshark. I have applied this display filter  tshark -r test.pcap -2 -Y &quot;http and http.host==test.com and http.request.method==&quot;POST&quot;&quot; -T text but I didn&#x27;t get the complete request I only got a small part of the request.  In wireshark (GUI) I can see the fu...'''
date = "2014-10-27T12:51:00Z"
lastmod = "2014-10-27T12:51:00Z"
weight = 37376
keywords = [ "http", "tshark" ]
aliases = [ "/questions/37376" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [tshark defragmentation with http](/questions/37376/tshark-defragmentation-with-http)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37376-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37376-score" class="post-score" title="current number of votes">0</div><span id="post-37376-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to get the complete POST request from tshark. I have applied this display filter</p><p>tshark -r test.pcap -2 -Y "http and http.host==test.com and http.request.method=="POST"" -T text</p><p>but I didn't get the complete request I only got a small part of the request.</p><p>In wireshark (GUI) I can see the full request because it is the assembly of 3 packets.</p><p>Any ideas?</p><p>PS: I have tried the -2 option too</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Oct '14, 12:51</strong></p><img src="https://secure.gravatar.com/avatar/4991e2bdb134a1a649b81c1540ec76c8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="storm&#39;s gravatar image" /><p><span>storm</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="storm has no accepted answers">0%</span></p></div></div><div id="comments-container-37376" class="comments-container"></div><div id="comment-tools-37376" class="comment-tools"></div><div class="clear"></div><div id="comment-37376-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

