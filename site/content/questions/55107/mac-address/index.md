+++
type = "question"
title = "Mac Address"
description = '''Dear, I want to know about the mac addresses of internal network. The internal network running inside office and Switch port need to track NAT Internal traffic also of private IPs. Thank You'''
date = "2016-08-25T01:58:00Z"
lastmod = "2016-08-25T01:58:00Z"
weight = 55107
keywords = [ "mac" ]
aliases = [ "/questions/55107" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Mac Address](/questions/55107/mac-address)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55107-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55107-score" class="post-score" title="current number of votes">0</div><span id="post-55107-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear,</p><p>I want to know about the mac addresses of internal network. The internal network running inside office and Switch port need to track NAT Internal traffic also of private IPs.</p><p>Thank You</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Aug '16, 01:58</strong></p><img src="https://secure.gravatar.com/avatar/0edcec49a20010ba39500edf19cc9264?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MoMispl&#39;s gravatar image" /><p><span>MoMispl</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MoMispl has no accepted answers">0%</span></p></div></div><div id="comments-container-55107" class="comments-container"></div><div id="comment-tools-55107" class="comment-tools"></div><div class="clear"></div><div id="comment-55107-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

