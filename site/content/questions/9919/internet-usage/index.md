+++
type = "question"
title = "Internet Usage"
description = '''We are almost maxing out our isp bandwidth at certain times of the day. Using wireshark and Cascade, how can I determine which IP or MAC address is utilizing the most bandwidth?'''
date = "2012-04-03T11:19:00Z"
lastmod = "2012-04-03T11:19:00Z"
weight = 9919
keywords = [ "bandwidth", "internet" ]
aliases = [ "/questions/9919" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Internet Usage](/questions/9919/internet-usage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9919-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9919-score" class="post-score" title="current number of votes">0</div><span id="post-9919-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We are almost maxing out our isp bandwidth at certain times of the day. Using wireshark and Cascade, how can I determine which IP or MAC address is utilizing the most bandwidth?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bandwidth" rel="tag" title="see questions tagged &#39;bandwidth&#39;">bandwidth</span> <span class="post-tag tag-link-internet" rel="tag" title="see questions tagged &#39;internet&#39;">internet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Apr '12, 11:19</strong></p><img src="https://secure.gravatar.com/avatar/54514a872fe915e5bd82a60fbd474906?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jmillsapps&#39;s gravatar image" /><p><span>jmillsapps</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jmillsapps has no accepted answers">0%</span></p></div></div><div id="comments-container-9919" class="comments-container"></div><div id="comment-tools-9919" class="comment-tools"></div><div class="clear"></div><div id="comment-9919-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

