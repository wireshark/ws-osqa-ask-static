+++
type = "question"
title = "Why won&#x27;t Wireshark recognize Buffalo router?"
description = '''I have Verizon DSL with a Westell modem. I have attached a Buffalo N300 with DD-WRT router and have moved all of my devices to the Buffalo router and disabled the wireless on the Westell. Although the device list in Wireshark indicates that there is a wireless connection monitored (the Buffalo), the...'''
date = "2016-07-11T09:21:00Z"
lastmod = "2016-07-11T12:20:00Z"
weight = 53986
keywords = [ "router", "buffalo" ]
aliases = [ "/questions/53986" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why won't Wireshark recognize Buffalo router?](/questions/53986/why-wont-wireshark-recognize-buffalo-router)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53986-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53986-score" class="post-score" title="current number of votes">0</div><span id="post-53986-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have Verizon DSL with a Westell modem. I have attached a Buffalo N300 with DD-WRT router and have moved all of my devices to the Buffalo router and disabled the wireless on the Westell. Although the device list in Wireshark indicates that there is a wireless connection monitored (the Buffalo), the software is not identifying any packets transmitted. I have a laptop, two phones, and a Roku operating on wireless through the Buffalo at this time, but Wireshark does not see any activity. Please help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span> <span class="post-tag tag-link-buffalo" rel="tag" title="see questions tagged &#39;buffalo&#39;">buffalo</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jul '16, 09:21</strong></p><img src="https://secure.gravatar.com/avatar/95dd6a85f971b0c3809c9a9ea259a081?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Viletone&#39;s gravatar image" /><p><span>Viletone</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Viletone has no accepted answers">0%</span></p></div></div><div id="comments-container-53986" class="comments-container"></div><div id="comment-tools-53986" class="comment-tools"></div><div class="clear"></div><div id="comment-53986-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53992"></span>

<div id="answer-container-53992" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53992-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53992-score" class="post-score" title="current number of votes">0</div><span id="post-53992-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I suggest you start here:</p><p><a href="https://wiki.wireshark.org/CaptureSetup/WLAN">https://wiki.wireshark.org/CaptureSetup/WLAN</a></p><blockquote><blockquote><p>Although the device list in Wireshark indicates that there is a wireless connection monitored (the Buffalo), the software is not identifying any packets transmitted</p></blockquote></blockquote><p>I can't imagine a device could show up in the device list in Wireshark unless there are packets, so this can't be. I probably don't understand so I would recommend perhaps you add some screenshots and make a sample trace available after your work through the link above. Good packet captures on 802.11 takes a lot of work, sometimes requiring additional hardware and/or software.<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jul '16, 12:20</strong></p><img src="https://secure.gravatar.com/avatar/0a47ef51dd9c9996d194a4983295f5a4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bob%20Jones&#39;s gravatar image" /><p><span>Bob Jones</span><br />
<span class="score" title="1014 reputation points"><span>1.0k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bob Jones has 19 accepted answers">21%</span> </br></p></div></div><div id="comments-container-53992" class="comments-container"></div><div id="comment-tools-53992" class="comment-tools"></div><div class="clear"></div><div id="comment-53992-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

