+++
type = "question"
title = "messaging help"
description = '''Hi, I am trying to recover some messages on this capture and cant seem to find any at all. I was wondering if any of you could have a look for me if I provide the capture. It would be much appreciated. Thank you. https://www.dropbox.com/s/l0imj9yqileihv3/try.pcap?dl=0 '''
date = "2017-03-27T11:35:00Z"
lastmod = "2017-03-27T11:35:00Z"
weight = 60361
keywords = [ "wireshark" ]
aliases = [ "/questions/60361" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [messaging help](/questions/60361/messaging-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60361-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60361-score" class="post-score" title="current number of votes">0</div><span id="post-60361-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am trying to recover some messages on this capture and cant seem to find any at all. I was wondering if any of you could have a look for me if I provide the capture.</p><p>It would be much appreciated.</p><p>Thank you.</p><p><a href="https://www.dropbox.com/s/l0imj9yqileihv3/try.pcap?dl=0">https://www.dropbox.com/s/l0imj9yqileihv3/try.pcap?dl=0</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Mar '17, 11:35</strong></p><img src="https://secure.gravatar.com/avatar/9331fbc9e5ad1aec6c88229c257e2565?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="emma123&#39;s gravatar image" /><p><span>emma123</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="emma123 has no accepted answers">0%</span></p></div></div><div id="comments-container-60361" class="comments-container"></div><div id="comment-tools-60361" class="comment-tools"></div><div class="clear"></div><div id="comment-60361-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

