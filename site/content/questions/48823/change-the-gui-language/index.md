+++
type = "question"
title = "Change the GUI language"
description = '''Hi Support-Team, after upgrading to the version 2.0.1 the language of the GUI has been changed from english to german. I found no option via &quot;Settings&quot; to change the language back to english. Pls. let me know how it is possible to change the GUI language. Many thanks! Christian'''
date = "2016-01-04T02:03:00Z"
lastmod = "2016-01-04T03:14:00Z"
weight = 48823
keywords = [ "gui", "language" ]
aliases = [ "/questions/48823" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Change the GUI language](/questions/48823/change-the-gui-language)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48823-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48823-score" class="post-score" title="current number of votes">1</div><span id="post-48823-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Support-Team,</p><p>after upgrading to the version 2.0.1 the language of the GUI has been changed from english to german. I found no option via "Settings" to change the language back to english. Pls. let me know how it is possible to change the GUI language. Many thanks!</p><p>Christian</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gui" rel="tag" title="see questions tagged &#39;gui&#39;">gui</span> <span class="post-tag tag-link-language" rel="tag" title="see questions tagged &#39;language&#39;">language</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jan '16, 02:03</strong></p><img src="https://secure.gravatar.com/avatar/33463a360bd1f82471a1e709baf971a3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="soulbrother&#39;s gravatar image" /><p><span>soulbrother</span><br />
<span class="score" title="26 reputation points">26</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="soulbrother has no accepted answers">0%</span></p></div></div><div id="comments-container-48823" class="comments-container"></div><div id="comment-tools-48823" class="comment-tools"></div><div class="clear"></div><div id="comment-48823-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48825"></span>

<div id="answer-container-48825" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48825-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48825-score" class="post-score" title="current number of votes">4</div><span id="post-48825-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="soulbrother has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>From the Edit (Bearbeiten) menu, -&gt; Preferences (Einstellung), in the Appearance (Darstellung) item (or Ctrl + Shift + P), the bottom droplist sets the Language (Sprache). By default I think it follows the System Setting (Systemeinstellung verwerden).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jan '16, 02:32</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-48825" class="comments-container"><span id="48826"></span><div id="comment-48826" class="comment"><div id="post-48826-score" class="comment-score"></div><div class="comment-text"><p>Hi Grahamb,</p><p>perfect. Thank you!</p><p>Christian</p></div><div id="comment-48826-info" class="comment-info"><span class="comment-age">(04 Jan '16, 02:52)</span> <span class="comment-user userinfo">soulbrother</span></div></div><span id="48828"></span><div id="comment-48828" class="comment"><div id="post-48828-score" class="comment-score"></div><div class="comment-text"><p><span>@soulbrother</span></p><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-48828-info" class="comment-info"><span class="comment-age">(04 Jan '16, 03:14)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-48825" class="comment-tools"></div><div class="clear"></div><div id="comment-48825-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

