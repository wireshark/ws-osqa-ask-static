+++
type = "question"
title = "Windows 7 compatibility"
description = '''Will Wireshark work with Windows 7 workstations?'''
date = "2011-01-27T05:23:00Z"
lastmod = "2011-03-26T08:09:00Z"
weight = 1968
keywords = [ "windows7" ]
aliases = [ "/questions/1968" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Windows 7 compatibility](/questions/1968/windows-7-compatibility)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1968-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1968-score" class="post-score" title="current number of votes">0</div><span id="post-1968-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Will Wireshark work with Windows 7 workstations?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jan '11, 05:23</strong></p><img src="https://secure.gravatar.com/avatar/e4b2e17a43ee8e3087e747c40c900f76?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Cadeaui&#39;s gravatar image" /><p><span>Cadeaui</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Cadeaui has no accepted answers">0%</span></p></div></div><div id="comments-container-1968" class="comments-container"></div><div id="comment-tools-1968" class="comment-tools"></div><div class="clear"></div><div id="comment-1968-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1970"></span>

<div id="answer-container-1970" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1970-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1970-score" class="post-score" title="current number of votes">0</div><span id="post-1970-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes !</p><p>Administrative privileges will be required to do captures.</p><p>(Wireshark works fine on my Windows 7 system).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jan '11, 05:52</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div></div><div id="comments-container-1970" class="comments-container"><span id="2278"></span><div id="comment-2278" class="comment"><div id="post-2278-score" class="comment-score"></div><div class="comment-text"><p>I just download wire shark 64bit version and isn't activiting to capture traffic? Can any one give some feed back? thanks good day</p></div><div id="comment-2278-info" class="comment-info"><span class="comment-age">(10 Feb '11, 14:38)</span> <span class="comment-user userinfo">souloo</span></div></div><span id="3141"></span><div id="comment-3141" class="comment"><div id="post-3141-score" class="comment-score"></div><div class="comment-text"><p>I don't have administrative rights on my Win7 Pro (64 Bit) system. Capture works without a problem.</p></div><div id="comment-3141-info" class="comment-info"><span class="comment-age">(26 Mar '11, 05:20)</span> <span class="comment-user userinfo">harper</span></div></div><span id="3142"></span><div id="comment-3142" class="comment"><div id="post-3142-score" class="comment-score"></div><div class="comment-text"><p>That's probably because the NPF service is installed and running, allowing non administrative users to capture on the system.</p></div><div id="comment-3142-info" class="comment-info"><span class="comment-age">(26 Mar '11, 07:53)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="3143"></span><div id="comment-3143" class="comment"><div id="post-3143-score" class="comment-score"></div><div class="comment-text"><p>See http://wiki.wireshark.org/CaptureSetup/CapturePrivileges</p></div><div id="comment-3143-info" class="comment-info"><span class="comment-age">(26 Mar '11, 08:09)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-1970" class="comment-tools"></div><div class="clear"></div><div id="comment-1970-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

