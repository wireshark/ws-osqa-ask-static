+++
type = "question"
title = "Cannot run Wireshark on Mac OS Mavericks"
description = '''I&#x27;ve installed Wireshark on Mac OS Maverics and cannot run it - it displays the window which asks for the X11 the list does not contain anything similar to it. There is the button &quot;Browse&quot; but I do not know what should I browse for. I&#x27;ve installed Wireshark, then XQuarz, then reinstalled both and st...'''
date = "2013-12-02T18:35:00Z"
lastmod = "2014-09-11T14:12:00Z"
weight = 27683
keywords = [ "macosx", "mavericks" ]
aliases = [ "/questions/27683" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [Cannot run Wireshark on Mac OS Mavericks](/questions/27683/cannot-run-wireshark-on-mac-os-mavericks)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27683-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27683-score" class="post-score" title="current number of votes">0</div><span id="post-27683-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've installed Wireshark on Mac OS Maverics and cannot run it - it displays the window which asks for the X11 the list does not contain anything similar to it. There is the button "Browse" but I do not know what should I browse for.</p><p>I've installed Wireshark, then XQuarz, then reinstalled both and still have this issue.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span> <span class="post-tag tag-link-mavericks" rel="tag" title="see questions tagged &#39;mavericks&#39;">mavericks</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Dec '13, 18:35</strong></p><img src="https://secure.gravatar.com/avatar/5ae618e4950c81bf0d83d17c2c2ddaab?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AlexVN&#39;s gravatar image" /><p><span>AlexVN</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AlexVN has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Dec '13, 18:35</strong> </span></p></div></div><div id="comments-container-27683" class="comments-container"></div><div id="comment-tools-27683" class="comment-tools"></div><div class="clear"></div><div id="comment-27683-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="28153"></span>

<div id="answer-container-28153" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28153-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28153-score" class="post-score" title="current number of votes">1</div><span id="post-28153-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I thought it was an issue initially and was looking at this thread for a solution while I had Wireshark(X11 &amp; Wireshark Icon at dock) running and after few mins, It did load.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Dec '13, 06:14</strong></p><img src="https://secure.gravatar.com/avatar/377c78f86455e40cf52dd449e19bd72d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="GaNi&#39;s gravatar image" /><p><span>GaNi</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="GaNi has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Dec '13, 06:15</strong> </span></p></div></div><div id="comments-container-28153" class="comments-container"><span id="29492"></span><div id="comment-29492" class="comment"><div id="post-29492-score" class="comment-score"></div><div class="comment-text"><p>Wow, you're right - after about 5 minutes it finally got to the main window. Luckily after that first time it appears to load quickly thereafter.</p></div><div id="comment-29492-info" class="comment-info"><span class="comment-age">(06 Feb '14, 09:59)</span> <span class="comment-user userinfo">Hadriel</span></div></div><span id="29503"></span><div id="comment-29503" class="comment"><div id="post-29503-score" class="comment-score"></div><div class="comment-text"><p>If that's the first X11 app you've ever run - or maybe it's "if that's the first GTK+-based X11 app you've ever run" - I think there's some first-time font initialization process.</p><p>Hopefully the move to Qt will get rid of a lot of the crap you currently have to do when running Wireshark on OS X.</p></div><div id="comment-29503-info" class="comment-info"><span class="comment-age">(06 Feb '14, 15:37)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="29544"></span><div id="comment-29544" class="comment"><div id="post-29544-score" class="comment-score"></div><div class="comment-text"><p>No, I run wireshark all the time in X11... but from source I build on my Mac. This was the first time I tried a pre-built package from wireshark.org, in Mavericks. (I'd run pre-built on Lion and Mountain Lion before)</p><p>But yeah I'm loving the Qt version. I already prefer it over the X11 one, despite the missing stuff.</p></div><div id="comment-29544-info" class="comment-info"><span class="comment-age">(07 Feb '14, 20:16)</span> <span class="comment-user userinfo">Hadriel</span></div></div></div><div id="comment-tools-28153" class="comment-tools"></div><div class="clear"></div><div id="comment-28153-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="27690"></span>

<div id="answer-container-27690" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27690-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27690-score" class="post-score" title="current number of votes">0</div><span id="post-27690-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi!</p><p>Which versions of Wireshark and XQuartz you have installed? I've just tried to install the Wireshark 1.10.3 and XQuartz 2.7.5 - it works fine.</p><p>Small note: try to log off and log on again or reboot your computer for applying XQuartz settings.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Dec '13, 01:03</strong></p><img src="https://secure.gravatar.com/avatar/00fc9b7ddbee4ec657351cff07ace3ce?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrav&#39;s gravatar image" /><p><span>mrav</span><br />
<span class="score" title="16 reputation points">16</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrav has no accepted answers">0%</span></p></div></div><div id="comments-container-27690" class="comments-container"><span id="27700"></span><div id="comment-27700" class="comment"><div id="post-27700-score" class="comment-score"></div><div class="comment-text"><p>I've downloaded and installed <code>Wireshark 1.10.3 Intel 64.dmg</code> and <code>XQuartz-2.7.5.dmg</code>. Reboot did not help - still the same window asking for X11</p></div><div id="comment-27700-info" class="comment-info"><span class="comment-age">(03 Dec '13, 05:03)</span> <span class="comment-user userinfo">AlexVN</span></div></div><span id="27702"></span><div id="comment-27702" class="comment"><div id="post-27702-score" class="comment-score"></div><div class="comment-text"><p>Please try to run at first application XQuartz ("Applications"--&gt;"Utilities"). After that run a Wireshark.</p></div><div id="comment-27702-info" class="comment-info"><span class="comment-age">(03 Dec '13, 05:25)</span> <span class="comment-user userinfo">mrav</span></div></div><span id="27709"></span><div id="comment-27709" class="comment"><div id="post-27709-score" class="comment-score"></div><div class="comment-text"><p>XQuatz icon appears in the dock, but Wireshark still asks for X11.</p><p>So I've clicked Browse button and navigated to "Applications"--&gt;"Utilities"--&gt;"XQuartz". Now Wireshark displays icon in the doc and exits immediately.</p></div><div id="comment-27709-info" class="comment-info"><span class="comment-age">(03 Dec '13, 05:59)</span> <span class="comment-user userinfo">AlexVN</span></div></div><span id="27710"></span><div id="comment-27710" class="comment"><div id="post-27710-score" class="comment-score"></div><div class="comment-text"><p>Try running it from terminal, in this way you will see what it reports.</p></div><div id="comment-27710-info" class="comment-info"><span class="comment-age">(03 Dec '13, 06:15)</span> <span class="comment-user userinfo">Edmond</span></div></div><span id="27712"></span><div id="comment-27712" class="comment"><div id="post-27712-score" class="comment-score"></div><div class="comment-text"><p>Thank you - finally, it works when it is started from the XQuarz terminal. Starting it from OSX terminal gives this error:</p><pre><code>013-12-03 14:31:11.752 defaults[2296:507] 
The domain/default pair of (kCFPreferencesAnyApplication, AppleAquaColorVariant) does not exist
2013-12-03 14:31:11.762 defaults[2297:507] 
The domain/default pair of (kCFPreferencesAnyApplication, AppleHighlightColor) does not exist

(process:2286): Gtk-WARNING **: Locale not supported by C library.
    Using the fallback &#39;C&#39; locale.

(wireshark-bin:2286): Gtk-WARNING **: cannot open display:</code></pre><p>XQuarz is loaded, of course.</p></div><div id="comment-27712-info" class="comment-info"><span class="comment-age">(03 Dec '13, 06:34)</span> <span class="comment-user userinfo">AlexVN</span></div></div><span id="27733"></span><div id="comment-27733" class="comment not_top_scorer"><div id="post-27733-score" class="comment-score"></div><div class="comment-text"><p>From logs it is the same output, only that in my case i can open it through <em>Applications</em> &amp; <em>Terminal</em></p></div><div id="comment-27733-info" class="comment-info"><span class="comment-age">(03 Dec '13, 12:12)</span> <span class="comment-user userinfo">Edmond</span></div></div><span id="36215"></span><div id="comment-36215" class="comment not_top_scorer"><div id="post-36215-score" class="comment-score"></div><div class="comment-text"><blockquote><p>it works when it is started from the XQuarz terminal. Starting it from OSX terminal gives this error:</p></blockquote><p>...</p><blockquote><pre><code>(wireshark-bin:2286): Gtk-WARNING **: cannot open display:</code></pre></blockquote><p>If the DISPLAY environment variable isn't set in the OS X terminal (do <code>env | egrep DISPLAY</code>), that will prevent X11 apps from opening the display. After you install X11, you might have to close the OS X Terminal app and restart it in order to get DISPLAY set, or you might find that OS X Terminal windows opened after you install X11 have it set.</p></div><div id="comment-36215-info" class="comment-info"><span class="comment-age">(11 Sep '14, 14:12)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-27690" class="comment-tools"><span class="comments-showing"> showing 5 of 7 </span> <a href="#" class="show-all-comments-link">show 2 more comments</a></div><div class="clear"></div><div id="comment-27690-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="36211"></span>

<div id="answer-container-36211" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36211-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36211-score" class="post-score" title="current number of votes">0</div><span id="post-36211-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Same problem. I had Wireshark 1.6 Deleted it and got newest version 1.12. Works now.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Sep '14, 13:51</strong></p><img src="https://secure.gravatar.com/avatar/3ace5a4c69db95c89ac3cf35bd927fee?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Zombie&#39;s gravatar image" /><p><span>Zombie</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Zombie has no accepted answers">0%</span></p></div></div><div id="comments-container-36211" class="comments-container"></div><div id="comment-tools-36211" class="comment-tools"></div><div class="clear"></div><div id="comment-36211-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

