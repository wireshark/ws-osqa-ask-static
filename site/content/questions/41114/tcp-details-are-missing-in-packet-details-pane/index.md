+++
type = "question"
title = "TCP Details are missing in Packet Details Pane"
description = '''Hello Team, I&#x27;m using WireShark Version 1.12.4. I cannot see the traffic details for TCP in &quot;Packet Details&quot; section. All I can see is: Frame 1: ... Ethernet II: .... Internet Protocol Version 4: ... User Datagram Protocol: ... Cisco Hot Standby Router: .... If I install the same version in a VM mac...'''
date = "2015-04-01T15:52:00Z"
lastmod = "2015-04-06T15:51:00Z"
weight = 41114
keywords = [ "details", "tcp", "missing" ]
aliases = [ "/questions/41114" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [TCP Details are missing in Packet Details Pane](/questions/41114/tcp-details-are-missing-in-packet-details-pane)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41114-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41114-score" class="post-score" title="current number of votes">0</div><span id="post-41114-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Team,</p><p>I'm using WireShark Version 1.12.4. I cannot see the traffic details for TCP in "Packet Details" section. All I can see is:</p><p>Frame 1: ... Ethernet II: .... Internet Protocol Version 4: ... User Datagram Protocol: ... Cisco Hot Standby Router: ....</p><p>If I install the same version in a VM machine, I can see the traffic details for TCP.</p><p>Any suggestion on this please?</p><p>Moin<img src="https://osqa-ask.wireshark.org/upfiles/TCP.PNG" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-details" rel="tag" title="see questions tagged &#39;details&#39;">details</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span> <span class="post-tag tag-link-missing" rel="tag" title="see questions tagged &#39;missing&#39;">missing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Apr '15, 15:52</strong></p><img src="https://secure.gravatar.com/avatar/8a082088ae5f1d495da8ffa552531376?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="moin_sobhan&#39;s gravatar image" /><p><span>moin_sobhan</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="moin_sobhan has no accepted answers">0%</span></p></img></div></div><div id="comments-container-41114" class="comments-container"><span id="41123"></span><div id="comment-41123" class="comment"><div id="post-41123-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I cannot see the traffic details <strong>for TCP</strong> in "Packet Details" section.<br />
All <strong>I can see is: User Datagram Protocol:</strong></p></blockquote><p>User Datagram Protocol stands for UDP, so it's no wonder you don't see <strong>TCP</strong> Details in that frame ;-)</p><p>Sounds like a misunderstanding. Can you please add more details to your question?</p></div><div id="comment-41123-info" class="comment-info"><span class="comment-age">(02 Apr '15, 01:53)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="41235"></span><div id="comment-41235" class="comment"><div id="post-41235-score" class="comment-score"></div><div class="comment-text"><p>Thanks. I'm not sure if this is a Bug but I've noticed if I check/uncheck 4/5 times "Use promiscuous mode", WireShark starts capturing TCP information.</p></div><div id="comment-41235-info" class="comment-info"><span class="comment-age">(06 Apr '15, 15:51)</span> <span class="comment-user userinfo">moin_sobhan</span></div></div></div><div id="comment-tools-41114" class="comment-tools"></div><div class="clear"></div><div id="comment-41114-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

