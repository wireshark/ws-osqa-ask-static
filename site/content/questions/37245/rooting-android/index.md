+++
type = "question"
title = "rooting android"
description = '''can someone explain how to root android phones to capture network packets from wireshark'''
date = "2014-10-21T10:04:00Z"
lastmod = "2014-10-21T12:50:00Z"
weight = 37245
keywords = [ "android", "root" ]
aliases = [ "/questions/37245" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [rooting android](/questions/37245/rooting-android)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37245-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37245-score" class="post-score" title="current number of votes">0</div><span id="post-37245-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can someone explain how to root android phones to capture network packets from wireshark</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-android" rel="tag" title="see questions tagged &#39;android&#39;">android</span> <span class="post-tag tag-link-root" rel="tag" title="see questions tagged &#39;root&#39;">root</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Oct '14, 10:04</strong></p><img src="https://secure.gravatar.com/avatar/e35c18b4fe32a6eb0aa4b21c55dff6d5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Harish%20Vaibhav&#39;s gravatar image" /><p><span>Harish Vaibhav</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Harish Vaibhav has no accepted answers">0%</span></p></div></div><div id="comments-container-37245" class="comments-container"></div><div id="comment-tools-37245" class="comment-tools"></div><div class="clear"></div><div id="comment-37245-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37248"></span>

<div id="answer-container-37248" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37248-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37248-score" class="post-score" title="current number of votes">0</div><span id="post-37248-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>One example is <a href="http://balintreczey.hu/blog/run-wireshark-on-android-using-lil-debi/">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Oct '14, 10:48</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-37248" class="comments-container"><span id="37252"></span><div id="comment-37252" class="comment"><div id="post-37252-score" class="comment-score"></div><div class="comment-text"><p>That example requires the android device to be rooted already ;-)</p><p><span>@Harish</span>: googling could have helped, e.g.: <a href="http://www.androidcentral.com/root">http://www.androidcentral.com/root</a></p></div><div id="comment-37252-info" class="comment-info"><span class="comment-age">(21 Oct '14, 12:50)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-37248" class="comment-tools"></div><div class="clear"></div><div id="comment-37248-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

