+++
type = "question"
title = "Wireshark decoder for MPLS-TP"
description = '''Hi, Is a decoder for MPLS-TP in the works ? If yes, when is it likely to be available ?'''
date = "2011-03-31T10:45:00Z"
lastmod = "2011-04-01T07:41:00Z"
weight = 3248
keywords = [ "mpls" ]
aliases = [ "/questions/3248" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark decoder for MPLS-TP](/questions/3248/wireshark-decoder-for-mpls-tp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3248-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3248-score" class="post-score" title="current number of votes">0</div><span id="post-3248-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Is a decoder for MPLS-TP in the works ? If yes, when is it likely to be available ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mpls" rel="tag" title="see questions tagged &#39;mpls&#39;">mpls</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Mar '11, 10:45</strong></p><img src="https://secure.gravatar.com/avatar/bb2affb692adba23cd6bf0647af17743?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smashie&#39;s gravatar image" /><p><span>smashie</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smashie has no accepted answers">0%</span></p></div></div><div id="comments-container-3248" class="comments-container"></div><div id="comment-tools-3248" class="comment-tools"></div><div class="clear"></div><div id="comment-3248-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3271"></span>

<div id="answer-container-3271" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3271-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3271-score" class="post-score" title="current number of votes">0</div><span id="post-3271-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="http://www.wireshark.org/faq.html#q1.11">http://www.wireshark.org/faq.html#q1.11</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Apr '11, 07:41</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-3271" class="comments-container"></div><div id="comment-tools-3271" class="comment-tools"></div><div class="clear"></div><div id="comment-3271-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

