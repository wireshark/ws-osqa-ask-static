+++
type = "question"
title = "Sending data to script during live capture"
description = '''I want to send data from a live capture in wireshark to a python script during the live capture. I need this to be done in the window&#x27;s command line and to be all one command. I have tried piping to the script, but that interrupts the capture. Is this even remotely possible?'''
date = "2015-07-13T06:56:00Z"
lastmod = "2015-07-13T06:56:00Z"
weight = 44093
keywords = [ "python", "capture", "live", "script", "wireshark" ]
aliases = [ "/questions/44093" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Sending data to script during live capture](/questions/44093/sending-data-to-script-during-live-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44093-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44093-score" class="post-score" title="current number of votes">1</div><span id="post-44093-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to send data from a live capture in wireshark to a python script during the live capture. I need this to be done in the window's command line and to be all one command. I have tried piping to the script, but that interrupts the capture. Is this even remotely possible?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-python" rel="tag" title="see questions tagged &#39;python&#39;">python</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-live" rel="tag" title="see questions tagged &#39;live&#39;">live</span> <span class="post-tag tag-link-script" rel="tag" title="see questions tagged &#39;script&#39;">script</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jul '15, 06:56</strong></p><img src="https://secure.gravatar.com/avatar/e11c789e2599b67daa0b0db281ac60d2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dippy&#39;s gravatar image" /><p><span>dippy</span><br />
<span class="score" title="21 reputation points">21</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dippy has no accepted answers">0%</span></p></div></div><div id="comments-container-44093" class="comments-container"></div><div id="comment-tools-44093" class="comment-tools"></div><div class="clear"></div><div id="comment-44093-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

