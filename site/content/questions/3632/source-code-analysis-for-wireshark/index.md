+++
type = "question"
title = "Source Code Analysis for Wireshark"
description = '''Klocwork’s open source program would like to analyze Wirehshark using its static analysis product, Klocwork Insight, to give a report on bugs and potential security vulnerabilities. The results would be hosted on a secure web portal where contributors can access the results. The results will not be ...'''
date = "2011-04-20T08:33:00Z"
lastmod = "2011-04-20T09:15:00Z"
weight = 3632
keywords = [ "report", "sca", "bug" ]
aliases = [ "/questions/3632" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Source Code Analysis for Wireshark](/questions/3632/source-code-analysis-for-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3632-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3632-score" class="post-score" title="current number of votes">0</div><span id="post-3632-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Klocwork’s open source program would like to analyze Wirehshark using its static analysis product, Klocwork Insight, to give a report on bugs and potential security vulnerabilities. The results would be hosted on a secure web portal where contributors can access the results. The results will not be published. This program will be offered free to open source projects on an ongoing basis, so if the results are of value we could analyze future versions of the project as well.</p><p>Would this be of interest to any Wireshark contributors?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-report" rel="tag" title="see questions tagged &#39;report&#39;">report</span> <span class="post-tag tag-link-sca" rel="tag" title="see questions tagged &#39;sca&#39;">sca</span> <span class="post-tag tag-link-bug" rel="tag" title="see questions tagged &#39;bug&#39;">bug</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Apr '11, 08:33</strong></p><img src="https://secure.gravatar.com/avatar/826f88b7b31e3916b88199672116c785?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lgayowski&#39;s gravatar image" /><p><span>lgayowski</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lgayowski has no accepted answers">0%</span></p></div></div><div id="comments-container-3632" class="comments-container"></div><div id="comment-tools-3632" class="comment-tools"></div><div class="clear"></div><div id="comment-3632-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3635"></span>

<div id="answer-container-3635" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3635-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3635-score" class="post-score" title="current number of votes">1</div><span id="post-3635-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>We already have some code analysis running, but any new insights would be nice.</p><p>You better discuss this in the <a href="https://www.wireshark.org/mailman/listinfo/wireshark-dev">Wireshark Developers Mailinglist</a> though.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Apr '11, 09:15</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-3635" class="comments-container"></div><div id="comment-tools-3635" class="comment-tools"></div><div class="clear"></div><div id="comment-3635-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

