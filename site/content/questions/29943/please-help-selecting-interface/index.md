+++
type = "question"
title = "Please help selecting interface"
description = '''There is only Ethernet interface. i want windows interface for listen online game packets i installed and runned it via adminastrator what can be problem ?? this is picture of problem http://postimg.org/image/adgzdk8c9/'''
date = "2014-02-17T13:44:00Z"
lastmod = "2014-02-17T14:36:00Z"
weight = 29943
keywords = [ "interface" ]
aliases = [ "/questions/29943" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Please help selecting interface](/questions/29943/please-help-selecting-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29943-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29943-score" class="post-score" title="current number of votes">0</div><span id="post-29943-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>There is only Ethernet interface. i want windows interface for listen online game packets</p><p>i installed and runned it via adminastrator what can be problem ??</p><p>this is picture of problem <a href="http://postimg.org/image/adgzdk8c9/">http://postimg.org/image/adgzdk8c9/</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Feb '14, 13:44</strong></p><img src="https://secure.gravatar.com/avatar/f2cae51265554578c27f8abd11e3eeb0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ali%20An%C4%B1l%20Y%C3%BCcel&#39;s gravatar image" /><p><span>Ali Anıl Yücel</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ali Anıl Yücel has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Feb '14, 03:59</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-29943" class="comments-container"></div><div id="comment-tools-29943" class="comment-tools"></div><div class="clear"></div><div id="comment-29943-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29945"></span>

<div id="answer-container-29945" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29945-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29945-score" class="post-score" title="current number of votes">2</div><span id="post-29945-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Ali, There is no problem in the picture - it shows the normal start options dialogue interface. If you press "Start" you should see wireshark displaying packets flowing in and out of your PC.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Feb '14, 13:50</strong></p><img src="https://secure.gravatar.com/avatar/57fbbe2a1e14ccc2a681a28886e5a484?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="martyvis&#39;s gravatar image" /><p><span>martyvis</span><br />
<span class="score" title="891 reputation points">891</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="martyvis has 5 accepted answers">7%</span></p></div></div><div id="comments-container-29945" class="comments-container"><span id="29946"></span><div id="comment-29946" class="comment"><div id="post-29946-score" class="comment-score"></div><div class="comment-text"><p>Actually its not catching my game packets or maybe im newbie bcz of that, but thank you for fast comment</p></div><div id="comment-29946-info" class="comment-info"><span class="comment-age">(17 Feb '14, 13:54)</span> <span class="comment-user userinfo">Ali Anıl Yücel</span></div></div><span id="29947"></span><div id="comment-29947" class="comment"><div id="post-29947-score" class="comment-score">1</div><div class="comment-text"><p>It should capture those packets if you runnning the game on the same PC as Wireshark. If it is a different PC or maybe a console you will likely not see that traffic because of the way an Ethernet switch works. You may want to review <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">http://wiki.wireshark.org/CaptureSetup/Ethernet</a></p></div><div id="comment-29947-info" class="comment-info"><span class="comment-age">(17 Feb '14, 14:36)</span> <span class="comment-user userinfo">martyvis</span></div></div></div><div id="comment-tools-29945" class="comment-tools"></div><div class="clear"></div><div id="comment-29945-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

