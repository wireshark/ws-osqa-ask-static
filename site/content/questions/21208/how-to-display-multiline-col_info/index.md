+++
type = "question"
title = "How to display multiline COL_INFO ?"
description = '''I need the INFO_ COL to be displayed in multiple line if the text does not fit into width of the column. I couldn&#x27;t find any option inside Edit/Preferences.  (Note: Version 1.6.7)'''
date = "2013-05-17T00:54:00Z"
lastmod = "2013-05-17T06:31:00Z"
weight = 21208
keywords = [ "col_info", "multiline" ]
aliases = [ "/questions/21208" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to display multiline COL\_INFO ?](/questions/21208/how-to-display-multiline-col_info)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21208-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21208-score" class="post-score" title="current number of votes">0</div><span id="post-21208-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need the INFO_ COL to be displayed in multiple line if the text does not fit into width of the column. I couldn't find any option inside Edit/Preferences. (Note: Version 1.6.7)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-col_info" rel="tag" title="see questions tagged &#39;col_info&#39;">col_info</span> <span class="post-tag tag-link-multiline" rel="tag" title="see questions tagged &#39;multiline&#39;">multiline</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 May '13, 00:54</strong></p><img src="https://secure.gravatar.com/avatar/6a00de8bbb0f734aa577de7dd00b3e52?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="barisalis&#39;s gravatar image" /><p><span>barisalis</span><br />
<span class="score" title="16 reputation points">16</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="barisalis has one accepted answer">100%</span></p></div></div><div id="comments-container-21208" class="comments-container"></div><div id="comment-tools-21208" class="comment-tools"></div><div class="clear"></div><div id="comment-21208-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="21221"></span>

<div id="answer-container-21221" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21221-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21221-score" class="post-score" title="current number of votes">2</div><span id="post-21221-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can't. The INFO column is supposed to be a single line. You might want to reconsider putting so much information in the column...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 May '13, 06:23</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-21221" class="comments-container"><span id="21223"></span><div id="comment-21223" class="comment"><div id="post-21223-score" class="comment-score"></div><div class="comment-text"><p>We just have many different messages inside a single packet and need to display which messages are inside that packet. When we realize that the horizontal scroll-bar, of the packet list table, does not working properly, we thought we would do something inside the COL__ INFO. But we won't :)</p><p>Thanks for the answer.</p></div><div id="comment-21223-info" class="comment-info"><span class="comment-age">(17 May '13, 06:31)</span> <span class="comment-user userinfo">barisalis</span></div></div></div><div id="comment-tools-21221" class="comment-tools"></div><div class="clear"></div><div id="comment-21221-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

