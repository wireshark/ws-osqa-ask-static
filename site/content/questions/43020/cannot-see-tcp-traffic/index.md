+++
type = "question"
title = "Cannot see TCP traffic"
description = '''I have been using wireshark on ubuntu before, it was working correctly. Now I have installed Kali linux, and I cannot get wireshark to show TCP traffic. I did not set up an encryption key, I captured the eapol packages instead. All the 4 eapol packages are collected. On ubuntu wireshark could see th...'''
date = "2015-06-09T13:30:00Z"
lastmod = "2015-06-10T06:14:00Z"
weight = 43020
keywords = [ "kali", "wireshark" ]
aliases = [ "/questions/43020" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Cannot see TCP traffic](/questions/43020/cannot-see-tcp-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43020-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43020-score" class="post-score" title="current number of votes">0</div><span id="post-43020-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have been using wireshark on ubuntu before, it was working correctly. Now I have installed Kali linux, and I cannot get wireshark to show TCP traffic. I did not set up an encryption key, I captured the eapol packages instead. All the 4 eapol packages are collected. On ubuntu wireshark could see the TCP traffic from that point. I am using the same wifi, as it is mine. In the preferences, I have enabled the encryption and played around a little with the "Ignore protection bit" option, but didn't help either.</p><p>Edit: I am using my wifi with WPA2-PSK encryption</p><p>What am I missing? I suspect I have set some option long ago I don't remember...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-kali" rel="tag" title="see questions tagged &#39;kali&#39;">kali</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jun '15, 13:30</strong></p><img src="https://secure.gravatar.com/avatar/392600405170d10772bb7063d5fce27e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Regic&#39;s gravatar image" /><p><span>Regic</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Regic has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Jun '15, 13:32</strong> </span></p></div></div><div id="comments-container-43020" class="comments-container"><span id="43044"></span><div id="comment-43044" class="comment"><div id="post-43044-score" class="comment-score"></div><div class="comment-text"><p>These might be silly questions, but I have to ask: 1. Are you able to decrypt the WiFi capture on another machine that is not running Kali (i.e., save the encrypted capture and open it on a Windows, Mac, or other Linux distribution machine)?</p><ol><li><p>Are you certain that your capture contains TCP traffic?</p></li><li><p>Did you enter the correct SSID and passphrase? Sounds silly, but I have lost time because of a typographical error.</p></li><li><p>Did you toggle the Wireless toolbar decryption menu from Wireshark to None and then back to Wireshark? I am not certain if Kali displays the wireless toolbars - most Linux distributions do not.</p></li></ol></div><div id="comment-43044-info" class="comment-info"><span class="comment-age">(10 Jun '15, 06:14)</span> <span class="comment-user userinfo">Amato_C</span></div></div></div><div id="comment-tools-43020" class="comment-tools"></div><div class="clear"></div><div id="comment-43020-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

