+++
type = "question"
title = "windows 10 and win10pcap"
description = '''Trying to get wireshark to run on windows 10 build 10240. Running on ASUS laptop 64 bit home version. I loaded win10pcap and then wireshark. When I run wireshark it says npf driver not running. If I try sc start npf it says service does not exist. Thanks...Brian'''
date = "2015-08-18T20:50:00Z"
lastmod = "2015-08-19T07:49:00Z"
weight = 45226
keywords = [ "windows", "10", "win10pcap" ]
aliases = [ "/questions/45226" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [windows 10 and win10pcap](/questions/45226/windows-10-and-win10pcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45226-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45226-score" class="post-score" title="current number of votes">0</div><span id="post-45226-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Trying to get wireshark to run on windows 10 build 10240. Running on ASUS laptop 64 bit home version. I loaded win10pcap and then wireshark. When I run wireshark it says npf driver not running. If I try sc start npf it says service does not exist. Thanks...Brian</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-10" rel="tag" title="see questions tagged &#39;10&#39;">10</span> <span class="post-tag tag-link-win10pcap" rel="tag" title="see questions tagged &#39;win10pcap&#39;">win10pcap</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Aug '15, 20:50</strong></p><img src="https://secure.gravatar.com/avatar/b1f31e5570f5b425ca7f1552244a54e3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bmark&#39;s gravatar image" /><p><span>bmark</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bmark has no accepted answers">0%</span></p></div></div><div id="comments-container-45226" class="comments-container"><span id="45232"></span><div id="comment-45232" class="comment"><div id="post-45232-score" class="comment-score"></div><div class="comment-text"><p>WinPcap 4.1.3 is working fine on Windows 10. Did you try uninstalling win10pcap and installing WinPcap instead?</p></div><div id="comment-45232-info" class="comment-info"><span class="comment-age">(19 Aug '15, 02:29)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div></div><div id="comment-tools-45226" class="comment-tools"></div><div class="clear"></div><div id="comment-45226-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="45242"></span>

<div id="answer-container-45242" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45242-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45242-score" class="post-score" title="current number of votes">0</div><span id="post-45242-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Win10pcap does not install a driver called npf, so Wireshark complains about it not being present. If you see capture interfaces in the list you can ignore that message.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Aug '15, 07:49</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-45242" class="comments-container"></div><div id="comment-tools-45242" class="comment-tools"></div><div class="clear"></div><div id="comment-45242-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

