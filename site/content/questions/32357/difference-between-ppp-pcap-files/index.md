+++
type = "question"
title = "Difference between .PPP &amp; .PCAP files ?"
description = '''I&#x27;m capturing files using QxDM, for 3G: I&#x27;m getting PPP files for 4G: .PCAP files, can anyone please tell me the difference ?'''
date = "2014-05-01T10:15:00Z"
lastmod = "2014-05-01T10:30:00Z"
weight = 32357
keywords = [ "interface", "pcap", "ppp", "3g", "wireshark" ]
aliases = [ "/questions/32357" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Difference between .PPP & .PCAP files ?](/questions/32357/difference-between-ppp-pcap-files)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32357-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32357-score" class="post-score" title="current number of votes">0</div><span id="post-32357-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm capturing files using QxDM,</p><p>for 3G: I'm getting PPP files</p><p>for 4G: .PCAP files, can anyone please tell me the difference ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-ppp" rel="tag" title="see questions tagged &#39;ppp&#39;">ppp</span> <span class="post-tag tag-link-3g" rel="tag" title="see questions tagged &#39;3g&#39;">3g</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 May '14, 10:15</strong></p><img src="https://secure.gravatar.com/avatar/9bfd60ccb2b230b76a67d8bed3cd34c4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Onboard&#39;s gravatar image" /><p><span>Onboard</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Onboard has no accepted answers">0%</span></p></div></div><div id="comments-container-32357" class="comments-container"></div><div id="comment-tools-32357" class="comment-tools"></div><div class="clear"></div><div id="comment-32357-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32358"></span>

<div id="answer-container-32358" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32358-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32358-score" class="post-score" title="current number of votes">0</div><span id="post-32358-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><ul><li>PPP is probably the binary dump of a Point to Point Protocol (PPP is the legacy mode for 2G/3G devices)</li><li>PCAP is the libpcap format described <a href="http://wiki.wireshark.org/Development/LibpcapFileFormat">here</a></li></ul></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 May '14, 10:30</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-32358" class="comments-container"></div><div id="comment-tools-32358" class="comment-tools"></div><div class="clear"></div><div id="comment-32358-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

