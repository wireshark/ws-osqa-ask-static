+++
type = "question"
title = "Determine Physical USB Port in Wireshark Dump"
description = '''Is it possible to relate an usbmonX interface in Linux to a certain physical USB-interface on the host machine, with Wireshark? So you could say that packets Y (something like that): 27 6.062927000 3.3 host USB 79 URB_INTERRUPT in 28 6.062979000 host 3.3 USB 64 URB_INTERRUPT in  ...were captured fro...'''
date = "2014-08-21T14:11:00Z"
lastmod = "2014-08-21T14:11:00Z"
weight = 35660
keywords = [ "wireshark", "libusb", "usb", "usbmon", "linux" ]
aliases = [ "/questions/35660" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Determine Physical USB Port in Wireshark Dump](/questions/35660/determine-physical-usb-port-in-wireshark-dump)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35660-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35660-score" class="post-score" title="current number of votes">0</div><span id="post-35660-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to relate an usbmonX interface in Linux to a certain physical USB-interface on the host machine, with Wireshark? So you could say that packets Y (something like that):</p><pre><code>27  6.062927000 3.3 host    USB 79  URB_INTERRUPT in
28  6.062979000 host    3.3 USB 64  URB_INTERRUPT in</code></pre><p>...were captured from the USB interface Z.</p><p>I mean, while opening the interface overview and e.g. moving the mouse, I can see the packets increasing on usbmonX. But is it also possible to see the correlation between physical port and packets live by examining the dump?</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-libusb" rel="tag" title="see questions tagged &#39;libusb&#39;">libusb</span> <span class="post-tag tag-link-usb" rel="tag" title="see questions tagged &#39;usb&#39;">usb</span> <span class="post-tag tag-link-usbmon" rel="tag" title="see questions tagged &#39;usbmon&#39;">usbmon</span> <span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Aug '14, 14:11</strong></p><img src="https://secure.gravatar.com/avatar/ed42d94d476f543682fea8aab051d515?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rolf&#39;s gravatar image" /><p><span>Rolf</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rolf has no accepted answers">0%</span></p></div></div><div id="comments-container-35660" class="comments-container"></div><div id="comment-tools-35660" class="comment-tools"></div><div class="clear"></div><div id="comment-35660-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

