+++
type = "question"
title = "Protocol DIS"
description = '''I have a ton of these on my VOIP network.  Set Data [Malformed Packet] LE Fire Data query {Malformed Packet] Receiver Stop/Freeze What are all these? '''
date = "2016-12-05T10:43:00Z"
lastmod = "2016-12-12T08:32:00Z"
weight = 57873
keywords = [ "protocol", "dis" ]
aliases = [ "/questions/57873" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Protocol DIS](/questions/57873/protocol-dis)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57873-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57873-score" class="post-score" title="current number of votes">0</div><span id="post-57873-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a ton of these on my VOIP network.</p><p>Set Data [Malformed Packet] LE Fire Data query {Malformed Packet] Receiver Stop/Freeze</p><p>What are all these?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span> <span class="post-tag tag-link-dis" rel="tag" title="see questions tagged &#39;dis&#39;">dis</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Dec '16, 10:43</strong></p><img src="https://secure.gravatar.com/avatar/8b3a5f496dccbdfba2e2873875976f5f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sharkme&#39;s gravatar image" /><p><span>sharkme</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sharkme has no accepted answers">0%</span></p></div></div><div id="comments-container-57873" class="comments-container"></div><div id="comment-tools-57873" class="comment-tools"></div><div class="clear"></div><div id="comment-57873-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58021"></span>

<div id="answer-container-58021" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58021-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58021-score" class="post-score" title="current number of votes">0</div><span id="post-58021-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That is probably RTP traffic that is misinterpreted as DIS.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Dec '16, 08:32</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-58021" class="comments-container"></div><div id="comment-tools-58021" class="comment-tools"></div><div class="clear"></div><div id="comment-58021-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

