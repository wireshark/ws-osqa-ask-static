+++
type = "question"
title = "How to capture LLDP packets using Atheros AR8151 PCI-E gigabit Ethernet card"
description = '''I can not capture LLDP packets when using Atheros AR8151 PCI-E gigabit Ethernet card while using other Ethernet cards work fine. And I have update driver to 1.0.0.45 for Windows XP.'''
date = "2012-05-02T20:54:00Z"
lastmod = "2012-05-02T20:54:00Z"
weight = 10619
keywords = [ "lldp" ]
aliases = [ "/questions/10619" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture LLDP packets using Atheros AR8151 PCI-E gigabit Ethernet card](/questions/10619/how-to-capture-lldp-packets-using-atheros-ar8151-pci-e-gigabit-ethernet-card)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10619-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10619-score" class="post-score" title="current number of votes">0</div><span id="post-10619-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can not capture LLDP packets when using Atheros AR8151 PCI-E gigabit Ethernet card while using other Ethernet cards work fine. And I have update driver to 1.0.0.45 for Windows XP.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lldp" rel="tag" title="see questions tagged &#39;lldp&#39;">lldp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 May '12, 20:54</strong></p><img src="https://secure.gravatar.com/avatar/d6de905eb175878338b0714e4ba690b5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasmine&#39;s gravatar image" /><p><span>Jasmine</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasmine has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 May '12, 21:00</strong> </span></p></div></div><div id="comments-container-10619" class="comments-container"></div><div id="comment-tools-10619" class="comment-tools"></div><div class="clear"></div><div id="comment-10619-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

