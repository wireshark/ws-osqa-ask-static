+++
type = "question"
title = "CLI Identity"
description = '''Can you please help to find how can i check the correct Displayed Caller line Identity(CLI) on DNIS in wireshark trace.'''
date = "2015-03-13T08:56:00Z"
lastmod = "2015-03-13T08:56:00Z"
weight = 40539
keywords = [ "dnis", "cli" ]
aliases = [ "/questions/40539" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [CLI Identity](/questions/40539/cli-identity)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40539-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40539-score" class="post-score" title="current number of votes">0</div><span id="post-40539-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can you please help to find how can i check the correct Displayed Caller line Identity(CLI) on DNIS in wireshark trace.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dnis" rel="tag" title="see questions tagged &#39;dnis&#39;">dnis</span> <span class="post-tag tag-link-cli" rel="tag" title="see questions tagged &#39;cli&#39;">cli</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Mar '15, 08:56</strong></p><img src="https://secure.gravatar.com/avatar/e603764d3b5af3b14aa0eaab3f5692ce?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Deepak%20Rajput%20VGL&#39;s gravatar image" /><p><span>Deepak Rajpu...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Deepak Rajput VGL has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Mar '15, 09:30</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-40539" class="comments-container"></div><div id="comment-tools-40539" class="comment-tools"></div><div class="clear"></div><div id="comment-40539-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

