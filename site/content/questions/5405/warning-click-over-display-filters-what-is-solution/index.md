+++
type = "question"
title = "Warning click over display filters? What is solution?"
description = '''When I click on any display filters in the display-filter textbox, this warning happens: (lt-wireshark:4477): GLib-GObject-WARNING **: /build/buildd/glib2.0-2.24.1/gobj ct/gsignal.c:3079: signal name `depressed&#x27; is invalid for instance `0x8e39408&#x27;  What is the solution? Thanks.'''
date = "2011-08-02T09:57:00Z"
lastmod = "2011-08-08T02:55:00Z"
weight = 5405
keywords = [ "warning", "ubuntu" ]
aliases = [ "/questions/5405" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Warning click over display filters? What is solution?](/questions/5405/warning-click-over-display-filters-what-is-solution)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5405-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5405-score" class="post-score" title="current number of votes">0</div><span id="post-5405-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I click on any display filters in the display-filter textbox, this warning happens:</p><pre><code>(lt-wireshark:4477): GLib-GObject-WARNING **: /build/buildd/glib2.0-2.24.1/gobj
ct/gsignal.c:3079: signal name `depressed&#39; is invalid for instance `0x8e39408&#39;</code></pre><p>What is the solution?</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-warning" rel="tag" title="see questions tagged &#39;warning&#39;">warning</span> <span class="post-tag tag-link-ubuntu" rel="tag" title="see questions tagged &#39;ubuntu&#39;">ubuntu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Aug '11, 09:57</strong></p><img src="https://secure.gravatar.com/avatar/5905f96c1d7b3e960e209fd33429dfa4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ignacio%20Rivera&#39;s gravatar image" /><p><span>Ignacio Rivera</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ignacio Rivera has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Aug '11, 10:01</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-5405" class="comments-container"><span id="5407"></span><div id="comment-5407" class="comment"><div id="post-5407-score" class="comment-score"></div><div class="comment-text"><p>What do you mean by "click on ... display filters"? Are you talking about the drop-down list of recent display filters to the right of the "Filter:" box at the top of the Wireshark window?</p></div><div id="comment-5407-info" class="comment-info"><span class="comment-age">(02 Aug '11, 14:47)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="5525"></span><div id="comment-5525" class="comment"><div id="post-5525-score" class="comment-score"></div><div class="comment-text"><p>Yes, I mean, excuse my English.</p></div><div id="comment-5525-info" class="comment-info"><span class="comment-age">(05 Aug '11, 03:13)</span> <span class="comment-user userinfo">Ignacio Rivera</span></div></div><span id="5530"></span><div id="comment-5530" class="comment"><div id="post-5530-score" class="comment-score"></div><div class="comment-text"><p>What version of Wireshark is this?</p></div><div id="comment-5530-info" class="comment-info"><span class="comment-age">(05 Aug '11, 09:23)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="5531"></span><div id="comment-5531" class="comment"><div id="post-5531-score" class="comment-score"></div><div class="comment-text"><p>Version: 1.2.7</p></div><div id="comment-5531-info" class="comment-info"><span class="comment-age">(05 Aug '11, 09:55)</span> <span class="comment-user userinfo">Ignacio Rivera</span></div></div><span id="5532"></span><div id="comment-5532" class="comment"><div id="post-5532-score" class="comment-score"></div><div class="comment-text"><p>And my OS is Ubuntu 10.04 i386 Desktop</p></div><div id="comment-5532-info" class="comment-info"><span class="comment-age">(05 Aug '11, 09:56)</span> <span class="comment-user userinfo">Ignacio Rivera</span></div></div><span id="5562"></span><div id="comment-5562" class="comment not_top_scorer"><div id="post-5562-score" class="comment-score"></div><div class="comment-text"><p>Does this happen with newer versions of Wireshark? 1.2.7 is very old.</p><p>Is this a version you've built yourself (the "lt-wireshark" suggests it is)? If so, have you modified it?</p></div><div id="comment-5562-info" class="comment-info"><span class="comment-age">(07 Aug '11, 14:48)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-5405" class="comment-tools"><span class="comments-showing"> showing 5 of 6 </span> <a href="#" class="show-all-comments-link">show 1 more comments</a></div><div class="clear"></div><div id="comment-5405-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5570"></span>

<div id="answer-container-5570" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5570-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5570-score" class="post-score" title="current number of votes">0</div><span id="post-5570-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The 1.2 branch is <a href="http://wiki.wireshark.org/Development/LifeCycle">no longer maintained by us</a>. If you need it fixed in Wireshark 1.2.7 in Ubuntu 10.04 you'll have to report there.</p><p>Anyway, the issue is not a big issue, an annoyance it is.</p><p>Wireshark releases have progressed to the 1.4 and now 1.6 branch, and even the 1.2 branch has seen 11 further (maintenance-)releases since 1.2.7. These have become <a href="https://launchpad.net/ubuntu/+source/wireshark">available</a> in newer Ubuntu releases.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Aug '11, 02:55</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-5570" class="comments-container"></div><div id="comment-tools-5570" class="comment-tools"></div><div class="clear"></div><div id="comment-5570-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

