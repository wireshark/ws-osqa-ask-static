+++
type = "question"
title = "USSD session timeout"
description = '''My USSD conversation session give timeout after 45 second.abourt is coming from MS side but i don&#x27;t know which platform need to increase the timer to be avoid from this timeout.'''
date = "2013-06-25T01:21:00Z"
lastmod = "2013-06-25T03:06:00Z"
weight = 22307
keywords = [ "5555" ]
aliases = [ "/questions/22307" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [USSD session timeout](/questions/22307/ussd-session-timeout)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22307-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22307-score" class="post-score" title="current number of votes">0</div><span id="post-22307-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My USSD conversation session give timeout after 45 second.abourt is coming from MS side but i don't know which platform need to increase the timer to be avoid from this timeout.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-5555" rel="tag" title="see questions tagged &#39;5555&#39;">5555</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jun '13, 01:21</strong></p><img src="https://secure.gravatar.com/avatar/3758b972d853e9581da61ce3f7ae12bf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hafiz&#39;s gravatar image" /><p><span>Hafiz</span><br />
<span class="score" title="26 reputation points">26</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hafiz has no accepted answers">0%</span></p></div></div><div id="comments-container-22307" class="comments-container"><span id="22308"></span><div id="comment-22308" class="comment"><div id="post-22308-score" class="comment-score">1</div><div class="comment-text"><p>How is this a Wireshark question? You should ask the question in an appropriate support forum for the software you are using.</p></div><div id="comment-22308-info" class="comment-info"><span class="comment-age">(25 Jun '13, 01:49)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="22311"></span><div id="comment-22311" class="comment"><div id="post-22311-score" class="comment-score"></div><div class="comment-text"><p>thank Grhamb</p></div><div id="comment-22311-info" class="comment-info"><span class="comment-age">(25 Jun '13, 03:06)</span> <span class="comment-user userinfo">Hafiz</span></div></div></div><div id="comment-tools-22307" class="comment-tools"></div><div class="clear"></div><div id="comment-22307-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

