+++
type = "question"
title = "Problems sniffing in monitor mode on raspberry pi"
description = '''Hello community, Using a raspberry pi with a RT5370 wifi dongle I try to sniff another pc. After configuring a monitor device using airmon-ng I entered my WPA2 key in the wireshark options. When I sniff on mon0 I connect another computer to the internet and browse to some http sites. Back at my wire...'''
date = "2014-03-18T17:12:00Z"
lastmod = "2014-03-18T17:12:00Z"
weight = 30937
keywords = [ "eapol", "linux", "wpa2", "sniff", "monitor" ]
aliases = [ "/questions/30937" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Problems sniffing in monitor mode on raspberry pi](/questions/30937/problems-sniffing-in-monitor-mode-on-raspberry-pi)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30937-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30937-score" class="post-score" title="current number of votes">0</div><span id="post-30937-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello community,</p><p>Using a raspberry pi with a RT5370 wifi dongle I try to sniff another pc. After configuring a monitor device using airmon-ng I entered my WPA2 key in the wireshark options. When I sniff on mon0 I connect another computer to the internet and browse to some http sites. Back at my wireshark device I stop the capture and filter for EAPOL. I only traced EAPOL 2 and 4, the other two are missing. Therefore I am unable to see the http traffic. I tried this many times, but every time EAPOL 1/3 are missing. There are less than 7 meters between the computers.</p><p>I googled the problem for two days now, followed every tutorial I found. Please help me, I am getting sick of that. Yaron</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-eapol" rel="tag" title="see questions tagged &#39;eapol&#39;">eapol</span> <span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span> <span class="post-tag tag-link-wpa2" rel="tag" title="see questions tagged &#39;wpa2&#39;">wpa2</span> <span class="post-tag tag-link-sniff" rel="tag" title="see questions tagged &#39;sniff&#39;">sniff</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Mar '14, 17:12</strong></p><img src="https://secure.gravatar.com/avatar/175be8e3284c3552464ed56e2712add5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Yaron&#39;s gravatar image" /><p><span>Yaron</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Yaron has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Mar '14, 23:04</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-30937" class="comments-container"></div><div id="comment-tools-30937" class="comment-tools"></div><div class="clear"></div><div id="comment-30937-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

