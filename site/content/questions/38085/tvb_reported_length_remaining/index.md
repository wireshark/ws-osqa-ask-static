+++
type = "question"
title = "tvb_reported_length_remaining"
description = '''Hi  can anyone tell me what   tvb_reported_length_remaining (tvb, hdr); will do. to measure the Length ? what this function will return ? (guint8 || gint || int )  What is the main diff between  tvb_reported_length and tvb_reported_length_remaining  Thanks!'''
date = "2014-11-23T19:21:00Z"
lastmod = "2014-11-23T19:39:00Z"
weight = 38085
keywords = [ "reported", "length", "remaining", "tvb" ]
aliases = [ "/questions/38085" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [tvb\_reported\_length\_remaining](/questions/38085/tvb_reported_length_remaining)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38085-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38085-score" class="post-score" title="current number of votes">0</div><span id="post-38085-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>can anyone tell me what</p><pre><code>           tvb_reported_length_remaining (tvb, hdr); will do. to measure the Length ? what this function will return ? (guint8 || gint || int )</code></pre><p>What is the main diff between</p><pre><code>              tvb_reported_length and tvb_reported_length_remaining</code></pre><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-reported" rel="tag" title="see questions tagged &#39;reported&#39;">reported</span> <span class="post-tag tag-link-length" rel="tag" title="see questions tagged &#39;length&#39;">length</span> <span class="post-tag tag-link-remaining" rel="tag" title="see questions tagged &#39;remaining&#39;">remaining</span> <span class="post-tag tag-link-tvb" rel="tag" title="see questions tagged &#39;tvb&#39;">tvb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Nov '14, 19:21</strong></p><img src="https://secure.gravatar.com/avatar/1339589a92af9455063c09e56bfc6299?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="umar&#39;s gravatar image" /><p><span>umar</span><br />
<span class="score" title="26 reputation points">26</span><span title="22 badges"><span class="badge1">●</span><span class="badgecount">22</span></span><span title="24 badges"><span class="silver">●</span><span class="badgecount">24</span></span><span title="27 badges"><span class="bronze">●</span><span class="badgecount">27</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="umar has no accepted answers">0%</span></p></div></div><div id="comments-container-38085" class="comments-container"></div><div id="comment-tools-38085" class="comment-tools"></div><div class="clear"></div><div id="comment-38085-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38087"></span>

<div id="answer-container-38087" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38087-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38087-score" class="post-score" title="current number of votes">0</div><span id="post-38087-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <code>epan/tvbuff.h</code></p><p>(The best way to get information about the Wireshark API is to search the .h files (and,in many cases, to search/read <code>doc/README.dissector</code> and <code>doc/README.developer</code>)<br />
</p><p>:)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Nov '14, 19:39</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span> </br></p></div></div><div id="comments-container-38087" class="comments-container"></div><div id="comment-tools-38087" class="comment-tools"></div><div class="clear"></div><div id="comment-38087-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

