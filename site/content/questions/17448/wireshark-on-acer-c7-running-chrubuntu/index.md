+++
type = "question"
title = "Wireshark on Acer C7 running ChrUbuntu"
description = '''Hi- I recently installed ChrUbuntu on my Acer C7 ChromeBook in a dual boot configuration. While running ChrUbuntu I downloaded and installed Wireshark. It does not see any ethernet adapters, wireless or wired. Has anybody tried this and if so how did you get it to work? Thanks!'''
date = "2013-01-04T10:36:00Z"
lastmod = "2013-02-10T12:49:00Z"
weight = 17448
keywords = [ "c-7", "chrubuntu", "acer" ]
aliases = [ "/questions/17448" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark on Acer C7 running ChrUbuntu](/questions/17448/wireshark-on-acer-c7-running-chrubuntu)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17448-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17448-score" class="post-score" title="current number of votes">1</div><span id="post-17448-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi- I recently installed ChrUbuntu on my Acer C7 ChromeBook in a dual boot configuration. While running ChrUbuntu I downloaded and installed Wireshark. It does not see any ethernet adapters, wireless or wired. Has anybody tried this and if so how did you get it to work? Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-c-7" rel="tag" title="see questions tagged &#39;c-7&#39;">c-7</span> <span class="post-tag tag-link-chrubuntu" rel="tag" title="see questions tagged &#39;chrubuntu&#39;">chrubuntu</span> <span class="post-tag tag-link-acer" rel="tag" title="see questions tagged &#39;acer&#39;">acer</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jan '13, 10:36</strong></p><img src="https://secure.gravatar.com/avatar/ef6f005ba7f6e5db4176e832bbcae551?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="snivvy&#39;s gravatar image" /><p><span>snivvy</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="snivvy has no accepted answers">0%</span></p></div></div><div id="comments-container-17448" class="comments-container"></div><div id="comment-tools-17448" class="comment-tools"></div><div class="clear"></div><div id="comment-17448-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="17449"></span>

<div id="answer-container-17449" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17449-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17449-score" class="post-score" title="current number of votes">0</div><span id="post-17449-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please check if the <a href="http://wiki.wireshark.org/CaptureSetup/CapturePrivileges">Capture Privileges</a> are set properly on ChrUbuntu.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jan '13, 10:41</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Jan '13, 10:43</strong> </span></p></div></div><div id="comments-container-17449" class="comments-container"><span id="17452"></span><div id="comment-17452" class="comment"><div id="post-17452-score" class="comment-score"></div><div class="comment-text"><p>I'm not a Linux expert, but I am running as administrator. Should it work as administrator?</p></div><div id="comment-17452-info" class="comment-info"><span class="comment-age">(04 Jan '13, 11:48)</span> <span class="comment-user userinfo">snivvy</span></div></div><span id="17467"></span><div id="comment-17467" class="comment"><div id="post-17467-score" class="comment-score"></div><div class="comment-text"><p>I don't know what "administrator" means on ChrUbuntu.</p></div><div id="comment-17467-info" class="comment-info"><span class="comment-age">(04 Jan '13, 14:29)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="17470"></span><div id="comment-17470" class="comment"><div id="post-17470-score" class="comment-score"></div><div class="comment-text"><p>From what I can tell this is a group that allows you to do sudo. Which is perfect if you want to implement the changes as described in <a href="http://wiki.wireshark.org/CaptureSetup/CapturePrivileges">Capture Privileges</a>. Yes, this could/should be done automagically, go yell at the Ubuntu packagers.</p></div><div id="comment-17470-info" class="comment-info"><span class="comment-age">(04 Jan '13, 15:16)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-17449" class="comment-tools"></div><div class="clear"></div><div id="comment-17449-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="18478"></span>

<div id="answer-container-18478" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18478-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18478-score" class="post-score" title="current number of votes">0</div><span id="post-18478-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>setcap 'CAP_NET_RAW+eip CAP_NET_ADMIN+eip' /usr/bin/dumpcap</p><p>Running this command got Wireshark to see my wlan interface on the Samsung ARM chromebook.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Feb '13, 12:49</strong></p><img src="https://secure.gravatar.com/avatar/b8108b153c79a596c024254daa4b6a1f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cryptoOCDrob&#39;s gravatar image" /><p><span>cryptoOCDrob</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cryptoOCDrob has no accepted answers">0%</span></p></div></div><div id="comments-container-18478" class="comments-container"></div><div id="comment-tools-18478" class="comment-tools"></div><div class="clear"></div><div id="comment-18478-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

