+++
type = "question"
title = "uninstall wireshark"
description = '''when i launch wireshark it shows that the app has stopped working and not responding when i wanted to remove it i cant and show the massage &quot;wireshark or one of it&#x27;s associated programs is running&quot; what is the solution? thanks'''
date = "2016-10-29T05:42:00Z"
lastmod = "2016-10-29T05:55:00Z"
weight = 56813
keywords = [ "uninstall" ]
aliases = [ "/questions/56813" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [uninstall wireshark](/questions/56813/uninstall-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56813-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56813-score" class="post-score" title="current number of votes">0</div><span id="post-56813-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>when i launch wireshark it shows that the app has stopped working and not responding when i wanted to remove it i cant and show the massage "wireshark or one of it's associated programs is running" what is the solution? thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-uninstall" rel="tag" title="see questions tagged &#39;uninstall&#39;">uninstall</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Oct '16, 05:42</strong></p><img src="https://secure.gravatar.com/avatar/351aef697fcb9fde440b1bd54143300f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Iman&#39;s gravatar image" /><p><span>Iman</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Iman has no accepted answers">0%</span></p></div></div><div id="comments-container-56813" class="comments-container"></div><div id="comment-tools-56813" class="comment-tools"></div><div class="clear"></div><div id="comment-56813-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="56815"></span>

<div id="answer-container-56815" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56815-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56815-score" class="post-score" title="current number of votes">1</div><span id="post-56815-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>To remove Wireshark in this case, try rebooting your PC and then removing the application before doing anything else.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Oct '16, 05:55</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-56815" class="comments-container"></div><div id="comment-tools-56815" class="comment-tools"></div><div class="clear"></div><div id="comment-56815-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

