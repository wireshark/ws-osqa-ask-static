+++
type = "question"
title = "Preference warnings during Wireshark launch after Linux OS upgrade from SUSE SP2 to SP3"
description = '''How to fix these warning and does these errors impact wirkshark functions? '''
date = "2015-12-06T19:36:00Z"
lastmod = "2015-12-07T06:37:00Z"
weight = 48314
keywords = [ "warning", "preferences", "wireshark" ]
aliases = [ "/questions/48314" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Preference warnings during Wireshark launch after Linux OS upgrade from SUSE SP2 to SP3](/questions/48314/preference-warnings-during-wireshark-launch-after-linux-os-upgrade-from-suse-sp2-to-sp3)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48314-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48314-score" class="post-score" title="current number of votes">0</div><span id="post-48314-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How to fix these warning and does these errors impact wirkshark functions? <img src="https://osqa-ask.wireshark.org/upfiles/23123.JPG" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-warning" rel="tag" title="see questions tagged &#39;warning&#39;">warning</span> <span class="post-tag tag-link-preferences" rel="tag" title="see questions tagged &#39;preferences&#39;">preferences</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Dec '15, 19:36</strong></p><img src="https://secure.gravatar.com/avatar/0c584cc786e8f32c1a1b1464c46e5c72?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Estonia%20He&#39;s gravatar image" /><p><span>Estonia He</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Estonia He has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Dec '15, 23:45</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-48314" class="comments-container"></div><div id="comment-tools-48314" class="comment-tools"></div><div class="clear"></div><div id="comment-48314-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48316"></span>

<div id="answer-container-48316" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48316-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48316-score" class="post-score" title="current number of votes">0</div><span id="post-48316-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, follow the advise in the printout, save your preferences. The reason for the printout is that some preferences may have been removed or their name changed.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Dec '15, 23:07</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-48316" class="comments-container"><span id="48323"></span><div id="comment-48323" class="comment"><div id="post-48323-score" class="comment-score"></div><div class="comment-text"><p>As Anders said, it's not much to worry about. But it does appear that by upgrading your SUSE version you've actually <em>downgraded</em> the Wireshark version. Maybe someone had installed a custom-built version on SP2?</p><p>The reason I say this is that normally when you upgrade versions you shouldn't get "unknown preferences" warnings.</p></div><div id="comment-48323-info" class="comment-info"><span class="comment-age">(07 Dec '15, 06:37)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div></div><div id="comment-tools-48316" class="comment-tools"></div><div class="clear"></div><div id="comment-48316-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

