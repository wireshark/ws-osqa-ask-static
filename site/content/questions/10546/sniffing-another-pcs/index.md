+++
type = "question"
title = "sniffing another PC&#x27;s ?!"
description = '''can i start sniffing another pc&#x27;s packets without a LAN connection , maybe through remote network programs such as &quot;hamachi&quot; ? P.S. how can i see information which are encrypted (like accounts info , mmpor games actually ) ?'''
date = "2012-05-01T05:17:00Z"
lastmod = "2013-08-30T04:09:00Z"
weight = 10546
keywords = [ "sniffing", "encryption", "packets" ]
aliases = [ "/questions/10546" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [sniffing another PC's ?!](/questions/10546/sniffing-another-pcs)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10546-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10546-score" class="post-score" title="current number of votes">0</div><span id="post-10546-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can i start sniffing another pc's packets without a LAN connection , maybe through remote network programs such as "hamachi" ?</p><p>P.S. how can i see information which are encrypted (like accounts info , mmpor games actually ) ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-encryption" rel="tag" title="see questions tagged &#39;encryption&#39;">encryption</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 May '12, 05:17</strong></p><img src="https://secure.gravatar.com/avatar/2622ce1049136b7d110eaea594d2da51?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kimocool&#39;s gravatar image" /><p><span>kimocool</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kimocool has no accepted answers">0%</span></p></div></div><div id="comments-container-10546" class="comments-container"></div><div id="comment-tools-10546" class="comment-tools"></div><div class="clear"></div><div id="comment-10546-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="10555"></span>

<div id="answer-container-10555" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10555-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10555-score" class="post-score" title="current number of votes">1</div><span id="post-10555-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, you can't, unless you're able to use some tricks like ARP cache poisoning which might or might not work on a connection like hamachi.</p><p>BTW: Basically, your way of asking your question indicates that you're trying to find ways to spy on other people's data communications (and credentials). Besides being rude it is also illegal in most countries, so you better be VERY careful. Or to say it in other words: stop thinking about it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 May '12, 16:35</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 May '12, 16:36</strong> </span></p></div></div><div id="comments-container-10555" class="comments-container"><span id="10573"></span><div id="comment-10573" class="comment"><div id="post-10573-score" class="comment-score"></div><div class="comment-text"><p>thank you mr.Jasper for your answer , but i wish further assistant . P.S. i truly have spying intentions but not for stealing people's data , sometimes you'll have to make sure that your own daughter is well behaving on social websites ( you know what i mean )</p></div><div id="comment-10573-info" class="comment-info"><span class="comment-age">(02 May '12, 01:49)</span> <span class="comment-user userinfo">kimocool</span></div></div><span id="10574"></span><div id="comment-10574" class="comment"><div id="post-10574-score" class="comment-score"></div><div class="comment-text"><p>Okay, I understand your motivation - I still think it would be better to have a good talk with her to make sure she knows what's safe/off limits and what isn't :-)</p><p>Anyway, even if you could capture the traffic you probably will not be able to see account info etc since encryption is exactly meant to keep others from reading what's going on. So unless you have decryption keys Wireshark can't help even IF you manage to capture the data (and to get those keys you usually need administrative access to the server she's working with)</p></div><div id="comment-10574-info" class="comment-info"><span class="comment-age">(02 May '12, 02:11)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="10577"></span><div id="comment-10577" class="comment"><div id="post-10577-score" class="comment-score"></div><div class="comment-text"><p>I don't think a sniffer is a good choice here. You will not be able to monitor what your daughter is doing, as soon as the website offers/uses SSL/TLS. Furthermore you will need a lot of experience (sniffer, protocols) to analyze the traffic. However, based on your question, I assume this technology ist quite new to you.</p><p>I suggest to look for parental control software to keep an eye on your daughters activities.</p><p>Regards<br />
Kurt</p></div><div id="comment-10577-info" class="comment-info"><span class="comment-age">(02 May '12, 03:03)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="10578"></span><div id="comment-10578" class="comment"><div id="post-10578-score" class="comment-score"></div><div class="comment-text"><p>I did not mention "parental control software" because as far as my experience with that kind of software goes it is very likely that the parents are usually much less experienced in setting it up than their kids are in getting around it :-D</p></div><div id="comment-10578-info" class="comment-info"><span class="comment-age">(02 May '12, 03:06)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="10602"></span><div id="comment-10602" class="comment not_top_scorer"><div id="post-10602-score" class="comment-score"></div><div class="comment-text"><p>hehehe you know ... i've tried what people call "key logger" , it used to send keystrokes to my G-mail ,when i logged in my gmail ,i found a "don't try to mess with me" message and no any other msgs , i guess i was uncovered ... anyway , i'll try to find another way as i see decrypting packets looks like impossible ( is it ? ), thank you Jasper and Kurt for your assistant.</p></div><div id="comment-10602-info" class="comment-info"><span class="comment-age">(02 May '12, 07:47)</span> <span class="comment-user userinfo">kimocool</span></div></div><span id="10603"></span><div id="comment-10603" class="comment"><div id="post-10603-score" class="comment-score">1</div><div class="comment-text"><p>--- "don't try to mess with me"</p><p>THAT came from your daugther?? Well, if she is that clever (and detected your keylogger), you better ask HER how to sniff on her "social network traffic".</p><p>Decrypting packets would be impossible in your "scenario".</p></div><div id="comment-10603-info" class="comment-info"><span class="comment-age">(02 May '12, 07:52)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-10555" class="comment-tools"><span class="comments-showing"> showing 5 of 6 </span> <a href="#" class="show-all-comments-link">show 1 more comments</a></div><div class="clear"></div><div id="comment-10555-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="24192"></span>

<div id="answer-container-24192" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24192-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24192-score" class="post-score" title="current number of votes">0</div><span id="post-24192-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Firstly, if you want to see if she logs in, wipe the auto-fill on the form to sign in to the website, then go back later and type in one of every letter until it tries to guess. I catch people that way all the time for more serious things.</p><p>The connections tell you a lot, too, and just because the browser window isn't open doesn't mean the connections don't linger. Running "netstat -a -v" in CMD/terminal/command-prompt will sometimes reveal connections kept alive after being disconnected. Facebook connections are https and almost always have an "f" in the name. Here's an example where I had signed off for an entire day, but the connection still showed: "channel-ecmp-06-f:https CLOSE_WAIT" (I like to sit and watch this kind of thing sometimes)<br />
</p><p>You can password protect your computer's log folder. Select permissions that allow system access, but not user access without the key. Computers are basically just giant logging/tracking machines. lol</p><p>Just my experiences and observations over the years. =)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Aug '13, 04:09</strong></p><img src="https://secure.gravatar.com/avatar/14f060b714d9f178abe918e5f391dedb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AdrianThePhotog&#39;s gravatar image" /><p><span>AdrianThePhotog</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AdrianThePhotog has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-24192" class="comments-container"></div><div id="comment-tools-24192" class="comment-tools"></div><div class="clear"></div><div id="comment-24192-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

