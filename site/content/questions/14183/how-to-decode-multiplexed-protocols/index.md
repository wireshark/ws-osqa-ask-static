+++
type = "question"
title = "how to decode multiplexed protocols"
description = '''protocols like stun uses the same port as rtp and rtcp. if rtp is multiplexed with rtcp then three protocols uses the same udp port. how can all protocols be decoded correctly?'''
date = "2012-09-10T23:59:00Z"
lastmod = "2012-09-10T23:59:00Z"
weight = 14183
keywords = [ "rtcp", "rtp", "mux" ]
aliases = [ "/questions/14183" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to decode multiplexed protocols](/questions/14183/how-to-decode-multiplexed-protocols)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14183-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14183-score" class="post-score" title="current number of votes">0</div><span id="post-14183-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>protocols like stun uses the same port as rtp and rtcp. if rtp is multiplexed with rtcp then three protocols uses the same udp port. how can all protocols be decoded correctly?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtcp" rel="tag" title="see questions tagged &#39;rtcp&#39;">rtcp</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-mux" rel="tag" title="see questions tagged &#39;mux&#39;">mux</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Sep '12, 23:59</strong></p><img src="https://secure.gravatar.com/avatar/da49e0ad226c824f0b5e482806a6f4c9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Munich&#39;s gravatar image" /><p><span>Munich</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Munich has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>11 Sep '12, 00:02</strong> </span></p></div></div><div id="comments-container-14183" class="comments-container"></div><div id="comment-tools-14183" class="comment-tools"></div><div class="clear"></div><div id="comment-14183-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

