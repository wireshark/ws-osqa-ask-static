+++
type = "question"
title = "Mac Installer does not work correctly"
description = '''I’m running 10.12.5 on a late 2011 MacBook Pro 11,2. I’m using the Mac Intel 64, that was downloaded from your site. WILL YOU PLEASE FIX IT...'''
date = "2017-05-18T09:17:00Z"
lastmod = "2017-05-18T09:34:00Z"
weight = 61489
keywords = [ "installer", "mac" ]
aliases = [ "/questions/61489" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Mac Installer does not work correctly](/questions/61489/mac-installer-does-not-work-correctly)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61489-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61489-score" class="post-score" title="current number of votes">0</div><span id="post-61489-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I’m running 10.12.5 on a late 2011 MacBook Pro 11,2. I’m using the Mac Intel 64, that was downloaded from your site.</p><p>WILL YOU PLEASE FIX IT...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-installer" rel="tag" title="see questions tagged &#39;installer&#39;">installer</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 May '17, 09:17</strong></p><img src="https://secure.gravatar.com/avatar/1be648b45357666b07372ecae809c2f0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rbiam&#39;s gravatar image" /><p><span>rbiam</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rbiam has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 May '17, 09:39</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-61489" class="comments-container"><span id="61490"></span><div id="comment-61490" class="comment"><div id="post-61490-score" class="comment-score"></div><div class="comment-text"><p>You could be more helpful by explaining exactly what the failure is, but regardless of that the 1.12 versions are now unsupported so won't be "fixed".</p><p>Have you tried the current stable 2.2 release?</p></div><div id="comment-61490-info" class="comment-info"><span class="comment-age">(18 May '17, 09:34)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-61489" class="comment-tools"></div><div class="clear"></div><div id="comment-61489-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

