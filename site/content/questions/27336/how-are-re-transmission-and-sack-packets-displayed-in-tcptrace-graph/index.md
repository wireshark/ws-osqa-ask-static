+++
type = "question"
title = "How are re-transmission and SACK packets displayed in tcptrace graph?"
description = '''I have two captures (one with SACK and other one without SACK ). From tcptrace graph it is not clear how SACK feature affects the progress of sequence numbers. The graph is very hard to comprehend without any legends. And what are those dark black lines? SACK enabled:  Without SACK:  P.S: The pictur...'''
date = "2013-11-25T02:34:00Z"
lastmod = "2014-03-03T23:23:00Z"
weight = 27336
keywords = [ "tcptrace" ]
aliases = [ "/questions/27336" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How are re-transmission and SACK packets displayed in tcptrace graph?](/questions/27336/how-are-re-transmission-and-sack-packets-displayed-in-tcptrace-graph)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27336-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27336-score" class="post-score" title="current number of votes">0</div><span id="post-27336-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have two captures (one with SACK and other one without SACK ). From tcptrace graph it is not clear how SACK feature affects the progress of sequence numbers. The graph is very hard to comprehend without any legends. And what are those dark black lines?</p><p>SACK enabled: <img src="https://osqa-ask.wireshark.org/upfiles/tcp_sack.png" alt="SACK enabled" /></p><p>Without SACK: <img src="https://osqa-ask.wireshark.org/upfiles/tcp_nosack.png" alt="SACK disabled" /></p><p>P.S: The pictures are zoomed in versions of a larger capture file.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcptrace" rel="tag" title="see questions tagged &#39;tcptrace&#39;">tcptrace</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Nov '13, 02:34</strong></p><img src="https://secure.gravatar.com/avatar/e14ca2c421c54ea693198e806821f50d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="xkgt&#39;s gravatar image" /><p><span>xkgt</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="xkgt has no accepted answers">0%</span></p></img></div></div><div id="comments-container-27336" class="comments-container"><span id="30383"></span><div id="comment-30383" class="comment"><div id="post-30383-score" class="comment-score"></div><div class="comment-text"><p>check this out - <a href="http://www.tcptrace.org/manual/node12_mn.html">http://www.tcptrace.org/manual/node12_mn.html</a></p></div><div id="comment-30383-info" class="comment-info"><span class="comment-age">(03 Mar '14, 23:23)</span> <span class="comment-user userinfo">kishan pandey</span></div></div></div><div id="comment-tools-27336" class="comment-tools"></div><div class="clear"></div><div id="comment-27336-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

