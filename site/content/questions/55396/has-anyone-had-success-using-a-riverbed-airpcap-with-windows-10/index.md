+++
type = "question"
title = "Has anyone had success using a Riverbed AirPcap with Windows 10?"
description = '''Hello: I am interested in buying/using the Riverbed AirPcap Nx: USB 802.11a/b/g/n Adapter (capture + injection) Wireshark® Enhancement Product on a laptop running Windows 10. According to the Riverbed AirPcap website, Windows 10 is not listed as supported. And according to the Riverbed support team ...'''
date = "2016-09-08T08:52:00Z"
lastmod = "2016-09-08T16:16:00Z"
weight = 55396
keywords = [ "airpcap", "windows10" ]
aliases = [ "/questions/55396" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Has anyone had success using a Riverbed AirPcap with Windows 10?](/questions/55396/has-anyone-had-success-using-a-riverbed-airpcap-with-windows-10)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55396-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55396-score" class="post-score" title="current number of votes">0</div><span id="post-55396-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello: I am interested in buying/using the Riverbed AirPcap Nx: USB 802.11a/b/g/n Adapter (capture + injection) Wireshark® Enhancement Product on a laptop running Windows 10.</p><p>According to the Riverbed AirPcap website, Windows 10 is not listed as supported. And according to the Riverbed support team that replied to my email inquiry: "... Windows 10 is not supported. ..."</p><p>So it seems at this time Windows 10 is not officially supported... but will it work? Has anyone had success using a Riverbed AirPcap card with Windows 10?</p><p>Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-airpcap" rel="tag" title="see questions tagged &#39;airpcap&#39;">airpcap</span> <span class="post-tag tag-link-windows10" rel="tag" title="see questions tagged &#39;windows10&#39;">windows10</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Sep '16, 08:52</strong></p><img src="https://secure.gravatar.com/avatar/110564d17dbe73d2f04b89704e988a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mikes&#39;s gravatar image" /><p><span>mikes</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mikes has no accepted answers">0%</span></p></div></div><div id="comments-container-55396" class="comments-container"></div><div id="comment-tools-55396" class="comment-tools"></div><div class="clear"></div><div id="comment-55396-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="55399"></span>

<div id="answer-container-55399" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55399-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55399-score" class="post-score" title="current number of votes">0</div><span id="post-55399-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="mikes has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I've done some informal testing (capturing traffic) and it works without issues for me. Note that this was Win 10 build 1511, I haven't tested with the Anniversary Update build 1607 yet, but I wouldn't expect any issues.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Sep '16, 10:45</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-55399" class="comments-container"><span id="55404"></span><div id="comment-55404" class="comment"><div id="post-55404-score" class="comment-score"></div><div class="comment-text"><p>Thanks grahamb</p></div><div id="comment-55404-info" class="comment-info"><span class="comment-age">(08 Sep '16, 16:16)</span> <span class="comment-user userinfo">mikes</span></div></div></div><div id="comment-tools-55399" class="comment-tools"></div><div class="clear"></div><div id="comment-55399-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

