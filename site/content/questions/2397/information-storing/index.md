+++
type = "question"
title = "information storing"
description = '''here i want to store the packet details which are found by WIRE SHARK in a file.. what is the procedure for that.'''
date = "2011-02-17T01:17:00Z"
lastmod = "2011-02-17T01:37:00Z"
weight = 2397
keywords = [ "storage" ]
aliases = [ "/questions/2397" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [information storing](/questions/2397/information-storing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2397-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2397-score" class="post-score" title="current number of votes">0</div><span id="post-2397-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>here i want to store the packet details which are found by WIRE SHARK in a file.. what is the procedure for that.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-storage" rel="tag" title="see questions tagged &#39;storage&#39;">storage</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Feb '11, 01:17</strong></p><img src="https://secure.gravatar.com/avatar/dd5484aa3d325ef452ece41f0851d7d3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Karthik&#39;s gravatar image" /><p><span>Karthik</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Karthik has no accepted answers">0%</span></p></div></div><div id="comments-container-2397" class="comments-container"></div><div id="comment-tools-2397" class="comment-tools"></div><div class="clear"></div><div id="comment-2397-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2398"></span>

<div id="answer-container-2398" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2398-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2398-score" class="post-score" title="current number of votes">0</div><span id="post-2398-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I guess you want to export the decodes / packet list to a text file? You can do that by using "File" -&gt; "Export" -&gt; "File" and then either select "Plain Text" if you want Decode and/or Hex view, or "csv" if you want just the packet list as comma separated values. You can use the "packet range" settings to specify which packets you want to export.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Feb '11, 01:37</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-2398" class="comments-container"></div><div id="comment-tools-2398" class="comment-tools"></div><div class="clear"></div><div id="comment-2398-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

