+++
type = "question"
title = "SNMPV3 request not decrypted"
description = '''SNMPV3 priv requests are not decrypted but responses and traps are decrypted in Wireshark 1.6.3.  Wireshark just skips the data in SNMP-requests after the authentication header or becomes [Malformed Packet]. The SNMP-response to this, is decrypted and decoded correctly.'''
date = "2011-11-15T07:23:00Z"
lastmod = "2011-12-05T17:33:00Z"
weight = 7443
keywords = [ "snmpv3" ]
aliases = [ "/questions/7443" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [SNMPV3 request not decrypted](/questions/7443/snmpv3-request-not-decrypted)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7443-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7443-score" class="post-score" title="current number of votes">0</div><span id="post-7443-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>SNMPV3 priv requests are not decrypted but responses and traps are decrypted in Wireshark 1.6.3.</p><p>Wireshark just skips the data in SNMP-requests after the authentication header or becomes [Malformed Packet]. The SNMP-response to this, is decrypted and decoded correctly.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-snmpv3" rel="tag" title="see questions tagged &#39;snmpv3&#39;">snmpv3</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Nov '11, 07:23</strong></p><img src="https://secure.gravatar.com/avatar/8f47e01166210d76b0439c903d4d6f42?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="olw&#39;s gravatar image" /><p><span>olw</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="olw has no accepted answers">0%</span></p></div></div><div id="comments-container-7443" class="comments-container"><span id="7784"></span><div id="comment-7784" class="comment"><div id="post-7784-score" class="comment-score"></div><div class="comment-text"><p>Is this still a problem with Wireshark 1.6.4? If so, you might want to file a <a href="https://bugs.wireshark.org/bugzilla/">bug</a> report, or at least post a small capture file that depicts the problem to the <a href="http://www.wireshark.org/lists/">wireshark-dev</a> mailing list (or somewhere else) so someone could take a look at it.</p></div><div id="comment-7784-info" class="comment-info"><span class="comment-age">(05 Dec '11, 17:33)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-7443" class="comment-tools"></div><div class="clear"></div><div id="comment-7443-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

