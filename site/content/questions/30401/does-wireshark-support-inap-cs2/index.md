+++
type = "question"
title = "Does Wireshark support INAP CS2?"
description = '''I have a tcap message that I am trying to decode using wireshark that is a connectArg with a BearerCapability parameter. This parameter was added in cs2 and has a tag value that is context specific, is a constructor and has a value of 51. This ID will take up 2 octets. It decodes the tag properly bu...'''
date = "2014-03-04T08:36:00Z"
lastmod = "2014-03-04T08:36:00Z"
weight = 30401
keywords = [ "inap" ]
aliases = [ "/questions/30401" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Does Wireshark support INAP CS2?](/questions/30401/does-wireshark-support-inap-cs2)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30401-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30401-score" class="post-score" title="current number of votes">0</div><span id="post-30401-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a tcap message that I am trying to decode using wireshark that is a connectArg with a BearerCapability parameter. This parameter was added in cs2 and has a tag value that is context specific, is a constructor and has a value of 51. This ID will take up 2 octets. It decodes the tag properly but then gives a BER Error: This field lies beyond the end of the known sequence definition.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-inap" rel="tag" title="see questions tagged &#39;inap&#39;">inap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Mar '14, 08:36</strong></p><img src="https://secure.gravatar.com/avatar/25148bbcf297dd233893ff98ba4c6cec?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wgroleau&#39;s gravatar image" /><p><span>wgroleau</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wgroleau has no accepted answers">0%</span></p></div></div><div id="comments-container-30401" class="comments-container"></div><div id="comment-tools-30401" class="comment-tools"></div><div class="clear"></div><div id="comment-30401-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

