+++
type = "question"
title = "Only able to capture broadcast packets in monitor mode"
description = '''I&#x27;m setting my WLAN interface to monitor mode using &quot;airmon-ng start wlan0&quot; and it reports back that monitor mode has been started. However captures on mon0 only show broadcast or multicast packets. How reliable is airmons claim that monitor mode is on ?'''
date = "2012-11-08T07:41:00Z"
lastmod = "2012-11-08T07:41:00Z"
weight = 15724
keywords = [ "wlan", "airmon", "monitor", "mode" ]
aliases = [ "/questions/15724" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Only able to capture broadcast packets in monitor mode](/questions/15724/only-able-to-capture-broadcast-packets-in-monitor-mode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15724-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15724-score" class="post-score" title="current number of votes">0</div><span id="post-15724-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm setting my WLAN interface to monitor mode using "airmon-ng start wlan0" and it reports back that monitor mode has been started. However captures on mon0 only show broadcast or multicast packets.</p><p>How reliable is airmons claim that monitor mode is on ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wlan" rel="tag" title="see questions tagged &#39;wlan&#39;">wlan</span> <span class="post-tag tag-link-airmon" rel="tag" title="see questions tagged &#39;airmon&#39;">airmon</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span> <span class="post-tag tag-link-mode" rel="tag" title="see questions tagged &#39;mode&#39;">mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Nov '12, 07:41</strong></p><img src="https://secure.gravatar.com/avatar/afe39c5876518d84943dbc30652e38d8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jan&#39;s gravatar image" /><p><span>Jan</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jan has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Nov '12, 08:03</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-15724" class="comments-container"></div><div id="comment-tools-15724" class="comment-tools"></div><div class="clear"></div><div id="comment-15724-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

