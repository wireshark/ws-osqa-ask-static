+++
type = "question"
title = "How to remove tcp header?"
description = '''I am saving packets with multiple file or with dumpcap and I see like this :  € X þ $À6 @ íëÃNxXÀ¨£ P&amp;amp;E»kKí¨&amp;lt;PEà; HTTP/1.1 200 OK Cache-Control: private And I want to appear in file like this : HTTP/1.1 200 OK Cache-Control: private I am working on windows and for dumpcap I use this commands ...'''
date = "2014-07-18T02:15:00Z"
lastmod = "2014-07-18T02:15:00Z"
weight = 34747
keywords = [ "multiple-files", "header", "dumpcap", "tcp" ]
aliases = [ "/questions/34747" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to remove tcp header?](/questions/34747/how-to-remove-tcp-header)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34747-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34747-score" class="post-score" title="current number of votes">0</div><span id="post-34747-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am saving packets with multiple file or with dumpcap and I see like this : € X <code>þ $À6 @ íëÃNxXÀ¨£ P&amp;E»kKí¨&lt;</code>PEà;<br />
HTTP/1.1 200 OK Cache-Control: private</p><p>And I want to appear in file like this : HTTP/1.1 200 OK Cache-Control: private</p><p>I am working on windows and for dumpcap I use this commands : cd\pro <em>cd wires</em> tshark -D dumpcap -i 5 -s 96-w packets.cap</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-multiple-files" rel="tag" title="see questions tagged &#39;multiple-files&#39;">multiple-files</span> <span class="post-tag tag-link-header" rel="tag" title="see questions tagged &#39;header&#39;">header</span> <span class="post-tag tag-link-dumpcap" rel="tag" title="see questions tagged &#39;dumpcap&#39;">dumpcap</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jul '14, 02:15</strong></p><img src="https://secure.gravatar.com/avatar/539c5817ff1a73a599fb47b852dd1840?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="UserD&#39;s gravatar image" /><p><span>UserD</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="UserD has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-34747" class="comments-container"></div><div id="comment-tools-34747" class="comment-tools"></div><div class="clear"></div><div id="comment-34747-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

