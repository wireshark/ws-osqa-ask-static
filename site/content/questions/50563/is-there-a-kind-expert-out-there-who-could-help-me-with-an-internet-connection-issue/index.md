+++
type = "question"
title = "Is there a kind expert out there who could help me with an internet connection issue"
description = '''I have a problem with my windows 10 PC in that it wont open some secure websites (not all) I have tried all the usual things that I can think of but something seems to be making windows 10 think that I am using a proxy - the sites most affected are ones with a sign in - eg Windows Store - this wont ...'''
date = "2016-02-27T17:17:00Z"
lastmod = "2016-02-27T17:17:00Z"
weight = 50563
keywords = [ "ssl_connection", "internet_connection", "windows_10" ]
aliases = [ "/questions/50563" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Is there a kind expert out there who could help me with an internet connection issue](/questions/50563/is-there-a-kind-expert-out-there-who-could-help-me-with-an-internet-connection-issue)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50563-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50563-score" class="post-score" title="current number of votes">0</div><span id="post-50563-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a problem with my windows 10 PC in that it wont open some secure websites (not all) I have tried all the usual things that I can think of but something seems to be making windows 10 think that I am using a proxy - the sites most affected are ones with a sign in - eg Windows Store - this wont open in a browser or in the store app. My wb hosting and email provider 1and1.co.uk, any wiki site hosted by wikia.com. I either get an unable to connect message or I get a text only version of the web page which is unusable because the links dont connect. Another PC running Windows 8.1 and a Macbook are runnning on the same router connection and do not display the problem at all. I have been searching for a tool that can analyse my settings to find out what is blocking things. I have ruled out wiindows firewall and my anti virus by trying with these turned off but the problem subsists. Most MS experts tell me to perform a refresh or perform a clean install but I would prefer to diagnose and fix the issue as I do not want to lose my existing software - I have several software giveaways that would cost me a lot of money to re-install as well as one old microsoft program (Image Composer) which runs fine on windows 10 following my upgrade but I know that Windows 10 will not allow it to install as a new program.</p><p>I am competent with straightforward stuff but not when it comes to networks and apart from running it I would not know where to begin with this program nor would I know if it is even capable of giving me the information about what is wrong with my connection.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl_connection" rel="tag" title="see questions tagged &#39;ssl_connection&#39;">ssl_connection</span> <span class="post-tag tag-link-internet_connection" rel="tag" title="see questions tagged &#39;internet_connection&#39;">internet_connection</span> <span class="post-tag tag-link-windows_10" rel="tag" title="see questions tagged &#39;windows_10&#39;">windows_10</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Feb '16, 17:17</strong></p><img src="https://secure.gravatar.com/avatar/7aa4e44f8aef7005fff01d0a6c01a33b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gbswales&#39;s gravatar image" /><p><span>gbswales</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gbswales has no accepted answers">0%</span></p></div></div><div id="comments-container-50563" class="comments-container"></div><div id="comment-tools-50563" class="comment-tools"></div><div class="clear"></div><div id="comment-50563-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

