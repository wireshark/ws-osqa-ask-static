+++
type = "question"
title = "Please allow me to introduce Wireshark to our customer"
description = '''Our group company provides ecucational services. On this services, We&#x27;d like to introduce your product Wireshark. Specifically, on our educational text, We &#x27;d like to write real name, usage, with screen image of Wireshark. Would that be possible? If possible, in public seminar is included below, als...'''
date = "2014-07-18T00:33:00Z"
lastmod = "2014-07-18T00:40:00Z"
weight = 34743
keywords = [ "wireshark" ]
aliases = [ "/questions/34743" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Please allow me to introduce Wireshark to our customer](/questions/34743/please-allow-me-to-introduce-wireshark-to-our-customer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34743-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34743-score" class="post-score" title="current number of votes">0</div><span id="post-34743-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Our group company provides ecucational services. On this services, We'd like to introduce your product Wireshark. Specifically, on our educational text, We 'd like to write real name, usage, with screen image of Wireshark.</p><p>Would that be possible?</p><p>If possible, in public seminar is included below, also we will introduce.</p><p><a href="http://tcj.jp/news/">http://tcj.jp/news/</a></p><p>Sincerely,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jul '14, 00:33</strong></p><img src="https://secure.gravatar.com/avatar/91ba5f642ac9c5263beaaff59815ae38?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="fuku&#39;s gravatar image" /><p><span>fuku</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="fuku has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Jul '14, 01:46</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-34743" class="comments-container"></div><div id="comment-tools-34743" class="comment-tools"></div><div class="clear"></div><div id="comment-34743-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34744"></span>

<div id="answer-container-34744" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34744-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34744-score" class="post-score" title="current number of votes">0</div><span id="post-34744-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I think this question is quite similar: <a href="http://ask.wireshark.org/questions/28795/is-it-allowed-to-have-wireshark-screenshots-on-the-company-homepage">http://ask.wireshark.org/questions/28795/is-it-allowed-to-have-wireshark-screenshots-on-the-company-homepage</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jul '14, 00:40</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Jul '14, 00:41</strong> </span></p></div></div><div id="comments-container-34744" class="comments-container"></div><div id="comment-tools-34744" class="comment-tools"></div><div class="clear"></div><div id="comment-34744-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

