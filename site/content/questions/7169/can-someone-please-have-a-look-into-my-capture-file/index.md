+++
type = "question"
title = "Can someone please have a look into my capture file?"
description = '''I captured some packets sent to/from two websites i&#x27;m having problems with. However, unable to diagnose the problem myself so asking for help. Can someone please diagnose the problem for me? Thank you in advance '''
date = "2011-10-30T23:35:00Z"
lastmod = "2011-11-01T22:21:00Z"
weight = 7169
keywords = [ "requested", "analysis" ]
aliases = [ "/questions/7169" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can someone please have a look into my capture file?](/questions/7169/can-someone-please-have-a-look-into-my-capture-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7169-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7169-score" class="post-score" title="current number of votes">0</div><span id="post-7169-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I captured some packets sent to/from two websites i'm having problems with. However, unable to diagnose the problem myself so asking for help.</p><p>Can someone please diagnose the problem for me?</p><p>Thank you in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-requested" rel="tag" title="see questions tagged &#39;requested&#39;">requested</span> <span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Oct '11, 23:35</strong></p><img src="https://secure.gravatar.com/avatar/4555af4d8483ed553e2cea430a2a196f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ulznsn&#39;s gravatar image" /><p><span>ulznsn</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ulznsn has no accepted answers">0%</span></p></div></div><div id="comments-container-7169" class="comments-container"><span id="7188"></span><div id="comment-7188" class="comment"><div id="post-7188-score" class="comment-score"></div><div class="comment-text"><p>Hi Ulznsn, you have not attached the capture file</p></div><div id="comment-7188-info" class="comment-info"><span class="comment-age">(01 Nov '11, 22:21)</span> <span class="comment-user userinfo">deepacket</span></div></div></div><div id="comment-tools-7169" class="comment-tools"></div><div class="clear"></div><div id="comment-7169-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

