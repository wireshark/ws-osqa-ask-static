+++
type = "question"
title = "No packets"
description = '''I have Wireshark v 1.10.7 and WinPcap 4.1.3 on Windows 8.1 on Realtek RTL8187SE Wireless Network Adapter and I am trying to capture packets for an AP and it wont capture any packets. I went somewhere else with totally different AP&#x27;s and it captured packets. What do I do to capture packets?'''
date = "2014-06-07T16:51:00Z"
lastmod = "2014-06-09T10:28:00Z"
weight = 33541
keywords = [ "capture", "accesspoint", "error", "hack" ]
aliases = [ "/questions/33541" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [No packets](/questions/33541/no-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33541-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33541-score" class="post-score" title="current number of votes">0</div><span id="post-33541-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have Wireshark v 1.10.7 and WinPcap 4.1.3 on Windows 8.1 on Realtek RTL8187SE Wireless Network Adapter and I am trying to capture packets for an AP and it wont capture any packets. I went somewhere else with totally different AP's and it captured packets.</p><p>What do I do to capture packets?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-accesspoint" rel="tag" title="see questions tagged &#39;accesspoint&#39;">accesspoint</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span> <span class="post-tag tag-link-hack" rel="tag" title="see questions tagged &#39;hack&#39;">hack</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Jun '14, 16:51</strong></p><img src="https://secure.gravatar.com/avatar/86bc5c15a0a97c18a85bde864a1a9a6e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kprovost7314&#39;s gravatar image" /><p><span>kprovost7314</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kprovost7314 has no accepted answers">0%</span></p></div></div><div id="comments-container-33541" class="comments-container"><span id="33542"></span><div id="comment-33542" class="comment"><div id="post-33542-score" class="comment-score"></div><div class="comment-text"><p>and it uses 802.11b/g.</p></div><div id="comment-33542-info" class="comment-info"><span class="comment-age">(07 Jun '14, 16:52)</span> <span class="comment-user userinfo">kprovost7314</span></div></div></div><div id="comment-tools-33541" class="comment-tools"></div><div class="clear"></div><div id="comment-33541-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="33588"></span>

<div id="answer-container-33588" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33588-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33588-score" class="post-score" title="current number of votes">0</div><span id="post-33588-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>and it uses 802.11b/g.</p></blockquote><p>Well, maybe your AP was using only 802.11n (including all attached clients). Although I'm not absolutely sure if you won't see any frame at all with a 802.11b/g adapter, it could be a possible explanation. Can you please check that?</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Jun '14, 10:28</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-33588" class="comments-container"></div><div id="comment-tools-33588" class="comment-tools"></div><div class="clear"></div><div id="comment-33588-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

