+++
type = "question"
title = "Conversations"
description = '''I am developing a module for Wireshark and am in need of some help. I was wondering if there was any way in which to get all the packets from a specific conversation which is found in the conversations dialog. For example, with my module, I would like to be able to select a conversation and display ...'''
date = "2017-01-27T10:07:00Z"
lastmod = "2017-01-27T10:07:00Z"
weight = 59111
keywords = [ "development", "conversation", "code" ]
aliases = [ "/questions/59111" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Conversations](/questions/59111/conversations)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59111-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59111-score" class="post-score" title="current number of votes">0</div><span id="post-59111-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am developing a module for Wireshark and am in need of some help.</p><p>I was wondering if there was any way in which to get all the packets from a specific conversation which is found in the conversations dialog. For example, with my module, I would like to be able to select a conversation and display all the packets to do with that conversation without having to go through the filter system.</p><p>Is there any way in which to get a list of packets if I have the conversation ID within the existing code and if there isn't how would I go about implementing it?</p><p>Any help would be appreciated.</p><p>Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-development" rel="tag" title="see questions tagged &#39;development&#39;">development</span> <span class="post-tag tag-link-conversation" rel="tag" title="see questions tagged &#39;conversation&#39;">conversation</span> <span class="post-tag tag-link-code" rel="tag" title="see questions tagged &#39;code&#39;">code</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jan '17, 10:07</strong></p><img src="https://secure.gravatar.com/avatar/3b7eb282c454b776eac0e960a3798043?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ModuleMan&#39;s gravatar image" /><p><span>ModuleMan</span><br />
<span class="score" title="21 reputation points">21</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="11 badges"><span class="bronze">●</span><span class="badgecount">11</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ModuleMan has no accepted answers">0%</span></p></div></div><div id="comments-container-59111" class="comments-container"></div><div id="comment-tools-59111" class="comment-tools"></div><div class="clear"></div><div id="comment-59111-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

