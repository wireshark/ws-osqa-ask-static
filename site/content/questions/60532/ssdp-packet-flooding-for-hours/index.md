+++
type = "question"
title = "SSDP Packet Flooding for hours"
description = '''For the past 2 hours now, wireshark has picked up SSDP traffic (NOTIFY * HTTP/1.1) at rates of 20-30 kb/s. Glasswire has logged the SSDP traffic for 2 hours to my default gateway address. How do i stop this broadcast?'''
date = "2017-04-02T18:50:00Z"
lastmod = "2017-04-02T23:20:00Z"
weight = 60532
keywords = [ "flood", "ssdp", "notify" ]
aliases = [ "/questions/60532" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [SSDP Packet Flooding for hours](/questions/60532/ssdp-packet-flooding-for-hours)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60532-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60532-score" class="post-score" title="current number of votes">0</div><span id="post-60532-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>For the past 2 hours now, wireshark has picked up SSDP traffic (NOTIFY * HTTP/1.1) at rates of 20-30 kb/s. Glasswire has logged the SSDP traffic for 2 hours to my default gateway address. How do i stop this broadcast?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-flood" rel="tag" title="see questions tagged &#39;flood&#39;">flood</span> <span class="post-tag tag-link-ssdp" rel="tag" title="see questions tagged &#39;ssdp&#39;">ssdp</span> <span class="post-tag tag-link-notify" rel="tag" title="see questions tagged &#39;notify&#39;">notify</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Apr '17, 18:50</strong></p><img src="https://secure.gravatar.com/avatar/23b4ab997beb29a0384d78d6b45f6c85?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tsusurra&#39;s gravatar image" /><p><span>tsusurra</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tsusurra has no accepted answers">0%</span></p></div></div><div id="comments-container-60532" class="comments-container"></div><div id="comment-tools-60532" class="comment-tools"></div><div class="clear"></div><div id="comment-60532-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60536"></span>

<div id="answer-container-60536" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60536-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60536-score" class="post-score" title="current number of votes">0</div><span id="post-60536-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Stop the application or services that advertizes its services, because that is what this is for. Basically nothing to worry about.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Apr '17, 23:20</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-60536" class="comments-container"></div><div id="comment-tools-60536" class="comment-tools"></div><div class="clear"></div><div id="comment-60536-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

