+++
type = "question"
title = "Permission Error trying to setup remote interface"
description = '''Hi Trying to monitor a remote interface, I get permission denied error. Wireshark on windows. trying to set remote interface which is a linux machine. What sort of permissions do i need to give? Thanx Meher'''
date = "2014-06-01T13:57:00Z"
lastmod = "2014-06-01T14:21:00Z"
weight = 33245
keywords = [ "permission" ]
aliases = [ "/questions/33245" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Permission Error trying to setup remote interface](/questions/33245/permission-error-trying-to-setup-remote-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33245-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33245-score" class="post-score" title="current number of votes">0</div><span id="post-33245-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>Trying to monitor a remote interface, I get permission denied error.</p><p>Wireshark on windows. trying to set remote interface which is a linux machine.</p><p>What sort of permissions do i need to give?</p><p>Thanx</p><p>Meher</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-permission" rel="tag" title="see questions tagged &#39;permission&#39;">permission</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Jun '14, 13:57</strong></p><img src="https://secure.gravatar.com/avatar/021170936ec178ff7eedd3622e70285b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mg03&#39;s gravatar image" /><p><span>mg03</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mg03 has no accepted answers">0%</span></p></div></div><div id="comments-container-33245" class="comments-container"><span id="33246"></span><div id="comment-33246" class="comment"><div id="post-33246-score" class="comment-score"></div><div class="comment-text"><p>How did you prepare the Linux system? How did you start rpcapd?</p></div><div id="comment-33246-info" class="comment-info"><span class="comment-age">(01 Jun '14, 14:21)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-33245" class="comment-tools"></div><div class="clear"></div><div id="comment-33245-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

