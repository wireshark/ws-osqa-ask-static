+++
type = "question"
title = "How to Automate Export Object?"
description = '''Hi All, I am a newbie in using Wireshark.  I have captured some DICOM packets and I can export the original DICOM files using Wireshark.  Can I make it become a scheduled task? Seems tShark do not have the &quot;Export Object&quot; function. Is there any library I can use the &quot;Export Object&quot;, not by the UI? T...'''
date = "2013-12-10T18:28:00Z"
lastmod = "2013-12-11T23:34:00Z"
weight = 27984
keywords = [ "export" ]
aliases = [ "/questions/27984" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to Automate Export Object?](/questions/27984/how-to-automate-export-object)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27984-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27984-score" class="post-score" title="current number of votes">0</div><span id="post-27984-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>I am a newbie in using Wireshark.<br />
I have captured some DICOM packets and I can export the original DICOM files using Wireshark.<br />
</p><p>Can I make it become a scheduled task? Seems tShark do not have the "Export Object" function.<br />
Is there any library I can use the "Export Object", not by the UI? Thanks!</p><p><img src="https://osqa-ask.wireshark.org/upfiles/wireshark2.png" alt="alt text" /></p><p><img src="https://osqa-ask.wireshark.org/upfiles/wireshark_6.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-export" rel="tag" title="see questions tagged &#39;export&#39;">export</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Dec '13, 18:28</strong></p><img src="https://secure.gravatar.com/avatar/2df11aeed7dab4945ecf364da0634466?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jacky&#39;s gravatar image" /><p><span>jacky</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jacky has no accepted answers">0%</span> </br></br></p></img></div></div><div id="comments-container-27984" class="comments-container"><span id="28029"></span><div id="comment-28029" class="comment"><div id="post-28029-score" class="comment-score"></div><div class="comment-text"><p>Can anyone help? I am willing to pay for it.</p></div><div id="comment-28029-info" class="comment-info"><span class="comment-age">(11 Dec '13, 18:14)</span> <span class="comment-user userinfo">jacky</span></div></div><span id="28034"></span><div id="comment-28034" class="comment"><div id="post-28034-score" class="comment-score"></div><div class="comment-text"><p>I have spend some time on this issue in one of my project, but no good news yet...</p><p>I tried find this function in TShark but fail. I saw some linux guy suggested to use nFex, tcpxtract but I need solution on windows platform.</p><p>anyone could help on this question?</p></div><div id="comment-28034-info" class="comment-info"><span class="comment-age">(11 Dec '13, 23:34)</span> <span class="comment-user userinfo">wind snow</span></div></div></div><div id="comment-tools-27984" class="comment-tools"></div><div class="clear"></div><div id="comment-27984-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

