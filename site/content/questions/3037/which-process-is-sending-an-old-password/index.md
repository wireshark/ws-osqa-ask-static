+++
type = "question"
title = "Which process is sending an old password?"
description = '''I had to change my password, it is used for several purposes, eg. HTTP-proxy, netdrive-connections etc. Unfortunately, some process under the hood is still using the old password and this periodically loggs me out. How can I use wireshark to find this process? I have a 90% guess that is is via http-...'''
date = "2011-03-23T02:15:00Z"
lastmod = "2011-03-23T02:15:00Z"
weight = 3037
keywords = [ "password" ]
aliases = [ "/questions/3037" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Which process is sending an old password?](/questions/3037/which-process-is-sending-an-old-password)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3037-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3037-score" class="post-score" title="current number of votes">0</div><span id="post-3037-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I had to change my password, it is used for several purposes, eg. HTTP-proxy, netdrive-connections etc.</p><p>Unfortunately, some process under the hood is still using the old password and this periodically loggs me out.</p><p>How can I use wireshark to find this process?</p><p>I have a 90% guess that is is via http-proxy attempts, but I'm not sure.</p><p>Thx in advance, Pit.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-password" rel="tag" title="see questions tagged &#39;password&#39;">password</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Mar '11, 02:15</strong></p><img src="https://secure.gravatar.com/avatar/ff52520fcd788714bec0b890104445e1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pit&#39;s gravatar image" /><p><span>Pit</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pit has no accepted answers">0%</span></p></div></div><div id="comments-container-3037" class="comments-container"></div><div id="comment-tools-3037" class="comment-tools"></div><div class="clear"></div><div id="comment-3037-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

