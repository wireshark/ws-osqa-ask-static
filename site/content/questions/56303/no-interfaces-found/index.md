+++
type = "question"
title = "No interfaces found !!"
description = '''I have it open but it says no interfaces found? I am using a Ethernet cable so im not too sure if thats why? i need help!! Thanks J'''
date = "2016-10-11T14:08:00Z"
lastmod = "2016-10-13T13:28:00Z"
weight = 56303
keywords = [ "interface" ]
aliases = [ "/questions/56303" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [No interfaces found !!](/questions/56303/no-interfaces-found)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56303-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56303-score" class="post-score" title="current number of votes">0</div><span id="post-56303-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have it open but it says no interfaces found? I am using a Ethernet cable so im not too sure if thats why? i need help!!</p><p>Thanks J</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Oct '16, 14:08</strong></p><img src="https://secure.gravatar.com/avatar/f90d17faf2139f61fe4d1afe56eca589?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JKeaney&#39;s gravatar image" /><p><span>JKeaney</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JKeaney has no accepted answers">0%</span></p></div></div><div id="comments-container-56303" class="comments-container"></div><div id="comment-tools-56303" class="comment-tools"></div><div class="clear"></div><div id="comment-56303-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="56308"></span>

<div id="answer-container-56308" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56308-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56308-score" class="post-score" title="current number of votes">0</div><span id="post-56308-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Maybe this helps?</p><p><a href="https://ask.wireshark.org/questions/7523/ubuntu-machine-no-interfaces-listed">https://ask.wireshark.org/questions/7523/ubuntu-machine-no-interfaces-listed</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Oct '16, 02:28</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-56308" class="comments-container"><span id="56352"></span><div id="comment-56352" class="comment"><div id="post-56352-score" class="comment-score"></div><div class="comment-text"><p>Yea i dont know where i put the sudo code im still stick ;_;</p></div><div id="comment-56352-info" class="comment-info"><span class="comment-age">(13 Oct '16, 13:02)</span> <span class="comment-user userinfo">JKeaney</span></div></div><span id="56356"></span><div id="comment-56356" class="comment"><div id="post-56356-score" class="comment-score"></div><div class="comment-text"><p>Added that to the older question. Forgot to write there that you need to open a text terminal window to use the <code>sudo</code> commands.</p></div><div id="comment-56356-info" class="comment-info"><span class="comment-age">(13 Oct '16, 13:28)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-56308" class="comment-tools"></div><div class="clear"></div><div id="comment-56308-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

