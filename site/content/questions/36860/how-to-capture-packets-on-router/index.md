+++
type = "question"
title = "How to capture packets on router?"
description = '''How do I capture packets on router not on local machine. wiki says this is possible through Monitor Mode. But how do I set my adapter to monitor mode on Windows 8 ?( My adapter supports Promiscuous mode). Any help would be appropriated. Thank you'''
date = "2014-10-05T16:01:00Z"
lastmod = "2014-10-20T04:59:00Z"
weight = 36860
keywords = [ "router", "packets" ]
aliases = [ "/questions/36860" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture packets on router?](/questions/36860/how-to-capture-packets-on-router)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36860-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36860-score" class="post-score" title="current number of votes">0</div><span id="post-36860-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I capture packets on router not on local machine. wiki says this is possible through Monitor Mode. But how do I set my adapter to monitor mode on Windows 8 ?( My adapter supports Promiscuous mode). Any help would be appropriated.</p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Oct '14, 16:01</strong></p><img src="https://secure.gravatar.com/avatar/20b31d6cf505821f019b0e0966f36323?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Points&#39;s gravatar image" /><p><span>Points</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Points has no accepted answers">0%</span></p></div></div><div id="comments-container-36860" class="comments-container"></div><div id="comment-tools-36860" class="comment-tools"></div><div class="clear"></div><div id="comment-36860-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37189"></span>

<div id="answer-container-37189" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37189-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37189-score" class="post-score" title="current number of votes">0</div><span id="post-37189-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Monitor mode usually applies to wifi connections, is that what you're trying to do? Capture all wifi packets going to your wifi access point?</p><p>Or are you trying to capture all packets (wired and wireless) going through your router?</p><p>Regardless, for all capture related things, it's usually best to start with the <a href="http://wiki.wireshark.org/CaptureSetup">CaptureSetup</a> page on the wiki. If you are capturing wifi, go straight to the <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">WLAN</a> page.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Oct '14, 04:59</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-37189" class="comments-container"></div><div id="comment-tools-37189" class="comment-tools"></div><div class="clear"></div><div id="comment-37189-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

