+++
type = "question"
title = "Capturing and decoding http data from 802.11 packets"
description = '''I&#x27;m trying to capture http packets through Wireshark in monitor mode started by airmon-ng, bonded to the specific channel the desired SSID is broadcasting. It&#x27;s an open wireless network (no encryption), so I thought it would be easy to get complete HTTP packets after decoding. The problem is all the...'''
date = "2012-07-03T18:32:00Z"
lastmod = "2012-10-31T12:07:00Z"
weight = 12427
keywords = [ "open", "802.11", "network", "packet", "http" ]
aliases = [ "/questions/12427" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing and decoding http data from 802.11 packets](/questions/12427/capturing-and-decoding-http-data-from-80211-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12427-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12427-score" class="post-score" title="current number of votes">0</div><span id="post-12427-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm trying to capture http packets through Wireshark in monitor mode started by airmon-ng, bonded to the specific channel the desired SSID is broadcasting. It's an open wireless network (no encryption), so I thought it would be easy to get complete HTTP packets after decoding. The problem is all the 802.11 packets captured by Wireshark contain no data at all. They are described as Beacon Frames and contain nothing more than information about the adapter. What am I doing wrong?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-open" rel="tag" title="see questions tagged &#39;open&#39;">open</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jul '12, 18:32</strong></p><img src="https://secure.gravatar.com/avatar/4552ed53c41ee6a6b3861430ff47414d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="The_General&#39;s gravatar image" /><p><span>The_General</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="The_General has no accepted answers">0%</span></p></div></div><div id="comments-container-12427" class="comments-container"><span id="15431"></span><div id="comment-15431" class="comment"><div id="post-15431-score" class="comment-score"></div><div class="comment-text"><p>Hi</p><p>did get any solution for this problem i have the some problem and i searched for a days to have e repsone but no way</p><p>please share the infromation if you got any solution thanks</p></div><div id="comment-15431-info" class="comment-info"><span class="comment-age">(31 Oct '12, 12:07)</span> <span class="comment-user userinfo">Noury</span></div></div></div><div id="comment-tools-12427" class="comment-tools"></div><div class="clear"></div><div id="comment-12427-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

