+++
type = "question"
title = "Implementation of ISATAP Protocol and Wireshark"
description = '''Can anybody help me how to implement ISATAP packet ? I&#x27;m creating packets in C++ (Winpcap). I can&#x27;t imagine how it should be. Specification: http://www.networksorcery.com/enp/protocol/isatap.htm Is that example of ISATAP packet ? 0000 5EFE C0A8 0110 (IP Address - 192.168.1.16) 4548 9559 (Some data) ...'''
date = "2015-04-09T07:10:00Z"
lastmod = "2015-04-09T07:10:00Z"
weight = 41324
keywords = [ "winpcap", "c++" ]
aliases = [ "/questions/41324" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Implementation of ISATAP Protocol and Wireshark](/questions/41324/implementation-of-isatap-protocol-and-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41324-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41324-score" class="post-score" title="current number of votes">0</div><span id="post-41324-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can anybody help me how to implement ISATAP packet ? I'm creating packets in C++ (Winpcap). I can't imagine how it should be.</p><p>Specification: <a href="http://www.networksorcery.com/enp/protocol/isatap.htm">http://www.networksorcery.com/enp/protocol/isatap.htm</a></p><p>Is that example of ISATAP packet ?</p><pre><code>0000 5EFE
C0A8 0110 (IP Address - 192.168.1.16)
4548 9559 (Some data)</code></pre><p>Because, when I open .pcap in Wireshark, it don't recognize that.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span> <span class="post-tag tag-link-c++" rel="tag" title="see questions tagged &#39;c++&#39;">c++</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Apr '15, 07:10</strong></p><img src="https://secure.gravatar.com/avatar/2d2f216d2b0ed0b9501a16ae36b5bc61?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Igerko&#39;s gravatar image" /><p><span>Igerko</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Igerko has no accepted answers">0%</span></p></div></div><div id="comments-container-41324" class="comments-container"></div><div id="comment-tools-41324" class="comment-tools"></div><div class="clear"></div><div id="comment-41324-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

