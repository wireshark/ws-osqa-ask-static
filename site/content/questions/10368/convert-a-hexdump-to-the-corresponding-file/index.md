+++
type = "question"
title = "Convert a hexdump to the corresponding file."
description = '''I downloaded a zip file and have the hexdump,c Arrays output in wireshark. How would i convert this back to the zip file.'''
date = "2012-04-20T22:44:00Z"
lastmod = "2012-04-20T22:44:00Z"
weight = 10368
keywords = [ "wireshark1.2.5" ]
aliases = [ "/questions/10368" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Convert a hexdump to the corresponding file.](/questions/10368/convert-a-hexdump-to-the-corresponding-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10368-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10368-score" class="post-score" title="current number of votes">0</div><span id="post-10368-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I downloaded a zip file and have the hexdump,c Arrays output in wireshark. How would i convert this back to the zip file.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark1.2.5" rel="tag" title="see questions tagged &#39;wireshark1.2.5&#39;">wireshark1.2.5</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Apr '12, 22:44</strong></p><img src="https://secure.gravatar.com/avatar/110009a3f82707f54b04f7ef73b9d32b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Developer&#39;s gravatar image" /><p><span>Developer</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Developer has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Apr '12, 22:45</strong> </span></p></div></div><div id="comments-container-10368" class="comments-container"></div><div id="comment-tools-10368" class="comment-tools"></div><div class="clear"></div><div id="comment-10368-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

