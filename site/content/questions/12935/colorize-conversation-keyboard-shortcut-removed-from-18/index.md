+++
type = "question"
title = "Colorize Conversation keyboard shortcut removed from 1.8?"
description = '''Hello, In WireShark 1.6 and 1.4 (maybe earlier I haven&#x27;t checked), CTRL+1 through CTRL+0 would do the action of View &amp;gt; Colorize Conversation &amp;gt; Color 1 (or more specifically, it would colorize the TCP stream). I am unable to do this same keyboard shortcut in 1.8. I did not see this listed in th...'''
date = "2012-07-23T16:37:00Z"
lastmod = "2012-07-23T22:52:00Z"
weight = 12935
keywords = [ "shortkey", "colorize" ]
aliases = [ "/questions/12935" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Colorize Conversation keyboard shortcut removed from 1.8?](/questions/12935/colorize-conversation-keyboard-shortcut-removed-from-18)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12935-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12935-score" class="post-score" title="current number of votes">0</div><span id="post-12935-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>In WireShark 1.6 and 1.4 (maybe earlier I haven't checked), CTRL+1 through CTRL+0 would do the action of View &gt; Colorize Conversation &gt; Color 1 (or more specifically, it would colorize the TCP stream).</p><p>I am unable to do this same keyboard shortcut in 1.8. I did not see this listed in the release notes, and am not sure why such a feature would be removed but not replaced with anything else (CTRL+1 doesn't seem to do anything). CTRL+&lt;space&gt; still works to clear the colorized conversations.</p><p>Was this intentional, and if yes what is the rationale? Is there a way to bring this shortcut back without needing to use an external script? I had to use a script to bring back the "middle-click button marks the packet" feature.</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-shortkey" rel="tag" title="see questions tagged &#39;shortkey&#39;">shortkey</span> <span class="post-tag tag-link-colorize" rel="tag" title="see questions tagged &#39;colorize&#39;">colorize</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jul '12, 16:37</strong></p><img src="https://secure.gravatar.com/avatar/6af533edcd07f58511d208454431454d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="thechaosmachine&#39;s gravatar image" /><p><span>thechaosmachine</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="thechaosmachine has no accepted answers">0%</span></p></div></div><div id="comments-container-12935" class="comments-container"></div><div id="comment-tools-12935" class="comment-tools"></div><div class="clear"></div><div id="comment-12935-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12937"></span>

<div id="answer-container-12937" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12937-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12937-score" class="post-score" title="current number of votes">2</div><span id="post-12937-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="thechaosmachine has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's a bug please open a bug report.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jul '12, 22:09</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-12937" class="comments-container"><span id="12939"></span><div id="comment-12939" class="comment"><div id="post-12939-score" class="comment-score"></div><div class="comment-text"><p>Never mind a fix Committed revision 43953.</p></div><div id="comment-12939-info" class="comment-info"><span class="comment-age">(23 Jul '12, 22:52)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-12937" class="comment-tools"></div><div class="clear"></div><div id="comment-12937-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

