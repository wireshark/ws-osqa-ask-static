+++
type = "question"
title = "How to get window  &quot;endpoint&quot; information from command line"
description = '''Hi there, I neeed to analyse a bunch of shark capture data, and to move some result fields to excel sheets. Most of them Im getting from capinfos but, in order to obtain the total amount transfered and received per ip , Im still using the usual and manual method (open the *.pcap whith wireshark, loo...'''
date = "2011-09-23T08:29:00Z"
lastmod = "2011-09-23T10:33:00Z"
weight = 6513
keywords = [ "endpoints", "tshark", "capinfos" ]
aliases = [ "/questions/6513" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to get window "endpoint" information from command line](/questions/6513/how-to-get-window-endpoint-information-from-command-line)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6513-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6513-score" class="post-score" title="current number of votes">0</div><span id="post-6513-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi there,</p><p>I neeed to analyse a bunch of shark capture data, and to move some result fields to excel sheets. Most of them Im getting from capinfos but, in order to obtain the total amount transfered and received per ip , Im still using the usual and manual method (open the *.pcap whith wireshark, look the window "endpoints" and to write the values).</p><p>The question is, can I get the window endpoint's values (tab "ipv:4") with a tshark and capinfos combination?</p><p>Thanks a lot.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-endpoints" rel="tag" title="see questions tagged &#39;endpoints&#39;">endpoints</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-capinfos" rel="tag" title="see questions tagged &#39;capinfos&#39;">capinfos</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Sep '11, 08:29</strong></p><img src="https://secure.gravatar.com/avatar/f5a60c14bfbe79406dc402d1cd539469?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Julian%20Diaz&#39;s gravatar image" /><p><span>Julian Diaz</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Julian Diaz has no accepted answers">0%</span></p></div></div><div id="comments-container-6513" class="comments-container"><span id="6519"></span><div id="comment-6519" class="comment"><div id="post-6519-score" class="comment-score"></div><div class="comment-text"><p>Sorry, I respond to myself, <a href="http://ask.wireshark.org/questions/4086/capinfos-data-size-of-received-and-sent-packages">here</a> is what I was looking for.</p></div><div id="comment-6519-info" class="comment-info"><span class="comment-age">(23 Sep '11, 10:33)</span> <span class="comment-user userinfo">Julian Diaz</span></div></div></div><div id="comment-tools-6513" class="comment-tools"></div><div class="clear"></div><div id="comment-6513-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

