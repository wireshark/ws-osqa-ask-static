+++
type = "question"
title = "New Wireshark Mirror: Internet Gateway of South Beach"
description = '''Good afternoon, We&#x27;d like to be added to the Wireshark mirrors list, as we&#x27;ve been mirroring the site for the past week and we&#x27;re on the mailing list. We&#x27;re proud to give something back to the community and your great project. If your group is in need of an additional dedicated server or VPS, please...'''
date = "2011-07-28T19:07:00Z"
lastmod = "2011-07-29T17:59:00Z"
weight = 5355
keywords = [ "isp", "mirroring", "mirror" ]
aliases = [ "/questions/5355" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [New Wireshark Mirror: Internet Gateway of South Beach](/questions/5355/new-wireshark-mirror-internet-gateway-of-south-beach)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5355-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5355-score" class="post-score" title="current number of votes">0</div><span id="post-5355-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Good afternoon,</p><p>We'd like to be added to the Wireshark mirrors list, as we've been mirroring the site for the past week and we're on the mailing list.</p><p>We're proud to give something back to the community and your great project. If your group is in need of an additional dedicated server or VPS, please let us know, we're happy to contribute to a great project such as yours; I've personally found Wireshark useful, as I'm a user since day one (and before, with Ethereal) or quite some time for the past 10 years.</p><p>Here are the answers to the questions on the mirror list form:</p><pre><code>The server address: http://mirrors.igsobe.com/wireshark
                    ftp://mirrors.igsobe.com/wireshark
Your country (e.g., Italy): USA
Your name and email: John C. Young / [email protected]
Your available bandwidth: 10 GB to 7 Tier-1 Providers
Your update frequency: Daily
Your update method (rsync, fmirror, etc) rsync</code></pre><p>Please let us know if you have any questions for us or there's anything else we can do for the project. Take care!</p><p>John C. Young Managing Director Internet Gateway of South Beach http://www.igsobe.com</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-isp" rel="tag" title="see questions tagged &#39;isp&#39;">isp</span> <span class="post-tag tag-link-mirroring" rel="tag" title="see questions tagged &#39;mirroring&#39;">mirroring</span> <span class="post-tag tag-link-mirror" rel="tag" title="see questions tagged &#39;mirror&#39;">mirror</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jul '11, 19:07</strong></p><img src="https://secure.gravatar.com/avatar/66ec81224d3d549eee5e781a9e07260d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jcy1978&#39;s gravatar image" /><p><span>jcy1978</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jcy1978 has no accepted answers">0%</span></p></div></div><div id="comments-container-5355" class="comments-container"><span id="5365"></span><div id="comment-5365" class="comment"><div id="post-5365-score" class="comment-score"></div><div class="comment-text"><p>This is the place to ask questions. I think you would be better off contacting Gerald directly or possibly posting this information to one of the mailing lists like wireshark-dev.</p></div><div id="comment-5365-info" class="comment-info"><span class="comment-age">(29 Jul '11, 17:59)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-5355" class="comment-tools"></div><div class="clear"></div><div id="comment-5355-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

