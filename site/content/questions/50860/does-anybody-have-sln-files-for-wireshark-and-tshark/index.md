+++
type = "question"
title = "Does anybody have sln files for wireshark and Tshark?"
description = '''Does anybody have sln files for wireshark and Tshark?'''
date = "2016-03-13T08:00:00Z"
lastmod = "2016-03-13T08:31:00Z"
weight = 50860
keywords = [ "tshark" ]
aliases = [ "/questions/50860" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Does anybody have sln files for wireshark and Tshark?](/questions/50860/does-anybody-have-sln-files-for-wireshark-and-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50860-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50860-score" class="post-score" title="current number of votes">0</div><span id="post-50860-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does anybody have sln files for wireshark and Tshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Mar '16, 08:00</strong></p><img src="https://secure.gravatar.com/avatar/6b0f8713921b5e13728bdc74d9e42b51?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nirajgulave&#39;s gravatar image" /><p><span>nirajgulave</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nirajgulave has no accepted answers">0%</span></p></div></div><div id="comments-container-50860" class="comments-container"></div><div id="comment-tools-50860" class="comment-tools"></div><div class="clear"></div><div id="comment-50860-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50863"></span>

<div id="answer-container-50863" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50863-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50863-score" class="post-score" title="current number of votes">0</div><span id="post-50863-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you use Wireshark 2.0.x source code or the master branch source code (2.1.x development versions), you can use CMake to generate the sln files. You simply need to follow the procedure described in the <a href="https://www.wireshark.org/docs/wsdg_html_chunked/ChSetupCMake.html">developer guide</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Mar '16, 08:31</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-50863" class="comments-container"></div><div id="comment-tools-50863" class="comment-tools"></div><div class="clear"></div><div id="comment-50863-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

