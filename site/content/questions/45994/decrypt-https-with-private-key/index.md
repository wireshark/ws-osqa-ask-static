+++
type = "question"
title = "Decrypt HTTPS with Private Key"
description = '''Hi, I have a HTTPS server behind load balancer. My vendor give me the private key with dot key extension . What is the best way for my to decrypt and do the analysis in Wireshark? Any recommended ways to do?  Appreciate the helps. Thanks.'''
date = "2015-09-21T01:06:00Z"
lastmod = "2015-09-21T04:53:00Z"
weight = 45994
keywords = [ "https" ]
aliases = [ "/questions/45994" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Decrypt HTTPS with Private Key](/questions/45994/decrypt-https-with-private-key)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45994-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45994-score" class="post-score" title="current number of votes">0</div><span id="post-45994-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I have a HTTPS server behind load balancer. My vendor give me the private key with dot key extension . What is the best way for my to decrypt and do the analysis in Wireshark? Any recommended ways to do?</p><p>Appreciate the helps.</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-https" rel="tag" title="see questions tagged &#39;https&#39;">https</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Sep '15, 01:06</strong></p><img src="https://secure.gravatar.com/avatar/da0590c1b52e4d823169fec48c06946a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Macha&#39;s gravatar image" /><p><span>Macha</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Macha has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Sep '15, 01:07</strong> </span></p></div></div><div id="comments-container-45994" class="comments-container"></div><div id="comment-tools-45994" class="comment-tools"></div><div class="clear"></div><div id="comment-45994-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="46004"></span>

<div id="answer-container-46004" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46004-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46004-score" class="post-score" title="current number of votes">0</div><span id="post-46004-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>What is the best way for my to decrypt and do the analysis in Wireshark?</p></blockquote><p>Please read the Wiki</p><blockquote><p><a href="https://wiki.wireshark.org/SSL">https://wiki.wireshark.org/SSL</a><br />
</p></blockquote><p>and watch some videos:</p><blockquote><p><a href="https://www.youtube.com/watch?v=vQtur8fqErI">https://www.youtube.com/watch?v=vQtur8fqErI</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Sep '15, 04:53</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-46004" class="comments-container"></div><div id="comment-tools-46004" class="comment-tools"></div><div class="clear"></div><div id="comment-46004-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

