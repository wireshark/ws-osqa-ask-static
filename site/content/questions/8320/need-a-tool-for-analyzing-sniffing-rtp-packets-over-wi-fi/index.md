+++
type = "question"
title = "Need a tool for analyzing / sniffing RTP packets over Wi-fi"
description = '''Need a tool for analyzing / sniffing RTP packets over Wi-fi'''
date = "2012-01-11T03:03:00Z"
lastmod = "2012-01-11T15:27:00Z"
weight = 8320
keywords = [ "wireshark" ]
aliases = [ "/questions/8320" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Need a tool for analyzing / sniffing RTP packets over Wi-fi](/questions/8320/need-a-tool-for-analyzing-sniffing-rtp-packets-over-wi-fi)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8320-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8320-score" class="post-score" title="current number of votes">0</div><span id="post-8320-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Need a tool for analyzing / sniffing RTP packets over Wi-fi</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jan '12, 03:03</strong></p><img src="https://secure.gravatar.com/avatar/c491394da41a0aca8a6b764fae2f42fa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rameshnairmphasis&#39;s gravatar image" /><p><span>rameshnairmp...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rameshnairmphasis has no accepted answers">0%</span></p></div></div><div id="comments-container-8320" class="comments-container"></div><div id="comment-tools-8320" class="comment-tools"></div><div class="clear"></div><div id="comment-8320-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8331"></span>

<div id="answer-container-8331" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8331-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8331-score" class="post-score" title="current number of votes">2</div><span id="post-8331-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I think there's a program called "Wireshark" that can dissect RTP and do some statistical analysis of RTP and that can capture traffic on Wi-Fi networks.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jan '12, 15:27</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-8331" class="comments-container"></div><div id="comment-tools-8331" class="comment-tools"></div><div class="clear"></div><div id="comment-8331-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

