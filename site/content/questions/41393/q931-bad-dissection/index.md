+++
type = "question"
title = "Q.931 bad dissection"
description = '''Hi, working with wireshark version 1.12.4 (v1.12.4-0-gb4861da from master-1.12): Bad dussection of protocol Q.931 over iua. any bypass/solution? 10X '''
date = "2015-04-12T06:31:00Z"
lastmod = "2015-04-12T06:46:00Z"
weight = 41393
keywords = [ "iua", "over", "q.931" ]
aliases = [ "/questions/41393" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Q.931 bad dissection](/questions/41393/q931-bad-dissection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41393-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41393-score" class="post-score" title="current number of votes">0</div><span id="post-41393-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>working with wireshark version 1.12.4 (v1.12.4-0-gb4861da from master-1.12): Bad dussection of protocol Q.931 over iua. any bypass/solution?</p><p>10X</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-iua" rel="tag" title="see questions tagged &#39;iua&#39;">iua</span> <span class="post-tag tag-link-over" rel="tag" title="see questions tagged &#39;over&#39;">over</span> <span class="post-tag tag-link-q.931" rel="tag" title="see questions tagged &#39;q.931&#39;">q.931</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Apr '15, 06:31</strong></p><img src="https://secure.gravatar.com/avatar/59f06448a9e796a07d480e45686f2cee?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="HRHR&#39;s gravatar image" /><p><span>HRHR</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="HRHR has no accepted answers">0%</span></p></div></div><div id="comments-container-41393" class="comments-container"></div><div id="comment-tools-41393" class="comment-tools"></div><div class="clear"></div><div id="comment-41393-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41395"></span>

<div id="answer-container-41395" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41395-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41395-score" class="post-score" title="current number of votes">0</div><span id="post-41395-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please fill a bug report to <a href="http://bugs.wireshark.org">http://bugs.wireshark.org</a> with a sample pcap showing the defect and an explanation of the issue.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Apr '15, 06:46</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-41395" class="comments-container"></div><div id="comment-tools-41395" class="comment-tools"></div><div class="clear"></div><div id="comment-41395-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

