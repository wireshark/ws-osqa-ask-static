+++
type = "question"
title = "overcoming p2p restriction in university to play."
description = '''Does anyone know how to overcome this situation? I don&#x27;t really know how to do it or so, but i ve been told, that using openvpn i could play with university internet. They restrict torrent and p2p i guess so i can&#x27;t play sc2 and wow and any game like these. Please help!'''
date = "2010-12-12T03:38:00Z"
lastmod = "2010-12-12T06:11:00Z"
weight = 1320
keywords = [ "restriction", "university", "sc2", "p2p", "wow" ]
aliases = [ "/questions/1320" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [overcoming p2p restriction in university to play.](/questions/1320/overcoming-p2p-restriction-in-university-to-play)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1320-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1320-score" class="post-score" title="current number of votes">-3</div><span id="post-1320-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does anyone know how to overcome this situation? I don't really know how to do it or so, but i ve been told, that using openvpn i could play with university internet. They restrict torrent and p2p i guess so i can't play sc2 and wow and any game like these. Please help!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-restriction" rel="tag" title="see questions tagged &#39;restriction&#39;">restriction</span> <span class="post-tag tag-link-university" rel="tag" title="see questions tagged &#39;university&#39;">university</span> <span class="post-tag tag-link-sc2" rel="tag" title="see questions tagged &#39;sc2&#39;">sc2</span> <span class="post-tag tag-link-p2p" rel="tag" title="see questions tagged &#39;p2p&#39;">p2p</span> <span class="post-tag tag-link-wow" rel="tag" title="see questions tagged &#39;wow&#39;">wow</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Dec '10, 03:38</strong></p><img src="https://secure.gravatar.com/avatar/77675f3b60b6180848d2eb0384bb6d5c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="angusjm&#39;s gravatar image" /><p><span>angusjm</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="angusjm has no accepted answers">0%</span></p></div></div><div id="comments-container-1320" class="comments-container"><span id="1321"></span><div id="comment-1321" class="comment"><div id="post-1321-score" class="comment-score"></div><div class="comment-text"><p>I'm sorry, I fail to see the relevance of this question to this Q&amp;A site. If you have a problem with the restrictions imposed by the people that provide you with Internet access, You will have to discuss this with them. If you need more info on what to ask them to open up for you, yes, wireshark can be of help, but I doubt that this is the information you seek.</p></div><div id="comment-1321-info" class="comment-info"><span class="comment-age">(12 Dec '10, 06:11)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-1320" class="comment-tools"></div><div class="clear"></div><div id="comment-1320-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

