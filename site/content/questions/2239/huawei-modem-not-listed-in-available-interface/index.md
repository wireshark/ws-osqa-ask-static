+++
type = "question"
title = "Huawei modem not listed in available interface"
description = '''Hi Iam using a huawei modem to connect to internet with Model No E1820. But this device is not listed in the available interfaces.  Any suggestion how to solve this. Thanks, Jaya.'''
date = "2011-02-08T13:21:00Z"
lastmod = "2011-02-08T13:21:00Z"
weight = 2239
keywords = [ "huawei", "modem" ]
aliases = [ "/questions/2239" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Huawei modem not listed in available interface](/questions/2239/huawei-modem-not-listed-in-available-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2239-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2239-score" class="post-score" title="current number of votes">0</div><span id="post-2239-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Iam using a huawei modem to connect to internet with Model No E1820. But this device is not listed in the available interfaces.</p><p>Any suggestion how to solve this.</p><p>Thanks, Jaya.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-huawei" rel="tag" title="see questions tagged &#39;huawei&#39;">huawei</span> <span class="post-tag tag-link-modem" rel="tag" title="see questions tagged &#39;modem&#39;">modem</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Feb '11, 13:21</strong></p><img src="https://secure.gravatar.com/avatar/74768f95daee425f64f795ef4004b27f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jayadevan_n&#39;s gravatar image" /><p><span>jayadevan_n</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jayadevan_n has no accepted answers">0%</span></p></div></div><div id="comments-container-2239" class="comments-container"></div><div id="comment-tools-2239" class="comment-tools"></div><div class="clear"></div><div id="comment-2239-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

