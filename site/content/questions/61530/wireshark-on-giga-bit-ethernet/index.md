+++
type = "question"
title = "Wireshark on Giga Bit Ethernet"
description = '''Hi, As Gigabit Ethernet handles high speed data, How can I use wireshark to analyze iperf TCP data for 120Sec duration..  Note following observations made on Red hat linux  1) Getting 250Mbps througthput by i-perf application  2) Wireshark application is slow, (even after completion of data transere...'''
date = "2017-05-21T22:44:00Z"
lastmod = "2017-05-22T01:14:00Z"
weight = 61530
keywords = [ "on", "bit", "ethernet", "giga", "wireshark" ]
aliases = [ "/questions/61530" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark on Giga Bit Ethernet](/questions/61530/wireshark-on-giga-bit-ethernet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61530-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61530-score" class="post-score" title="current number of votes">0</div><span id="post-61530-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>As Gigabit Ethernet handles high speed data, How can I use wireshark to analyze iperf TCP data for 120Sec duration..</p><p>Note following observations made on Red hat linux</p><p>1) Getting 250Mbps througthput by i-perf application 2) Wireshark application is slow, (even after completion of data transered confirmed by i-perf final result, Frames are displaying slowly crashing after some time) 3) While capturing, Packet drops are observed on the bottom (i.e at captured / displayed / marked / dropped)</p><p>Regards M Srinivasa Rao</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-on" rel="tag" title="see questions tagged &#39;on&#39;">on</span> <span class="post-tag tag-link-bit" rel="tag" title="see questions tagged &#39;bit&#39;">bit</span> <span class="post-tag tag-link-ethernet" rel="tag" title="see questions tagged &#39;ethernet&#39;">ethernet</span> <span class="post-tag tag-link-giga" rel="tag" title="see questions tagged &#39;giga&#39;">giga</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 May '17, 22:44</strong></p><img src="https://secure.gravatar.com/avatar/ce1843f92a1c18db26bc79b3afa9bd50?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="srinu_bel&#39;s gravatar image" /><p><span>srinu_bel</span><br />
<span class="score" title="20 reputation points">20</span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="20 badges"><span class="bronze">●</span><span class="badgecount">20</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="srinu_bel has no accepted answers">0%</span></p></div></div><div id="comments-container-61530" class="comments-container"></div><div id="comment-tools-61530" class="comment-tools"></div><div class="clear"></div><div id="comment-61530-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61533"></span>

<div id="answer-container-61533" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61533-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61533-score" class="post-score" title="current number of votes">0</div><span id="post-61533-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://www.ntop.org/?s=wireshark">This</a> gives you a nice set of articles how to tackle high speed link capture.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 May '17, 01:14</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-61533" class="comments-container"></div><div id="comment-tools-61533" class="comment-tools"></div><div class="clear"></div><div id="comment-61533-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

