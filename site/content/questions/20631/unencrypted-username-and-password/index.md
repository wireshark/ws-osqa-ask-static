+++
type = "question"
title = "unencrypted username and password"
description = '''hello i want to find an unencrypted username and password. but i am interested in a real username and password, and not login attempts that failed. and get user name and password. i can do it with tcp follow stream , but i want do it with filter and command in wireshark. (not manually) tnx for your...'''
date = "2013-04-19T09:24:00Z"
lastmod = "2013-04-20T14:18:00Z"
weight = 20631
keywords = [ "unencrypted", "username", "and", "password" ]
aliases = [ "/questions/20631" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [unencrypted username and password](/questions/20631/unencrypted-username-and-password)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20631-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20631-score" class="post-score" title="current number of votes">0</div><span id="post-20631-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello i want to find an unencrypted username and password. but i am interested in a real username and password, and not login attempts that failed. and get user name and password.</p><p>i can do it with tcp follow stream , but i want do it with filter and command in wireshark. (not manually)</p><p>tnx for your help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-unencrypted" rel="tag" title="see questions tagged &#39;unencrypted&#39;">unencrypted</span> <span class="post-tag tag-link-username" rel="tag" title="see questions tagged &#39;username&#39;">username</span> <span class="post-tag tag-link-and" rel="tag" title="see questions tagged &#39;and&#39;">and</span> <span class="post-tag tag-link-password" rel="tag" title="see questions tagged &#39;password&#39;">password</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Apr '13, 09:24</strong></p><img src="https://secure.gravatar.com/avatar/c0ff7a862e45e0272e8d0f0cb72879af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="riyaha&#39;s gravatar image" /><p><span>riyaha</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="riyaha has no accepted answers">0%</span></p></div></div><div id="comments-container-20631" class="comments-container"><span id="20654"></span><div id="comment-20654" class="comment"><div id="post-20654-score" class="comment-score"></div><div class="comment-text"><blockquote><p>but i want do it with filter and command in wireshark.</p></blockquote><p>well, then it would be a good idea to tell us the protocol (telnet, http, ftp, etc.) you are interested in</p></div><div id="comment-20654-info" class="comment-info"><span class="comment-age">(20 Apr '13, 05:46)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="20661"></span><div id="comment-20661" class="comment"><div id="post-20661-score" class="comment-score"></div><div class="comment-text"><p>all of them.</p><p>of course i do it for ftp, but for others i can not do it</p></div><div id="comment-20661-info" class="comment-info"><span class="comment-age">(20 Apr '13, 12:01)</span> <span class="comment-user userinfo">riyaha</span></div></div></div><div id="comment-tools-20631" class="comment-tools"></div><div class="clear"></div><div id="comment-20631-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20664"></span>

<div id="answer-container-20664" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20664-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20664-score" class="post-score" title="current number of votes">0</div><span id="post-20664-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>all of them. of course i do it for ftp, but for others i can not do it but i want do it with filter and command in wireshark. <strong>(not manually)</strong></p></blockquote><p>with Wireshark you can extract clear text passwords for all of the mentioned protocols, however there is no functionality in Wireshark to do it automatically.</p><p>I suggest to search for a 'password sniffer' at google.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Apr '13, 14:18</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-20664" class="comments-container"></div><div id="comment-tools-20664" class="comment-tools"></div><div class="clear"></div><div id="comment-20664-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

