+++
type = "question"
title = "use cable modem for ipad"
description = '''I have a REAL need (not idiosyncratic) for which I’ve invested research time that I don’t have. Details aside: in the comfort of my Peekskill, NY apartment, I HAVE NO ACCESS TO WI-FI (Boingo or AT&amp;amp;T)! The only way I can work on my iPhone or iPAD at home, I have to pay AT&amp;amp;T $25/mo for 3G (slo...'''
date = "2011-05-22T13:43:00Z"
lastmod = "2011-05-22T13:50:00Z"
weight = 4174
keywords = [ "ipad", "wifi", "3g", "network", "cable" ]
aliases = [ "/questions/4174" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [use cable modem for ipad](/questions/4174/use-cable-modem-for-ipad)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4174-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4174-score" class="post-score" title="current number of votes">0</div><span id="post-4174-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a REAL need (not idiosyncratic) for which I’ve invested research time that I don’t have. Details aside: in the comfort of my Peekskill, NY apartment, I HAVE NO ACCESS TO WI-FI (Boingo or AT&amp;T)! The only way I can work on my iPhone or iPAD at home, I have to pay AT&amp;T $25/mo for 3G (slow and limited) just for my iPAD. I'm already paying $25/mo for my iPhone 3G. Unless someone has another solution, my question is: Is there a clever way to use my CABLE modem (Optimum) via my desktop, attach my iPAD and use the cable modem to access the internet? For one reason, cable is way faster and more reliable than Wi-Fi or 3G.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ipad" rel="tag" title="see questions tagged &#39;ipad&#39;">ipad</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-3g" rel="tag" title="see questions tagged &#39;3g&#39;">3g</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-cable" rel="tag" title="see questions tagged &#39;cable&#39;">cable</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 May '11, 13:43</strong></p><img src="https://secure.gravatar.com/avatar/3a890f55bb5bf54c6069c4abf8aaf3bb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="neurologist&#39;s gravatar image" /><p><span>neurologist</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="neurologist has no accepted answers">0%</span></p></div></div><div id="comments-container-4174" class="comments-container"></div><div id="comment-tools-4174" class="comment-tools"></div><div class="clear"></div><div id="comment-4174-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4175"></span>

<div id="answer-container-4175" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4175-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4175-score" class="post-score" title="current number of votes">4</div><span id="post-4175-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not sure why you think your question has anything to do with Wireshark (which is the main topic here, or network analysis in general, but not "how to connect device a to network b"), but I suggest you go and buy a WiFi router, connect it to your cable modem on one side and your computer and iPAD to the router on the other.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 May '11, 13:50</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-4175" class="comments-container"></div><div id="comment-tools-4175" class="comment-tools"></div><div class="clear"></div><div id="comment-4175-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

