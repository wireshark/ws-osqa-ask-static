+++
type = "question"
title = "IBM Trackpad scrolling"
description = '''Hello, I have an IBM x230 laptop. When I try to scroll anywhere in Wireshark with the track-pad (Button nub in the middle of the keyboard) I am unable to. I have read in this post that other people have had the same issue back in 2007. Is this still a problem? Is there a solution? http://forum.think...'''
date = "2015-03-28T13:13:00Z"
lastmod = "2015-03-30T11:09:00Z"
weight = 40961
keywords = [ "trackpad", "x230", "scrolling", "ibm" ]
aliases = [ "/questions/40961" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [IBM Trackpad scrolling](/questions/40961/ibm-trackpad-scrolling)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40961-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40961-score" class="post-score" title="current number of votes">0</div><span id="post-40961-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I have an IBM x230 laptop. When I try to scroll anywhere in Wireshark with the track-pad (Button nub in the middle of the keyboard) I am unable to. I have read in this post that other people have had the same issue back in 2007. Is this still a problem? Is there a solution?</p><p><a href="http://forum.thinkpads.com/viewtopic.php?t=80305">http://forum.thinkpads.com/viewtopic.php?t=80305</a></p><p>Dave</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-trackpad" rel="tag" title="see questions tagged &#39;trackpad&#39;">trackpad</span> <span class="post-tag tag-link-x230" rel="tag" title="see questions tagged &#39;x230&#39;">x230</span> <span class="post-tag tag-link-scrolling" rel="tag" title="see questions tagged &#39;scrolling&#39;">scrolling</span> <span class="post-tag tag-link-ibm" rel="tag" title="see questions tagged &#39;ibm&#39;">ibm</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Mar '15, 13:13</strong></p><img src="https://secure.gravatar.com/avatar/7bba3e7bd7ee229b2548d6fc1b565016?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="davehouser1&#39;s gravatar image" /><p><span>davehouser1</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="davehouser1 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Mar '15, 19:01</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-40961" class="comments-container"><span id="41029"></span><div id="comment-41029" class="comment"><div id="post-41029-score" class="comment-score"></div><div class="comment-text"><p>What OS and Wireshark version are you using?</p></div><div id="comment-41029-info" class="comment-info"><span class="comment-age">(30 Mar '15, 09:52)</span> <span class="comment-user userinfo">Roland</span></div></div></div><div id="comment-tools-40961" class="comment-tools"></div><div class="clear"></div><div id="comment-40961-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41031"></span>

<div id="answer-container-41031" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41031-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41031-score" class="post-score" title="current number of votes">0</div><span id="post-41031-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sounds like a driver problem with Synaptics and GTK applications.</p><p><a href="https://bugzilla.gnome.org/show_bug.cgi?id=480477#c8">https://bugzilla.gnome.org/show_bug.cgi?id=480477#c8</a></p><p>possible workarounds are described in that bug report as well (comment #9)</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Mar '15, 11:09</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-41031" class="comments-container"></div><div id="comment-tools-41031" class="comment-tools"></div><div class="clear"></div><div id="comment-41031-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

