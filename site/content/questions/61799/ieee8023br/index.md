+++
type = "question"
title = "IEEE802.3br"
description = '''this new document specifies the IET (Intersparsing Express Traffic) frame structure. both devices must be able to support the IET frame otherwise it will not be recognized as a valid frame. Is wireshark supporting this new spec? thanks Mastro59'''
date = "2017-06-06T02:26:00Z"
lastmod = "2017-06-06T19:59:00Z"
weight = 61799
keywords = [ "express", "traffic", "intersparsing" ]
aliases = [ "/questions/61799" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [IEEE802.3br](/questions/61799/ieee8023br)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61799-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61799-score" class="post-score" title="current number of votes">0</div><span id="post-61799-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>this new document specifies the IET (Intersparsing Express Traffic) frame structure. both devices must be able to support the IET frame otherwise it will not be recognized as a valid frame. Is wireshark supporting this new spec? thanks Mastro59</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-express" rel="tag" title="see questions tagged &#39;express&#39;">express</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-intersparsing" rel="tag" title="see questions tagged &#39;intersparsing&#39;">intersparsing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jun '17, 02:26</strong></p><img src="https://secure.gravatar.com/avatar/33d5c1eb3707a321c9903f844ca38b6b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mastro59&#39;s gravatar image" /><p><span>Mastro59</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mastro59 has no accepted answers">0%</span></p></div></div><div id="comments-container-61799" class="comments-container"><span id="61812"></span><div id="comment-61812" class="comment"><div id="post-61812-score" class="comment-score"></div><div class="comment-text"><blockquote><p>Is wireshark supporting this new spec?</p></blockquote><p>What part? If fragmentation/defragmentation of preemptible packets is done by the Ethernet adapter, Wireshark probably won't see them, it'll just see regular Ethernet packets, so that would leave only the LLDP TLVs from clause 79 as stuff that Wireshark would have to support.</p></div><div id="comment-61812-info" class="comment-info"><span class="comment-age">(06 Jun '17, 19:59)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-61799" class="comment-tools"></div><div class="clear"></div><div id="comment-61799-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

