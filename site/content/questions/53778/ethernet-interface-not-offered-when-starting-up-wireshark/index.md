+++
type = "question"
title = "Ethernet interface not offered when starting up Wireshark ?"
description = '''After updating Wireshark, I launch Wireshark and only USBPcap interfaces are listed on the opening page. Wonder why I don&#x27;t see any Ethernet interfaces - I have WinPCAP 4.1.3 installed on this Windows 10 PC. Is something wrong or do I not understand at what I am looking ?? Thanks'''
date = "2016-07-01T16:05:00Z"
lastmod = "2016-07-01T16:05:00Z"
weight = 53778
keywords = [ "ethernet", "usbpcap", "winpcap" ]
aliases = [ "/questions/53778" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Ethernet interface not offered when starting up Wireshark ?](/questions/53778/ethernet-interface-not-offered-when-starting-up-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53778-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53778-score" class="post-score" title="current number of votes">0</div><span id="post-53778-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>After updating Wireshark, I launch Wireshark and only USBPcap interfaces are listed on the opening page. Wonder why I don't see any Ethernet interfaces - I have WinPCAP 4.1.3 installed on this Windows 10 PC.</p><p>Is something wrong or do I not understand at what I am looking ??</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ethernet" rel="tag" title="see questions tagged &#39;ethernet&#39;">ethernet</span> <span class="post-tag tag-link-usbpcap" rel="tag" title="see questions tagged &#39;usbpcap&#39;">usbpcap</span> <span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Jul '16, 16:05</strong></p><img src="https://secure.gravatar.com/avatar/15bfd5e4c9e6e03d2a86678c43916ce2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gerlad0&#39;s gravatar image" /><p><span>gerlad0</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gerlad0 has no accepted answers">0%</span></p></div></div><div id="comments-container-53778" class="comments-container"></div><div id="comment-tools-53778" class="comment-tools"></div><div class="clear"></div><div id="comment-53778-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

