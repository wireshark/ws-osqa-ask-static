+++
type = "question"
title = "Capturing network traffic (rtmp) between VMs or using loopback in the same VM using wireshark"
description = '''I need to to capture RTMP traffic between two virtual machines (server and client) or atleast have the server and client on the same machine and capture the traffic. I am able to capture the traffic if I run wireshark on the host rather than on the VMs. But I need to capture it on the VM to be able ...'''
date = "2012-09-26T11:48:00Z"
lastmod = "2012-09-26T11:48:00Z"
weight = 14548
keywords = [ "security", "packet-capture", "networking", "wireshark" ]
aliases = [ "/questions/14548" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing network traffic (rtmp) between VMs or using loopback in the same VM using wireshark](/questions/14548/capturing-network-traffic-rtmp-between-vms-or-using-loopback-in-the-same-vm-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14548-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14548-score" class="post-score" title="current number of votes">0</div><span id="post-14548-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need to to capture RTMP traffic between two virtual machines (server and client) or atleast have the server and client on the same machine and capture the traffic. I am able to capture the traffic if I run wireshark on the host rather than on the VMs. But I need to capture it on the VM to be able to manipulate it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-security" rel="tag" title="see questions tagged &#39;security&#39;">security</span> <span class="post-tag tag-link-packet-capture" rel="tag" title="see questions tagged &#39;packet-capture&#39;">packet-capture</span> <span class="post-tag tag-link-networking" rel="tag" title="see questions tagged &#39;networking&#39;">networking</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Sep '12, 11:48</strong></p><img src="https://secure.gravatar.com/avatar/9202d33eb022558d3e6d30f3fd42d0b2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ssn&#39;s gravatar image" /><p><span>ssn</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ssn has no accepted answers">0%</span></p></div></div><div id="comments-container-14548" class="comments-container"></div><div id="comment-tools-14548" class="comment-tools"></div><div class="clear"></div><div id="comment-14548-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

