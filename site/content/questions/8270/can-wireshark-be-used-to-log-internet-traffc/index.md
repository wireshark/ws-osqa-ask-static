+++
type = "question"
title = "Can wireshark be used to log internet traffc?"
description = '''I have many computers in my house. I want to know where my bandwith is going, and what sites my kids are visiting. I am willing to dedicate a computer to log the data. I am developer and would be happy to write code to parase and report on the logged data.'''
date = "2012-01-07T18:59:00Z"
lastmod = "2012-01-08T07:28:00Z"
weight = 8270
keywords = [ "traffic", "log", "internet" ]
aliases = [ "/questions/8270" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can wireshark be used to log internet traffc?](/questions/8270/can-wireshark-be-used-to-log-internet-traffc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8270-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8270-score" class="post-score" title="current number of votes">0</div><span id="post-8270-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have many computers in my house. I want to know where my bandwith is going, and what sites my kids are visiting. I am willing to dedicate a computer to log the data. I am developer and would be happy to write code to parase and report on the logged data.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-log" rel="tag" title="see questions tagged &#39;log&#39;">log</span> <span class="post-tag tag-link-internet" rel="tag" title="see questions tagged &#39;internet&#39;">internet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Jan '12, 18:59</strong></p><img src="https://secure.gravatar.com/avatar/466a220baea3e2b6dfb2b10359499b2f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kirkb&#39;s gravatar image" /><p><span>Kirkb</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kirkb has no accepted answers">0%</span></p></div></div><div id="comments-container-8270" class="comments-container"><span id="8272"></span><div id="comment-8272" class="comment"><div id="post-8272-score" class="comment-score"></div><div class="comment-text"><p>To clarify, my goal is to have one computer monitor all activity in my house.</p></div><div id="comment-8272-info" class="comment-info"><span class="comment-age">(07 Jan '12, 21:22)</span> <span class="comment-user userinfo">Kirkb</span></div></div></div><div id="comment-tools-8270" class="comment-tools"></div><div class="clear"></div><div id="comment-8270-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8275"></span>

<div id="answer-container-8275" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8275-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8275-score" class="post-score" title="current number of votes">0</div><span id="post-8275-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Assuming that you are using a LAN I think one of the easiest way is to use a hub. If you connect your network to a hub and connect that dedicated computer to one of the hub ports and make the interface run in promiscuous mode you should be able to capture all the traffic in your network. This is because of the hub functionality which will broadcast the packets to all the hub ports meaning all the interfaces will be able to see the traffic. Similar strategy which is used by WLAN APs.</p><p>Good Luck, A.G</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Jan '12, 07:28</strong></p><img src="https://secure.gravatar.com/avatar/559f374efd2eaeaafac5616bbec62008?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AminGho&#39;s gravatar image" /><p><span>AminGho</span><br />
<span class="score" title="51 reputation points">51</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AminGho has no accepted answers">0%</span></p></div></div><div id="comments-container-8275" class="comments-container"></div><div id="comment-tools-8275" class="comment-tools"></div><div class="clear"></div><div id="comment-8275-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

