+++
type = "question"
title = "WireShark 1.12.0 for Mac OSX 32-bit dmg file looks odd"
description = '''Hello The DMG file for WireShark 1.12.0 for Mac OSX 32-bit dmg file looks odd. It&#x27;s file size is only 29kb and it looks like it&#x27;s invalid. Please check this file (it&#x27;s on url: https://1.eu.dl.wireshark.org/osx/Wireshark%201.12.0%20Intel%2032.dmg) Thanks!'''
date = "2014-08-08T02:26:00Z"
lastmod = "2014-08-08T02:50:00Z"
weight = 35319
keywords = [ "osx", "mac", "dmg" ]
aliases = [ "/questions/35319" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [WireShark 1.12.0 for Mac OSX 32-bit dmg file looks odd](/questions/35319/wireshark-1120-for-mac-osx-32-bit-dmg-file-looks-odd)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35319-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35319-score" class="post-score" title="current number of votes">0</div><span id="post-35319-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>The DMG file for WireShark 1.12.0 for Mac OSX 32-bit dmg file looks odd. It's file size is only 29kb and it looks like it's invalid. Please check this file (it's on url: <a href="https://1.eu.dl.wireshark.org/osx/Wireshark%201.12.0%20Intel%2032.dmg)">https://1.eu.dl.wireshark.org/osx/Wireshark%201.12.0%20Intel%2032.dmg)</a></p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-dmg" rel="tag" title="see questions tagged &#39;dmg&#39;">dmg</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Aug '14, 02:26</strong></p><img src="https://secure.gravatar.com/avatar/5d44a75f0145abd5c469730acf01fde6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TonyS&#39;s gravatar image" /><p><span>TonyS</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TonyS has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Aug '14, 02:49</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-35319" class="comments-container"><span id="35320"></span><div id="comment-35320" class="comment"><div id="post-35320-score" class="comment-score"></div><div class="comment-text"><p>Same size on the main wireshark.org site: <a href="https://www.wireshark.org/download/osx/">https://www.wireshark.org/download/osx/</a></p></div><div id="comment-35320-info" class="comment-info"><span class="comment-age">(08 Aug '14, 02:50)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-35319" class="comment-tools"></div><div class="clear"></div><div id="comment-35319-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

