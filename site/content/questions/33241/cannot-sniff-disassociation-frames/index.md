+++
type = "question"
title = "Cannot sniff Disassociation frames"
description = '''Hello, I am trying to sniff 802.11 Disassociation frames in wireshark but it seems impossible. In the filter expression I am using wlan.fc.type_subtype==0x0A but I don&#x27;t see anything. I tried disconnecting a connected wireless client using the first time the normal windows disconnect options and the...'''
date = "2014-06-01T10:14:00Z"
lastmod = "2014-06-01T10:14:00Z"
weight = 33241
keywords = [ "frames", "disassociation" ]
aliases = [ "/questions/33241" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Cannot sniff Disassociation frames](/questions/33241/cannot-sniff-disassociation-frames)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33241-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33241-score" class="post-score" title="current number of votes">0</div><span id="post-33241-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I am trying to sniff 802.11 Disassociation frames in wireshark but it seems impossible. In the filter expression I am using wlan.fc.type_subtype==0x0A but I don't see anything. I tried disconnecting a connected wireless client using the first time the normal windows disconnect options and the second time to shut down the wireless interface. Nothing... I even shut down unexpectedly the wireless interface in the Access Point but still the same result. Is something wrong in my filtering expression? I can sniff without any problem all other types of frames. I am using the ALFA AWUS036H adaptor in Ubuntu 12.04.</p><p>Kind regards</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-frames" rel="tag" title="see questions tagged &#39;frames&#39;">frames</span> <span class="post-tag tag-link-disassociation" rel="tag" title="see questions tagged &#39;disassociation&#39;">disassociation</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Jun '14, 10:14</strong></p><img src="https://secure.gravatar.com/avatar/5d8ed9d0276abc07e95388f19d7d985e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smoutsin&#39;s gravatar image" /><p><span>smoutsin</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smoutsin has no accepted answers">0%</span></p></div></div><div id="comments-container-33241" class="comments-container"></div><div id="comment-tools-33241" class="comment-tools"></div><div class="clear"></div><div id="comment-33241-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

