+++
type = "question"
title = "XML error when accessing Smooth Manifest files"
description = '''Hi, When I do a cpature of - http://mediadl.microsoft.com/mediadl/iisnet/smoothmedia/Experience/BigBuckBunny_720p.ism/Manifest I get xml errors in the xml dissector - [ ERROR: Unrecognized text ] Any suggestions on how to fix? Thanks'''
date = "2014-09-03T01:55:00Z"
lastmod = "2014-09-03T01:55:00Z"
weight = 35948
keywords = [ "xml", "smooth", "for", "error" ]
aliases = [ "/questions/35948" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [XML error when accessing Smooth Manifest files](/questions/35948/xml-error-when-accessing-smooth-manifest-files)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35948-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35948-score" class="post-score" title="current number of votes">0</div><span id="post-35948-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>When I do a cpature of - <a href="http://mediadl.microsoft.com/mediadl/iisnet/smoothmedia/Experience/BigBuckBunny_720p.ism/Manifest">http://mediadl.microsoft.com/mediadl/iisnet/smoothmedia/Experience/BigBuckBunny_720p.ism/Manifest</a></p><p>I get xml errors in the xml dissector - [ ERROR: Unrecognized text ]</p><p>Any suggestions on how to fix?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-xml" rel="tag" title="see questions tagged &#39;xml&#39;">xml</span> <span class="post-tag tag-link-smooth" rel="tag" title="see questions tagged &#39;smooth&#39;">smooth</span> <span class="post-tag tag-link-for" rel="tag" title="see questions tagged &#39;for&#39;">for</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Sep '14, 01:55</strong></p><img src="https://secure.gravatar.com/avatar/e6515ec88aa60946aa35237add596c66?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Markl2014&#39;s gravatar image" /><p><span>Markl2014</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Markl2014 has no accepted answers">0%</span></p></div></div><div id="comments-container-35948" class="comments-container"></div><div id="comment-tools-35948" class="comment-tools"></div><div class="clear"></div><div id="comment-35948-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

