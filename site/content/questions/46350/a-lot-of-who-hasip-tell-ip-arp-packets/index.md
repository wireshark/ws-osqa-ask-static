+++
type = "question"
title = "a lot of &quot;who has(IP)?&quot; &quot;tell (IP)&quot; ARP packets"
description = '''o I noticed that the network traffic light on my router was constantly flashing meaning that it was seeing network activity even though I didn&#x27;t have any devices connected to the router, so I fired up wireshark which didn&#x27;t show anything interesting until I put the router in modem mode which bypasse...'''
date = "2015-10-03T11:22:00Z"
lastmod = "2015-10-04T23:23:00Z"
weight = 46350
keywords = [ "arp" ]
aliases = [ "/questions/46350" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [a lot of "who has(IP)?" "tell (IP)" ARP packets](/questions/46350/a-lot-of-who-hasip-tell-ip-arp-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46350-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46350-score" class="post-score" title="current number of votes">0</div><span id="post-46350-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>o I noticed that the network traffic light on my router was constantly flashing meaning that it was seeing network activity even though I didn't have any devices connected to the router, so I fired up wireshark which didn't show anything interesting until I put the router in modem mode which bypasses the firewall etc the only thing that stays on is the modem part. So In modem mode I started seeing a huge amount of ARP packets "who has (IP)?" "Tell (IP)". The IP addresses are not from lan. Most of them look like they have the same IP range (2 first numbers) as the internet in my area. So my first question is are those ARP packets related to the traffic light on the router constantly showing network activity? If so is that normal. I don't remember it being like that a week ago. My second question is why am I able to see IP addresses in my area????? From my (tiny) knowledge wireshark doesn't have the ability to go past LAN. Please keep in mind that I'm a newbie when it comes to stuff like this <a href="http://tinypic.com/view.php?pic=nofvkg&amp;s=8#.VhAH9c_TVTc">http://tinypic.com/view.php?pic=nofvkg&amp;s=8#.VhAH9c_TVTc</a> <embed src="http://tinypic.com/view.php?pic=nofvkg&amp;s=8#.VhAH9c_TVTc" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-arp" rel="tag" title="see questions tagged &#39;arp&#39;">arp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Oct '15, 11:22</strong></p><img src="https://secure.gravatar.com/avatar/a06935623f0abb0a10502f804ae524d5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworldhtml&#39;s gravatar image" /><p><span>helloworldhtml</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="helloworldhtml has no accepted answers">0%</span></p></img></div></div><div id="comments-container-46350" class="comments-container"><span id="46357"></span><div id="comment-46357" class="comment"><div id="post-46357-score" class="comment-score">1</div><div class="comment-text"><p>Cable modem?</p></div><div id="comment-46357-info" class="comment-info"><span class="comment-age">(04 Oct '15, 23:23)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-46350" class="comment-tools"></div><div class="clear"></div><div id="comment-46350-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

