+++
type = "question"
title = "[closed] How to collect packets into flows based on 5-tuples offline"
description = '''Hi I have some pcap files I need to puts these files in flows base instead of packets base by using 5-tuple (Src port, dst port,src ip, dst ip, protocol). is there any C code can do that, and calculate the statistical features such as mean interarrival time for the flow  great thank'''
date = "2012-10-31T20:16:00Z"
lastmod = "2012-10-31T20:16:00Z"
weight = 15441
keywords = [ "wireshark" ]
aliases = [ "/questions/15441" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] How to collect packets into flows based on 5-tuples offline](/questions/15441/how-to-collect-packets-into-flows-based-on-5-tuples-offline)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15441-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15441-score" class="post-score" title="current number of votes">0</div><span id="post-15441-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I have some pcap files I need to puts these files in flows base instead of packets base by using 5-tuple (Src port, dst port,src ip, dst ip, protocol). is there any C code can do that, and calculate the statistical features such as mean interarrival time for the flow great thank</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Oct '12, 20:16</strong></p><img src="https://secure.gravatar.com/avatar/27a37dd0db96baea7c1384b21b668120?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hy2012&#39;s gravatar image" /><p><span>hy2012</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hy2012 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>01 Nov '12, 00:57</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-15441" class="comments-container"></div><div id="comment-tools-15441" class="comment-tools"></div><div class="clear"></div><div id="comment-15441-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by grahamb 01 Nov '12, 00:57

</div>

</div>

</div>

