+++
type = "question"
title = "Throughput graph: Why are there parallel lines?"
description = '''Hello, This is my throughput graph:  Could anyone tell me what I can conclude about the 2 parallel lines in this graph? Thanks in advance.'''
date = "2011-06-19T09:18:00Z"
lastmod = "2011-06-20T20:36:00Z"
weight = 4620
keywords = [ "graph", "throughput" ]
aliases = [ "/questions/4620" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [Throughput graph: Why are there parallel lines?](/questions/4620/throughput-graph-why-are-there-parallel-lines)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4620-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4620-score" class="post-score" title="current number of votes">0</div><span id="post-4620-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>This is my throughput graph: <img src="http://cC7.upanh.com/23.912.30980296.ln0/untitled.png" alt="alt text" /> Could anyone tell me what I can conclude about the 2 parallel lines in this graph? Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span> <span class="post-tag tag-link-throughput" rel="tag" title="see questions tagged &#39;throughput&#39;">throughput</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jun '11, 09:18</strong></p><img src="https://secure.gravatar.com/avatar/9f29926733cc92c6c9d358d1fb5ecdf7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ord&#39;s gravatar image" /><p><span>ord</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ord has no accepted answers">0%</span></p></img></div></div><div id="comments-container-4620" class="comments-container"></div><div id="comment-tools-4620" class="comment-tools"></div><div class="clear"></div><div id="comment-4620-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="4623"></span>

<div id="answer-container-4623" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4623-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4623-score" class="post-score" title="current number of votes">2</div><span id="post-4623-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="ord has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I see more that 2 parallel lines, but I assume you mean the two most "thick" ones. They tell you that most of he time, you have an average throughput of ~11.25 or ~11.75 MByte/s. So this looks like a healthy and fully utilized 100Mbit/s network.</p><p>You can zoom in on the graph to get more detail...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jun '11, 14:32</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Jun '11, 11:39</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-4623" class="comments-container"><span id="4629"></span><div id="comment-4629" class="comment"><div id="post-4629-score" class="comment-score"></div><div class="comment-text"><p>Hmmm... thinking about this for a second time, I can't tell from the graph that the network is healthy, I can only tell from it that Wireshark sees near 100Mbit/s bandwidth.</p><p>(which can easily be the case when a Gbit/s link gets spanned to a 100Mbit/s span port)</p></div><div id="comment-4629-info" class="comment-info"><span class="comment-age">(19 Jun '11, 20:36)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div><span id="4631"></span><div id="comment-4631" class="comment"><div id="post-4631-score" class="comment-score"></div><div class="comment-text"><p>Hi SYNbit,</p><p>Thanks a lot. Actually, this is the case we've done with 2 PCs transmitting data to each other over a direct 100Mbps link. I've already zoomed in the graph and seen that in fact, there's nothing parallel but many dots together :)</p><p>BR,</p></div><div id="comment-4631-info" class="comment-info"><span class="comment-age">(20 Jun '11, 00:17)</span> <span class="comment-user userinfo">ord</span></div></div></div><div id="comment-tools-4623" class="comment-tools"></div><div class="clear"></div><div id="comment-4623-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="4636"></span>

<div id="answer-container-4636" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4636-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4636-score" class="post-score" title="current number of votes">0</div><span id="post-4636-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hello,</p><p>Can I ask you another question about the throughput graph? Following is my another graph: <img src="http://cC0.upanh.com/23.964.31034059.91m0/untitled.png" alt="alt text" /></p><p>Capacity of the link is 100Mbps; however, you can see the throughput can reach around 1Gbps at some time. Is there any inconsistency?</p><p>BR,</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jun '11, 08:19</strong></p><img src="https://secure.gravatar.com/avatar/9f29926733cc92c6c9d358d1fb5ecdf7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ord&#39;s gravatar image" /><p><span>ord</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ord has no accepted answers">0%</span></p></img></div></div><div id="comments-container-4636" class="comments-container"><span id="4641"></span><div id="comment-4641" class="comment"><div id="post-4641-score" class="comment-score"></div><div class="comment-text"><p>I can imagine this being caused by in-accurate timestamping. When your capturing device is busy, it might queue a couple of packets and when it gets to timestamp them, they might get (almost) the same timestamps. Have a detailed look at the packets at the spike to see whether this is the case...</p></div><div id="comment-4641-info" class="comment-info"><span class="comment-age">(20 Jun '11, 20:36)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-4636" class="comment-tools"></div><div class="clear"></div><div id="comment-4636-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

