+++
type = "question"
title = "Malformed radiotap header in wireshark and background noise parameter missing"
description = '''0 down vote favorite share [g+] share [fb] share [tw] I have set up an experiment to measure Wireless communication parameters using two laptops. I am injecting custom packets into the network on a monitor interface using one laptop programmed as a transmitter(Macbook Pro using Broadcom BCM 4331 int...'''
date = "2012-06-18T15:47:00Z"
lastmod = "2012-06-18T15:47:00Z"
weight = 12031
keywords = [ "wireless", "radiotap" ]
aliases = [ "/questions/12031" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Malformed radiotap header in wireshark and background noise parameter missing](/questions/12031/malformed-radiotap-header-in-wireshark-and-background-noise-parameter-missing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12031-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12031-score" class="post-score" title="current number of votes">0</div><span id="post-12031-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>0 down vote favorite share [g+] share [fb] share [tw]</p><p>I have set up an experiment to measure Wireless communication parameters using two laptops. I am injecting custom packets into the network on a monitor interface using one laptop programmed as a transmitter(Macbook Pro using Broadcom BCM 4331 interface card) and another laptop ( Dell Latitude E6410 using the Intel Corporation Centrino Ultimate-N 6300 interface card) programmed as a receiver sniffs the network for the custom injected packets and reports parameters such as Received signal strength, propagation delay etc... The transmission is working fine and i am able to get the injected packets on the receiving side.</p><p>However , When i use Wireshark to analyze the incoming packets using the monitor interface created using airmon-ng, the radio tap header seems to be malformed. This is the same for all packets .</p><p>Also why is the Noise parameter now shown in the radio tap details?</p><p>Could anyone help me out with this?</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Screenshot_from_2012-06-18_11:36:04.png" alt="malformed-radio-tap-header" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-radiotap" rel="tag" title="see questions tagged &#39;radiotap&#39;">radiotap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jun '12, 15:47</strong></p><img src="https://secure.gravatar.com/avatar/c8295900dc6e2ff2fccacb803179e07f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hektor&#39;s gravatar image" /><p><span>hektor</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hektor has no accepted answers">0%</span></p></img></div></div><div id="comments-container-12031" class="comments-container"></div><div id="comment-tools-12031" class="comment-tools"></div><div class="clear"></div><div id="comment-12031-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

