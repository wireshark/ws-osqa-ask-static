+++
type = "question"
title = "getting Expert Info (Warn/Undecoded): Unknown bit(s): 0x01"
description = '''Hi,  i&#x27;m using wireshark Version 1.10.0rc1 and i getting this error in my ssl packet Expert Info (Warn/Undecoded): Unknown bit(s): 0x01 was it safe to ignore it? seem like the dissector cant decode that info'''
date = "2013-05-01T23:44:00Z"
lastmod = "2013-05-01T23:44:00Z"
weight = 20890
keywords = [ "info", "bits", "expect", "unknow" ]
aliases = [ "/questions/20890" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [getting Expert Info (Warn/Undecoded): Unknown bit(s): 0x01](/questions/20890/getting-expert-info-warnundecoded-unknown-bits-0x01)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20890-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20890-score" class="post-score" title="current number of votes">0</div><span id="post-20890-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, i'm using wireshark Version 1.10.0rc1 and i getting this error in my ssl packet Expert Info (Warn/Undecoded): Unknown bit(s): 0x01 was it safe to ignore it? seem like the dissector cant decode that info</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-info" rel="tag" title="see questions tagged &#39;info&#39;">info</span> <span class="post-tag tag-link-bits" rel="tag" title="see questions tagged &#39;bits&#39;">bits</span> <span class="post-tag tag-link-expect" rel="tag" title="see questions tagged &#39;expect&#39;">expect</span> <span class="post-tag tag-link-unknow" rel="tag" title="see questions tagged &#39;unknow&#39;">unknow</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 May '13, 23:44</strong></p><img src="https://secure.gravatar.com/avatar/ba7415b503be15241d880cab78574700?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="splibytes&#39;s gravatar image" /><p><span>splibytes</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="splibytes has no accepted answers">0%</span></p></div></div><div id="comments-container-20890" class="comments-container"></div><div id="comment-tools-20890" class="comment-tools"></div><div class="clear"></div><div id="comment-20890-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

