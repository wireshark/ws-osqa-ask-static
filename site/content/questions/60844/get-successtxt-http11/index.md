+++
type = "question"
title = "GET /success.txt HTTP/1.1"
description = '''Hello.  I am quite new in Wireshark. I was just checking the traffic that occured from a youtube video link, when I saw this &quot;GET /success.txt HTTP/1.1&quot; in the info column. Why is this happening? Also the video context is downloaded to my computer through HTTP? Thank you '''
date = "2017-04-15T09:04:00Z"
lastmod = "2017-04-15T09:04:00Z"
weight = 60844
keywords = [ "request", "video", "youtube", "http", "wireshark" ]
aliases = [ "/questions/60844" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [GET /success.txt HTTP/1.1](/questions/60844/get-successtxt-http11)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60844-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60844-score" class="post-score" title="current number of votes">0</div><span id="post-60844-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello. I am quite new in Wireshark. I was just checking the traffic that occured from a youtube video link, when I saw this "GET /success.txt HTTP/1.1" in the info column. Why is this happening? Also the video context is downloaded to my computer through HTTP?</p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-request" rel="tag" title="see questions tagged &#39;request&#39;">request</span> <span class="post-tag tag-link-video" rel="tag" title="see questions tagged &#39;video&#39;">video</span> <span class="post-tag tag-link-youtube" rel="tag" title="see questions tagged &#39;youtube&#39;">youtube</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Apr '17, 09:04</strong></p><img src="https://secure.gravatar.com/avatar/235cc9f26011af9e6ce88f72a4580101?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Valadis&#39;s gravatar image" /><p><span>Valadis</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Valadis has no accepted answers">0%</span></p></div></div><div id="comments-container-60844" class="comments-container"></div><div id="comment-tools-60844" class="comment-tools"></div><div class="clear"></div><div id="comment-60844-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

