+++
type = "question"
title = "Can you build support for TIP (Telepresence Interoperability Protocol)"
description = '''TIP is an extension on SIP to allow Telepresence systems to set up streams in the best way possible. Several new options can be negotiated. More info on http://www.imtc.org/tip/ and http://www.cisco.com/web/about/doing_business/tip/index.html Regards, Paul'''
date = "2011-05-18T06:39:00Z"
lastmod = "2011-05-18T18:12:00Z"
weight = 4120
keywords = [ "tip", "telepresence", "protocol" ]
aliases = [ "/questions/4120" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can you build support for TIP (Telepresence Interoperability Protocol)](/questions/4120/can-you-build-support-for-tip-telepresence-interoperability-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4120-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4120-score" class="post-score" title="current number of votes">0</div><span id="post-4120-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>TIP is an extension on SIP to allow Telepresence systems to set up streams in the best way possible. Several new options can be negotiated. More info on <a href="http://www.imtc.org/tip/">http://www.imtc.org/tip/</a> and <a href="http://www.cisco.com/web/about/doing_business/tip/index.html">http://www.cisco.com/web/about/doing_business/tip/index.html</a></p><p>Regards, Paul</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tip" rel="tag" title="see questions tagged &#39;tip&#39;">tip</span> <span class="post-tag tag-link-telepresence" rel="tag" title="see questions tagged &#39;telepresence&#39;">telepresence</span> <span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 May '11, 06:39</strong></p><img src="https://secure.gravatar.com/avatar/212412506773db9ea34da343ad51594f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="PeteA&#39;s gravatar image" /><p><span>PeteA</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="PeteA has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 May '11, 18:05</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-4120" class="comments-container"></div><div id="comment-tools-4120" class="comment-tools"></div><div class="clear"></div><div id="comment-4120-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4125"></span>

<div id="answer-container-4125" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4125-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4125-score" class="post-score" title="current number of votes">0</div><span id="post-4125-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>New protocol dissectors typically get added by someone with the time, skill and interest to add it. If you're interested in it, the ideal thing would be for you to write a dissector and submit it via an enhancement bug report at <a href="https://bugs.wireshark.org/bugzilla/">https://bugs.wireshark.org/bugzilla/</a>. Of course you can always open an enhancement bug report asking for this protocol to be added, but there's no telling when or even if that will ever happen.</p><p><a href="http://www.wireshark.org/faq.html#q1.11">Question 11</a> of the Wireshark FAQ sums this up more succinctly.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 May '11, 18:12</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-4125" class="comments-container"></div><div id="comment-tools-4125" class="comment-tools"></div><div class="clear"></div><div id="comment-4125-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

