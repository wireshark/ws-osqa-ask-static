+++
type = "question"
title = "iax protocol"
description = '''wireshark doesn&#x27;t capture iax protocol. asterisk version is 1.8.4.2 '''
date = "2012-11-02T00:45:00Z"
lastmod = "2012-11-02T04:23:00Z"
weight = 15484
keywords = [ "iax" ]
aliases = [ "/questions/15484" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [iax protocol](/questions/15484/iax-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15484-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15484-score" class="post-score" title="current number of votes">0</div><span id="post-15484-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>wireshark doesn't capture iax protocol. asterisk version is 1.8.4.2<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-iax" rel="tag" title="see questions tagged &#39;iax&#39;">iax</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Nov '12, 00:45</strong></p><img src="https://secure.gravatar.com/avatar/615153d3e5fa92d2122432f7d4c9f84a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kleinehexe&#39;s gravatar image" /><p><span>kleinehexe</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kleinehexe has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-15484" class="comments-container"></div><div id="comment-tools-15484" class="comment-tools"></div><div class="clear"></div><div id="comment-15484-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15492"></span>

<div id="answer-container-15492" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15492-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15492-score" class="post-score" title="current number of votes">0</div><span id="post-15492-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If the packets isn't captured there is something wrong with your capture setup, if the packets captured isn't recognised as IAX it might be due to preference settings such as configuring the ports used or you may have to use "decode as" on the IAX packets. If you know the IP addresses you could check for traffic between the IP addresses in question.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Nov '12, 04:23</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-15492" class="comments-container"></div><div id="comment-tools-15492" class="comment-tools"></div><div class="clear"></div><div id="comment-15492-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

