+++
type = "question"
title = "can i change the system language inside to software?"
description = '''hi, I&#x27;ve the last version of wireshark and open it in italian language, probably because wireshark know my os and so open it in bases at the version of them. but I would have original wireshark, I give see all in english language, I yet try at downoload disame version of it but I don&#x27;t have wireshar...'''
date = "2016-01-10T09:13:00Z"
lastmod = "2016-01-10T10:30:00Z"
weight = 49051
keywords = [ "wireshark" ]
aliases = [ "/questions/49051" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [can i change the system language inside to software?](/questions/49051/can-i-change-the-system-language-inside-to-software)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49051-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49051-score" class="post-score" title="current number of votes">0</div><span id="post-49051-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>hi, I've the last version of wireshark and open it in italian language, probably because wireshark know my os and so open it in bases at the version of them. but I would have original wireshark, I give see all in english language, I yet try at downoload disame version of it but I don't have wireshark in english; sorry for like esprime me, I'm italian and I try improve english</p><p>very thanks to respond me</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jan '16, 09:13</strong></p><img src="https://secure.gravatar.com/avatar/2349aa8abfc5cec1d2d0ac44ce4a8ac5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anatolij%20Prosen&#39;s gravatar image" /><p><span>Anatolij Prosen</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anatolij Prosen has no accepted answers">0%</span></p></div></div><div id="comments-container-49051" class="comments-container"></div><div id="comment-tools-49051" class="comment-tools"></div><div class="clear"></div><div id="comment-49051-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="49052"></span>

<div id="answer-container-49052" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49052-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49052-score" class="post-score" title="current number of votes">2</div><span id="post-49052-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See under Edit ! Preferences ! Appearance (However this is shown in Italian).</p><p>Also see <a href="https://ask.wireshark.org/questions/48823/change-the-gui-language">Change the GUI Language</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Jan '16, 09:39</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div></div><div id="comments-container-49052" class="comments-container"><span id="49053"></span><div id="comment-49053" class="comment"><div id="post-49053-score" class="comment-score"></div><div class="comment-text"><p>Ty . Now i try it</p></div><div id="comment-49053-info" class="comment-info"><span class="comment-age">(10 Jan '16, 10:12)</span> <span class="comment-user userinfo">Anatolij Prosen</span></div></div><span id="49054"></span><div id="comment-49054" class="comment"><div id="post-49054-score" class="comment-score"></div><div class="comment-text"><p>Another way to describe it is the last item in the first set of preferences.</p></div><div id="comment-49054-info" class="comment-info"><span class="comment-age">(10 Jan '16, 10:30)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div></div><div id="comment-tools-49052" class="comment-tools"></div><div class="clear"></div><div id="comment-49052-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

