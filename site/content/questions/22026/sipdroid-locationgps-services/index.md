+++
type = "question"
title = "SIPdroid Location(gps) services"
description = '''Hello I am trying to find where the gps location is located in a SIP message using the SIPdroid app on android. I&#x27;ve been searching through wireshark packets for hours and haven&#x27;t had any luck. If anyone has any knowledge about this it will be greatly appreciated. Such as if the location is even in ...'''
date = "2013-06-13T13:50:00Z"
lastmod = "2013-06-13T13:50:00Z"
weight = 22026
keywords = [ "gps", "android", "sip", "location", "wireshark" ]
aliases = [ "/questions/22026" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [SIPdroid Location(gps) services](/questions/22026/sipdroid-locationgps-services)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22026-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22026-score" class="post-score" title="current number of votes">0</div><span id="post-22026-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello I am trying to find where the gps location is located in a SIP message using the SIPdroid app on android. I've been searching through wireshark packets for hours and haven't had any luck.</p><p>If anyone has any knowledge about this it will be greatly appreciated. Such as if the location is even in the sip, what it looks like and if wireshark is even capable of picking this up/decoding it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gps" rel="tag" title="see questions tagged &#39;gps&#39;">gps</span> <span class="post-tag tag-link-android" rel="tag" title="see questions tagged &#39;android&#39;">android</span> <span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span> <span class="post-tag tag-link-location" rel="tag" title="see questions tagged &#39;location&#39;">location</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jun '13, 13:50</strong></p><img src="https://secure.gravatar.com/avatar/98c32c04c1bd6b08de82960de68b0fc3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="everyday09&#39;s gravatar image" /><p><span>everyday09</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="everyday09 has no accepted answers">0%</span></p></div></div><div id="comments-container-22026" class="comments-container"></div><div id="comment-tools-22026" class="comment-tools"></div><div class="clear"></div><div id="comment-22026-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

