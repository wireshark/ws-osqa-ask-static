+++
type = "question"
title = "last modified time on server"
description = '''How can I learn the last modified time of the file that I retrieve at the server ? There is something like &#x27;the delta time&#x27; in frame segment but I don&#x27;t know.'''
date = "2010-10-30T14:11:00Z"
lastmod = "2010-10-30T18:49:00Z"
weight = 747
keywords = [ "modified", "time" ]
aliases = [ "/questions/747" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [last modified time on server](/questions/747/last-modified-time-on-server)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-747-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-747-score" class="post-score" title="current number of votes">0</div><span id="post-747-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can I learn the last modified time of the file that I retrieve at the server ? There is something like 'the delta time' in frame segment but I don't know.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-modified" rel="tag" title="see questions tagged &#39;modified&#39;">modified</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Oct '10, 14:11</strong></p><img src="https://secure.gravatar.com/avatar/54b86d2c753de05e2532008fb8ff933c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tyrve&#39;s gravatar image" /><p><span>tyrve</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tyrve has no accepted answers">0%</span></p></div></div><div id="comments-container-747" class="comments-container"></div><div id="comment-tools-747" class="comment-tools"></div><div class="clear"></div><div id="comment-747-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="750"></span>

<div id="answer-container-750" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-750-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-750-score" class="post-score" title="current number of votes">0</div><span id="post-750-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not exactly sure of the details on this yet, but can't you just go to the server and look at the Last Modified Time value for the file?</p><p>Depending on the application used to download a file, the application may include a Last Modified time field - such as in the case of an HTTP OK reply - look in the details and it should say when the file on the server was last modified.</p><p>The delta time you refer to, if in the Wireshark frame section, indicates when the packet was seen compared to the (a) first packet or time referenced packet if set or (b) the previous displayed packet, if a display filter was set.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Oct '10, 18:49</strong></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchappell has 6 accepted answers">8%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Oct '10, 18:50</strong> </span></p></div></div><div id="comments-container-750" class="comments-container"></div><div id="comment-tools-750" class="comment-tools"></div><div class="clear"></div><div id="comment-750-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

