+++
type = "question"
title = "How to compile release version of wireshark?"
description = '''I want to compile the release version of Wireshark, what config should I modify?'''
date = "2014-01-14T18:28:00Z"
lastmod = "2014-01-21T00:56:00Z"
weight = 28892
keywords = [ "release", "wireshark" ]
aliases = [ "/questions/28892" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [How to compile release version of wireshark?](/questions/28892/how-to-compile-release-version-of-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28892-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28892-score" class="post-score" title="current number of votes">0</div><span id="post-28892-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to compile the release version of Wireshark, what config should I modify?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-release" rel="tag" title="see questions tagged &#39;release&#39;">release</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jan '14, 18:28</strong></p><img src="https://secure.gravatar.com/avatar/13679628c84abac93be65773340d2589?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="metamatrix&#39;s gravatar image" /><p><span>metamatrix</span><br />
<span class="score" title="56 reputation points">56</span><span title="16 badges"><span class="badge1">●</span><span class="badgecount">16</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="19 badges"><span class="bronze">●</span><span class="badgecount">19</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="metamatrix has one accepted answer">100%</span></p></div></div><div id="comments-container-28892" class="comments-container"><span id="28900"></span><div id="comment-28900" class="comment"><div id="post-28900-score" class="comment-score"></div><div class="comment-text"><p>???</p><p>according to another question, you have already compiled Wireshark</p><blockquote><p><a href="http://ask.wireshark.org/questions/27139/linking-problem-when-add-an-extra-lib-to-wireshark">http://ask.wireshark.org/questions/27139/linking-problem-when-add-an-extra-lib-to-wireshark</a></p></blockquote><p>So, what exactly is your question?</p></div><div id="comment-28900-info" class="comment-info"><span class="comment-age">(15 Jan '14, 00:42)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="29026"></span><div id="comment-29026" class="comment"><div id="post-29026-score" class="comment-score"></div><div class="comment-text"><p>I think it's the debug version, then is there any release version to compile?</p></div><div id="comment-29026-info" class="comment-info"><span class="comment-age">(20 Jan '14, 01:18)</span> <span class="comment-user userinfo">metamatrix</span></div></div><span id="29027"></span><div id="comment-29027" class="comment"><div id="post-29027-score" class="comment-score"></div><div class="comment-text"><p>What OS are you compiling on?</p></div><div id="comment-29027-info" class="comment-info"><span class="comment-age">(20 Jan '14, 02:13)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="29035"></span><div id="comment-29035" class="comment"><div id="post-29035-score" class="comment-score"></div><div class="comment-text"><p>I'm compiling on windows.</p></div><div id="comment-29035-info" class="comment-info"><span class="comment-age">(20 Jan '14, 16:30)</span> <span class="comment-user userinfo">metamatrix</span></div></div></div><div id="comment-tools-28892" class="comment-tools"></div><div class="clear"></div><div id="comment-28892-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29046"></span>

<div id="answer-container-29046" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29046-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29046-score" class="post-score" title="current number of votes">0</div><span id="post-29046-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="metamatrix has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>We don't have any notion of debug and release builds, so there's no release version to build and nothing to modify.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Jan '14, 00:56</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-29046" class="comments-container"></div><div id="comment-tools-29046" class="comment-tools"></div><div class="clear"></div><div id="comment-29046-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

