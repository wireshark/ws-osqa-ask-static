+++
type = "question"
title = "Error 105 (net::ERR_NAME_NOT_RESOLVED)"
description = '''After flushing my DNS, thinking it was the problem on why im not able to browse on the internet unfortunately it didn&#x27;t worked, and having this error message: &quot;Error 105 (net::ERR_NAME_NOT_RESOLVED): Unable to resolve the server&#x27;s DNS address.&quot; Please help, thanks in advance.'''
date = "2011-08-03T02:42:00Z"
lastmod = "2011-08-03T17:18:00Z"
weight = 5420
keywords = [ "105", "error" ]
aliases = [ "/questions/5420" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Error 105 (net::ERR\_NAME\_NOT\_RESOLVED)](/questions/5420/error-105-neterr_name_not_resolved)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5420-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5420-score" class="post-score" title="current number of votes">0</div><span id="post-5420-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>After flushing my DNS, thinking it was the problem on why im not able to browse on the internet unfortunately it didn't worked, and having this error message: "Error 105 (net::ERR_NAME_NOT_RESOLVED): Unable to resolve the server's DNS address." Please help, thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-105" rel="tag" title="see questions tagged &#39;105&#39;">105</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Aug '11, 02:42</strong></p><img src="https://secure.gravatar.com/avatar/c3d8290c6fcfb178b932433e9760e864?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kane&#39;s gravatar image" /><p><span>Kane</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kane has no accepted answers">0%</span></p></div></div><div id="comments-container-5420" class="comments-container"></div><div id="comment-tools-5420" class="comment-tools"></div><div class="clear"></div><div id="comment-5420-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5461"></span>

<div id="answer-container-5461" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5461-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5461-score" class="post-score" title="current number of votes">0</div><span id="post-5461-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I fail to find the relevance of this question to Wireshark. It looks like you've got some sort of DNS problem. You might want to try a google search for your error.</p><p>From the <a href="http://ask.wireshark.org/faq/">FAQ</a>:</p><p><strong>What kinds of questions can I ask here?</strong></p><p>Questions should be relevant to Wireshark features, protocol analysis, or Wireshark development. Before asking please search for similar questions. You can search for questions by their title or tags.</p><p><strong>What kinds of questions should be avoided?</strong></p><p>Please avoid asking questions that are not relevant to this community, too subjective and argumentative.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Aug '11, 17:18</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-5461" class="comments-container"></div><div id="comment-tools-5461" class="comment-tools"></div><div class="clear"></div><div id="comment-5461-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

