+++
type = "question"
title = "please please please  some help"
description = '''please any one can help me to do the following filters in wireshark  number of connections to the same host as current connection in the past two seconds. number of connections to the same services as current connection % connection that have syn error to the same host % connection that have syn err...'''
date = "2012-04-22T21:05:00Z"
lastmod = "2012-04-22T21:05:00Z"
weight = 10386
keywords = [ "filter", "homework" ]
aliases = [ "/questions/10386" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [please please please some help](/questions/10386/please-please-please-some-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10386-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10386-score" class="post-score" title="current number of votes">-1</div><span id="post-10386-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>please any one can help me to do the following filters in wireshark</p><ol><li>number of connections to the same host as current connection in the past two seconds.</li><li>number of connections to the same services as current connection</li><li>% connection that have syn error to the same host</li><li>% connection that have syn error to the same service</li><li>% connection that have reg error to the same host</li><li>% connection that have syn error to the same service</li><li>% connection to the same service in same host</li><li>% connection to the different services in same host</li><li>% connection to different host</li></ol></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-homework" rel="tag" title="see questions tagged &#39;homework&#39;">homework</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Apr '12, 21:05</strong></p><img src="https://secure.gravatar.com/avatar/5cbecc11a3841ad2ca933a86ce70a792?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="azhad&#39;s gravatar image" /><p><span>azhad</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="azhad has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Apr '12, 04:49</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-10386" class="comments-container"></div><div id="comment-tools-10386" class="comment-tools"></div><div class="clear"></div><div id="comment-10386-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

