+++
type = "question"
title = "How to summarized the out put of Wireshark into netwok connection records? (Like KDD99)"
description = '''Hi, I used Wireshark to capture network packets and now I need to summarized the outputs into network connection recods like KDD99 structures. Any Ideas? Thanks'''
date = "2015-06-30T03:52:00Z"
lastmod = "2015-06-30T03:52:00Z"
weight = 43722
keywords = [ "analyzedata" ]
aliases = [ "/questions/43722" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to summarized the out put of Wireshark into netwok connection records? (Like KDD99)](/questions/43722/how-to-summarized-the-out-put-of-wireshark-into-netwok-connection-records-like-kdd99)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43722-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43722-score" class="post-score" title="current number of votes">0</div><span id="post-43722-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I used Wireshark to capture network packets and now I need to summarized the outputs into network connection recods like KDD99 structures. Any Ideas?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-analyzedata" rel="tag" title="see questions tagged &#39;analyzedata&#39;">analyzedata</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jun '15, 03:52</strong></p><img src="https://secure.gravatar.com/avatar/a68c587b14cb104b706f06dfefd3daed?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="p-my&#39;s gravatar image" /><p><span>p-my</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="p-my has no accepted answers">0%</span></p></div></div><div id="comments-container-43722" class="comments-container"></div><div id="comment-tools-43722" class="comment-tools"></div><div class="clear"></div><div id="comment-43722-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

