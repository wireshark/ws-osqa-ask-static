+++
type = "question"
title = "Is Wireshark approved for use on DoD networks?"
description = '''Is wireshark approved for use on DoD networks? Is it on the Air Force Evaluated Products List?'''
date = "2016-03-09T11:27:00Z"
lastmod = "2016-03-12T04:32:00Z"
weight = 50789
keywords = [ "dod" ]
aliases = [ "/questions/50789" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [Is Wireshark approved for use on DoD networks?](/questions/50789/is-wireshark-approved-for-use-on-dod-networks)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50789-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50789-score" class="post-score" title="current number of votes">0</div><span id="post-50789-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is wireshark approved for use on DoD networks? Is it on the Air Force Evaluated Products List?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dod" rel="tag" title="see questions tagged &#39;dod&#39;">dod</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Mar '16, 11:27</strong></p><img src="https://secure.gravatar.com/avatar/f7d60fc735c7026864c1ed02a3f6dd14?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Walter%20Bick&#39;s gravatar image" /><p><span>Walter Bick</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Walter Bick has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Mar '16, 11:34</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-50789" class="comments-container"></div><div id="comment-tools-50789" class="comment-tools"></div><div class="clear"></div><div id="comment-50789-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="50790"></span>

<div id="answer-container-50790" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50790-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50790-score" class="post-score" title="current number of votes">3</div><span id="post-50790-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Jaap has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I guess you'll have to ask whoever is responsible for approval of software at the DoD or the Airforce.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Mar '16, 00:19</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-50790" class="comments-container"></div><div id="comment-tools-50790" class="comment-tools"></div><div class="clear"></div><div id="comment-50790-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="50839"></span>

<div id="answer-container-50839" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50839-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50839-score" class="post-score" title="current number of votes">0</div><span id="post-50839-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark Certified Network Anaylst exam was DoD 8570-approved by the US Army.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Mar '16, 04:32</strong></p><img src="https://secure.gravatar.com/avatar/d9f93da56cb61ea33642fec103b9565a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Paulhwentzel&#39;s gravatar image" /><p><span>Paulhwentzel</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Paulhwentzel has no accepted answers">0%</span></p></div></div><div id="comments-container-50839" class="comments-container"></div><div id="comment-tools-50839" class="comment-tools"></div><div class="clear"></div><div id="comment-50839-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

