+++
type = "question"
title = "Using this filter?"
description = '''what i can do with this question... i mean what i can mention in this icon.... to start this software???'''
date = "2016-01-09T17:48:00Z"
lastmod = "2016-01-11T08:04:00Z"
weight = 49035
keywords = [ "wireshark" ]
aliases = [ "/questions/49035" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Using this filter?](/questions/49035/using-this-filter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49035-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49035-score" class="post-score" title="current number of votes">0</div><span id="post-49035-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>what i can do with this question... i mean what i can mention in this icon.... to start this software???</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jan '16, 17:48</strong></p><img src="https://secure.gravatar.com/avatar/3641d29f60dd4a481057614e1b6c1f79?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="John%20Ibraham&#39;s gravatar image" /><p><span>John Ibraham</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="John Ibraham has no accepted answers">0%</span></p></div></div><div id="comments-container-49035" class="comments-container"><span id="49037"></span><div id="comment-49037" class="comment"><div id="post-49037-score" class="comment-score">1</div><div class="comment-text"><p>Could you please reword this question? Start by explaining what you are trying to accomplish, how you are trying to do it, and where you encounter a problem.</p><p>I really don't understand what you're trying to ask here.</p></div><div id="comment-49037-info" class="comment-info"><span class="comment-age">(09 Jan '16, 18:27)</span> <span class="comment-user userinfo">Quadratic</span></div></div><span id="49066"></span><div id="comment-49066" class="comment"><div id="post-49066-score" class="comment-score"></div><div class="comment-text"><p>can you guide me step by step for using wireshark? i have no idea to use this software</p></div><div id="comment-49066-info" class="comment-info"><span class="comment-age">(10 Jan '16, 21:09)</span> <span class="comment-user userinfo">John Ibraham</span></div></div></div><div id="comment-tools-49035" class="comment-tools"></div><div class="clear"></div><div id="comment-49035-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="49068"></span>

<div id="answer-container-49068" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49068-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49068-score" class="post-score" title="current number of votes">0</div><span id="post-49068-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>For starters you might want to visit the Sharkfest presentations going back to 2008 where you find the 'Downloaded wireshark - Now What?" https://sharkfest.wireshark.org/sf08.html<br />
or visit the Sharkfest youtube videos <a href="https://www.youtube.com/watch?v=635snt797bQ">https://www.youtube.com/watch?v=635snt797bQ</a><br />
This is a Q&amp;A site where specific questions hopefully get answers ...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Jan '16, 23:35</strong></p><img src="https://secure.gravatar.com/avatar/5500bd1decb766660522dfb347eedc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde&#39;s gravatar image" /><p><span>mrEEde</span><br />
<span class="score" title="3892 reputation points"><span>3.9k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="70 badges"><span class="bronze">●</span><span class="badgecount">70</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde has 48 accepted answers">20%</span> </br></br></p></div></div><div id="comments-container-49068" class="comments-container"></div><div id="comment-tools-49068" class="comment-tools"></div><div class="clear"></div><div id="comment-49068-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="49085"></span>

<div id="answer-container-49085" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49085-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49085-score" class="post-score" title="current number of votes">0</div><span id="post-49085-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>can you guide me step by step for using wireshark? i have no idea to use this software</p></blockquote><p>please take a look at answers to similar questions:</p><blockquote><p><a href="https://ask.wireshark.org/questions/11370/a-good-way-to-learn-how-to-use-wireshark">https://ask.wireshark.org/questions/11370/a-good-way-to-learn-how-to-use-wireshark</a><br />
<a href="https://ask.wireshark.org/questions/10166/online-tutorial-for-reading-packet-capture-files">https://ask.wireshark.org/questions/10166/online-tutorial-for-reading-packet-capture-files</a><br />
<a href="https://ask.wireshark.org/questions/19980/how-to-study-to-use-wireshark">https://ask.wireshark.org/questions/19980/how-to-study-to-use-wireshark</a><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jan '16, 08:04</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-49085" class="comments-container"></div><div id="comment-tools-49085" class="comment-tools"></div><div class="clear"></div><div id="comment-49085-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

