+++
type = "question"
title = "Why is there no interface found on a Windows 10 installation?"
description = '''I tried to capture a trace with Wireshark version 2.0.3 on a Windows 10 desktop computer, but it said there is no interface found. I tried to use control panel to allow it to pass Windows 10 Firewall, it didn&#x27;t work. I have another installation on a Windows 10 Notebook computer, it didn&#x27;t have this ...'''
date = "2016-05-17T10:05:00Z"
lastmod = "2016-05-17T10:14:00Z"
weight = 52679
keywords = [ "windows10", "nointerfacefound" ]
aliases = [ "/questions/52679" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Why is there no interface found on a Windows 10 installation?](/questions/52679/why-is-there-no-interface-found-on-a-windows-10-installation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52679-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52679-score" class="post-score" title="current number of votes">0</div><span id="post-52679-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I tried to capture a trace with Wireshark version 2.0.3 on a Windows 10 desktop computer, but it said there is no interface found. I tried to use control panel to allow it to pass Windows 10 Firewall, it didn't work. I have another installation on a Windows 10 Notebook computer, it didn't have this problem. Any idea? The desktop computer OS is an upgrade from Windows 8.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows10" rel="tag" title="see questions tagged &#39;windows10&#39;">windows10</span> <span class="post-tag tag-link-nointerfacefound" rel="tag" title="see questions tagged &#39;nointerfacefound&#39;">nointerfacefound</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 May '16, 10:05</strong></p><img src="https://secure.gravatar.com/avatar/22769c126211a728df8995bdf43dcd82?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JohnSmith325&#39;s gravatar image" /><p><span>JohnSmith325</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JohnSmith325 has no accepted answers">0%</span></p></div></div><div id="comments-container-52679" class="comments-container"><span id="52680"></span><div id="comment-52680" class="comment"><div id="post-52680-score" class="comment-score"></div><div class="comment-text"><p>What does the Help -&gt; About menu show, particularly the "Running ..." bit. Mine shows:</p><pre><code>Running on 64-bit Windows 10, build 10586, with locale C, with WinPcap version
4.1.3 (packet.dll version 4.1.0.2980), based on libpcap version 1.0 branch
1_0_rel0b (20091008), with GnuTLS 3.2.15, with Gcrypt 1.6.2, without AirPcap.
Intel(R) Core(TM) i7-4770 CPU @ 3.40GHz (with SSE4.2), with 32719MB of physical memory.</code></pre></div><div id="comment-52680-info" class="comment-info"><span class="comment-age">(17 May '16, 10:14)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-52679" class="comment-tools"></div><div class="clear"></div><div id="comment-52679-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

