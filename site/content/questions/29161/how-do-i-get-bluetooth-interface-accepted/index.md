+++
type = "question"
title = "How do I get bluetooth interface accepted"
description = '''How can I get my bluetooth interface accepted by Wireshark? Get an error message, which I have googled on the internet and searched here in Wireshark. The capture session could not be initiated (Can&#x27;t attach to device 0 22:Invalid argument). Please check to make sure you have sufficient permissions,...'''
date = "2014-01-25T19:27:00Z"
lastmod = "2014-01-25T19:27:00Z"
weight = 29161
keywords = [ "linux", "hcidump", "bluetooth" ]
aliases = [ "/questions/29161" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How do I get bluetooth interface accepted](/questions/29161/how-do-i-get-bluetooth-interface-accepted)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29161-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29161-score" class="post-score" title="current number of votes">0</div><span id="post-29161-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can I get my bluetooth interface accepted by Wireshark?</p><p>Get an error message, which I have googled on the internet and searched here in Wireshark.</p><p>The capture session could not be initiated (Can't attach to device 0 22:Invalid argument).</p><p>Please check to make sure you have sufficient permissions, and that you have the proper interface or pipe specified.</p><p>Running su root at present in PCLinuxOS Mate 64 bit. System will catch fine if I change to eth0</p><p>Edit: Discovered later, following can be seen in root,</p><p>[<span class="__cf_email__" data-cfemail="691b06061d2904081d0c5f5d">[email protected]</span> gert]# dumpcap -D 1. eth0 2. wlan0 3. bluetooth0 4. usbmon1 5. usbmon2 6. usbmon3 7. usbmon4 8. usbmon5 9. any 10. lo (Loopback) [<span class="__cf_email__" data-cfemail="24564b4b5064494550411210">[email protected]</span> gert]#</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span> <span class="post-tag tag-link-hcidump" rel="tag" title="see questions tagged &#39;hcidump&#39;">hcidump</span> <span class="post-tag tag-link-bluetooth" rel="tag" title="see questions tagged &#39;bluetooth&#39;">bluetooth</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jan '14, 19:27</strong></p><img src="https://secure.gravatar.com/avatar/b383ad9bfce0817dec73bf95f6d556e7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="handy2209&#39;s gravatar image" /><p><span>handy2209</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="handy2209 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Jan '14, 21:46</strong> </span></p></div></div><div id="comments-container-29161" class="comments-container"></div><div id="comment-tools-29161" class="comment-tools"></div><div class="clear"></div><div id="comment-29161-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

