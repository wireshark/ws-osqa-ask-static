+++
type = "question"
title = "Can&#x27;t right click?"
description = '''Hi Wonder if someone can help? I&#x27;m currently using Wireshark to try and obtain my user name and password for my isp so that I can swap their router for my own. Ive looked on YouTube to see how I can obtain this. I&#x27;ve selected a packet and then opened bootstrap protocol. I then need to right click on...'''
date = "2016-11-20T01:10:00Z"
lastmod = "2016-11-20T02:18:00Z"
weight = 57476
keywords = [ "right-click" ]
aliases = [ "/questions/57476" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can't right click?](/questions/57476/cant-right-click)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57476-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57476-score" class="post-score" title="current number of votes">0</div><span id="post-57476-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Wonder if someone can help?</p><p>I'm currently using Wireshark to try and obtain my user name and password for my isp so that I can swap their router for my own. Ive looked <a href="https://www.youtube.com/watch?v=BdcsmYX7eEQ">on YouTube</a> to see how I can obtain this. I've selected a packet and then opened bootstrap protocol. I then need to right click on option (61) client identifier. After doing this nothing happens. What I am meant to see is a pop up menu that will let me select - copy, bytes &amp; printable text only, but this is not appearing.<br />
</p><p>Any help would be appreciated.<br />
</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-right-click" rel="tag" title="see questions tagged &#39;right-click&#39;">right-click</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Nov '16, 01:10</strong></p><img src="https://secure.gravatar.com/avatar/7b3fbac35b0b2ddc198e45467d55589a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mrcmh&#39;s gravatar image" /><p><span>Mrcmh</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mrcmh has no accepted answers">0%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Nov '16, 02:40</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-57476" class="comments-container"><span id="57480"></span><div id="comment-57480" class="comment"><div id="post-57480-score" class="comment-score"></div><div class="comment-text"><p>Are you running Wireshark on a Mac OS?</p></div><div id="comment-57480-info" class="comment-info"><span class="comment-age">(20 Nov '16, 02:18)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-57476" class="comment-tools"></div><div class="clear"></div><div id="comment-57476-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

