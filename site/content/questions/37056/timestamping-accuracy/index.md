+++
type = "question"
title = "Timestamping Accuracy"
description = '''Hi all, does anybody know which is the accuracy of the packet timestamping for the packet &quot;arrival Time&quot; info in Wireshark log? I mean, should I or should not believe to the dipslayed timestamp down to the microsecond? Thanks a lot! MMM'''
date = "2014-10-15T04:09:00Z"
lastmod = "2014-10-15T04:28:00Z"
weight = 37056
keywords = [ "timing", "timestamping", "accuracy" ]
aliases = [ "/questions/37056" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Timestamping Accuracy](/questions/37056/timestamping-accuracy)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37056-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37056-score" class="post-score" title="current number of votes">0</div><span id="post-37056-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all, does anybody know which is the accuracy of the packet timestamping for the packet "arrival Time" info in Wireshark log? I mean, should I or should not believe to the dipslayed timestamp down to the microsecond? Thanks a lot! MMM</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-timing" rel="tag" title="see questions tagged &#39;timing&#39;">timing</span> <span class="post-tag tag-link-timestamping" rel="tag" title="see questions tagged &#39;timestamping&#39;">timestamping</span> <span class="post-tag tag-link-accuracy" rel="tag" title="see questions tagged &#39;accuracy&#39;">accuracy</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Oct '14, 04:09</strong></p><img src="https://secure.gravatar.com/avatar/3e3017ce150afcd8a315c019d3d3d1f5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MMM&#39;s gravatar image" /><p><span>MMM</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MMM has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Oct '14, 04:35</strong> </span></p></div></div><div id="comments-container-37056" class="comments-container"></div><div id="comment-tools-37056" class="comment-tools"></div><div class="clear"></div><div id="comment-37056-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37057"></span>

<div id="answer-container-37057" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37057-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37057-score" class="post-score" title="current number of votes">0</div><span id="post-37057-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Where packet timstamping happens depends on the OS and the capturing mechanism but gennerally packets are timestamped by the kernel on its way from the NIC to the application reading them, so no I wouldent expect the timstamps to be that acurate I'd guess at ballpark +- 10 ms</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Oct '14, 04:28</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-37057" class="comments-container"></div><div id="comment-tools-37057" class="comment-tools"></div><div class="clear"></div><div id="comment-37057-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

