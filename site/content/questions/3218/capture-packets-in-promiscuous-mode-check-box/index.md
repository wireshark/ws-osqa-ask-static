+++
type = "question"
title = "&quot;Capture packets in promiscuous mode&quot; check-box"
description = '''Wireshark Version 1.4.2; Win7Pro64; 802.11 sniffing. Does the presence of the &quot;Capture packets in promiscuous mode&quot; option box imply that it is, in fact, possible to put my adapter into promiscuous mode? The box is present, and is checked, yet I am only seeing traffic to or from the machine running ...'''
date = "2011-03-29T17:50:00Z"
lastmod = "2011-04-03T15:01:00Z"
weight = 3218
keywords = [ "wireless", "windows7", "promiscuous" ]
aliases = [ "/questions/3218" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# ["Capture packets in promiscuous mode" check-box](/questions/3218/capture-packets-in-promiscuous-mode-check-box)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3218-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3218-score" class="post-score" title="current number of votes">0</div><span id="post-3218-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark Version 1.4.2; Win7Pro64; 802.11 sniffing. Does the presence of the "Capture packets in promiscuous mode" option box imply that it is, in fact, possible to put my adapter into promiscuous mode? The box is present, and is checked, yet I am only seeing traffic to or from the machine running Wireshark, suggesting that it is not really in promiscuous mode.</p><p>Thanks, Howard</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-promiscuous" rel="tag" title="see questions tagged &#39;promiscuous&#39;">promiscuous</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Mar '11, 17:50</strong></p><img src="https://secure.gravatar.com/avatar/52d67f1a733af0acc0310b8313fc21e2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hdelman&#39;s gravatar image" /><p><span>hdelman</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hdelman has no accepted answers">0%</span></p></div></div><div id="comments-container-3218" class="comments-container"></div><div id="comment-tools-3218" class="comment-tools"></div><div class="clear"></div><div id="comment-3218-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="3224"></span>

<div id="answer-container-3224" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3224-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3224-score" class="post-score" title="current number of votes">1</div><span id="post-3224-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark running on Windows cannot put wifi adapters into monitor mode unless it is an AirPCAP adapter. If you do not have such an adapter the promiscuous mode check box doesn't help and you'll only see your own traffic, and without 802.11 layer as well.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Mar '11, 02:04</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-3224" class="comments-container"></div><div id="comment-tools-3224" class="comment-tools"></div><div class="clear"></div><div id="comment-3224-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="3232"></span>

<div id="answer-container-3232" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3232-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3232-score" class="post-score" title="current number of votes">1</div><span id="post-3232-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There's no way to query a device driver to ask whether promiscuous mode is supported or works, so Wireshark always offers the checkbox, whether it works or not. On Windows, a lot of 802.11 devices and drivers don't support promiscuous mode - for 802.11, that would mean a mode in which the device captures all packets on the network with which the device is associated, but not other devices - and that's probably the case with your adapter.</p><p>Unfortunately, WinPcap doesn't support "monitor mode", in which an 802.11 adapter captures all packets the radio receives, whether they're for that network or not, so Wireshark doesn't support it on Windows, either. You'd need an <a href="http://www.riverbed.com/us/products/cascade/airpcap.php">AirPcap adapter</a> to capture 802.11 traffic not sent from or to your machine.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Mar '11, 10:24</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-3232" class="comments-container"><span id="3312"></span><div id="comment-3312" class="comment"><div id="post-3312-score" class="comment-score"></div><div class="comment-text"><p>Thank you to all who answered. I now own an AirPcap adapter, and now I'm seeing everything! But that has introduced a whole new set of problems... TMI! For all that I've read on the 'net concerning capture filters and display filters, I'm still quite befuddled.</p><p>Is there a way to have the 802.11 data packets displayed as Ethernet packets, so that I can use conventional TCP/IP display filters?</p><p>Is there a way to filter out beacon and management frames, so as to only capture data frames?</p><p>Thanks again, Howard</p></div><div id="comment-3312-info" class="comment-info"><span class="comment-age">(03 Apr '11, 14:41)</span> <span class="comment-user userinfo">hdelman</span></div></div><span id="3313"></span><div id="comment-3313" class="comment"><div id="post-3313-score" class="comment-score">1</div><div class="comment-text"><p>Separate questions should be asked as separate questions, not as "answers" to your own question. I've converted your questions to a comment under my reply.</p><p>No, there's no way to have the 802.11 data packets displayed as Ethernet packets - and there's no need to, as TCP/IP display filters only look at the TCP and IP headers, and Wireshark can dissect IP atop 802.11 just as well as it can dissect it atop Ethernet, or PPP, or any of the other layers atop which it can run, while TCP runs on top of IP, so the display filters will work.</p></div><div id="comment-3313-info" class="comment-info"><span class="comment-age">(03 Apr '11, 15:00)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="3314"></span><div id="comment-3314" class="comment"><div id="post-3314-score" class="comment-score"></div><div class="comment-text"><p>If you want a capture filter that captures only data frames, "type data" should work, at least with recent versions of WinPcap.</p></div><div id="comment-3314-info" class="comment-info"><span class="comment-age">(03 Apr '11, 15:01)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-3232" class="comment-tools"></div><div class="clear"></div><div id="comment-3232-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="3219"></span>

<div id="answer-container-3219" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3219-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3219-score" class="post-score" title="current number of votes">0</div><span id="post-3219-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The check box doesn't mean that the interface can go into promiscuous mode. If it is checked it will attempt to put it into promiscuous mode. I have some wireless adapters that when it is checked, it doesn't capture at all.<br />
</p><p>Your issue might not be related to this though. Is your nic connected to a switch? If so, did you configure the switchport to be in "span" or "monitor" mode?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Mar '11, 18:10</strong></p><img src="https://secure.gravatar.com/avatar/e62501f00394530927e4b0c9e86bfb46?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Paul%20Stewart&#39;s gravatar image" /><p><span>Paul Stewart</span><br />
<span class="score" title="301 reputation points">301</span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Paul Stewart has 3 accepted answers">6%</span> </br></p></div></div><div id="comments-container-3219" class="comments-container"></div><div id="comment-tools-3219" class="comment-tools"></div><div class="clear"></div><div id="comment-3219-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

