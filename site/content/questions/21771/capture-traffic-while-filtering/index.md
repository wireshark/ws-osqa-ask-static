+++
type = "question"
title = "Capture traffic while filtering"
description = '''Hi, I want to start Wireshark and continue saving to a text file WHILE the capture/filter is running...but it seems I can only save the capture once I stop it, is there any way to save it while it is running?'''
date = "2013-06-05T10:00:00Z"
lastmod = "2013-06-06T18:18:00Z"
weight = 21771
keywords = [ "capture", "filtering", "ftp" ]
aliases = [ "/questions/21771" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capture traffic while filtering](/questions/21771/capture-traffic-while-filtering)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21771-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21771-score" class="post-score" title="current number of votes">0</div><span id="post-21771-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I want to start Wireshark and continue saving to a text file WHILE the capture/filter is running...but it seems I can only save the capture once I stop it, is there any way to save it while it is running?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-filtering" rel="tag" title="see questions tagged &#39;filtering&#39;">filtering</span> <span class="post-tag tag-link-ftp" rel="tag" title="see questions tagged &#39;ftp&#39;">ftp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jun '13, 10:00</strong></p><img src="https://secure.gravatar.com/avatar/908dce768dea1d7941aabe5e077e2748?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sharwal&#39;s gravatar image" /><p><span>sharwal</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sharwal has no accepted answers">0%</span></p></div></div><div id="comments-container-21771" class="comments-container"></div><div id="comment-tools-21771" class="comment-tools"></div><div class="clear"></div><div id="comment-21771-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="21809"></span>

<div id="answer-container-21809" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21809-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21809-score" class="post-score" title="current number of votes">0</div><span id="post-21809-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes. Assuming you have the latest version, click on the Capture Interfaces button on the far left-hand side, select the interface(s) you want to capture, and click Options. Under the "Capture File(s)" section of that menu, specify the file name and the stop criteria (if you want automatic stop criteria other than clicking the "Stop" button). Since you want to capture in real-time as well, make sure the "Update list of packets in real time" option and click Start.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Jun '13, 18:18</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Jun '13, 18:19</strong> </span></p></div></div><div id="comments-container-21809" class="comments-container"></div><div id="comment-tools-21809" class="comment-tools"></div><div class="clear"></div><div id="comment-21809-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

