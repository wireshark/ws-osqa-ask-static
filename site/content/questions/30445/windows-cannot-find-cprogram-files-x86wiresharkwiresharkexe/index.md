+++
type = "question"
title = "Windows cannot find &#x27;C:&#92;Program Files (x86)&#92;Wireshark&#92;Wireshark.exe&#x27;"
description = '''When i try to run wireshark, it says that windows cannot find it. If i manually look in the place where it can&#x27;t find the exe, i can see it right there! When i click the exe there, it repeats the message... Very annoying, any help appreciated Windows 7 64bit, tried multiple version with the same res...'''
date = "2014-03-05T13:04:00Z"
lastmod = "2014-03-06T13:16:00Z"
weight = 30445
keywords = [ "windows7", "executable", "find", "install", "error" ]
aliases = [ "/questions/30445" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Windows cannot find 'C:\\Program Files (x86)\\Wireshark\\Wireshark.exe'](/questions/30445/windows-cannot-find-cprogram-files-x86wiresharkwiresharkexe)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30445-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30445-score" class="post-score" title="current number of votes">0</div><span id="post-30445-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When i try to run wireshark, it says that windows cannot find it. If i manually look in the place where it can't find the exe, i can see it right there! When i click the exe there, it repeats the message... Very annoying, any help appreciated</p><p>Windows 7 64bit, tried multiple version with the same result...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-executable" rel="tag" title="see questions tagged &#39;executable&#39;">executable</span> <span class="post-tag tag-link-find" rel="tag" title="see questions tagged &#39;find&#39;">find</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Mar '14, 13:04</strong></p><img src="https://secure.gravatar.com/avatar/a4df1ce7aa500b168938cfe9ad6ec894?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="daniel507&#39;s gravatar image" /><p><span>daniel507</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="daniel507 has no accepted answers">0%</span></p></div></div><div id="comments-container-30445" class="comments-container"><span id="30464"></span><div id="comment-30464" class="comment"><div id="post-30464-score" class="comment-score"></div><div class="comment-text"><p>Bump ;) Still same problem</p></div><div id="comment-30464-info" class="comment-info"><span class="comment-age">(05 Mar '14, 23:58)</span> <span class="comment-user userinfo">daniel507</span></div></div><span id="30469"></span><div id="comment-30469" class="comment"><div id="post-30469-score" class="comment-score"></div><div class="comment-text"><p><span>@daniel507</span></p><p>This is not a forum, there's no need to bump, and you should not post an "answer" when it's really a comment. Please read the FAQ for more info.</p></div><div id="comment-30469-info" class="comment-info"><span class="comment-age">(06 Mar '14, 01:37)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="30470"></span><div id="comment-30470" class="comment"><div id="post-30470-score" class="comment-score"></div><div class="comment-text"><p>So, what are you actually doing, your question is light on detail? Exactly where or what are you clicking, or are you trying to start wireshark from a command shell?</p></div><div id="comment-30470-info" class="comment-info"><span class="comment-age">(06 Mar '14, 01:38)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="30491"></span><div id="comment-30491" class="comment"><div id="post-30491-score" class="comment-score"></div><div class="comment-text"><p>Sorry i am new, didn't see the comment section before. I am clicking on the wireshark.exe, when i click this it comes up with that error message.</p></div><div id="comment-30491-info" class="comment-info"><span class="comment-age">(06 Mar '14, 12:45)</span> <span class="comment-user userinfo">daniel507</span></div></div><span id="30496"></span><div id="comment-30496" class="comment"><div id="post-30496-score" class="comment-score"></div><div class="comment-text"><p>Some questions:</p><ul><li>Where did you get the installer package from?</li><li>How did you install Wireshark?</li><li>What is the SHA1 hash of the installer package?</li><li>What is the SHA1 hash of the Wireshark binary?</li><li>please download ExeInfo (<a href="http://www.nirsoft.net/utils/exeinfo.html)">http://www.nirsoft.net/utils/exeinfo.html)</a> and post the output of the binary analysis here</li></ul></div><div id="comment-30496-info" class="comment-info"><span class="comment-age">(06 Mar '14, 13:13)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-30445" class="comment-tools"></div><div class="clear"></div><div id="comment-30445-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="30472"></span>

<div id="answer-container-30472" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30472-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30472-score" class="post-score" title="current number of votes">0</div><span id="post-30472-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There's two paths in Win 7 , one for 32 bits program files and one for 64 bit program files: "C:\Program Files\Wireshark\Wireshark.exe" &gt;&gt; is the 64 bit one "C:\Program Files (x86)\ is the 32 bit one. Maybe that is where the confusion comes from.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Mar '14, 01:50</strong></p><img src="https://secure.gravatar.com/avatar/69710b84acce4cdf0a0cbdcb5930fda1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Marc&#39;s gravatar image" /><p><span>Marc</span><br />
<span class="score" title="147 reputation points">147</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="13 badges"><span class="silver">●</span><span class="badgecount">13</span></span><span title="16 badges"><span class="bronze">●</span><span class="badgecount">16</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Marc has 3 accepted answers">27%</span></p></div></div><div id="comments-container-30472" class="comments-container"><span id="30490"></span><div id="comment-30490" class="comment"><div id="post-30490-score" class="comment-score"></div><div class="comment-text"><p>i think i may have the 32bit version as my wireshark files are in the (x86) program files... Is this why it says can't find file, even though it states that it is looking in the area that it is in...?</p><p>Thanks</p></div><div id="comment-30490-info" class="comment-info"><span class="comment-age">(06 Mar '14, 12:41)</span> <span class="comment-user userinfo">daniel507</span></div></div><span id="30492"></span><div id="comment-30492" class="comment"><div id="post-30492-score" class="comment-score"></div><div class="comment-text"><p>You can use either version on Windows 64 bit. What are you clicking on, the file in Windows Explorer after navigating to the Wireshark folder, or an icon in the Start Menu?</p></div><div id="comment-30492-info" class="comment-info"><span class="comment-age">(06 Mar '14, 12:54)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="30498"></span><div id="comment-30498" class="comment"><div id="post-30498-score" class="comment-score"></div><div class="comment-text"><p>I have tried the wireshark folder exe and the star menu icon</p></div><div id="comment-30498-info" class="comment-info"><span class="comment-age">(06 Mar '14, 13:16)</span> <span class="comment-user userinfo">daniel507</span></div></div></div><div id="comment-tools-30472" class="comment-tools"></div><div class="clear"></div><div id="comment-30472-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

