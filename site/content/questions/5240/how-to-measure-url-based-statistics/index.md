+++
type = "question"
title = "How to measure URL based statistics?"
description = '''Hello, I need to measure statistics on specific URL including D/L bytes, U/L bytes, average speed, etc. Looking forward to get help. Thanks, asmash'''
date = "2011-07-25T20:33:00Z"
lastmod = "2011-07-30T15:59:00Z"
weight = 5240
keywords = [ "url", "http" ]
aliases = [ "/questions/5240" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to measure URL based statistics?](/questions/5240/how-to-measure-url-based-statistics)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5240-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5240-score" class="post-score" title="current number of votes">0</div><span id="post-5240-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I need to measure statistics on specific URL including D/L bytes, U/L bytes, average speed, etc.</p><p>Looking forward to get help.</p><p>Thanks, asmash</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-url" rel="tag" title="see questions tagged &#39;url&#39;">url</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jul '11, 20:33</strong></p><img src="https://secure.gravatar.com/avatar/0f7203a6036a06c073ba085d6778eabb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="asmash&#39;s gravatar image" /><p><span>asmash</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="asmash has no accepted answers">0%</span></p></div></div><div id="comments-container-5240" class="comments-container"></div><div id="comment-tools-5240" class="comment-tools"></div><div class="clear"></div><div id="comment-5240-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5373"></span>

<div id="answer-container-5373" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5373-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5373-score" class="post-score" title="current number of votes">0</div><span id="post-5373-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I doubt Wireshark is what you need. Look into using the netflow capability of your switch if you have a netflow capable one, or consider using a proxy which may be able to help with accounting info.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Jul '11, 15:59</strong></p><img src="https://secure.gravatar.com/avatar/17749b08eebcd8b4bc34c753a56cd8d3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="staticinterference&#39;s gravatar image" /><p><span>staticinterf...</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="staticinterference has no accepted answers">0%</span></p></div></div><div id="comments-container-5373" class="comments-container"></div><div id="comment-tools-5373" class="comment-tools"></div><div class="clear"></div><div id="comment-5373-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

