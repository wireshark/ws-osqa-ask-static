+++
type = "question"
title = "[closed] How to clear the possibly delay SACK in SCTP associations of type M2PA?"
description = '''Hello 1)what are the Causes for delay SACK in SCTP associations ? 2)How to clear the possibly delay SACK in SCTP associations of type M2PA? 3)Whether CRC32 Checksum off loading is related to delay SACK in SCTP associations? Request to reply my queries at the earliest. Regards Pradeep Doddawad'''
date = "2012-04-09T01:50:00Z"
lastmod = "2012-04-09T01:50:00Z"
weight = 10024
keywords = [ "delay", "associations", "sctp", "sack", "in" ]
aliases = [ "/questions/10024" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] How to clear the possibly delay SACK in SCTP associations of type M2PA?](/questions/10024/how-to-clear-the-possibly-delay-sack-in-sctp-associations-of-type-m2pa)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10024-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10024-score" class="post-score" title="current number of votes">0</div><span id="post-10024-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>1)what are the Causes for delay SACK in SCTP associations ? 2)How to clear the possibly delay SACK in SCTP associations of type M2PA? 3)Whether CRC32 Checksum off loading is related to delay SACK in SCTP associations?</p><p>Request to reply my queries at the earliest.</p><p>Regards Pradeep Doddawad</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-delay" rel="tag" title="see questions tagged &#39;delay&#39;">delay</span> <span class="post-tag tag-link-associations" rel="tag" title="see questions tagged &#39;associations&#39;">associations</span> <span class="post-tag tag-link-sctp" rel="tag" title="see questions tagged &#39;sctp&#39;">sctp</span> <span class="post-tag tag-link-sack" rel="tag" title="see questions tagged &#39;sack&#39;">sack</span> <span class="post-tag tag-link-in" rel="tag" title="see questions tagged &#39;in&#39;">in</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Apr '12, 01:50</strong></p><img src="https://secure.gravatar.com/avatar/f97c870774e6ba9b98f5789ef24d62e8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pradeepdoddawad&#39;s gravatar image" /><p><span>pradeepdoddawad</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pradeepdoddawad has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>27 Apr '12, 08:04</strong> </span></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span></p></div></div><div id="comments-container-10024" class="comments-container"></div><div id="comment-tools-10024" class="comment-tools"></div><div class="clear"></div><div id="comment-10024-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by JeffMorriss 27 Apr '12, 08:04

</div>

</div>

</div>

