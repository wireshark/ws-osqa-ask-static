+++
type = "question"
title = "How do I remote capture tcpdump to Wireshark?"
description = '''I have tcpdump running on a Nix box and I&#x27;d like to pipe/feed the capture directly into Wireshark that is running on a Window PC. I don&#x27;t want to write the tcpdump to a file, copy the file to the windows box and the open it up in Wireshark. Essentially I wan to stream the capture live from the Nix b...'''
date = "2014-04-02T08:45:00Z"
lastmod = "2014-04-02T08:57:00Z"
weight = 31395
keywords = [ "tcpdump" ]
aliases = [ "/questions/31395" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How do I remote capture tcpdump to Wireshark?](/questions/31395/how-do-i-remote-capture-tcpdump-to-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31395-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31395-score" class="post-score" title="current number of votes">0</div><span id="post-31395-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have tcpdump running on a Nix box and I'd like to pipe/feed the capture directly into Wireshark that is running on a Window PC. I don't want to write the tcpdump to a file, copy the file to the windows box and the open it up in Wireshark. Essentially I wan to stream the capture live from the Nix box to the windows box.</p><p>I've been able to find references on the net that it can be done, but can't figure out how. SSH and named pipes pop up a lot.</p><p>Can anyone help me out with this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcpdump" rel="tag" title="see questions tagged &#39;tcpdump&#39;">tcpdump</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Apr '14, 08:45</strong></p><img src="https://secure.gravatar.com/avatar/9501a0a9cba9c6ae399345ab0baf8b3a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dsuida&#39;s gravatar image" /><p><span>dsuida</span><br />
<span class="score" title="46 reputation points">46</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dsuida has no accepted answers">0%</span></p></div></div><div id="comments-container-31395" class="comments-container"></div><div id="comment-tools-31395" class="comment-tools"></div><div class="clear"></div><div id="comment-31395-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31398"></span>

<div id="answer-container-31398" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31398-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31398-score" class="post-score" title="current number of votes">1</div><span id="post-31398-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the answer to <a href="http://ask.wireshark.org/questions/23609/remote-capture-via-ssh-and-pipe">this</a> question.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Apr '14, 08:57</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-31398" class="comments-container"></div><div id="comment-tools-31398" class="comment-tools"></div><div class="clear"></div><div id="comment-31398-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

