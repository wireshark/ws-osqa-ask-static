+++
type = "question"
title = "hex to word"
description = '''Hi, Good day. I am using the Wireshark application on Lan for tracing the packets and activities but couldn&#x27;t find a way for converting the hex stream to word. Is there any way? Thank you for your help. My email is ls20001@hotmail.com Thank you again. Kind Regards, Lilly'''
date = "2011-02-02T00:57:00Z"
lastmod = "2011-02-02T03:00:00Z"
weight = 2092
keywords = [ "to", "hex", "word" ]
aliases = [ "/questions/2092" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [hex to word](/questions/2092/hex-to-word)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2092-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2092-score" class="post-score" title="current number of votes">0</div><span id="post-2092-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, Good day. I am using the Wireshark application on Lan for tracing the packets and activities but couldn't find a way for converting the hex stream to word. Is there any way? Thank you for your help.</p><p>My email is <span class="__cf_email__" data-cfemail="b7dbc48587878786f7dfd8c3dad6dedb99d4d8da">[email protected]</span></p><p>Thank you again. Kind Regards, Lilly</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-to" rel="tag" title="see questions tagged &#39;to&#39;">to</span> <span class="post-tag tag-link-hex" rel="tag" title="see questions tagged &#39;hex&#39;">hex</span> <span class="post-tag tag-link-word" rel="tag" title="see questions tagged &#39;word&#39;">word</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Feb '11, 00:57</strong></p><img src="https://secure.gravatar.com/avatar/611180b9b256697c3db993cdafc1d403?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lilly&#39;s gravatar image" /><p><span>Lilly</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lilly has no accepted answers">0%</span></p></div></div><div id="comments-container-2092" class="comments-container"></div><div id="comment-tools-2092" class="comment-tools"></div><div class="clear"></div><div id="comment-2092-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2097"></span>

<div id="answer-container-2097" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2097-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2097-score" class="post-score" title="current number of votes">0</div><span id="post-2097-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Right-click a packet of the hex stream and select follow ... stream. This pops up a dialog showing the data in various formats.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Feb '11, 03:00</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-2097" class="comments-container"></div><div id="comment-tools-2097" class="comment-tools"></div><div class="clear"></div><div id="comment-2097-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

