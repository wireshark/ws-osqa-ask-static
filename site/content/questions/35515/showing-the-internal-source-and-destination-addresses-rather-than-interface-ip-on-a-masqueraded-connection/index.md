+++
type = "question"
title = "Showing the internal source and destination addresses rather than interface IP on a masqueraded connection"
description = '''I have a linux (Ubuntu Server) machine configured as a PPP router (iptables masquerade), when capturing on the ppp0 interface I would like to be able to see the source or destination as the internal machine to which the packet is being routed to/from rather than the router&#x27;s ppp0 IP address. This co...'''
date = "2014-08-16T17:26:00Z"
lastmod = "2014-08-16T17:26:00Z"
weight = 35515
keywords = [ "iptables", "router", "ppp", "interface", "filter" ]
aliases = [ "/questions/35515" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Showing the internal source and destination addresses rather than interface IP on a masqueraded connection](/questions/35515/showing-the-internal-source-and-destination-addresses-rather-than-interface-ip-on-a-masqueraded-connection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35515-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35515-score" class="post-score" title="current number of votes">0</div><span id="post-35515-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a linux (Ubuntu Server) machine configured as a PPP router (iptables masquerade), when capturing on the ppp0 interface I would like to be able to see the source or destination as the internal machine to which the packet is being routed to/from rather than the router's ppp0 IP address.</p><p>This could be done by capturing eth0 but I would like to only see packets going through the ppp0 interface and the actual router box's traffic over ppp0. I'd prefer to do this without filters.</p><p>Thanks in advance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-iptables" rel="tag" title="see questions tagged &#39;iptables&#39;">iptables</span> <span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span> <span class="post-tag tag-link-ppp" rel="tag" title="see questions tagged &#39;ppp&#39;">ppp</span> <span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Aug '14, 17:26</strong></p><img src="https://secure.gravatar.com/avatar/062213801c5c8f8b65b35bf5aaad38ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="CallumA&#39;s gravatar image" /><p><span>CallumA</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="CallumA has no accepted answers">0%</span></p></div></div><div id="comments-container-35515" class="comments-container"></div><div id="comment-tools-35515" class="comment-tools"></div><div class="clear"></div><div id="comment-35515-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

