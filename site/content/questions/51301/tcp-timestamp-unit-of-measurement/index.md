+++
type = "question"
title = "TCP Timestamp Unit of Measurement"
description = '''Does anyone know the unit of measurement for the TCP Timestamp in the options field? i.e. Timestamp: TSval 1009700925, TSecr 862779597 Is the unit of measurement milliseconds, microseconds, or something else? Thanks.'''
date = "2016-03-30T12:50:00Z"
lastmod = "2016-04-11T09:57:00Z"
weight = 51301
keywords = [ "timestamp", "tsecr", "tcp", "tsval" ]
aliases = [ "/questions/51301" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [TCP Timestamp Unit of Measurement](/questions/51301/tcp-timestamp-unit-of-measurement)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51301-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51301-score" class="post-score" title="current number of votes">0</div><span id="post-51301-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does anyone know the unit of measurement for the TCP Timestamp in the options field?</p><p>i.e. Timestamp: TSval 1009700925, TSecr 862779597</p><p>Is the unit of measurement milliseconds, microseconds, or something else?</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-timestamp" rel="tag" title="see questions tagged &#39;timestamp&#39;">timestamp</span> <span class="post-tag tag-link-tsecr" rel="tag" title="see questions tagged &#39;tsecr&#39;">tsecr</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span> <span class="post-tag tag-link-tsval" rel="tag" title="see questions tagged &#39;tsval&#39;">tsval</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Mar '16, 12:50</strong></p><img src="https://secure.gravatar.com/avatar/28dd213bb6b909199cd90abb2fda5e98?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bjmarty&#39;s gravatar image" /><p><span>bjmarty</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bjmarty has no accepted answers">0%</span></p></div></div><div id="comments-container-51301" class="comments-container"></div><div id="comment-tools-51301" class="comment-tools"></div><div class="clear"></div><div id="comment-51301-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51302"></span>

<div id="answer-container-51302" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51302-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51302-score" class="post-score" title="current number of votes">0</div><span id="post-51302-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The unit is implementation dependent and can vary. See section 4.2.2 of <a href="https://www.ietf.org/rfc/rfc1323.txt">RFC 1323</a>, "TCP Extensions for High Performance."</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Mar '16, 13:46</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-51302" class="comments-container"><span id="51558"></span><div id="comment-51558" class="comment"><div id="post-51558-score" class="comment-score"></div><div class="comment-text"><p>Thanks you for the response. I'll dig more into that RFC</p></div><div id="comment-51558-info" class="comment-info"><span class="comment-age">(11 Apr '16, 09:57)</span> <span class="comment-user userinfo">bjmarty</span></div></div></div><div id="comment-tools-51302" class="comment-tools"></div><div class="clear"></div><div id="comment-51302-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

