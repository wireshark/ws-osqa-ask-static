+++
type = "question"
title = "How to set wireshark to be left-justified on windows"
description = '''Hi, how to set everything to be left justified? Thanks'''
date = "2017-04-25T00:19:00Z"
lastmod = "2017-07-26T12:36:00Z"
weight = 61024
keywords = [ "justified", "right", "direction", "left" ]
aliases = [ "/questions/61024" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [How to set wireshark to be left-justified on windows](/questions/61024/how-to-set-wireshark-to-be-left-justified-on-windows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61024-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61024-score" class="post-score" title="current number of votes">0</div><span id="post-61024-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>how to set everything to be left justified?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-justified" rel="tag" title="see questions tagged &#39;justified&#39;">justified</span> <span class="post-tag tag-link-right" rel="tag" title="see questions tagged &#39;right&#39;">right</span> <span class="post-tag tag-link-direction" rel="tag" title="see questions tagged &#39;direction&#39;">direction</span> <span class="post-tag tag-link-left" rel="tag" title="see questions tagged &#39;left&#39;">left</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Apr '17, 00:19</strong></p><img src="https://secure.gravatar.com/avatar/1e9afc45f5369a66f4a7161215749ddd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="epd&#39;s gravatar image" /><p><span>epd</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="epd has no accepted answers">0%</span></p></div></div><div id="comments-container-61024" class="comments-container"><span id="61029"></span><div id="comment-61029" class="comment"><div id="post-61029-score" class="comment-score"></div><div class="comment-text"><p>What in particular would you like to be left justified?</p></div><div id="comment-61029-info" class="comment-info"><span class="comment-age">(25 Apr '17, 03:06)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="61031"></span><div id="comment-61031" class="comment"><div id="post-61031-score" class="comment-score"></div><div class="comment-text"><p>It's just that everything in wireshark is right justified for some reason, such as file menu, for example, it's on a top right corner of the screen, while "help" menu on the left. Packet columns are also from right to left (time column, for example is the rightmost column). I would like to fix that..</p></div><div id="comment-61031-info" class="comment-info"><span class="comment-age">(25 Apr '17, 03:19)</span> <span class="comment-user userinfo">epd</span></div></div></div><div id="comment-tools-61024" class="comment-tools"></div><div class="clear"></div><div id="comment-61024-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="61034"></span>

<div id="answer-container-61034" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61034-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61034-score" class="post-score" title="current number of votes">0</div><span id="post-61034-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>What country or region do you have set in Windows? I've never come across Wireshark displaying the menus and columns the wrong way around.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Apr '17, 04:55</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-61034" class="comments-container"><span id="61036"></span><div id="comment-61036" class="comment"><div id="post-61036-score" class="comment-score"></div><div class="comment-text"><p>Firstly, thanks!</p><p>Changing the format in "Region and language" to English (US)" fixed that. Previously it was defined as "Hebrew".</p><p>Interestingly enough, all other programs on my machine displayed normally except wireshark. My win7 is also left-to-right english version.</p><p>Thanks again!</p></div><div id="comment-61036-info" class="comment-info"><span class="comment-age">(25 Apr '17, 05:07)</span> <span class="comment-user userinfo">epd</span></div></div><span id="61039"></span><div id="comment-61039" class="comment"><div id="post-61039-score" class="comment-score"></div><div class="comment-text"><p>It seems that Wireshark takes account of text flow direction for the region and language set. Good to know.</p><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-61039-info" class="comment-info"><span class="comment-age">(25 Apr '17, 06:02)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="61052"></span><div id="comment-61052" class="comment"><div id="post-61052-score" class="comment-score"></div><div class="comment-text"><blockquote><p>Changing the format in "Region and language" to English (US)" fixed that. Previously it was defined as "Hebrew".</p><p>Interestingly enough, all other programs on my machine displayed normally except wireshark. My win7 is also left-to-right english version.</p></blockquote><p>What does "normally" mean? If "Region and language" were set to Hebrew (or Arabic or Farsi or...), I'd expect the menu bars for applications to be right-justified, with the menu going from right to left.</p></div><div id="comment-61052-info" class="comment-info"><span class="comment-age">(25 Apr '17, 16:12)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-61034" class="comment-tools"></div><div class="clear"></div><div id="comment-61034-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="63131"></span>

<div id="answer-container-63131" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63131-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63131-score" class="post-score" title="current number of votes">0</div><span id="post-63131-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can solve this by change the language to be English instead of System default.</p><p>From the Edit menu, -&gt; Preferences , in the Appearance item (or Ctrl + Shift + P), the bottom droplist sets the Language . By default I think it follows the System Setting --&gt; change to English and it will be left justified</p><p>Regards, Yohai</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jul '17, 06:55</strong></p><img src="https://secure.gravatar.com/avatar/2b680db6b5e1f336bafa59be9c9ae7c0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yohaial&#39;s gravatar image" /><p><span>yohaial</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yohaial has no accepted answers">0%</span></p></div></div><div id="comments-container-63131" class="comments-container"><span id="63150"></span><div id="comment-63150" class="comment"><div id="post-63150-score" class="comment-score"></div><div class="comment-text"><p>Yes, by default Wireshark attempts to follow the system locale settings, as it should do (if there are places where it doesn't, those are either bugs in Wireshark, bugs in Qt, or places where Wireshark isn't fully localized; <a href="https://www.transifex.com/wireshark/wireshark/">here's where you can help with translations</a>).</p></div><div id="comment-63150-info" class="comment-info"><span class="comment-age">(26 Jul '17, 12:36)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-63131" class="comment-tools"></div><div class="clear"></div><div id="comment-63131-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

