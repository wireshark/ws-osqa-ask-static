+++
type = "question"
title = "Wireshark on netbook with Win7 &quot;Starter&quot; version"
description = '''Any idea whether wireshark 32 will work on an Acer Aspire D255F netbook? It has a 1.67 Ghz intel Atom processor with 1 Gb of RAM. '''
date = "2011-01-28T12:14:00Z"
lastmod = "2011-01-28T14:02:00Z"
weight = 2000
keywords = [ "windows7", "netbook" ]
aliases = [ "/questions/2000" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark on netbook with Win7 "Starter" version](/questions/2000/wireshark-on-netbook-with-win7-starter-version)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2000-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2000-score" class="post-score" title="current number of votes">0</div><span id="post-2000-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Any idea whether wireshark 32 will work on an Acer Aspire D255F netbook? It has a 1.67 Ghz intel Atom processor with 1 Gb of RAM.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-netbook" rel="tag" title="see questions tagged &#39;netbook&#39;">netbook</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jan '11, 12:14</strong></p><img src="https://secure.gravatar.com/avatar/bf522497bf6d6b1abd236cc7ccbb0e19?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dale&#39;s gravatar image" /><p><span>dale</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dale has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-2000" class="comments-container"></div><div id="comment-tools-2000" class="comment-tools"></div><div class="clear"></div><div id="comment-2000-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2002"></span>

<div id="answer-container-2002" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2002-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2002-score" class="post-score" title="current number of votes">0</div><span id="post-2002-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't see why it shouldn't. I have Wireshark running on my Samsung NC-10 netbook with an Atom 1.6Ghz, not a problem at all. I have Win7 Pro on that though, but I don't see why Starter shouldn't work too.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Jan '11, 14:02</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-2002" class="comments-container"></div><div id="comment-tools-2002" class="comment-tools"></div><div class="clear"></div><div id="comment-2002-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

