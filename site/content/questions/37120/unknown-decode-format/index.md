+++
type = "question"
title = "Unknown Decode Format"
description = '''Hey everyone, complete newb here, hopefully someone can help.  I have a piece of equipment that&#x27;s connected to my network. I know it&#x27;s IP address, and I can filter results based on that IP, but I&#x27;m not sure how to decode the data. How can I tell how to do this?  I want to eventually control the equi...'''
date = "2014-10-16T15:38:00Z"
lastmod = "2014-10-17T09:07:00Z"
weight = 37120
keywords = [ "decode", "ip", "noob" ]
aliases = [ "/questions/37120" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Unknown Decode Format](/questions/37120/unknown-decode-format)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37120-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37120-score" class="post-score" title="current number of votes">0</div><span id="post-37120-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey everyone, complete newb here, hopefully someone can help.</p><p>I have a piece of equipment that's connected to my network. I know it's IP address, and I can filter results based on that IP, but I'm not sure how to decode the data. How can I tell how to do this?</p><p>I want to eventually control the equipment from my own software, not the (useless) software that was provided with the unit.</p><p>Any help is much appreciated</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span> <span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-noob" rel="tag" title="see questions tagged &#39;noob&#39;">noob</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Oct '14, 15:38</strong></p><img src="https://secure.gravatar.com/avatar/a33178159b229725f49652b125fa1f3f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="fcreight&#39;s gravatar image" /><p><span>fcreight</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="fcreight has no accepted answers">0%</span></p></div></div><div id="comments-container-37120" class="comments-container"></div><div id="comment-tools-37120" class="comment-tools"></div><div class="clear"></div><div id="comment-37120-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37128"></span>

<div id="answer-container-37128" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37128-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37128-score" class="post-score" title="current number of votes">1</div><span id="post-37128-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's called protocol reverse engineering. Assuming Wireshark can't dissect the packets (or is not configured correctly for it) you'll have to go and look for outside technical information. For instance the manufacturers website may list standards or technologies used, which may indicate possible protocols. Finding forums on the device may give insights in what's in there. It comes down to correlating information to get ahead. You may run into roadblocks (like encryption for instance) which may make it hard, but not impossible. It just comes down to motivation.<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Oct '14, 03:49</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span> </br></p></div></div><div id="comments-container-37128" class="comments-container"><span id="37136"></span><div id="comment-37136" class="comment"><div id="post-37136-score" class="comment-score"></div><div class="comment-text"><p>Thanks for your help Jaap. Since I can obtain the raw data, is there a way to test the packets with each protocol in Wireshark? Some of the data coming through is decoded, but other data typically looks like Morse Code. Sorry for my ignorance!</p></div><div id="comment-37136-info" class="comment-info"><span class="comment-age">(17 Oct '14, 09:07)</span> <span class="comment-user userinfo">fcreight</span></div></div></div><div id="comment-tools-37128" class="comment-tools"></div><div class="clear"></div><div id="comment-37128-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

