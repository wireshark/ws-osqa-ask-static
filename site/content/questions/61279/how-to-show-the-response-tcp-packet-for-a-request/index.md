+++
type = "question"
title = "How to show the response TCP packet for a request"
description = '''Hi, is there any way to display only packets related to a request packet from client.  Here is an example,  Clients makes a new TCP request via established TCP session, sending all data like ack, syn numbers .... How can I display only packets that are sent in response to the client&#x27;s request packet...'''
date = "2017-05-08T01:45:00Z"
lastmod = "2017-05-08T07:57:00Z"
weight = 61279
keywords = [ "tcppackets", "packets", "packet", "tcp" ]
aliases = [ "/questions/61279" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to show the response TCP packet for a request](/questions/61279/how-to-show-the-response-tcp-packet-for-a-request)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61279-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61279-score" class="post-score" title="current number of votes">0</div><span id="post-61279-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, is there any way to display only packets related to a request packet from client.</p><p>Here is an example,</p><p>Clients makes a new TCP request via established TCP session, sending all data like ack, syn numbers ....</p><p>How can I display only packets that are sent in response to the client's request packet ?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcppackets" rel="tag" title="see questions tagged &#39;tcppackets&#39;">tcppackets</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 May '17, 01:45</strong></p><img src="https://secure.gravatar.com/avatar/e4e30766980b23fe17e4982686a66b7c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ypro&#39;s gravatar image" /><p><span>Ypro</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ypro has no accepted answers">0%</span></p></div></div><div id="comments-container-61279" class="comments-container"><span id="61282"></span><div id="comment-61282" class="comment"><div id="post-61282-score" class="comment-score"></div><div class="comment-text"><p>If you right-click on the request packet in the packet list pane and choose either <em>"Follow -&gt; TCP Stream"</em> or <em>"Conversation filter -&gt; TCP"</em>, does either of those give you the results you're after? If not, please elaborate further.</p></div><div id="comment-61282-info" class="comment-info"><span class="comment-age">(08 May '17, 07:57)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-61279" class="comment-tools"></div><div class="clear"></div><div id="comment-61279-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

