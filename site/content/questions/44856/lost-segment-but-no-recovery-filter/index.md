+++
type = "question"
title = "Lost Segment but no Recovery Filter"
description = '''Hello, I&#x27;m looking to build a filter that lists only lost segments without any recovery. What might I need to append to tcp.analysis.ack_lost_segment to accomplish this? Or, is there a better filter string with which I can use to build an only lost packets filter. thanks, JTech'''
date = "2015-08-05T01:35:00Z"
lastmod = "2015-08-05T01:35:00Z"
weight = 44856
keywords = [ "filter", "segment", "lost" ]
aliases = [ "/questions/44856" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Lost Segment but no Recovery Filter](/questions/44856/lost-segment-but-no-recovery-filter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44856-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44856-score" class="post-score" title="current number of votes">0</div><span id="post-44856-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I'm looking to build a filter that lists only lost segments without any recovery. What might I need to append to tcp.analysis.ack_lost_segment to accomplish this? Or, is there a better filter string with which I can use to build an only lost packets filter.</p><p>thanks, JTech</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-segment" rel="tag" title="see questions tagged &#39;segment&#39;">segment</span> <span class="post-tag tag-link-lost" rel="tag" title="see questions tagged &#39;lost&#39;">lost</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Aug '15, 01:35</strong></p><img src="https://secure.gravatar.com/avatar/791c3a844bb1629d3a685adab364e2d1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JTech_17&#39;s gravatar image" /><p><span>JTech_17</span><br />
<span class="score" title="41 reputation points">41</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JTech_17 has no accepted answers">0%</span></p></div></div><div id="comments-container-44856" class="comments-container"></div><div id="comment-tools-44856" class="comment-tools"></div><div class="clear"></div><div id="comment-44856-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

