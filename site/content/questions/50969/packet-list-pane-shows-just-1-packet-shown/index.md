+++
type = "question"
title = "Packet List Pane Shows Just 1 Packet Shown?"
description = '''I am using wireshark 1.10.6 on ubuntu 14.04. When I open a capture file that has many packets, the Packet List pane only shows 1 packet. How can I get th4e Packet List pane to show say 20 packets at a time?'''
date = "2016-03-16T08:26:00Z"
lastmod = "2016-03-16T09:28:00Z"
weight = 50969
keywords = [ "packet-list-pane" ]
aliases = [ "/questions/50969" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Packet List Pane Shows Just 1 Packet Shown?](/questions/50969/packet-list-pane-shows-just-1-packet-shown)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50969-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50969-score" class="post-score" title="current number of votes">0</div><span id="post-50969-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using wireshark 1.10.6 on ubuntu 14.04. When I open a capture file that has many packets, the Packet List pane only shows 1 packet. How can I get th4e Packet List pane to show say 20 packets at a time?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packet-list-pane" rel="tag" title="see questions tagged &#39;packet-list-pane&#39;">packet-list-pane</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Mar '16, 08:26</strong></p><img src="https://secure.gravatar.com/avatar/4fb7ebed1d2d885e2eb9e835c881bf17?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zorn&#39;s gravatar image" /><p><span>zorn</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zorn has no accepted answers">0%</span></p></div></div><div id="comments-container-50969" class="comments-container"></div><div id="comment-tools-50969" class="comment-tools"></div><div class="clear"></div><div id="comment-50969-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50970"></span>

<div id="answer-container-50970" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50970-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50970-score" class="post-score" title="current number of votes">0</div><span id="post-50970-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Something is blocking the update of your application window. Hard to tell, you could look at any GTK decorations applied maybe? Or subject of a <a href="https://wiki.wireshark.org/PracticalJokes">practical joke</a>?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Mar '16, 09:28</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-50970" class="comments-container"></div><div id="comment-tools-50970" class="comment-tools"></div><div class="clear"></div><div id="comment-50970-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

