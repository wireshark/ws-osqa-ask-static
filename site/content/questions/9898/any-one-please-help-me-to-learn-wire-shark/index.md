+++
type = "question"
title = "any one please help me to learn wire shark"
description = '''1)i want to lean abt wire shak itryed many ways but i cont get the clear idea please some one help me to learn this  2)what is meant by malformed packet is it virus or some thing else '''
date = "2012-04-02T07:53:00Z"
lastmod = "2012-04-02T08:05:00Z"
weight = 9898
keywords = [ "windows7" ]
aliases = [ "/questions/9898" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [any one please help me to learn wire shark](/questions/9898/any-one-please-help-me-to-learn-wire-shark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9898-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9898-score" class="post-score" title="current number of votes">0</div><span id="post-9898-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>1)i want to lean abt wire shak itryed many ways but i cont get the clear idea please some one help me to learn this 2)what is meant by malformed packet is it virus or some thing else</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Apr '12, 07:53</strong></p><img src="https://secure.gravatar.com/avatar/c10742b15beca2fd871828ccdee554bc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="arasu&#39;s gravatar image" /><p><span>arasu</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="arasu has no accepted answers">0%</span></p></div></div><div id="comments-container-9898" class="comments-container"></div><div id="comment-tools-9898" class="comment-tools"></div><div class="clear"></div><div id="comment-9898-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9899"></span>

<div id="answer-container-9899" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9899-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9899-score" class="post-score" title="current number of votes">2</div><span id="post-9899-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><ol><li>Clear idea = "record what is going on on the network and show it in a human readable form". You'll still need to know how network protocols work, otherwise even the "human readable form" will not mean a lot to you. What you probably need is a TCP/IP class of some sort, or get a good book (I can recommend "TCP/IP Illustrated" by Stevens.</li><li>"malformed packet" usually means that Wireshark could not decode the packet bytes. This happens if the packet is damaged or Wireshark tries to decode it as a protocol that is isn't. Not a virus, just a bad packet/wrong decoding.</li></ol></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Apr '12, 08:05</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-9899" class="comments-container"></div><div id="comment-tools-9899" class="comment-tools"></div><div class="clear"></div><div id="comment-9899-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

