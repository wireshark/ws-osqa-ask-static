+++
type = "question"
title = "NBNS entries"
description = '''HI, We recently replaced 2 old windows 2003 dc&#x27;S with windows 2008r2 dc&#x27;S. All is working ok except on odd occasions when query the AD i get errors such as &quot;Server not operational&quot; or &quot;Domain does not exist&quot; Everything looks correct on DNS and there are no entries for the old servers so i am looking...'''
date = "2013-12-05T06:21:00Z"
lastmod = "2013-12-05T06:21:00Z"
weight = 27811
keywords = [ "nbns" ]
aliases = [ "/questions/27811" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [NBNS entries](/questions/27811/nbns-entries)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27811-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27811-score" class="post-score" title="current number of votes">0</div><span id="post-27811-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>HI,</p><p>We recently replaced 2 old windows 2003 dc'S with windows 2008r2 dc'S. All is working ok except on odd occasions when query the AD i get errors such as "Server not operational" or "Domain does not exist" Everything looks correct on DNS and there are no entries for the old servers so i am looking at alternatives. If i run wireshark i get entries such as</p><p>192.168.180.132 192.168.180.255 NBNS 92 Name query NB XXXXXXX</p><p>192.168.180.132 is the ip of a standard workstation 192.168.180.255 is an ip that has no harware associated with it XXXXXX is the server name for one of the old servers i removed</p><p>I am fairly new to all this. can anyone shed any light on it for me???</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nbns" rel="tag" title="see questions tagged &#39;nbns&#39;">nbns</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Dec '13, 06:21</strong></p><img src="https://secure.gravatar.com/avatar/8f2fa32ee1544e1df87b0330e47fc673?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gareth%20Parry&#39;s gravatar image" /><p><span>Gareth Parry</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gareth Parry has no accepted answers">0%</span></p></div></div><div id="comments-container-27811" class="comments-container"></div><div id="comment-tools-27811" class="comment-tools"></div><div class="clear"></div><div id="comment-27811-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

