+++
type = "question"
title = "WinPCAP not included in WiresharkPortable_2.0.1.paf.exe"
description = '''Hello i just wanted to deploy the WireSharkPortable 2.0.1 Binaries as i run into a Problem. As i found out the WinPcap binaries (WinPcap 4 1 3.exe) are not included in the portable package i was wondering if this is new by default or just gone missing (it was included in WiresharkPortable_1*.paf.exe...'''
date = "2016-02-10T05:55:00Z"
lastmod = "2016-02-10T05:55:00Z"
weight = 50050
keywords = [ "winpcap", "portable" ]
aliases = [ "/questions/50050" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [WinPCAP not included in WiresharkPortable\_2.0.1.paf.exe](/questions/50050/winpcap-not-included-in-wiresharkportable_201pafexe)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50050-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50050-score" class="post-score" title="current number of votes">0</div><span id="post-50050-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>i just wanted to deploy the WireSharkPortable 2.0.1 Binaries as i run into a Problem. As i found out the WinPcap binaries (WinPcap 4 1 3.exe) are not included in the portable package i was wondering if this is new by default or just gone missing (it was included in WiresharkPortable_1*.paf.exe). In my personal opinion the removal somehow breaks the thought of a portable application.</p><p>Information about that is appreciated</p><p>thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span> <span class="post-tag tag-link-portable" rel="tag" title="see questions tagged &#39;portable&#39;">portable</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Feb '16, 05:55</strong></p><img src="https://secure.gravatar.com/avatar/09687fb8170997924bedfe05b16a0f33?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrcrapman&#39;s gravatar image" /><p><span>mrcrapman</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrcrapman has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Feb '16, 06:27</strong> </span></p></div></div><div id="comments-container-50050" class="comments-container"></div><div id="comment-tools-50050" class="comment-tools"></div><div class="clear"></div><div id="comment-50050-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

