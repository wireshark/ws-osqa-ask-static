+++
type = "question"
title = "Understanding traffic"
description = '''I have little experience in understanding what is going on, and I know... I really need to dive into the manuals. If I could get a few pointers to help with this incident, I would really appreciate it. I use a program called WallWatcher, which reports in &quot;real time&quot;, the router logs from my old BEFR...'''
date = "2016-09-14T09:50:00Z"
lastmod = "2016-09-14T10:55:00Z"
weight = 55558
keywords = [ "badtraffic" ]
aliases = [ "/questions/55558" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Understanding traffic](/questions/55558/understanding-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55558-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55558-score" class="post-score" title="current number of votes">-1</div><span id="post-55558-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have little experience in understanding what is going on, and I know... I really need to dive into the manuals. If I could get a few pointers to help with this incident, I would really appreciate it.</p><p>I use a program called WallWatcher, which reports in "real time", the router logs from my old BEFRS41 router. I also use PeerBlock, where I will manually enter in various IP's that I want blocked. The image below contains a copy of the traffic that "bothers" me, and also includes a snippet of the router log file.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/port2323_wireshark.jpg" alt="alt text" /></p><p>The router log initially shows a rejected incoming packet on port 23, immediately followed by a processed packet from the same IP on port 2323 and an outgoing response. I'm not sure why the router is allowing this to happen in the first place but... I find that I am getting a LOT of "queries" going to this port 2323 and they all seem to get processed. Most seem to be coming from places in Brazil, and I can't seem to stop them.</p><p>Any suggestions? And yes, I have done full AV and malware scans using many different highly rated programs all with the latest definitions, and nothing has been found.</p><p>Thanks kindly in advance :)</p><p>Gary</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-badtraffic" rel="tag" title="see questions tagged &#39;badtraffic&#39;">badtraffic</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Sep '16, 09:50</strong></p><img src="https://secure.gravatar.com/avatar/639c4d7df18c5e53f8b3d33264f55d37?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="GaryM&#39;s gravatar image" /><p><span>GaryM</span><br />
<span class="score" title="5 reputation points">5</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="GaryM has no accepted answers">0%</span></p></img></div></div><div id="comments-container-55558" class="comments-container"></div><div id="comment-tools-55558" class="comment-tools"></div><div class="clear"></div><div id="comment-55558-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="55559"></span>

<div id="answer-container-55559" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55559-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55559-score" class="post-score" title="current number of votes">0</div><span id="post-55559-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The Wireshark capture is showing the results of what the router is doing from the point of view of the internal side of your network, but can't tell you why. You'll need to find a support forum for your router\software to answer that.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Sep '16, 10:55</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-55559" class="comments-container"></div><div id="comment-tools-55559" class="comment-tools"></div><div class="clear"></div><div id="comment-55559-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

