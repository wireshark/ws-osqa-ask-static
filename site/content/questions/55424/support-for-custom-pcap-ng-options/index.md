+++
type = "question"
title = "Support for custom pcap-ng options"
description = '''Section 3.5.1 of the PCAP Next Generation (pcapng) Capture File Format internet draft describes the possibility to add custom options in pcapng blocks. With this, it could be possible to add extra information to captured packets. Are there plans to support these in Wireshark? E.g.:  - the possibilit...'''
date = "2016-09-09T03:13:00Z"
lastmod = "2016-09-09T03:13:00Z"
weight = 55424
keywords = [ "pcapng", "custom" ]
aliases = [ "/questions/55424" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Support for custom pcap-ng options](/questions/55424/support-for-custom-pcap-ng-options)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55424-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55424-score" class="post-score" title="current number of votes">0</div><span id="post-55424-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Section 3.5.1 of the PCAP Next Generation (pcapng) Capture File Format internet draft describes the possibility to add custom options in pcapng blocks. With this, it could be possible to add extra information to captured packets.</p><p>Are there plans to support these in Wireshark? E.g.: - the possibility to register custom dissectors (plugin) to decode these options. - the possibility to add custom options with editcap e.g.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcapng" rel="tag" title="see questions tagged &#39;pcapng&#39;">pcapng</span> <span class="post-tag tag-link-custom" rel="tag" title="see questions tagged &#39;custom&#39;">custom</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Sep '16, 03:13</strong></p><img src="https://secure.gravatar.com/avatar/d697c94e69517c02c04ada06c3b89cd9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bruno%20Verstuyft&#39;s gravatar image" /><p><span>Bruno Verstuyft</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bruno Verstuyft has no accepted answers">0%</span></p></div></div><div id="comments-container-55424" class="comments-container"></div><div id="comment-tools-55424" class="comment-tools"></div><div class="clear"></div><div id="comment-55424-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

