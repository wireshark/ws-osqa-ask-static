+++
type = "question"
title = "packets captured but only the details of one are displayed, no list of packets"
description = '''i can&#x27;t see anything except the five lines you can see in the screenshot. it&#x27;s not a practical joke i already checked that, i really don&#x27;t know that it could be... i&#x27;m on Arch Linux, everything up to date. i tried both wifi and ethernet (cable) connection, no way screenshot'''
date = "2015-07-17T12:20:00Z"
lastmod = "2015-07-17T12:58:00Z"
weight = 44263
keywords = [ "packets" ]
aliases = [ "/questions/44263" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [packets captured but only the details of one are displayed, no list of packets](/questions/44263/packets-captured-but-only-the-details-of-one-are-displayed-no-list-of-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44263-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44263-score" class="post-score" title="current number of votes">0</div><span id="post-44263-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i can't see anything except the five lines you can see in the screenshot. it's not a practical joke i already checked that, i really don't know that it could be... i'm on Arch Linux, everything up to date. i tried both wifi and ethernet (cable) connection, no way</p><p><a href="http://imgur.com/yqNIePL">screenshot</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Jul '15, 12:20</strong></p><img src="https://secure.gravatar.com/avatar/befa9d0a7941ee05db0cb339e12a1db5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="andrello&#39;s gravatar image" /><p><span>andrello</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="andrello has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>17 Jul '15, 12:37</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-44263" class="comments-container"></div><div id="comment-tools-44263" class="comment-tools"></div><div class="clear"></div><div id="comment-44263-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="44264"></span>

<div id="answer-container-44264" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44264-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44264-score" class="post-score" title="current number of votes">1</div><span id="post-44264-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="andrello has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Something may have shrunk the size of the packet list pane to zero. (I was able to do that deliberately with a GTK+ 2.x version.)</p><p>See if there's anything you can grab, with the mouse/trackpad, right above the "Frame 1" line, and drag down.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Jul '15, 12:36</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-44264" class="comments-container"><span id="44265"></span><div id="comment-44265" class="comment"><div id="post-44265-score" class="comment-score"></div><div class="comment-text"><p>YES!! thank you so much! i'd not have ever thought that ahahah</p></div><div id="comment-44265-info" class="comment-info"><span class="comment-age">(17 Jul '15, 12:58)</span> <span class="comment-user userinfo">andrello</span></div></div></div><div id="comment-tools-44264" class="comment-tools"></div><div class="clear"></div><div id="comment-44264-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

