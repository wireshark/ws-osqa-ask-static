+++
type = "question"
title = "wiretap/.libs/libwiretap.so: undefined reference"
description = '''Hi ALL, I am trying to Make wireshark-1/0/5 version ./configure was successful. But when i run Make i get Below error wiretap/.libs/libwiretap.so: undefined reference to `g_direct_hash&#x27; wiretap/.libs/libwiretap.so: undefined reference to `g_list_append&#x27; wiretap/.libs/libwiretap.so: undefined referen...'''
date = "2014-04-03T04:02:00Z"
lastmod = "2014-04-03T04:02:00Z"
weight = 31471
keywords = [ "libwiretap.so" ]
aliases = [ "/questions/31471" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wiretap/.libs/libwiretap.so: undefined reference](/questions/31471/wiretaplibslibwiretapso-undefined-reference)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31471-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31471-score" class="post-score" title="current number of votes">0</div><span id="post-31471-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi ALL,</p><p>I am trying to Make wireshark-1/0/5 version ./configure was successful. But when i run Make i get Below error</p><pre><code>wiretap/.libs/libwiretap.so: undefined reference to `g_direct_hash&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_list_append&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_malloc&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_free&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_hash_table_remove&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_str_equal&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_hash_table_lookup&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_assertion_message_expr&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_array_new&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_strup&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_hash_table_new&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_array_prepend_vals&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_direct_equal&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_ptr_array_add&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_strdup_printf&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_ptr_array_new&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_array_append_vals&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_hash_table_insert&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_log&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_hash_table_destroy&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_list_free&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_str_hash&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_strdown&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_realloc&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_memdup&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_malloc0&#39;
wiretap/.libs/libwiretap.so: undefined reference to `g_strdup&#39;</code></pre><p>Please help....I have installed all the dependencies. Still i am getting this issue.</p><p>Please let me know if i have missed anything.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-libwiretap.so" rel="tag" title="see questions tagged &#39;libwiretap.so&#39;">libwiretap.so</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Apr '14, 04:02</strong></p><img src="https://secure.gravatar.com/avatar/aac0f612e12e99c0777b2473e290c534?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nisha&#39;s gravatar image" /><p><span>nisha</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nisha has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Apr '14, 04:59</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-31471" class="comments-container"></div><div id="comment-tools-31471" class="comment-tools"></div><div class="clear"></div><div id="comment-31471-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

