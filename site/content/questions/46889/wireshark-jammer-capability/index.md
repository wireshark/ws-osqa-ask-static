+++
type = "question"
title = "Wireshark &#x27;jammer&#x27; capability?"
description = '''Hi Does Wireshark have a &#x27;jammer&#x27; capability (modify frame contents, insert errors, etc.)? Thanks tl'''
date = "2015-10-23T11:31:00Z"
lastmod = "2015-10-23T12:31:00Z"
weight = 46889
keywords = [ "wireshark" ]
aliases = [ "/questions/46889" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark 'jammer' capability?](/questions/46889/wireshark-jammer-capability)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46889-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46889-score" class="post-score" title="current number of votes">0</div><span id="post-46889-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>Does Wireshark have a 'jammer' capability (modify frame contents, insert errors, etc.)?</p><p>Thanks tl</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Oct '15, 11:31</strong></p><img src="https://secure.gravatar.com/avatar/2d36cff4aac8328424127ec70a1e1fad?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tlemons&#39;s gravatar image" /><p><span>tlemons</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tlemons has no accepted answers">0%</span></p></div></div><div id="comments-container-46889" class="comments-container"></div><div id="comment-tools-46889" class="comment-tools"></div><div class="clear"></div><div id="comment-46889-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="46890"></span>

<div id="answer-container-46890" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46890-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46890-score" class="post-score" title="current number of votes">0</div><span id="post-46890-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, it is a recording and decoding tool. It doesn't do modifications.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Oct '15, 11:32</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-46890" class="comments-container"><span id="46891"></span><div id="comment-46891" class="comment"><div id="post-46891-score" class="comment-score"></div><div class="comment-text"><p>Thanks for the amazingly-fast answer!</p></div><div id="comment-46891-info" class="comment-info"><span class="comment-age">(23 Oct '15, 11:53)</span> <span class="comment-user userinfo">tlemons</span></div></div><span id="46892"></span><div id="comment-46892" class="comment"><div id="post-46892-score" class="comment-score"></div><div class="comment-text"><p>There is an (experimental) build in editor available. As you can see here: <a href="https://ask.wireshark.org/questions/42193/how-can-i-modify-a-value-within-the-80211-header-of-a-captured-packet">https://ask.wireshark.org/questions/42193/how-can-i-modify-a-value-within-the-80211-header-of-a-captured-packet</a></p><p>But it is still no jamming feature.</p></div><div id="comment-46892-info" class="comment-info"><span class="comment-age">(23 Oct '15, 12:00)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-46890" class="comment-tools"></div><div class="clear"></div><div id="comment-46890-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="46893"></span>

<div id="answer-container-46893" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46893-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46893-score" class="post-score" title="current number of votes">0</div><span id="post-46893-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, but its <a href="https://www.wireshark.org/docs/man-pages/editcap.html">editcap</a> text based companion can.</p><p>(hint: look for -E)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Oct '15, 12:31</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Oct '15, 14:33</strong> </span></p></div></div><div id="comments-container-46893" class="comments-container"></div><div id="comment-tools-46893" class="comment-tools"></div><div class="clear"></div><div id="comment-46893-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

