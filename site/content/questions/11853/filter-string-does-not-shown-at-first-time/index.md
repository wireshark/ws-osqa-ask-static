+++
type = "question"
title = "filter string does not shown at first time?"
description = '''Hi all  after upgrading wireshark into ver1.6.8  can&#x27;t input filter string on Filter as the below   cursor is pointing Filter:  but can&#x27;t insert any character or something else.  also after opening a wireshark files, when i click any frame, it&#x27;s color is shown as gray color but not blue.  as i know,...'''
date = "2012-06-12T06:50:00Z"
lastmod = "2012-09-05T03:19:00Z"
weight = 11853
keywords = [ "filter", "string" ]
aliases = [ "/questions/11853" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [filter string does not shown at first time?](/questions/11853/filter-string-does-not-shown-at-first-time)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11853-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11853-score" class="post-score" title="current number of votes">0</div><span id="post-11853-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all after upgrading wireshark into ver1.6.8 can't input filter string on Filter as the below <img src="https://osqa-ask.wireshark.org/upfiles/ScreenHunter_01_Jun._12_22.41.jpg" alt="alt text" /></p><p>cursor is pointing Filter: but can't insert any character or something else. also after opening a wireshark files, when i click any frame, it's color is shown as gray color but not blue. as i know, on normal operation, frame i clicked is shown as blue background color.</p><p>isn't there anyone knows about this?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-string" rel="tag" title="see questions tagged &#39;string&#39;">string</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jun '12, 06:50</strong></p><img src="https://secure.gravatar.com/avatar/27e4d1e97303115b07caf9ba39267f2b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ray_Han&#39;s gravatar image" /><p><span>Ray_Han</span><br />
<span class="score" title="56 reputation points">56</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="11 badges"><span class="bronze">●</span><span class="badgecount">11</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ray_Han has no accepted answers">0%</span></p></img></div></div><div id="comments-container-11853" class="comments-container"><span id="14049"></span><div id="comment-14049" class="comment"><div id="post-14049-score" class="comment-score"></div><div class="comment-text"><p>I have the same issue!</p></div><div id="comment-14049-info" class="comment-info"><span class="comment-age">(04 Sep '12, 20:06)</span> <span class="comment-user userinfo">iSunky</span></div></div><span id="14050"></span><div id="comment-14050" class="comment"><div id="post-14050-score" class="comment-score"></div><div class="comment-text"><p>BTW, my current version is 1.8.2</p></div><div id="comment-14050-info" class="comment-info"><span class="comment-age">(04 Sep '12, 20:07)</span> <span class="comment-user userinfo">iSunky</span></div></div></div><div id="comment-tools-11853" class="comment-tools"></div><div class="clear"></div><div id="comment-11853-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14056"></span>

<div id="answer-container-14056" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14056-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14056-score" class="post-score" title="current number of votes">0</div><span id="post-14056-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Ray_Han has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>sounds like the problem in this (unanswered) question:</p><blockquote><p><code>http://ask.wireshark.org/questions/13818/cannot-filter-when-i-open-wireshark</code><br />
</p></blockquote><p>Maybe a yet undiscovered bug ...</p><p><span>@Ray_Han</span>: Is the behaviour the same for every capture file? If so, please post the following information:</p><blockquote><p><code>wireshark -v</code><br />
</p></blockquote><p>Then please file a bug report at <a href="http://bugs.wireshark.org">bugs.wireshark.org</a>.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Sep '12, 03:19</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-14056" class="comments-container"></div><div id="comment-tools-14056" class="comment-tools"></div><div class="clear"></div><div id="comment-14056-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

