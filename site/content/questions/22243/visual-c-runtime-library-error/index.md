+++
type = "question"
title = "Visual C++ Runtime Library Error"
description = '''Windows 7 Pro (32-bit) Service Pack 1 Pentium Dual Core E5700 @ 3Ghz 2.00ghz RAM Wireshark Version 1.10.0 Mirosoft Visual C++ Runtime Library This application has requested the runtime to terminate in an unusual way. Please contact the application&#x27;s support team for more information. Log Name: Appli...'''
date = "2013-06-22T02:07:00Z"
lastmod = "2013-08-21T01:24:00Z"
weight = 22243
keywords = [ "terminated", "runtime", "flow", "voip" ]
aliases = [ "/questions/22243" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Visual C++ Runtime Library Error](/questions/22243/visual-c-runtime-library-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22243-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22243-score" class="post-score" title="current number of votes">0</div><span id="post-22243-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Windows 7 Pro (32-bit) Service Pack 1 Pentium Dual Core E5700 @ 3Ghz 2.00ghz RAM</p><p>Wireshark Version 1.10.0</p><p>Mirosoft Visual C++ Runtime Library This application has requested the runtime to terminate in an unusual way. Please contact the application's support team for more information.</p><p>Log Name: Application Source: Application Error Event ID: 1000 Level: Error</p><p>Faulting application name: Wireshark.exe, version: 1.10.0.49790, time stamp: 0x51af7ca6 Faulting module name: libcairo-2.dll, version: 0.0.0.0, time stamp: 0x508da16e Exception code: 0x40000015 Fault offset: 0x0007ea30 Faulting process id: 0x600 Faulting application start time: 0x01ce6f24fab0b0cb Faulting application path: C:\Program Files\Wireshark\Wireshark.exe Faulting module path: C:\Program Files\Wireshark\libcairo-2.dll Report Id: 3f365f65-db18-11e2-b117-002719f1d382</p><p>This error occurs when selecting Telephony/Voip calls and the when either selecting Flow or when selecting player and then decode.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-terminated" rel="tag" title="see questions tagged &#39;terminated&#39;">terminated</span> <span class="post-tag tag-link-runtime" rel="tag" title="see questions tagged &#39;runtime&#39;">runtime</span> <span class="post-tag tag-link-flow" rel="tag" title="see questions tagged &#39;flow&#39;">flow</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jun '13, 02:07</strong></p><img src="https://secure.gravatar.com/avatar/748dd00f8b855e11344244ef34d9f42e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Doddy&#39;s gravatar image" /><p><span>Doddy</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Doddy has no accepted answers">0%</span></p></div></div><div id="comments-container-22243" class="comments-container"><span id="22252"></span><div id="comment-22252" class="comment"><div id="post-22252-score" class="comment-score">1</div><div class="comment-text"><p>This is not a question, this is a bug report. Please take it over to <a href="http://bugs.wireshark.org">http://bugs.wireshark.org</a>. Thank you.</p></div><div id="comment-22252-info" class="comment-info"><span class="comment-age">(23 Jun '13, 00:37)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="23882"></span><div id="comment-23882" class="comment"><div id="post-23882-score" class="comment-score"></div><div class="comment-text"><p>I am also having this problem. Has there been a resolution?</p></div><div id="comment-23882-info" class="comment-info"><span class="comment-age">(20 Aug '13, 12:09)</span> <span class="comment-user userinfo">ybarrap</span></div></div><span id="23895"></span><div id="comment-23895" class="comment"><div id="post-23895-score" class="comment-score"></div><div class="comment-text"><p><span>@ybarrap</span>, as per the comment from <span>@Jasper</span> this should be reported as a bug at the Wireshark bugzilla. If no-one who suffers the problem bothers to report it, preferably with captures and a reproduction recipe, then its unlikely to get fixed.</p></div><div id="comment-23895-info" class="comment-info"><span class="comment-age">(21 Aug '13, 01:24)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-22243" class="comment-tools"></div><div class="clear"></div><div id="comment-22243-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

