+++
type = "question"
title = "Bluetooth Low Energy (BLE) packets capture and display"
description = '''I am using Nordic BLE sniffer and WireShark together. But I cannot see the other packets except Advertisement packets. How do I see ALL BLE packets? Thanks, Sam'''
date = "2014-02-10T07:48:00Z"
lastmod = "2014-02-10T11:27:00Z"
weight = 29635
keywords = [ "bd_sam" ]
aliases = [ "/questions/29635" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Bluetooth Low Energy (BLE) packets capture and display](/questions/29635/bluetooth-low-energy-ble-packets-capture-and-display)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29635-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29635-score" class="post-score" title="current number of votes">0</div><span id="post-29635-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using Nordic BLE sniffer and WireShark together. But I cannot see the other packets except Advertisement packets. How do I see ALL BLE packets?</p><p>Thanks,</p><p>Sam</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bd_sam" rel="tag" title="see questions tagged &#39;bd_sam&#39;">bd_sam</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Feb '14, 07:48</strong></p><img src="https://secure.gravatar.com/avatar/c7e7bb0d1f2f10d6d21f5da06df30f07?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sam_Su&#39;s gravatar image" /><p><span>Sam_Su</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sam_Su has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Feb '14, 21:14</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-29635" class="comments-container"><span id="29636"></span><div id="comment-29636" class="comment"><div id="post-29636-score" class="comment-score"></div><div class="comment-text"><p>can you please post a capture file somewhere (google drive, dropbox, cloudshark.org)?</p><p>BTW: What's the Wireshark version?</p></div><div id="comment-29636-info" class="comment-info"><span class="comment-age">(10 Feb '14, 07:55)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="29642"></span><div id="comment-29642" class="comment"><div id="post-29642-score" class="comment-score"></div><div class="comment-text"><p>1.10.5. I can ONLY see the packets in the Advertisement channels, i.e., 37 38 and 39.</p><p>Thanks</p></div><div id="comment-29642-info" class="comment-info"><span class="comment-age">(10 Feb '14, 09:32)</span> <span class="comment-user userinfo">Sam_Su</span></div></div><span id="29646"></span><div id="comment-29646" class="comment"><div id="post-29646-score" class="comment-score"></div><div class="comment-text"><p>can you please post a capture file somewhere (google drive, dropbox, cloudshark.org)?</p></div><div id="comment-29646-info" class="comment-info"><span class="comment-age">(10 Feb '14, 11:27)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-29635" class="comment-tools"></div><div class="clear"></div><div id="comment-29635-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

