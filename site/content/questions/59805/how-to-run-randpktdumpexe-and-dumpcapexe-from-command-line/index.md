+++
type = "question"
title = "How To run randpktdump.exe and dumpcap.exe from command line"
description = '''How do I run randpktdump.exe and dumpcap.exe from command line? Does dumpcap.exe create the named pipe or do I need to create the named pipe outside of dumpcap and randpktdump? What is the order that the two applications have to be started in? dumpcap then randpktdump Thanks'''
date = "2017-03-02T07:38:00Z"
lastmod = "2017-03-02T07:38:00Z"
weight = 59805
keywords = [ "dumpcap" ]
aliases = [ "/questions/59805" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How To run randpktdump.exe and dumpcap.exe from command line](/questions/59805/how-to-run-randpktdumpexe-and-dumpcapexe-from-command-line)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59805-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59805-score" class="post-score" title="current number of votes">0</div><span id="post-59805-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I run randpktdump.exe and dumpcap.exe from command line?</p><p>Does dumpcap.exe create the named pipe or do I need to create the named pipe outside of dumpcap and randpktdump?</p><p>What is the order that the two applications have to be started in? dumpcap then randpktdump</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dumpcap" rel="tag" title="see questions tagged &#39;dumpcap&#39;">dumpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Mar '17, 07:38</strong></p><img src="https://secure.gravatar.com/avatar/334b3772ba24e093b1c83a07da9e12c2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rob%20B&#39;s gravatar image" /><p><span>Rob B</span><br />
<span class="score" title="36 reputation points">36</span><span title="8 badges"><span class="badge1">●</span><span class="badgecount">8</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rob B has no accepted answers">0%</span></p></div></div><div id="comments-container-59805" class="comments-container"></div><div id="comment-tools-59805" class="comment-tools"></div><div class="clear"></div><div id="comment-59805-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

