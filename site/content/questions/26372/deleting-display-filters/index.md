+++
type = "question"
title = "Deleting Display Filters"
description = '''How do I delete a display filter?'''
date = "2013-10-24T11:36:00Z"
lastmod = "2013-10-24T12:13:00Z"
weight = 26372
keywords = [ "displsy" ]
aliases = [ "/questions/26372" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Deleting Display Filters](/questions/26372/deleting-display-filters)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26372-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26372-score" class="post-score" title="current number of votes">0</div><span id="post-26372-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I delete a display filter?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-displsy" rel="tag" title="see questions tagged &#39;displsy&#39;">displsy</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Oct '13, 11:36</strong></p><img src="https://secure.gravatar.com/avatar/b2faf5581f7c20cf54418a6d9fabb5d0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JJH54&#39;s gravatar image" /><p><span>JJH54</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JJH54 has no accepted answers">0%</span></p></div></div><div id="comments-container-26372" class="comments-container"></div><div id="comment-tools-26372" class="comment-tools"></div><div class="clear"></div><div id="comment-26372-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26373"></span>

<div id="answer-container-26373" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26373-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26373-score" class="post-score" title="current number of votes">2</div><span id="post-26373-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Steps to delete a display filter from the drop-down list of display filters:</p><ol><li>Start Wireshark.</li><li>Choose <code>Help -&gt; About Wireshark -&gt; Folders -&gt; Personal configuration -&gt; [double-click folder]</code>. This will open the folder containing the <code>recent_common</code> file.</li><li>Quit Wireshark.</li><li>Open the <code>recent_common</code> file using your editor of choice.</li><li>Delete the line containing the <code>recent.display_filter</code> that you no longer want.</li><li>Save and close the <code>recent_common</code> file.</li><li>Restart Wireshark. The display filter should no longer appear in the drop-down list.</li></ol></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Oct '13, 12:13</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-26373" class="comments-container"></div><div id="comment-tools-26373" class="comment-tools"></div><div class="clear"></div><div id="comment-26373-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

