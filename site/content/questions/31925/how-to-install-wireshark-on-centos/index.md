+++
type = "question"
title = "how to install wireshark on centos ?"
description = '''Dear All Hope you are well. I need help. How to install wireshark on centos5.7 32bit server properly? Thank you Alif'''
date = "2014-04-17T04:41:00Z"
lastmod = "2014-04-17T06:31:00Z"
weight = 31925
keywords = [ "wireshark" ]
aliases = [ "/questions/31925" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to install wireshark on centos ?](/questions/31925/how-to-install-wireshark-on-centos)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31925-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31925-score" class="post-score" title="current number of votes">1</div><span id="post-31925-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear All Hope you are well.</p><p>I need help.</p><p>How to install wireshark on centos5.7 32bit server properly?</p><p>Thank you</p><p>Alif</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Apr '14, 04:41</strong></p><img src="https://secure.gravatar.com/avatar/7d40538f90ea24216147e331395a52a2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="alif_alif&#39;s gravatar image" /><p><span>alif_alif</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="alif_alif has no accepted answers">0%</span></p></div></div><div id="comments-container-31925" class="comments-container"></div><div id="comment-tools-31925" class="comment-tools"></div><div class="clear"></div><div id="comment-31925-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31926"></span>

<div id="answer-container-31926" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31926-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31926-score" class="post-score" title="current number of votes">1</div><span id="post-31926-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you're happy with the version that CentOS delivers, just do:</p><pre><code>yum install wireshark-gnome</code></pre><p>(the "-gnome" part says you want the Wireshark GUI; if you don't--you're happy with just the command-line tools, you can drop the "-gnome" part of the package name.)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Apr '14, 06:31</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-31926" class="comments-container"></div><div id="comment-tools-31926" class="comment-tools"></div><div class="clear"></div><div id="comment-31926-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

