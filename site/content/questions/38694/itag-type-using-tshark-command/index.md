+++
type = "question"
title = "itag type using tshark command"
description = '''Hi Team, Would like to fetch the itag value from a pcap file of 1st frame. Could you please suggest the tshark for such. Regards, Udaya Shankar Das'''
date = "2014-12-24T02:34:00Z"
lastmod = "2015-01-04T19:57:00Z"
weight = 38694
keywords = [ "tshark", "itag" ]
aliases = [ "/questions/38694" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [itag type using tshark command](/questions/38694/itag-type-using-tshark-command)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38694-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38694-score" class="post-score" title="current number of votes">0</div><span id="post-38694-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Team,</p><p>Would like to fetch the itag value from a pcap file of 1st frame. Could you please suggest the tshark for such.</p><p>Regards, Udaya Shankar Das</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-itag" rel="tag" title="see questions tagged &#39;itag&#39;">itag</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Dec '14, 02:34</strong></p><img src="https://secure.gravatar.com/avatar/fbfa082235ab499c4eb41ae3d8f6fe36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="udaya&#39;s gravatar image" /><p><span>udaya</span><br />
<span class="score" title="21 reputation points">21</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="udaya has one accepted answer">100%</span></p></div></div><div id="comments-container-38694" class="comments-container"><span id="38734"></span><div id="comment-38734" class="comment"><div id="post-38734-score" class="comment-score"></div><div class="comment-text"><p>itag???</p><p>Please add (much) more information what you are referring to, otherwise almost nobody will know what you are asking for !?!</p></div><div id="comment-38734-info" class="comment-info"><span class="comment-age">(27 Dec '14, 08:36)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="38758"></span><div id="comment-38758" class="comment"><div id="post-38758-score" class="comment-score"></div><div class="comment-text"><p>Thanks Kurt. I got the info. It was related N2X trafficgen .</p></div><div id="comment-38758-info" class="comment-info"><span class="comment-age">(28 Dec '14, 20:36)</span> <span class="comment-user userinfo">udaya</span></div></div><span id="38780"></span><div id="comment-38780" class="comment"><div id="post-38780-score" class="comment-score"></div><div class="comment-text"><p>can you please add an aswer with that information?</p><p>Maybe it's helpful for others.</p></div><div id="comment-38780-info" class="comment-info"><span class="comment-age">(30 Dec '14, 03:50)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="38817"></span><div id="comment-38817" class="comment"><div id="post-38817-score" class="comment-score"></div><div class="comment-text"><p>That's the <del>Agilent</del> <del>Keysight</del> Ixia N2X protocol tester he's talking about. 'itag' may refer to the inner tag of a double tagged frame.</p></div><div id="comment-38817-info" class="comment-info"><span class="comment-age">(31 Dec '14, 00:46)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="38829"></span><div id="comment-38829" class="comment"><div id="post-38829-score" class="comment-score"></div><div class="comment-text"><p>I did not find anything usefull about <strong>itag</strong>, so maybe the OP can add some words to explain what he found out.</p><p><span>@udaya</span>: Would that be possible?</p></div><div id="comment-38829-info" class="comment-info"><span class="comment-age">(31 Dec '14, 08:10)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="38889"></span><div id="comment-38889" class="comment not_top_scorer"><div id="post-38889-score" class="comment-score"></div><div class="comment-text"><p>Happy New Year to all!!</p><p>Yes- itag refers to the inner tag in QinQ and related to N2X trafficgen.</p><p>We can close this discussion. Thanks all for your support :)</p></div><div id="comment-38889-info" class="comment-info"><span class="comment-age">(04 Jan '15, 19:57)</span> <span class="comment-user userinfo">udaya</span></div></div></div><div id="comment-tools-38694" class="comment-tools"><span class="comments-showing"> showing 5 of 6 </span> <a href="#" class="show-all-comments-link">show 1 more comments</a></div><div class="clear"></div><div id="comment-38694-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

