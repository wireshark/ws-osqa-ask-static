+++
type = "question"
title = "Wireshark stop to decode TCP frames"
description = '''Context: Fedora 14 with wireshark 1.4.0 Use: live capture when a burst Phenomenon appears, wireshark stops decode TCP frames. Only Ethernet protocol is showed on the screen. If I stop capture and I save it as a pcap file, all protocol frames are decoded. Anybody have an explanation ? Thanks.'''
date = "2011-06-09T02:01:00Z"
lastmod = "2011-06-09T02:01:00Z"
weight = 4468
keywords = [ "livecapturetcp" ]
aliases = [ "/questions/4468" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark stop to decode TCP frames](/questions/4468/wireshark-stop-to-decode-tcp-frames)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4468-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4468-score" class="post-score" title="current number of votes">0</div><span id="post-4468-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Context: Fedora 14 with wireshark 1.4.0 Use: live capture</p><p>when a burst Phenomenon appears, wireshark stops decode TCP frames. Only Ethernet protocol is showed on the screen. If I stop capture and I save it as a pcap file, all protocol frames are decoded. Anybody have an explanation ?</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-livecapturetcp" rel="tag" title="see questions tagged &#39;livecapturetcp&#39;">livecapturetcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jun '11, 02:01</strong></p><img src="https://secure.gravatar.com/avatar/447fff10d8055fd0b340d76873ae03ce?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dja92&#39;s gravatar image" /><p><span>dja92</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dja92 has no accepted answers">0%</span></p></div></div><div id="comments-container-4468" class="comments-container"></div><div id="comment-tools-4468" class="comment-tools"></div><div class="clear"></div><div id="comment-4468-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

