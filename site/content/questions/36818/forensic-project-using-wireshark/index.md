+++
type = "question"
title = "Forensic Project using wireshark"
description = '''Hi all Im currently doing a forensic project into TOR browser, im just wondering if anyone knows a way as to how to capture only packets from the virtual machine i will be using? thanks'''
date = "2014-10-03T06:58:00Z"
lastmod = "2014-10-07T13:26:00Z"
weight = 36818
keywords = [ "tor", "forensics", "vmware", "wireshark" ]
aliases = [ "/questions/36818" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Forensic Project using wireshark](/questions/36818/forensic-project-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36818-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36818-score" class="post-score" title="current number of votes">0</div><span id="post-36818-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all</p><p>Im currently doing a forensic project into TOR browser, im just wondering if anyone knows a way as to how to capture only packets from the virtual machine i will be using?</p><p>thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tor" rel="tag" title="see questions tagged &#39;tor&#39;">tor</span> <span class="post-tag tag-link-forensics" rel="tag" title="see questions tagged &#39;forensics&#39;">forensics</span> <span class="post-tag tag-link-vmware" rel="tag" title="see questions tagged &#39;vmware&#39;">vmware</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Oct '14, 06:58</strong></p><img src="https://secure.gravatar.com/avatar/396b76f5eb33e0ec0b3f112cb87f632f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="stemac92&#39;s gravatar image" /><p><span>stemac92</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="stemac92 has no accepted answers">0%</span></p></div></div><div id="comments-container-36818" class="comments-container"></div><div id="comment-tools-36818" class="comment-tools"></div><div class="clear"></div><div id="comment-36818-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36897"></span>

<div id="answer-container-36897" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36897-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36897-score" class="post-score" title="current number of votes">0</div><span id="post-36897-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>In WireShark, if you capture packets from the Virtual interface that is associated with your virtual machine, shouldn't that work? Please let me know if that works.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Oct '14, 13:26</strong></p><img src="https://secure.gravatar.com/avatar/4784c5fb1a0142030d51a339706a456c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Beldum&#39;s gravatar image" /><p><span>Beldum</span><br />
<span class="score" title="49 reputation points">49</span><span title="11 badges"><span class="badge1">●</span><span class="badgecount">11</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="16 badges"><span class="bronze">●</span><span class="badgecount">16</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Beldum has no accepted answers">0%</span></p></div></div><div id="comments-container-36897" class="comments-container"></div><div id="comment-tools-36897" class="comment-tools"></div><div class="clear"></div><div id="comment-36897-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

