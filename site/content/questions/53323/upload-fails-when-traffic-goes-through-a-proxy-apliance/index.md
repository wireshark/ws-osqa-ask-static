+++
type = "question"
title = "upload fails when traffic goes through a proxy apliance"
description = '''We are having issues uploading files from any web browser to a specific server when traffic goes to the appliance, either via Transparent or Explicit proxy, we currently use WCCP.  When we disable WCCP, upload works fine.  On a packet capture from the appliance we can see a lot of TCP Retransmission...'''
date = "2016-06-08T12:39:00Z"
lastmod = "2016-06-08T12:39:00Z"
weight = 53323
keywords = [ "proxy", "https" ]
aliases = [ "/questions/53323" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [upload fails when traffic goes through a proxy apliance](/questions/53323/upload-fails-when-traffic-goes-through-a-proxy-apliance)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53323-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53323-score" class="post-score" title="current number of votes">0</div><span id="post-53323-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>We are having issues uploading files from any web browser to a specific server when traffic goes to the appliance, either via Transparent or Explicit proxy, we currently use WCCP.</p><p>When we disable WCCP, upload works fine.</p><p>On a packet capture from the appliance we can see a lot of TCP Retransmission errors</p><p>[TCP Out-Of-Order] [TCP segment of a reassembled PDU] [TCP Retransmission]</p><p>Im just looking to see if anyone has seen something like this before.</p><p>Please see screenshot attached</p><blockquote><span>View post on imgur.com</span></blockquote>&lt;script async="" src="//s.imgur.com/min/embed.js" charset="utf-8"&gt;&lt;/script&gt;</div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-proxy" rel="tag" title="see questions tagged &#39;proxy&#39;">proxy</span> <span class="post-tag tag-link-https" rel="tag" title="see questions tagged &#39;https&#39;">https</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jun '16, 12:39</strong></p><img src="https://secure.gravatar.com/avatar/f01934ca2f96f700cc8b6d28fd8b8606?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="leandro&#39;s gravatar image" /><p><span>leandro</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="leandro has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Jun '16, 12:42</strong> </span></p></div></div><div id="comments-container-53323" class="comments-container"></div><div id="comment-tools-53323" class="comment-tools"></div><div class="clear"></div><div id="comment-53323-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

