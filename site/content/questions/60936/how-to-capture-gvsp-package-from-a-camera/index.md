+++
type = "question"
title = "How to capture GVSP package from a camera"
description = '''I can capture GVCP packages successfully, but none of the GVSP packages can be captured. I have disabled TCP chimney and enabled the GVSP protocol. It still does not work.'''
date = "2017-04-20T21:32:00Z"
lastmod = "2017-04-20T21:32:00Z"
weight = 60936
keywords = [ "gige", "gvsp", "camera", "missing_packets" ]
aliases = [ "/questions/60936" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture GVSP package from a camera](/questions/60936/how-to-capture-gvsp-package-from-a-camera)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60936-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60936-score" class="post-score" title="current number of votes">0</div><span id="post-60936-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can capture GVCP packages successfully, but none of the GVSP packages can be captured. I have disabled TCP chimney and enabled the GVSP protocol. It still does not work.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gige" rel="tag" title="see questions tagged &#39;gige&#39;">gige</span> <span class="post-tag tag-link-gvsp" rel="tag" title="see questions tagged &#39;gvsp&#39;">gvsp</span> <span class="post-tag tag-link-camera" rel="tag" title="see questions tagged &#39;camera&#39;">camera</span> <span class="post-tag tag-link-missing_packets" rel="tag" title="see questions tagged &#39;missing_packets&#39;">missing_packets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Apr '17, 21:32</strong></p><img src="https://secure.gravatar.com/avatar/7b1ce0d5efc5fccb2518554ec93c8848?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wendy30086&#39;s gravatar image" /><p><span>wendy30086</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wendy30086 has no accepted answers">0%</span></p></div></div><div id="comments-container-60936" class="comments-container"></div><div id="comment-tools-60936" class="comment-tools"></div><div class="clear"></div><div id="comment-60936-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

