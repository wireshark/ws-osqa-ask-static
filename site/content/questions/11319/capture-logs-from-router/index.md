+++
type = "question"
title = "[closed] capture logs from router"
description = '''How do I collect the logs that the Netgear FVS318v3 sends to the PC? I point the logs to PC, but I can not see a way to capture them.'''
date = "2012-05-24T19:45:00Z"
lastmod = "2012-05-26T04:41:00Z"
weight = 11319
keywords = [ "capture", "logs" ]
aliases = [ "/questions/11319" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] capture logs from router](/questions/11319/capture-logs-from-router)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11319-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11319-score" class="post-score" title="current number of votes">0</div><span id="post-11319-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I collect the logs that the Netgear FVS318v3 sends to the PC?</p><p>I point the logs to PC, but I can not see a way to capture them.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-logs" rel="tag" title="see questions tagged &#39;logs&#39;">logs</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 May '12, 19:45</strong></p><img src="https://secure.gravatar.com/avatar/5d86e2827790345022ce08b16dfc818b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AAB&#39;s gravatar image" /><p><span>AAB</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AAB has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>25 May '12, 06:12</strong> </span></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span></p></div></div><div id="comments-container-11319" class="comments-container"></div><div id="comment-tools-11319" class="comment-tools"></div><div class="clear"></div><div id="comment-11319-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by JeffMorriss 25 May '12, 06:12

</div>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="11321"></span>

<div id="answer-container-11321" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11321-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11321-score" class="post-score" title="current number of votes">1</div><span id="post-11321-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I can only guess, but based on your description, you are sending the <strong>router logs via syslog</strong> to the PC. Therefore you would need a <strong>syslog server on the PC</strong>.</p><p>HOWEVER, that's not a problem related to Wireshark and I suggest you try to get help in the Netgear forum: <a href="http://forum1.netgear.com/">http://forum1.netgear.com/</a></p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 May '12, 01:58</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-11321" class="comments-container"><span id="11366"></span><div id="comment-11366" class="comment"><div id="post-11366-score" class="comment-score"></div><div class="comment-text"><p>Hi Kurt,</p><p>Yes, I found a syslog server and the information is not useful. I will need to add a pc with 2 network cards to read the data from the network.</p></div><div id="comment-11366-info" class="comment-info"><span class="comment-age">(25 May '12, 17:15)</span> <span class="comment-user userinfo">AAB</span></div></div><span id="11372"></span><div id="comment-11372" class="comment"><div id="post-11372-score" class="comment-score"></div><div class="comment-text"><blockquote><p>Yes, I found a syslog server and the information is not useful.</p></blockquote><p>Why?</p><blockquote><p>I will need to add a pc with 2 network cards to read the data from the network.</p></blockquote><p>No, you don't</p></div><div id="comment-11372-info" class="comment-info"><span class="comment-age">(26 May '12, 04:41)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-11321" class="comment-tools"></div><div class="clear"></div><div id="comment-11321-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

