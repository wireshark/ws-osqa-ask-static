+++
type = "question"
title = "Diameter command code 318,316,317,321,319,320 not getting captured"
description = '''Hello Experts, I am using wireshark 1.8.3. I would like to capture the traces on the s6a interface(between MME and HLR).  I am getting traces related to Disconnect Peer Request/Answer(282) and Capabilities Exchange Request/Answer (257).  But I am not able to see the rest of the things like ULR/ULA(3...'''
date = "2012-10-03T00:57:00Z"
lastmod = "2014-04-04T09:18:00Z"
weight = 14658
keywords = [ "question" ]
aliases = [ "/questions/14658" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Diameter command code 318,316,317,321,319,320 not getting captured](/questions/14658/diameter-command-code-318316317321319320-not-getting-captured)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14658-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14658-score" class="post-score" title="current number of votes">0</div><span id="post-14658-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Experts,</p><p>I am using wireshark 1.8.3. I would like to capture the traces on the s6a interface(between MME and HLR). I am getting traces related to Disconnect Peer Request/Answer(282) and Capabilities Exchange Request/Answer (257). But I am not able to see the rest of the things like ULR/ULA(316),AIR/AIA(318). What could be the reason for this. Do I have to change any settings related to this in the wireshark?</p><p>Thanks!</p><p>Regards, Ravi.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-question" rel="tag" title="see questions tagged &#39;question&#39;">question</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Oct '12, 00:57</strong></p><img src="https://secure.gravatar.com/avatar/5cbb674a715ede302fed35577a22fc7f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ravikris&#39;s gravatar image" /><p><span>ravikris</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ravikris has no accepted answers">0%</span></p></div></div><div id="comments-container-14658" class="comments-container"></div><div id="comment-tools-14658" class="comment-tools"></div><div class="clear"></div><div id="comment-14658-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31517"></span>

<div id="answer-container-31517" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31517-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31517-score" class="post-score" title="current number of votes">0</div><span id="post-31517-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>check if everything is okay with your dictionary file.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Apr '14, 09:18</strong></p><img src="https://secure.gravatar.com/avatar/425d250364423a7595a3eb9dea779cb2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sanny_D&#39;s gravatar image" /><p><span>Sanny_D</span><br />
<span class="score" title="0 reputation points">0</span><span title="18 badges"><span class="badge1">●</span><span class="badgecount">18</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="21 badges"><span class="bronze">●</span><span class="badgecount">21</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sanny_D has 3 accepted answers">50%</span></p></div></div><div id="comments-container-31517" class="comments-container"></div><div id="comment-tools-31517" class="comment-tools"></div><div class="clear"></div><div id="comment-31517-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

