+++
type = "question"
title = "Mac OSX Mavericks X11 interface anomaly with multiple monitors"
description = '''Wireshark opens fine for me on Mavericks. The problem is, it&#x27;s window bar opens in the same position as Mac OSX&#x27;s window bar. So I can&#x27;t reposition wireshark. I cannot figure out a way to grab wireshark&#x27;s window bar to reposition. I have a quad-monitor set up. I have tried using mirroring mode to so...'''
date = "2014-06-18T10:18:00Z"
lastmod = "2014-06-18T10:18:00Z"
weight = 33935
keywords = [ "xquartz" ]
aliases = [ "/questions/33935" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Mac OSX Mavericks X11 interface anomaly with multiple monitors](/questions/33935/mac-osx-mavericks-x11-interface-anomaly-with-multiple-monitors)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33935-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33935-score" class="post-score" title="current number of votes">0</div><span id="post-33935-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark opens fine for me on Mavericks. The problem is, it's window bar opens in the same position as Mac OSX's window bar. So I can't reposition wireshark. I cannot figure out a way to grab wireshark's window bar to reposition. I have a quad-monitor set up. I have tried using mirroring mode to sort it out, but that seems to mess with X11/Xquartz and hide wireshark altogether. Is there a way to tell wireshark to open in a lower arbitrary position or open below the Mac OSX top menu bar? Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-xquartz" rel="tag" title="see questions tagged &#39;xquartz&#39;">xquartz</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jun '14, 10:18</strong></p><img src="https://secure.gravatar.com/avatar/2daaed0f953ed850693cdffc5e1866ce?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="burnbrighter&#39;s gravatar image" /><p><span>burnbrighter</span><br />
<span class="score" title="31 reputation points">31</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="burnbrighter has no accepted answers">0%</span></p></div></div><div id="comments-container-33935" class="comments-container"></div><div id="comment-tools-33935" class="comment-tools"></div><div class="clear"></div><div id="comment-33935-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

