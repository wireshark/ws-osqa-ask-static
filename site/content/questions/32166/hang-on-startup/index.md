+++
type = "question"
title = "Hang on startup"
description = '''I just installed Wireshark on my Windows 8.1 laptop (all current updates). After rebooting I tried to start Wireshark and it hung leaving the initial Wireshark logo displayed and the &quot;Loading configuration files...&quot; showing 100%. I did also install USBPcap. Would that cause a problem. Any help would...'''
date = "2014-04-24T22:02:00Z"
lastmod = "2014-04-24T22:16:00Z"
weight = 32166
keywords = [ "hang", "startup58" ]
aliases = [ "/questions/32166" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Hang on startup](/questions/32166/hang-on-startup)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32166-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32166-score" class="post-score" title="current number of votes">0</div><span id="post-32166-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just installed Wireshark on my Windows 8.1 laptop (all current updates). After rebooting I tried to start Wireshark and it hung leaving the initial Wireshark logo displayed and the "Loading configuration files..." showing 100%. I did also install USBPcap. Would that cause a problem. Any help would be appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hang" rel="tag" title="see questions tagged &#39;hang&#39;">hang</span> <span class="post-tag tag-link-startup58" rel="tag" title="see questions tagged &#39;startup58&#39;">startup58</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Apr '14, 22:02</strong></p><img src="https://secure.gravatar.com/avatar/0fb3d89df9281db54d6152dd5eda141a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Jackson&#39;s gravatar image" /><p><span>Jim Jackson</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Jackson has no accepted answers">0%</span></p></div></div><div id="comments-container-32166" class="comments-container"></div><div id="comment-tools-32166" class="comment-tools"></div><div class="clear"></div><div id="comment-32166-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32167"></span>

<div id="answer-container-32167" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32167-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32167-score" class="post-score" title="current number of votes">0</div><span id="post-32167-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Apologies as I have any only been on here for about 3 days so I may be very wrong.</p><p>When I installed Wireshark I right mouse clicked the installer and selected "run as administrator" and it worked fine.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Apr '14, 22:16</strong></p><img src="https://secure.gravatar.com/avatar/1de54471a0cc95cb8604b875d49fe7e9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yoyomonkey&#39;s gravatar image" /><p><span>yoyomonkey</span><br />
<span class="score" title="0 reputation points">0</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yoyomonkey has no accepted answers">0%</span></p></div></div><div id="comments-container-32167" class="comments-container"></div><div id="comment-tools-32167" class="comment-tools"></div><div class="clear"></div><div id="comment-32167-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

