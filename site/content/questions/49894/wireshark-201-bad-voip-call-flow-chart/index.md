+++
type = "question"
title = "Wireshark 2.0.1 - bad VoIP Call Flow Chart"
description = '''Hello, Can you please bring back the VoIP Call Flow chart as it was in previous versions? The new one is not useful at all as it doesn&#x27;t show IP addresses, is not colored and even scrolling using mouse wheel doesn&#x27;t work. I needed to go back to previous 1.12.9 version because of that. Thank you Toma...'''
date = "2016-02-05T06:21:00Z"
lastmod = "2016-02-05T17:04:00Z"
weight = 49894
keywords = [ "flow", "call", "chart", "voip" ]
aliases = [ "/questions/49894" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark 2.0.1 - bad VoIP Call Flow Chart](/questions/49894/wireshark-201-bad-voip-call-flow-chart)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49894-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49894-score" class="post-score" title="current number of votes">0</div><span id="post-49894-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>Can you please bring back the VoIP Call Flow chart as it was in previous versions? The new one is not useful at all as it doesn't show IP addresses, is not colored and even scrolling using mouse wheel doesn't work. I needed to go back to previous 1.12.9 version because of that.</p><p>Thank you Tomas</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-flow" rel="tag" title="see questions tagged &#39;flow&#39;">flow</span> <span class="post-tag tag-link-call" rel="tag" title="see questions tagged &#39;call&#39;">call</span> <span class="post-tag tag-link-chart" rel="tag" title="see questions tagged &#39;chart&#39;">chart</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Feb '16, 06:21</strong></p><img src="https://secure.gravatar.com/avatar/7cdcf769c08e290a4a63ab69e33a46b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tomas&#39;s gravatar image" /><p><span>Tomas</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tomas has no accepted answers">0%</span></p></div></div><div id="comments-container-49894" class="comments-container"></div><div id="comment-tools-49894" class="comment-tools"></div><div class="clear"></div><div id="comment-49894-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="49896"></span>

<div id="answer-container-49896" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49896-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49896-score" class="post-score" title="current number of votes">0</div><span id="post-49896-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can still use the older GTK UI in 2.0.1, run the legacy application wireshark-gtk. The binary should be right alongside the new Qt version.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Feb '16, 07:19</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-49896" class="comments-container"></div><div id="comment-tools-49896" class="comment-tools"></div><div class="clear"></div><div id="comment-49896-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="49916"></span>

<div id="answer-container-49916" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49916-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49916-score" class="post-score" title="current number of votes">0</div><span id="post-49916-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is being tracked in <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=11710">bug 11710</a>. Hopefully we can get it fixed in 2.0.2.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Feb '16, 17:04</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-49916" class="comments-container"></div><div id="comment-tools-49916" class="comment-tools"></div><div class="clear"></div><div id="comment-49916-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

