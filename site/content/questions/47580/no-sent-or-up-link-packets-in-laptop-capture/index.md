+++
type = "question"
title = "No sent or up-link packets in laptop capture"
description = '''Hello All, Recently I observed a peculiar behaviour with Wirehsark while trying to capture Wireless and/or LAN interface on my Laptop . I couldn&#x27;t see any up-link or sent packets in my capture. I double checked, there are no capture filters set. Can anyone help with understanding the issue? It&#x27;s a D...'''
date = "2015-11-13T11:28:00Z"
lastmod = "2015-11-13T23:29:00Z"
weight = 47580
keywords = [ "interface", "capture", "upload" ]
aliases = [ "/questions/47580" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [No sent or up-link packets in laptop capture](/questions/47580/no-sent-or-up-link-packets-in-laptop-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47580-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47580-score" class="post-score" title="current number of votes">0</div><span id="post-47580-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello All,</p><p>Recently I observed a peculiar behaviour with Wirehsark while trying to capture Wireless and/or LAN interface on my Laptop . I couldn't see any up-link or sent packets in my capture. I double checked, there are no capture filters set. Can anyone help with understanding the issue?</p><p>It's a Dell laptop, running Win 7.</p><p>Regards// Mike.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-upload" rel="tag" title="see questions tagged &#39;upload&#39;">upload</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Nov '15, 11:28</strong></p><img src="https://secure.gravatar.com/avatar/47fc7c930344eff0c1201716f0545e07?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mike_446&#39;s gravatar image" /><p><span>Mike_446</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mike_446 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Nov '15, 23:31</strong> </span></p></div></div><div id="comments-container-47580" class="comments-container"></div><div id="comment-tools-47580" class="comment-tools"></div><div class="clear"></div><div id="comment-47580-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="47589"></span>

<div id="answer-container-47589" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47589-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47589-score" class="post-score" title="current number of votes">1</div><span id="post-47589-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Mike_446 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There are some possible reasons, like offloading, security software, etc. Please read my answers to the following questions:</p><blockquote><p><a href="https://ask.wireshark.org/questions/27296/wireshark-only-capturing-incoming-packets">https://ask.wireshark.org/questions/27296/wireshark-only-capturing-incoming-packets</a><br />
<a href="https://ask.wireshark.org/questions/32090/unable-to-capture-outgoing-traffic-on-windows7-x64">https://ask.wireshark.org/questions/32090/unable-to-capture-outgoing-traffic-on-windows7-x64</a><br />
<a href="https://ask.wireshark.org/questions/28909/no-outgoing-packets">https://ask.wireshark.org/questions/28909/no-outgoing-packets</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Nov '15, 13:58</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-47589" class="comments-container"><span id="47596"></span><div id="comment-47596" class="comment"><div id="post-47596-score" class="comment-score"></div><div class="comment-text"><p>Thanks Kurt!!! Disabling "DNE LightWeight Filter" in on NIC properties solved the issue. :)</p></div><div id="comment-47596-info" class="comment-info"><span class="comment-age">(13 Nov '15, 23:29)</span> <span class="comment-user userinfo">Mike_446</span></div></div></div><div id="comment-tools-47589" class="comment-tools"></div><div class="clear"></div><div id="comment-47589-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

