+++
type = "question"
title = "compare two dump files"
description = '''How can i compare two files(one from server, second from client)? I found in documentation compare files   but i cannot find this menu item in my wireshark(version 2.2.5 windows x64), where is this item?'''
date = "2017-03-24T08:14:00Z"
lastmod = "2017-03-26T02:55:00Z"
weight = 60315
keywords = [ "files", "compare" ]
aliases = [ "/questions/60315" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [compare two dump files](/questions/60315/compare-two-dump-files)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60315-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60315-score" class="post-score" title="current number of votes">0</div><span id="post-60315-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can i compare two files(one from server, second from client)? I found in documentation <a href="https://www.wireshark.org/docs/wsug_html_chunked/ChStatCompareCaptureFiles.html">compare files</a></p><p><img src="https://www.wireshark.org/docs/wsug_html_chunked/wsug_graphics/ws-stats-compare.png" alt="alt text" /><br />
but i cannot find this menu item in my wireshark(version 2.2.5 windows x64), where is this item?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-files" rel="tag" title="see questions tagged &#39;files&#39;">files</span> <span class="post-tag tag-link-compare" rel="tag" title="see questions tagged &#39;compare&#39;">compare</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Mar '17, 08:14</strong></p><img src="https://secure.gravatar.com/avatar/31c3c7425c1acfdc7b08fc09bd41d56c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="slavick&#39;s gravatar image" /><p><span>slavick</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="slavick has no accepted answers">0%</span> </br></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Mar '17, 08:28</strong> </span></p></div></div><div id="comments-container-60315" class="comments-container"></div><div id="comment-tools-60315" class="comment-tools"></div><div class="clear"></div><div id="comment-60315-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60327"></span>

<div id="answer-container-60327" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60327-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60327-score" class="post-score" title="current number of votes">2</div><span id="post-60327-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The compare feature is currently only available in the old, "legacy" GTK UI (Wireshark-gtk.exe).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Mar '17, 02:55</strong></p><img src="https://secure.gravatar.com/avatar/11cda2a4be5391632a5b28af1927307b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Uli&#39;s gravatar image" /><p><span>Uli</span><br />
<span class="score" title="903 reputation points">903</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Uli has 16 accepted answers">29%</span></p></div></div><div id="comments-container-60327" class="comments-container"></div><div id="comment-tools-60327" class="comment-tools"></div><div class="clear"></div><div id="comment-60327-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

