+++
type = "question"
title = "Wireshark install on Mavericks hangs at &quot;Preparing for installation&quot;"
description = '''I am trying to install the Wireshark 1.12.0 version on an Intel 64 bit OS on iMac running Mavericks. I ran into an install issue. I read somewhere that an XQuartz app might need to be installed, but this article referred to a lion, and not Mavericks. I&#x27;ve tried three times installing the Wireshark, ...'''
date = "2014-08-07T08:48:00Z"
lastmod = "2014-08-07T14:41:00Z"
weight = 35308
keywords = [ "hang", "install", "mavericks" ]
aliases = [ "/questions/35308" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark install on Mavericks hangs at "Preparing for installation"](/questions/35308/wireshark-install-on-mavericks-hangs-at-preparing-for-installation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35308-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35308-score" class="post-score" title="current number of votes">0</div><span id="post-35308-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to install the Wireshark 1.12.0 version on an Intel 64 bit OS on iMac running Mavericks. I ran into an install issue. I read somewhere that an XQuartz app might need to be installed, but this article referred to a lion, and not Mavericks. I've tried three times installing the Wireshark, even authorizing apps downloaded from anywhere in Security &amp; Privacy pane, but it gets hung up at "Preparing for installation" and it goes on to an unreasonable timing of 15 minutes plus when I finally force quit. I would like to know if anyone has had this issue, and how were you able to correct it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hang" rel="tag" title="see questions tagged &#39;hang&#39;">hang</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span> <span class="post-tag tag-link-mavericks" rel="tag" title="see questions tagged &#39;mavericks&#39;">mavericks</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Aug '14, 08:48</strong></p><img src="https://secure.gravatar.com/avatar/4bf069c688b4aa9c599b9ff76958b608?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="macaid&#39;s gravatar image" /><p><span>macaid</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="macaid has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted <strong>07 Aug '14, 13:54</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-35308" class="comments-container"><span id="35314"></span><div id="comment-35314" class="comment"><div id="post-35314-score" class="comment-score"></div><div class="comment-text"><p><span>@Guy Harris</span></p><p>This was originally posted as an "answer" to the question where I moved it to a comment. I think this question is now a dupe of <a href="http://ask.wireshark.org/questions/35304/imac-wireshark-installation-gets-hung-up-cannot-install">http://ask.wireshark.org/questions/35304/imac-wireshark-installation-gets-hung-up-cannot-install</a></p></div><div id="comment-35314-info" class="comment-info"><span class="comment-age">(07 Aug '14, 14:41)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-35308" class="comment-tools"></div><div class="clear"></div><div id="comment-35308-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

