+++
type = "question"
title = "[closed] ad.internal conversations traffic in windows  2008 domain Win 7 clients"
description = '''We use Quest foglight and we see the following &quot;ad.internal&quot; conversations traffic We are using windows 2008 domain with Win 7 clients does anyone know what it is?'''
date = "2014-01-05T17:29:00Z"
lastmod = "2014-01-08T09:16:00Z"
weight = 28591
keywords = [ "ad.internal" ]
aliases = [ "/questions/28591" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] ad.internal conversations traffic in windows 2008 domain Win 7 clients](/questions/28591/adinternal-conversations-traffic-in-windows-2008-domain-win-7-clients)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28591-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28591-score" class="post-score" title="current number of votes">0</div><span id="post-28591-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We use Quest foglight and we see the following "ad.internal" conversations traffic We are using windows 2008 domain with Win 7 clients</p><p>does anyone know what it is?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ad.internal" rel="tag" title="see questions tagged &#39;ad.internal&#39;">ad.internal</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jan '14, 17:29</strong></p><img src="https://secure.gravatar.com/avatar/5869055942e0e45f142d7634a2d1d248?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RIA&#39;s gravatar image" /><p><span>RIA</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RIA has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>08 Jan '14, 09:16</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-28591" class="comments-container"><span id="28676"></span><div id="comment-28676" class="comment"><div id="post-28676-score" class="comment-score"></div><div class="comment-text"><p>Why do you ask this on a <strong>Wireshark</strong> site? I guess the <strong>vendor of that software</strong> will know better than anybody else what <strong>they</strong> mean by <code>ad.internal</code>, as that's what <strong>their tool</strong> is showing on the screen ;-)) !??!</p></div><div id="comment-28676-info" class="comment-info"><span class="comment-age">(08 Jan '14, 09:16)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-28591" class="comment-tools"></div><div class="clear"></div><div id="comment-28591-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Kurt Knochner 08 Jan '14, 09:16

</div>

</div>

</div>

