+++
type = "question"
title = "using wireshark to find out communication with specific application"
description = '''for example I have one SAP webclient application running on my box using wireshark I want to find out when client connects to that SAP server and does all communication, which ports it use and all communication process from start to end. Thanks for help in advance.'''
date = "2014-02-20T02:46:00Z"
lastmod = "2014-02-20T02:46:00Z"
weight = 30040
keywords = [ "sap" ]
aliases = [ "/questions/30040" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [using wireshark to find out communication with specific application](/questions/30040/using-wireshark-to-find-out-communication-with-specific-application)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30040-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30040-score" class="post-score" title="current number of votes">0</div><span id="post-30040-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>for example I have one SAP webclient application running on my box using wireshark I want to find out when client connects to that SAP server and does all communication, which ports it use and all communication process from start to end.</p><p>Thanks for help in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sap" rel="tag" title="see questions tagged &#39;sap&#39;">sap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Feb '14, 02:46</strong></p><img src="https://secure.gravatar.com/avatar/6615a61d69b703d89076bb0f18342bbf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="m_1607&#39;s gravatar image" /><p><span>m_1607</span><br />
<span class="score" title="35 reputation points">35</span><span title="12 badges"><span class="badge1">●</span><span class="badgecount">12</span></span><span title="13 badges"><span class="silver">●</span><span class="badgecount">13</span></span><span title="16 badges"><span class="bronze">●</span><span class="badgecount">16</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="m_1607 has no accepted answers">0%</span></p></div></div><div id="comments-container-30040" class="comments-container"></div><div id="comment-tools-30040" class="comment-tools"></div><div class="clear"></div><div id="comment-30040-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

