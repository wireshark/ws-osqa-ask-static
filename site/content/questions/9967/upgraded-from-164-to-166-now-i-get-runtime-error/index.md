+++
type = "question"
title = "Upgraded from 1.6.4 to 1.6.6, now I get &quot;runtime error&quot;"
description = '''I use named pipes to access serial port data. After I upgraded from Wireshark 1.6.4 to Wireshark 1.6.6, I get a runtime error when I try to enter my named pipe address in the capture dialog box.'''
date = "2012-04-05T10:08:00Z"
lastmod = "2012-05-21T15:08:00Z"
weight = 9967
keywords = [ "runtime", "error" ]
aliases = [ "/questions/9967" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Upgraded from 1.6.4 to 1.6.6, now I get "runtime error"](/questions/9967/upgraded-from-164-to-166-now-i-get-runtime-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9967-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9967-score" class="post-score" title="current number of votes">0</div><span id="post-9967-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I use named pipes to access serial port data.</p><p>After I upgraded from Wireshark 1.6.4 to Wireshark 1.6.6, I get a runtime error when I try to enter my named pipe address in the capture dialog box.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-runtime" rel="tag" title="see questions tagged &#39;runtime&#39;">runtime</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Apr '12, 10:08</strong></p><img src="https://secure.gravatar.com/avatar/d24c288a157c69a5410752c8764d1010?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="korwinula&#39;s gravatar image" /><p><span>korwinula</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="korwinula has no accepted answers">0%</span></p></div></div><div id="comments-container-9967" class="comments-container"></div><div id="comment-tools-9967" class="comment-tools"></div><div class="clear"></div><div id="comment-9967-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10487"></span>

<div id="answer-container-10487" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10487-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10487-score" class="post-score" title="current number of votes">0</div><span id="post-10487-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hmm, a problem having to do with named pipes was supposed to have been fixed with 1.6.6. You might want to try 1.6.7 just in case, but if that doesn't work, I'd suggest you open a <a href="https://bugs.wireshark.org">bug report</a>. Or try/use the development version (e.g., 1.7.1).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Apr '12, 08:03</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-10487" class="comments-container"><span id="11193"></span><div id="comment-11193" class="comment"><div id="post-11193-score" class="comment-score"></div><div class="comment-text"><p>I downgraded to 1.6.4 and wireshark is now crash free after a few hours of solid running</p></div><div id="comment-11193-info" class="comment-info"><span class="comment-age">(21 May '12, 15:08)</span> <span class="comment-user userinfo">h4ns0l0</span></div></div></div><div id="comment-tools-10487" class="comment-tools"></div><div class="clear"></div><div id="comment-10487-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

