+++
type = "question"
title = "Extract payload from dump"
description = '''Hi, I dumped some network traffic using wireshark and I need to extract the actual payload of the packets in a new file, i. e. to get rid of link layer, internet layer, transport layer headers and save the application data. In Wireshark, there&#x27;s this &quot;follow stream&quot; function, but I need it to be scr...'''
date = "2010-09-27T06:55:00Z"
lastmod = "2010-10-06T16:12:00Z"
weight = 338
keywords = [ "follow", "extract", "payload", "stream" ]
aliases = [ "/questions/338" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Extract payload from dump](/questions/338/extract-payload-from-dump)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-338-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-338-score" class="post-score" title="current number of votes">0</div><span id="post-338-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I dumped some network traffic using wireshark and I need to extract the actual payload of the packets in a new file, i. e. to get rid of link layer, internet layer, transport layer headers and save the application data. In Wireshark, there's this "follow stream" function, but I need it to be scriptable and non-interactive since there's gigabytes of dumps in hundreds of files. Does anyone have an idea how I could accomplish this? Thanks in advance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-follow" rel="tag" title="see questions tagged &#39;follow&#39;">follow</span> <span class="post-tag tag-link-extract" rel="tag" title="see questions tagged &#39;extract&#39;">extract</span> <span class="post-tag tag-link-payload" rel="tag" title="see questions tagged &#39;payload&#39;">payload</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Sep '10, 06:55</strong></p><img src="https://secure.gravatar.com/avatar/b59ff5ce5c7b340ca60bef38e5d7c7d3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Aventurepix&#39;s gravatar image" /><p><span>Aventurepix</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Aventurepix has no accepted answers">0%</span></p></div></div><div id="comments-container-338" class="comments-container"></div><div id="comment-tools-338" class="comment-tools"></div><div class="clear"></div><div id="comment-338-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="449"></span>

<div id="answer-container-449" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-449-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-449-score" class="post-score" title="current number of votes">0</div><span id="post-449-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://www.circlemud.org/~jelson/software/tcpflow/">Tcpflow?</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Oct '10, 16:12</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Oct '10, 16:18</strong> </span></p></div></div><div id="comments-container-449" class="comments-container"></div><div id="comment-tools-449" class="comment-tools"></div><div class="clear"></div><div id="comment-449-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

