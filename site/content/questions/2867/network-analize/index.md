+++
type = "question"
title = "Network analize"
description = '''When i&#x27;m connect to a network only packages that i can see are from my pc,what i&#x27;m doing wrong?'''
date = "2011-03-16T02:07:00Z"
lastmod = "2011-03-16T04:32:00Z"
weight = 2867
keywords = [ "packages", "network" ]
aliases = [ "/questions/2867" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Network analize](/questions/2867/network-analize)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2867-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2867-score" class="post-score" title="current number of votes">0</div><span id="post-2867-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When i'm connect to a network only packages that i can see are from my pc,what i'm doing wrong?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packages" rel="tag" title="see questions tagged &#39;packages&#39;">packages</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Mar '11, 02:07</strong></p><img src="https://secure.gravatar.com/avatar/7cd56f786ac305767716ee295f5e9029?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="GYBE&#39;s gravatar image" /><p><span>GYBE</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="GYBE has no accepted answers">0%</span></p></div></div><div id="comments-container-2867" class="comments-container"></div><div id="comment-tools-2867" class="comment-tools"></div><div class="clear"></div><div id="comment-2867-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2869"></span>

<div id="answer-container-2869" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2869-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2869-score" class="post-score" title="current number of votes">1</div><span id="post-2869-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>In today's switched networks you'll only receive packets for your network card plus broadcast/multicast packets. If you want to see more than that you'll have to use techniques to get access to packets sent by others, like those at <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">http://wiki.wireshark.org/CaptureSetup/Ethernet</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Mar '11, 04:32</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-2869" class="comments-container"></div><div id="comment-tools-2869" class="comment-tools"></div><div class="clear"></div><div id="comment-2869-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

