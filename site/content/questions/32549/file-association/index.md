+++
type = "question"
title = "file association"
description = '''first time opening Wireshark, I was asked what application to associate with. I mistakenly thought it was an output thing: and I clicked terminal. now every time I click the wireshark application, it opens terminal. I have completely uninstalled wireshark 10 times, and reinstalled, and it just opens...'''
date = "2014-05-06T08:16:00Z"
lastmod = "2014-05-06T10:10:00Z"
weight = 32549
keywords = [ "association", "file" ]
aliases = [ "/questions/32549" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [file association](/questions/32549/file-association)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32549-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32549-score" class="post-score" title="current number of votes">0</div><span id="post-32549-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>first time opening Wireshark, I was asked what application to associate with. I mistakenly thought it was an output thing: and I clicked terminal.</p><p>now every time I click the wireshark application, it opens terminal.</p><p>I have completely uninstalled wireshark 10 times, and reinstalled, and it just opens terminal.</p><p>how do I remove this association?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-association" rel="tag" title="see questions tagged &#39;association&#39;">association</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 May '14, 08:16</strong></p><img src="https://secure.gravatar.com/avatar/d35581482e2bd8f7bcffd501bd832158?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kaitana&#39;s gravatar image" /><p><span>Kaitana</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kaitana has no accepted answers">0%</span></p></div></div><div id="comments-container-32549" class="comments-container"><span id="32558"></span><div id="comment-32558" class="comment"><div id="post-32558-score" class="comment-score"></div><div class="comment-text"><p>What is your</p><ul><li>OS and OS version</li></ul></div><div id="comment-32558-info" class="comment-info"><span class="comment-age">(06 May '14, 10:08)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="32559"></span><div id="comment-32559" class="comment"><div id="post-32559-score" class="comment-score"></div><div class="comment-text"><p>MAC OS 10.9.2</p></div><div id="comment-32559-info" class="comment-info"><span class="comment-age">(06 May '14, 10:10)</span> <span class="comment-user userinfo">Kaitana</span></div></div></div><div id="comment-tools-32549" class="comment-tools"></div><div class="clear"></div><div id="comment-32549-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

