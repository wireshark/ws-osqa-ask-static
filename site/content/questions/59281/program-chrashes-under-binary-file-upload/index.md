+++
type = "question"
title = "Program chrashes under binary file upload"
description = '''Hello, I&#x27;m new in this forum and in this area. Please help me some advice if it is possible. So I&#x27;m working on a webserver application, and I use the lwip 1.4 tcp/ip stack. I implemented the file upload, but sometimes when I try to upload the files, get TCP retransmission and TCP Dup Ack error on ne...'''
date = "2017-02-09T05:07:00Z"
lastmod = "2017-02-09T05:07:00Z"
weight = 59281
keywords = [ "dup", "ack", "retransmissions", "tcp" ]
aliases = [ "/questions/59281" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Program chrashes under binary file upload](/questions/59281/program-chrashes-under-binary-file-upload)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59281-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59281-score" class="post-score" title="current number of votes">0</div><span id="post-59281-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I'm new in this forum and in this area. Please help me some advice if it is possible. So I'm working on a webserver application, and I use the lwip 1.4 tcp/ip stack. I implemented the file upload, but sometimes when I try to upload the files, get TCP retransmission and TCP Dup Ack error on network capture. Sometimes the file upload was succesfully and somtimes wasn't. I didn't find the right solution up to now.</p><p>The two captures(one when the upload was succesfully, one when wasn't) is available at this link.</p><p><a href="https://we.tl/BYayHqMAEV">https://we.tl/BYayHqMAEV</a></p><p>Please use the following filter to see the captures between my PC and my device: ip.addr == 192.168.0.23</p><p>Please help me. Kind regards, Szabolcs</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dup" rel="tag" title="see questions tagged &#39;dup&#39;">dup</span> <span class="post-tag tag-link-ack" rel="tag" title="see questions tagged &#39;ack&#39;">ack</span> <span class="post-tag tag-link-retransmissions" rel="tag" title="see questions tagged &#39;retransmissions&#39;">retransmissions</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Feb '17, 05:07</strong></p><img src="https://secure.gravatar.com/avatar/bdda0cce8e4e0c8f87529b92936dd956?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Szabolcs&#39;s gravatar image" /><p><span>Szabolcs</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Szabolcs has no accepted answers">0%</span></p></div></div><div id="comments-container-59281" class="comments-container"></div><div id="comment-tools-59281" class="comment-tools"></div><div class="clear"></div><div id="comment-59281-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

