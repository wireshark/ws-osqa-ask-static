+++
type = "question"
title = "&quot;import hex dump&quot; option in Tshark?"
description = '''GUI based wireshark provide &quot;import hex dump&quot;. Command based tshark provide &quot;import hex dump&quot; option?  or even a similar function? thanks.'''
date = "2013-09-05T03:55:00Z"
lastmod = "2013-09-05T06:17:00Z"
weight = 24372
keywords = [ "hexdump", "tshark" ]
aliases = [ "/questions/24372" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# ["import hex dump" option in Tshark?](/questions/24372/import-hex-dump-option-in-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24372-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24372-score" class="post-score" title="current number of votes">0</div><span id="post-24372-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>GUI based wireshark provide "import hex dump". Command based tshark provide "import hex dump" option? or even a similar function?</p><p>thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hexdump" rel="tag" title="see questions tagged &#39;hexdump&#39;">hexdump</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Sep '13, 03:55</strong></p><img src="https://secure.gravatar.com/avatar/87d9785e558dac3711884cc1afe5ad19?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kingko&#39;s gravatar image" /><p><span>kingko</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kingko has no accepted answers">0%</span></p></div></div><div id="comments-container-24372" class="comments-container"></div><div id="comment-tools-24372" class="comment-tools"></div><div class="clear"></div><div id="comment-24372-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="24378"></span>

<div id="answer-container-24378" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24378-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24378-score" class="post-score" title="current number of votes">0</div><span id="post-24378-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, that's what <a href="http://www.wireshark.org/docs/man-pages/text2pcap.html">text2pcap</a> is for. It's another command-line tool that's part of the Wireshark suite.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Sep '13, 06:17</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-24378" class="comments-container"></div><div id="comment-tools-24378" class="comment-tools"></div><div class="clear"></div><div id="comment-24378-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

