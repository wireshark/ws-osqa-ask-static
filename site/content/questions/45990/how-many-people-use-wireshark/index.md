+++
type = "question"
title = "How many people use Wireshark?"
description = '''I&#x27;m preparing a presentation on the subject of deep packet analysis. I&#x27;d like to use the number of Wireshark users as an illustration of the popularity of the tool. The only information I can find is the following from a press release regarding the takeover of CACE on the Riverbed website: In additi...'''
date = "2015-09-20T23:32:00Z"
lastmod = "2015-09-21T05:08:00Z"
weight = 45990
keywords = [ "wireshark" ]
aliases = [ "/questions/45990" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How many people use Wireshark?](/questions/45990/how-many-people-use-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45990-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45990-score" class="post-score" title="current number of votes">0</div><span id="post-45990-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm preparing a presentation on the subject of deep packet analysis. I'd like to use the number of Wireshark users as an illustration of the popularity of the tool. The only information I can find is the following from a press release regarding the takeover of CACE on the Riverbed website:</p><p><em>In addition to developing network analysis solutions, CACE is the sponsor of the award-winning Wireshark and WinPcap open source tools, the world’s most widely used tools for network traffic capture and analysis. The Wireshark community has millions of users and more than 400,000 downloads a month.</em></p><p>Are there any other figures available?</p><p>Thanks and regards...Paul</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Sep '15, 23:32</strong></p><img src="https://secure.gravatar.com/avatar/2e1b4057f2ff59fe059b23cc6571abaf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="PaulOfford&#39;s gravatar image" /><p><span>PaulOfford</span><br />
<span class="score" title="131 reputation points">131</span><span title="28 badges"><span class="badge1">●</span><span class="badgecount">28</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="PaulOfford has 5 accepted answers">11%</span></p></div></div><div id="comments-container-45990" class="comments-container"><span id="45991"></span><div id="comment-45991" class="comment"><div id="post-45991-score" class="comment-score"></div><div class="comment-text"><p>There used to be a great video on a world map showing how much developers worked on Wireshark from it's infancy until recent, showing how much of an open source tool it is, but i can't find it yet ..</p></div><div id="comment-45991-info" class="comment-info"><span class="comment-age">(21 Sep '15, 00:04)</span> <span class="comment-user userinfo">Marc</span></div></div></div><div id="comment-tools-45990" class="comment-tools"></div><div class="clear"></div><div id="comment-45990-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="46000"></span>

<div id="answer-container-46000" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46000-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46000-score" class="post-score" title="current number of votes">0</div><span id="post-46000-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The latest number of monthly downloads I got from Gerald Combs (Wireshark creator) back in February 2015 was above 1 million.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Sep '15, 04:38</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-46000" class="comments-container"><span id="46002"></span><div id="comment-46002" class="comment"><div id="post-46002-score" class="comment-score"></div><div class="comment-text"><p>don't we have more accurate numbers of <strong>users</strong> (not just downloads) from the auto-update attempts in the latest releases?</p></div><div id="comment-46002-info" class="comment-info"><span class="comment-age">(21 Sep '15, 04:46)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="46005"></span><div id="comment-46005" class="comment"><div id="post-46005-score" class="comment-score"></div><div class="comment-text"><p>We'll need to wait for Gerald to comment as I do not have this number either. That said auto updater only concerns Windows users and given the releases used by people asking questions on this site, we can still see a lot of obsolete versions still being used. I"m not sure we can easily figure out the number of active users.</p></div><div id="comment-46005-info" class="comment-info"><span class="comment-age">(21 Sep '15, 04:59)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div><span id="46006"></span><div id="comment-46006" class="comment"><div id="post-46006-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I"m not sure we can easily figure out the number of active users.</p></blockquote><p>I agree, because I myself downloaded Wireshark at least a hundred times or more, so download times won't tell us anything about the real number of <strong>users</strong>.</p></div><div id="comment-46006-info" class="comment-info"><span class="comment-age">(21 Sep '15, 05:08)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-46000" class="comment-tools"></div><div class="clear"></div><div id="comment-46000-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

