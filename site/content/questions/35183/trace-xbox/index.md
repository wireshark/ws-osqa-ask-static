+++
type = "question"
title = "Trace xbox"
description = '''Can I trace my xbox packets with wireshark and if so how do I do it please Sorry but my head is spinning trying to read this any chance of an idiot step by step guide my set up is an open reach modem to a plusnet finer router that goes to my PC and xbox one .I&#x27;m trying to trace packets and send reco...'''
date = "2014-08-05T01:07:00Z"
lastmod = "2014-08-05T01:09:00Z"
weight = 35183
keywords = [ "packets", "xbox" ]
aliases = [ "/questions/35183" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Trace xbox](/questions/35183/trace-xbox)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35183-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35183-score" class="post-score" title="current number of votes">0</div><span id="post-35183-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can I trace my xbox packets with wireshark and if so how do I do it please Sorry but my head is spinning trying to read this any chance of an idiot step by step guide my set up is an open reach modem to a plusnet finer router that goes to my PC and xbox one .I'm trying to trace packets and send record on to plusnet to sort my long standing latencey issues with Fifa 14,13 any help would be welcome you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-xbox" rel="tag" title="see questions tagged &#39;xbox&#39;">xbox</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Aug '14, 01:07</strong></p><img src="https://secure.gravatar.com/avatar/41620b4d28d7a515187d7fa547d60ab6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dazman7uk&#39;s gravatar image" /><p><span>dazman7uk</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dazman7uk has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Aug '14, 03:30</strong> </span></p></div></div><div id="comments-container-35183" class="comments-container"></div><div id="comment-tools-35183" class="comment-tools"></div><div class="clear"></div><div id="comment-35183-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35184"></span>

<div id="answer-container-35184" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35184-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35184-score" class="post-score" title="current number of votes">2</div><span id="post-35184-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Take a look at this Wiki page, it should help: <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">http://wiki.wireshark.org/CaptureSetup/Ethernet</a></p><p>Basically, you'll have to find a away to grab network packets from your setup. For that you either need a device that can provide copies of the packets it forwards (called SPAN port or monitor port), and I doubt your router can do that - check the manual if it does. Or, you can use a Network Switch that is manageable which has that feature and put it between router and XBox.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Aug '14, 01:09</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Aug '14, 01:38</strong> </span></p></div></div><div id="comments-container-35184" class="comments-container"></div><div id="comment-tools-35184" class="comment-tools"></div><div class="clear"></div><div id="comment-35184-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

