+++
type = "question"
title = "UDP Packet help please"
description = '''Hi! :) I need help. I have 3 UDP packets and I want decode this, and get this packet data. If I click to Follow -&amp;gt; UDP Stream, I have in this window with communication this: ... What does it mean?'''
date = "2017-04-15T10:59:00Z"
lastmod = "2017-04-15T11:33:00Z"
weight = 60846
keywords = [ "udp", "packets", "packet" ]
aliases = [ "/questions/60846" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [UDP Packet help please](/questions/60846/udp-packet-help-please)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60846-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60846-score" class="post-score" title="current number of votes">0</div><span id="post-60846-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi! :) I need help. I have 3 UDP packets and I want decode this, and get this packet data. If I click to Follow -&gt; UDP Stream, I have in this window with communication this: ...</p><p>What does it mean?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Apr '17, 10:59</strong></p><img src="https://secure.gravatar.com/avatar/01f4ed297682b0ea3e2891a6d7e59516?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MarteZ&#39;s gravatar image" /><p><span>MarteZ</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MarteZ has no accepted answers">0%</span></p></div></div><div id="comments-container-60846" class="comments-container"></div><div id="comment-tools-60846" class="comment-tools"></div><div class="clear"></div><div id="comment-60846-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60848"></span>

<div id="answer-container-60848" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60848-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60848-score" class="post-score" title="current number of votes">1</div><span id="post-60848-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Those dots represent values that don't have an ASCII representation, i.e. the hex values cannot be decoded. You can change the view from ASCII --&gt; Hex, or raw, etc.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Apr '17, 11:33</strong></p><img src="https://secure.gravatar.com/avatar/0a47ef51dd9c9996d194a4983295f5a4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bob%20Jones&#39;s gravatar image" /><p><span>Bob Jones</span><br />
<span class="score" title="1014 reputation points"><span>1.0k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bob Jones has 19 accepted answers">21%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Apr '17, 11:34</strong> </span></p></div></div><div id="comments-container-60848" class="comments-container"></div><div id="comment-tools-60848" class="comment-tools"></div><div class="clear"></div><div id="comment-60848-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

