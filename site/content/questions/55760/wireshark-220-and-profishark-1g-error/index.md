+++
type = "question"
title = "Wireshark 2.2.0 and PROFISHARK 1G error"
description = '''Seeing the following error when starting Wireshark: Field &#x27;Timestamp&#x27; (profishark.ts) is an FT_FLOAT but is being displayed as ABSOLUTE_TIME_LOCAL instead of BASE_NONE Wireshark then fails to start.'''
date = "2016-09-22T13:20:00Z"
lastmod = "2016-09-22T15:08:00Z"
weight = 55760
keywords = [ "starting", "error" ]
aliases = [ "/questions/55760" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark 2.2.0 and PROFISHARK 1G error](/questions/55760/wireshark-220-and-profishark-1g-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55760-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55760-score" class="post-score" title="current number of votes">0</div><span id="post-55760-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Seeing the following error when starting Wireshark: Field 'Timestamp' (profishark.ts) is an FT_FLOAT but is being displayed as ABSOLUTE_TIME_LOCAL instead of BASE_NONE</p><p>Wireshark then fails to start.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-starting" rel="tag" title="see questions tagged &#39;starting&#39;">starting</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Sep '16, 13:20</strong></p><img src="https://secure.gravatar.com/avatar/49b3bcf398ba78109e5a4a984b3dbe5c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="GaryRoger&#39;s gravatar image" /><p><span>GaryRoger</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="GaryRoger has no accepted answers">0%</span></p></div></div><div id="comments-container-55760" class="comments-container"></div><div id="comment-tools-55760" class="comment-tools"></div><div class="clear"></div><div id="comment-55760-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="55762"></span>

<div id="answer-container-55762" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55762-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55762-score" class="post-score" title="current number of votes">1</div><span id="post-55762-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Looks like your are having a plugin compiled for an older version of Wireshark that was not updated / recompiled for 2.2 version. You should ask ProfiShark support for an updated dll.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Sep '16, 13:26</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-55762" class="comments-container"><span id="55765"></span><div id="comment-55765" class="comment"><div id="post-55765-score" class="comment-score"></div><div class="comment-text"><p>Or ask them to contribute the plugin to the Wireshark project as source (or ask them for the source and then contribute that source to us yourself).</p></div><div id="comment-55765-info" class="comment-info"><span class="comment-age">(22 Sep '16, 15:08)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-55762" class="comment-tools"></div><div class="clear"></div><div id="comment-55762-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

