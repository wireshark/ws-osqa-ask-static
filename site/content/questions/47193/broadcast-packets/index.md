+++
type = "question"
title = "[closed] broadcast packets"
description = '''Hi can anyone help me with the this question: Add a set of counters to count the number of broadcast packets, the number of IP packets, and the number of ARP packets.Add code to print the value of these counters to the subroutine program_ending.'''
date = "2015-11-03T12:12:00Z"
lastmod = "2015-11-03T17:40:00Z"
weight = 47193
keywords = [ "wireshark" ]
aliases = [ "/questions/47193" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] broadcast packets](/questions/47193/broadcast-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47193-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47193-score" class="post-score" title="current number of votes">0</div><span id="post-47193-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi can anyone help me with the this question: Add a set of counters to count the number of broadcast packets, the number of IP packets, and the number of ARP packets.Add code to print the value of these counters to the subroutine program_ending.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Nov '15, 12:12</strong></p><img src="https://secure.gravatar.com/avatar/23c1e54fe0c74a4812b272ac55842da2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="omdina&#39;s gravatar image" /><p><span>omdina</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="omdina has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>04 Nov '15, 01:29</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-47193" class="comments-container"><span id="47196"></span><div id="comment-47196" class="comment"><div id="post-47196-score" class="comment-score"></div><div class="comment-text"><p>Is that a homework assignment?</p></div><div id="comment-47196-info" class="comment-info"><span class="comment-age">(03 Nov '15, 12:29)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="47197"></span><div id="comment-47197" class="comment"><div id="post-47197-score" class="comment-score"></div><div class="comment-text"><p>No it is not, my friends and I studying for a test and non of use know the answer for this question.</p></div><div id="comment-47197-info" class="comment-info"><span class="comment-age">(03 Nov '15, 12:34)</span> <span class="comment-user userinfo">omdina</span></div></div><span id="47201"></span><div id="comment-47201" class="comment"><div id="post-47201-score" class="comment-score"></div><div class="comment-text"><blockquote><p>Add code to print the value of these counters to the subroutine program_ending.</p></blockquote><p>what code and what subroutine? Sorry, it's unclear what you are asking for. Please be more specific!</p></div><div id="comment-47201-info" class="comment-info"><span class="comment-age">(03 Nov '15, 14:31)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="47203"></span><div id="comment-47203" class="comment"><div id="post-47203-score" class="comment-score"></div><div class="comment-text"><p>Hi,I am also not sure what is the main idea of this question, that is why I wrote asking for help, it is a very unclear question.sorry</p></div><div id="comment-47203-info" class="comment-info"><span class="comment-age">(03 Nov '15, 15:46)</span> <span class="comment-user userinfo">omdina</span></div></div><span id="47204"></span><div id="comment-47204" class="comment"><div id="post-47204-score" class="comment-score"></div><div class="comment-text"><p>You might try asking your teacher for more clarification to the question.</p></div><div id="comment-47204-info" class="comment-info"><span class="comment-age">(03 Nov '15, 17:36)</span> <span class="comment-user userinfo">Rooster_50</span></div></div><span id="47205"></span><div id="comment-47205" class="comment not_top_scorer"><div id="post-47205-score" class="comment-score"></div><div class="comment-text"><p>yes I think I should. Thanks</p></div><div id="comment-47205-info" class="comment-info"><span class="comment-age">(03 Nov '15, 17:40)</span> <span class="comment-user userinfo">omdina</span></div></div></div><div id="comment-tools-47193" class="comment-tools"><span class="comments-showing"> showing 5 of 6 </span> <a href="#" class="show-all-comments-link">show 1 more comments</a></div><div class="clear"></div><div id="comment-47193-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jaap 04 Nov '15, 01:29

</div>

</div>

</div>

