+++
type = "question"
title = "Part time packet analysis gigs - Suggestions / experiences?"
description = '''Hello, I have done packet analysis for a large enterprise for 20+ years, and it’s been very good to me (good compensation, GREAT job satisfaction). Down the road – in a few years – I would like to retire, but still do packet analysis gigs part time, for extra $$, and because I enjoy it thoroughly. I...'''
date = "2016-12-17T08:51:00Z"
lastmod = "2016-12-24T08:08:00Z"
weight = 58189
keywords = [ "job", "parttime" ]
aliases = [ "/questions/58189" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Part time packet analysis gigs - Suggestions / experiences?](/questions/58189/part-time-packet-analysis-gigs-suggestions-experiences)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58189-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58189-score" class="post-score" title="current number of votes">1</div><span id="post-58189-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I have done packet analysis for a large enterprise for 20+ years, and it’s been very good to me (good compensation, GREAT job satisfaction).</p><p>Down the road – in a few years – I would like to retire, but still do packet analysis gigs part time, for extra $$, and because I enjoy it thoroughly.</p><p>I suppose there are a few ways to go… Find a consulting company that will contact me when packet analysis is needed; Perhaps give seminars on packet analysis; etc. But these are just untested ideas.</p><p>I’d love to hear about experiences that you packeteers have had, or simply suggestions &amp; ideas with those types of arrangements / opportunities?</p><p>Thanx,</p><p>Feenyman99</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-job" rel="tag" title="see questions tagged &#39;job&#39;">job</span> <span class="post-tag tag-link-parttime" rel="tag" title="see questions tagged &#39;parttime&#39;">parttime</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Dec '16, 08:51</strong></p><img src="https://secure.gravatar.com/avatar/ba0791e3a82c059268b46a45ae90989f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="feenyman99&#39;s gravatar image" /><p><span>feenyman99</span><br />
<span class="score" title="96 reputation points">96</span><span title="22 badges"><span class="badge1">●</span><span class="badgecount">22</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="26 badges"><span class="bronze">●</span><span class="badgecount">26</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="feenyman99 has one accepted answer">25%</span></p></div></div><div id="comments-container-58189" class="comments-container"><span id="58191"></span><div id="comment-58191" class="comment"><div id="post-58191-score" class="comment-score"></div><div class="comment-text"><p>I'm in two minds as to whether this is off-topic. This is "Ask Wireshark", not a general discussion forum, although some questions do stretch it.</p></div><div id="comment-58191-info" class="comment-info"><span class="comment-age">(17 Dec '16, 10:00)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="58328"></span><div id="comment-58328" class="comment"><div id="post-58328-score" class="comment-score"></div><div class="comment-text"><p>Grahamb - I wondered the same thing :-)</p></div><div id="comment-58328-info" class="comment-info"><span class="comment-age">(24 Dec '16, 08:08)</span> <span class="comment-user userinfo">feenyman99</span></div></div></div><div id="comment-tools-58189" class="comment-tools"></div><div class="clear"></div><div id="comment-58189-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58190"></span>

<div id="answer-container-58190" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58190-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58190-score" class="post-score" title="current number of votes">0</div><span id="post-58190-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Aside from the direct route of a consulting company, one thought would be to set up an arrangement with the company you're retiring from. It's the place where you hopefully have the greatest reputation in the field, and where evidently they've had need of full-time network analysts of this sort at least up to this point.</p><p>I'm nowhere near retirement, though on the "hobby" side of the field I've had success putting together some online tutorials and doing in-house classes, where they can be as simple as lunch-hour or after-hour meeting room bookings with invites sent out to anybody interested in the enterprise (not necessarily a formal school or university environment). That's probably harder to do when you're retired, and it probably wouldn't be for money, but to keep it up as a hobby that's one angle.</p><p>Another thought would be to teach night classes at a local community college or university.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Dec '16, 09:50</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div></div><div id="comments-container-58190" class="comments-container"></div><div id="comment-tools-58190" class="comment-tools"></div><div class="clear"></div><div id="comment-58190-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

