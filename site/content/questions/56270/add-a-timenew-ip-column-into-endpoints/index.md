+++
type = "question"
title = "Add a Time/New Ip Column into Endpoints.?"
description = '''So all the new ips. That popup in the Endpoints are not Sorted. If a new one popup it can be anywhere in the list. So i would like an Time or Number by New added. First ip got number 1. Second 2 and so on. so if i sort By New i get the first added or last one added. I&#x27;am using Version 2.2.1 (v2.2.1-...'''
date = "2016-10-09T17:37:00Z"
lastmod = "2016-10-10T01:21:00Z"
weight = 56270
keywords = [ "column", "endpoints" ]
aliases = [ "/questions/56270" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Add a Time/New Ip Column into Endpoints.?](/questions/56270/add-a-timenew-ip-column-into-endpoints)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56270-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56270-score" class="post-score" title="current number of votes">0</div><span id="post-56270-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>So all the new ips. That popup in the Endpoints are not Sorted. If a new one popup it can be anywhere in the list. So i would like an Time or Number by New added. First ip got number 1. Second 2 and so on. so if i sort By New i get the first added or last one added. I'am using Version 2.2.1 (v2.2.1-0-ga6fbd27 from master-2.2)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-column" rel="tag" title="see questions tagged &#39;column&#39;">column</span> <span class="post-tag tag-link-endpoints" rel="tag" title="see questions tagged &#39;endpoints&#39;">endpoints</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Oct '16, 17:37</strong></p><img src="https://secure.gravatar.com/avatar/824ef9f1bc18eb16747e1bbefae01484?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Entok&#39;s gravatar image" /><p><span>Entok</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Entok has no accepted answers">0%</span></p></div></div><div id="comments-container-56270" class="comments-container"></div><div id="comment-tools-56270" class="comment-tools"></div><div class="clear"></div><div id="comment-56270-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="56273"></span>

<div id="answer-container-56273" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56273-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56273-score" class="post-score" title="current number of votes">0</div><span id="post-56273-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This isn't the right place for this. Go to <a href="https://bugs.wireshark.org">https://bugs.wireshark.org</a> and add a feature request instead. Thanks!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Oct '16, 01:21</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-56273" class="comments-container"></div><div id="comment-tools-56273" class="comment-tools"></div><div class="clear"></div><div id="comment-56273-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

