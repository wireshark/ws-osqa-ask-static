+++
type = "question"
title = "Help analaysing packet report"
description = '''I manage a small client server network and the users are complaining about webpages loading slow. Generally slow internet. I have narrowed this down to a device on the network that is uploading excessive data.  I can log in to the ISP customer data portal and run 5 minute data reports.  I would uplo...'''
date = "2015-08-17T17:12:00Z"
lastmod = "2015-08-17T18:47:00Z"
weight = 45179
keywords = [ "analysis" ]
aliases = [ "/questions/45179" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Help analaysing packet report](/questions/45179/help-analaysing-packet-report)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45179-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45179-score" class="post-score" title="current number of votes">0</div><span id="post-45179-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I manage a small client server network and the users are complaining about webpages loading slow. Generally slow internet. I have narrowed this down to a device on the network that is uploading excessive data. I can log in to the ISP customer data portal and run 5 minute data reports. I would upload an screen cap but I get a message requires &gt; karma 60? Anyway I have run Wireshark and I need the Packet report analysed by a trained eye as I am not sure of the results. Basically I am looking for an IP address that is transmitting a lot of packets that is using up the upstream bandwidth. There service is an ADSL2+ 4704/384. As you u can see their upstream bandwidth is limited so when the device transmits it uses all the bandwidth and slows the internet. How do I upload the packet report PCAPNG file? All help is greatly appreciated.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Aug '15, 17:12</strong></p><img src="https://secure.gravatar.com/avatar/bd85b6e4e855a8855694a0100a16f451?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="seanspcpowerIT&#39;s gravatar image" /><p><span>seanspcpowerIT</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="seanspcpowerIT has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-45179" class="comments-container"><span id="45181"></span><div id="comment-45181" class="comment"><div id="post-45181-score" class="comment-score"></div><div class="comment-text"><p>Upload your capture file to some place that is publicly accessible, such as Google Drive, Dropbox, or Cloudshark. I recommend Cloudshark (<a href="https://www.cloudshark.org">https://www.cloudshark.org</a>) and then edit your question to include the link to the uploaded file.</p></div><div id="comment-45181-info" class="comment-info"><span class="comment-age">(17 Aug '15, 18:44)</span> <span class="comment-user userinfo">Jim Aragon</span></div></div><span id="45182"></span><div id="comment-45182" class="comment"><div id="post-45182-score" class="comment-score"></div><div class="comment-text"><p>Thanks Jim for the assistance I will get that organised and post the link. Thanks again.</p></div><div id="comment-45182-info" class="comment-info"><span class="comment-age">(17 Aug '15, 18:47)</span> <span class="comment-user userinfo">seanspcpowerIT</span></div></div></div><div id="comment-tools-45179" class="comment-tools"></div><div class="clear"></div><div id="comment-45179-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

