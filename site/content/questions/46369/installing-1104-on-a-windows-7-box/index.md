+++
type = "question"
title = "installing 1.10.4 on a windows 7 box"
description = '''I need this version because at work we have a custom plugin I need to load that only works on 1.10.x versions of wireshark- I downloaded the folder wireshark-1.10.4.tar.bz2 and unzipped it to a folder on my windows box now how do I install it, I can&#x27;t find any executables, what am I missing here? al...'''
date = "2015-10-05T13:48:00Z"
lastmod = "2015-10-05T14:33:00Z"
weight = 46369
keywords = [ "installing" ]
aliases = [ "/questions/46369" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [installing 1.10.4 on a windows 7 box](/questions/46369/installing-1104-on-a-windows-7-box)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46369-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46369-score" class="post-score" title="current number of votes">0</div><span id="post-46369-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need this version because at work we have a custom plugin I need to load that only works on 1.10.x versions of wireshark- I downloaded the folder wireshark-1.10.4.tar.bz2 and unzipped it to a folder on my windows box now how do I install it, I can't find any executables, what am I missing here? all the install documentation points to 1.12 latest version.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-installing" rel="tag" title="see questions tagged &#39;installing&#39;">installing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Oct '15, 13:48</strong></p><img src="https://secure.gravatar.com/avatar/753d015b9aa132da7a096ea2cb1d95bf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="packetwatcher&#39;s gravatar image" /><p><span>packetwatcher</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="packetwatcher has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Oct '15, 14:34</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-46369" class="comments-container"></div><div id="comment-tools-46369" class="comment-tools"></div><div class="clear"></div><div id="comment-46369-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="46371"></span>

<div id="answer-container-46371" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46371-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46371-score" class="post-score" title="current number of votes">0</div><span id="post-46371-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's a source file tarball that you would need to compile to make use of.</p><p>If you're simply needing a 1.10.4 installer, then have a look <a href="https://www.wireshark.org/download/win32/all-versions/">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Oct '15, 14:33</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-46371" class="comments-container"></div><div id="comment-tools-46371" class="comment-tools"></div><div class="clear"></div><div id="comment-46371-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

