+++
type = "question"
title = "capture ipad tcp udp traffic with wireshark"
description = '''hi is it possible to capture ipad tcp udp traffic with wireshark'''
date = "2013-11-21T06:12:00Z"
lastmod = "2013-11-22T01:26:00Z"
weight = 27216
keywords = [ "wireshark" ]
aliases = [ "/questions/27216" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [capture ipad tcp udp traffic with wireshark](/questions/27216/capture-ipad-tcp-udp-traffic-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27216-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27216-score" class="post-score" title="current number of votes">0</div><span id="post-27216-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi</p><p>is it possible to capture ipad tcp udp traffic with wireshark</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Nov '13, 06:12</strong></p><img src="https://secure.gravatar.com/avatar/be8a9b2e9d87b13606c3b9e75d26e71d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="scara&#39;s gravatar image" /><p><span>scara</span><br />
<span class="score" title="31 reputation points">31</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="scara has no accepted answers">0%</span></p></div></div><div id="comments-container-27216" class="comments-container"></div><div id="comment-tools-27216" class="comment-tools"></div><div class="clear"></div><div id="comment-27216-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27220"></span>

<div id="answer-container-27220" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27220-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27220-score" class="post-score" title="current number of votes">0</div><span id="post-27220-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>yes</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Nov '13, 06:28</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-27220" class="comments-container"><span id="27224"></span><div id="comment-27224" class="comment"><div id="post-27224-score" class="comment-score"></div><div class="comment-text"><p>May i know how for ipad</p></div><div id="comment-27224-info" class="comment-info"><span class="comment-age">(21 Nov '13, 06:51)</span> <span class="comment-user userinfo">scara</span></div></div><span id="27226"></span><div id="comment-27226" class="comment"><div id="post-27226-score" class="comment-score"></div><div class="comment-text"><p>May I know how your iPad is connected to the network (wifi, GSM/3G, etc.)?</p></div><div id="comment-27226-info" class="comment-info"><span class="comment-age">(21 Nov '13, 06:52)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="27252"></span><div id="comment-27252" class="comment"><div id="post-27252-score" class="comment-score"></div><div class="comment-text"><p>my ipad is connected to wifi network and i am using windows 8 OS</p></div><div id="comment-27252-info" class="comment-info"><span class="comment-age">(21 Nov '13, 22:14)</span> <span class="comment-user userinfo">scara</span></div></div><span id="27260"></span><div id="comment-27260" class="comment"><div id="post-27260-score" class="comment-score"></div><div class="comment-text"><blockquote><p>connected to wifi network</p></blockquote><p>O.K. then please read the wifi/wlan capturing wiki</p><blockquote><p><a href="http://wiki.wireshark.org/CaptureSetup/WLAN">http://wiki.wireshark.org/CaptureSetup/WLAN</a><br />
</p></blockquote><p>Just one important thing in advance. If you want to capture wifi/wlan traffic on Windwos, you'll need special hardware (AirPcap) if you want to use Wireshark, as the capturing library of Wireshark (WinPcap) does not work well for wlan/wifi capturing in monitor mode on Windows. As an alternative you can use Linux (Ubuntu, Kali) on your PC to capture wifi/wlan traffic.</p><p>Another alternative would be to capture the ethernet traffic of the wireless AP: <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">http://wiki.wireshark.org/CaptureSetup/Ethernet</a></p><pre><code>iPad -- air --- AP ---- switch --- router --- internet
                           |
                           |
                       PC with Wireshark</code></pre></div><div id="comment-27260-info" class="comment-info"><span class="comment-age">(22 Nov '13, 01:26)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-27220" class="comment-tools"></div><div class="clear"></div><div id="comment-27220-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

