+++
type = "question"
title = "Troubleshoot Outlook Anywhere Outlook 2010 Connectivity Problems"
description = '''Client is stating they are receiving intermittent connectivity with our Exchange Outlook Anywhere server, this is an isolated incident. I had the client provide a .pcap trace over a 15 minute period where their Outlook client was connecting/disconnecting every 10 seconds or so. Can someone assist me...'''
date = "2012-02-15T05:27:00Z"
lastmod = "2012-02-15T05:48:00Z"
weight = 9019
keywords = [ "lan", "connectivity", "outlook", "network" ]
aliases = [ "/questions/9019" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Troubleshoot Outlook Anywhere Outlook 2010 Connectivity Problems](/questions/9019/troubleshoot-outlook-anywhere-outlook-2010-connectivity-problems)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9019-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9019-score" class="post-score" title="current number of votes">0</div><span id="post-9019-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Client is stating they are receiving intermittent connectivity with our Exchange Outlook Anywhere server, this is an isolated incident. I had the client provide a .pcap trace over a 15 minute period where their Outlook client was connecting/disconnecting every 10 seconds or so. Can someone assist me in what I should look for in this trace to try and determine why their network connectivity is sporadic?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lan" rel="tag" title="see questions tagged &#39;lan&#39;">lan</span> <span class="post-tag tag-link-connectivity" rel="tag" title="see questions tagged &#39;connectivity&#39;">connectivity</span> <span class="post-tag tag-link-outlook" rel="tag" title="see questions tagged &#39;outlook&#39;">outlook</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Feb '12, 05:27</strong></p><img src="https://secure.gravatar.com/avatar/5fd1d8eb41c925bef58401e7ffbadcb5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Vinsanity&#39;s gravatar image" /><p><span>Vinsanity</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Vinsanity has no accepted answers">0%</span></p></div></div><div id="comments-container-9019" class="comments-container"></div><div id="comment-tools-9019" class="comment-tools"></div><div class="clear"></div><div id="comment-9019-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9020"></span>

<div id="answer-container-9020" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9020-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9020-score" class="post-score" title="current number of votes">1</div><span id="post-9020-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That all depends on the nature of "Can someone assist me in what I should look for in this trace to try and determine why their network connectivity is sporadic?". This is not a get-your-consulting-for-free site. It is a community helping people to make better use of Wireshark which also includes some basic coaching on how network protocols work.</p><p>So, if you have specific questions about the things you see in the tracefile, then yes, your questions will be answered (if people find the time and interest to do so). But if you need someone to basically do the analysis for you, then you would have to resort to finding a Network Analysis consultant that can help you out.</p><p>If this was your intention, there are a couple of people on this site that do Network Analysis for a living (myself included) which you might want to get in contact with to help you out.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Feb '12, 05:38</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-9020" class="comments-container"><span id="9021"></span><div id="comment-9021" class="comment"><div id="post-9021-score" class="comment-score"></div><div class="comment-text"><p>I see, thanks anyway.</p></div><div id="comment-9021-info" class="comment-info"><span class="comment-age">(15 Feb '12, 05:48)</span> <span class="comment-user userinfo">Vinsanity</span></div></div></div><div id="comment-tools-9020" class="comment-tools"></div><div class="clear"></div><div id="comment-9020-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

