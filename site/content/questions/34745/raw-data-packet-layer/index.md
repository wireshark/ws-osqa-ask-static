+++
type = "question"
title = "Raw data packet layer"
description = '''Is there any way we can ignore or remove the raw packet data layer observed in wireshark trace having only RTP packets.'''
date = "2014-07-18T00:55:00Z"
lastmod = "2014-07-18T00:55:00Z"
weight = 34745
keywords = [ "raw", "data", "rtp", "packet" ]
aliases = [ "/questions/34745" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Raw data packet layer](/questions/34745/raw-data-packet-layer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34745-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34745-score" class="post-score" title="current number of votes">0</div><span id="post-34745-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there any way we can ignore or remove the raw packet data layer observed in wireshark trace having only RTP packets.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-raw" rel="tag" title="see questions tagged &#39;raw&#39;">raw</span> <span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jul '14, 00:55</strong></p><img src="https://secure.gravatar.com/avatar/847756fcd4cd227892a9745b9e030229?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Roopal&#39;s gravatar image" /><p><span>Roopal</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Roopal has no accepted answers">0%</span></p></div></div><div id="comments-container-34745" class="comments-container"></div><div id="comment-tools-34745" class="comment-tools"></div><div class="clear"></div><div id="comment-34745-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

