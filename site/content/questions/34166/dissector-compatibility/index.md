+++
type = "question"
title = "dissector compatibility?"
description = '''I&#x27;ve recently been given some &quot;home grown&quot; dissectors that were compiled for Wireshark 1.6.3. The current version that is on my Mageia 3 Linux system is 1.8.15. Can those older .so implemented dissectors be used w/o recompiling?'''
date = "2014-06-25T07:09:00Z"
lastmod = "2014-06-25T08:31:00Z"
weight = 34166
keywords = [ "dissector", "compatibility", "binary_compatibility" ]
aliases = [ "/questions/34166" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [dissector compatibility?](/questions/34166/dissector-compatibility)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34166-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34166-score" class="post-score" title="current number of votes">0</div><span id="post-34166-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've recently been given some "home grown" dissectors that were compiled for Wireshark 1.6.3. The current version that is on my Mageia 3 Linux system is 1.8.15. Can those older .so implemented dissectors be used w/o recompiling?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-compatibility" rel="tag" title="see questions tagged &#39;compatibility&#39;">compatibility</span> <span class="post-tag tag-link-binary_compatibility" rel="tag" title="see questions tagged &#39;binary_compatibility&#39;">binary_compatibility</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jun '14, 07:09</strong></p><img src="https://secure.gravatar.com/avatar/e041689c87f904923257ac6465eb1316?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="heymanj&#39;s gravatar image" /><p><span>heymanj</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="heymanj has no accepted answers">0%</span></p></div></div><div id="comments-container-34166" class="comments-container"><span id="34167"></span><div id="comment-34167" class="comment"><div id="post-34167-score" class="comment-score"></div><div class="comment-text"><p>additional information: undefined symbol rpc_auth_flavor</p></div><div id="comment-34167-info" class="comment-info"><span class="comment-age">(25 Jun '14, 07:12)</span> <span class="comment-user userinfo">heymanj</span></div></div></div><div id="comment-tools-34166" class="comment-tools"></div><div class="clear"></div><div id="comment-34166-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34169"></span>

<div id="answer-container-34169" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34169-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34169-score" class="post-score" title="current number of votes">1</div><span id="post-34169-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No the API is not frosen and there has been API changes so plugins has to be recompiled with the version they are to be run with.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Jun '14, 08:31</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-34169" class="comments-container"></div><div id="comment-tools-34169" class="comment-tools"></div><div class="clear"></div><div id="comment-34169-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

