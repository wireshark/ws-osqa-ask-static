+++
type = "question"
title = "wire shark initiates and hangs. task manager says not responding"
description = '''I&#x27;ve installed latest version of Wireshark on a Windows 8.1 laptop. Install went smoothly and could open Wireshark GUI, but after a few days, it now just starts and hangs at &quot;loading configuration files.&quot; Task Manager says app not responding. '''
date = "2014-09-08T04:11:00Z"
lastmod = "2014-09-08T09:28:00Z"
weight = 36065
keywords = [ "unresponsive" ]
aliases = [ "/questions/36065" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [wire shark initiates and hangs. task manager says not responding](/questions/36065/wire-shark-initiates-and-hangs-task-manager-says-not-responding)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36065-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36065-score" class="post-score" title="current number of votes">0</div><span id="post-36065-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've installed latest version of Wireshark on a Windows 8.1 laptop. Install went smoothly and could open Wireshark GUI, but after a few days, it now just starts and hangs at "loading configuration files." Task Manager says app not responding.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-unresponsive" rel="tag" title="see questions tagged &#39;unresponsive&#39;">unresponsive</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Sep '14, 04:11</strong></p><img src="https://secure.gravatar.com/avatar/7519dc040a596cbd59eed35940a34531?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mhein50&#39;s gravatar image" /><p><span>mhein50</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mhein50 has no accepted answers">0%</span></p></div></div><div id="comments-container-36065" class="comments-container"></div><div id="comment-tools-36065" class="comment-tools"></div><div class="clear"></div><div id="comment-36065-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="36067"></span>

<div id="answer-container-36067" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36067-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36067-score" class="post-score" title="current number of votes">0</div><span id="post-36067-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try to move the configuration file out of the way and see how far it gets when launched. Maybe there's something in your config file that it does not like any more. If you keep the old file you can compare it against the new one and find out what it was.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Sep '14, 05:56</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-36067" class="comments-container"></div><div id="comment-tools-36067" class="comment-tools"></div><div class="clear"></div><div id="comment-36067-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="36082"></span>

<div id="answer-container-36082" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36082-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36082-score" class="post-score" title="current number of votes">0</div><span id="post-36082-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is a know issue with WinPCAP on Windows 8.1. See <a href="https://ask.wireshark.org/questions/32166/hang-on-startup">https://ask.wireshark.org/questions/32166/hang-on-startup</a> and <a href="https://ask.wireshark.org/questions/33106/windows-81-can-not-use-wireshark-software">https://ask.wireshark.org/questions/33106/windows-81-can-not-use-wireshark-software</a> for known workarounds.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Sep '14, 09:28</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-36082" class="comments-container"></div><div id="comment-tools-36082" class="comment-tools"></div><div class="clear"></div><div id="comment-36082-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

