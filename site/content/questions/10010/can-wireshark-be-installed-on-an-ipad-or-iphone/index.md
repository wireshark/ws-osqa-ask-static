+++
type = "question"
title = "can wireshark be installed on an iPad or iPhone?"
description = '''I would like to know if wireshark or any other packet catcher can be installed on the Ipad2. 64gig,safari browser. newest version '''
date = "2012-04-07T16:22:00Z"
lastmod = "2015-03-09T19:17:00Z"
weight = 10010
keywords = [ "ipad", "download", "safari", "wireshark" ]
aliases = [ "/questions/10010" ]
osqa_answers = 4
osqa_accepted = false
+++

<div class="headNormal">

# [can wireshark be installed on an iPad or iPhone?](/questions/10010/can-wireshark-be-installed-on-an-ipad-or-iphone)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10010-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10010-score" class="post-score" title="current number of votes">0</div><span id="post-10010-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to know if wireshark or any other packet catcher can be installed on the Ipad2. 64gig,safari browser. newest version</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ipad" rel="tag" title="see questions tagged &#39;ipad&#39;">ipad</span> <span class="post-tag tag-link-download" rel="tag" title="see questions tagged &#39;download&#39;">download</span> <span class="post-tag tag-link-safari" rel="tag" title="see questions tagged &#39;safari&#39;">safari</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Apr '12, 16:22</strong></p><img src="https://secure.gravatar.com/avatar/9731f5664979dc2efd793a3f7df4e4c7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="redbud702&#39;s gravatar image" /><p><span>redbud702</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="redbud702 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>07 Apr '12, 22:42</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-10010" class="comments-container"><span id="40413"></span><div id="comment-40413" class="comment"><div id="post-40413-score" class="comment-score"></div><div class="comment-text"><p>Since this is the top google result for "ios wireshark": <a href="https://ask.wireshark.org/questions/17559/packet-capturing-application-for-the-iphone/36881">https://ask.wireshark.org/questions/17559/packet-capturing-application-for-the-iphone/36881</a></p></div><div id="comment-40413-info" class="comment-info"><span class="comment-age">(09 Mar '15, 19:17)</span> <span class="comment-user userinfo">bennettp123</span></div></div></div><div id="comment-tools-10010" class="comment-tools"></div><div class="clear"></div><div id="comment-10010-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

4 Answers:

</div>

</div>

<span id="10012"></span>

<div id="answer-container-10012" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10012-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10012-score" class="post-score" title="current number of votes">4</div><span id="post-10012-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>(Wireshark isn't a Web app, so it can't be installed in Safari or any other Web browser.)</p><p>There is no version of Wireshark that runs on <a href="http://en.wikipedia.org/wiki/IOS">iOS</a>, so it can't be installed on an iPad or an iPhone or an iPod touch. By default, in order to capture packets, a program needs to run as the <a href="http://en.wikipedia.org/wiki/Superuser">superuser</a> on <a href="http://en.wikipedia.org/wiki/Darwin_(operating_system)">Darwin</a>-based operating systems such as OS X and iOS. On OS X, the Wireshark installer modifies the system so that the user has sufficient privilege to capture packets; Apple's App Store restrictions prevent Wireshark from being able to do that on iOS.</p><p>In theory, a version of Wireshark for <em>jailbroken</em> iPads and iPhones and iPod touches (and perhaps jailbroken second-generation Apple TVs) could be created, but nobody's done so yet.</p><p>Unless Apple adds the ability for "approved" applications to capture network traffic on machines running iOS, only jailbroken iOS machines will be able to run applications that capture network traffic.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Apr '12, 22:48</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-10012" class="comments-container"></div><div id="comment-tools-10012" class="comment-tools"></div><div class="clear"></div><div id="comment-10012-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="11635"></span>

<div id="answer-container-11635" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11635-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11635-score" class="post-score" title="current number of votes">1</div><span id="post-11635-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There's no way to do captures directly on the iPad, but you CAN view captures in mobile browsers using CloudShark (<a href="http://www.cloudshark.org"></a><a href="http://www.cloudshark.org">www.cloudshark.org</a>). There's a plug-in for Wireshark to directly upload to CloudShark as well.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jun '12, 10:26</strong></p><img src="https://secure.gravatar.com/avatar/3296169772fdefbaaa84fed7f8fe6591?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cloudshark&#39;s gravatar image" /><p><span>cloudshark</span><br />
<span class="score" title="91 reputation points">91</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cloudshark has no accepted answers">0%</span></p></div></div><div id="comments-container-11635" class="comments-container"></div><div id="comment-tools-11635" class="comment-tools"></div><div class="clear"></div><div id="comment-11635-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="14498"></span>

<div id="answer-container-14498" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14498-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14498-score" class="post-score" title="current number of votes">1</div><span id="post-14498-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>On jailbroken devices, you CAN capture network traffic, directly on the device AND in PCAP format, by installing an application called "Pirni" from the Cydia store. It's a command-line application, so a terminal application (i.e. Prompt) is required.</p><p>For information on usage, i suggest entering "Pirni" at the command-line, followed by an &lt;enter&gt;.</p><p>After the capture, you can transfer the PCAP file to any computer (using an SSH client), running wireshark, to analyze the data. Alternatively, you can upload it directly from your device, to <a href="http://cloudshark.org">cloudshark.org</a>, BUT... in order to do this, you need to have the "safari upload enabler" installed. You can find it in the Cydia store.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Sep '12, 02:40</strong></p><img src="https://secure.gravatar.com/avatar/ee7ba4522b09cd22c2c1e6a6a1f10572?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="KevMit&#39;s gravatar image" /><p><span>KevMit</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="KevMit has no accepted answers">0%</span></p></div></div><div id="comments-container-14498" class="comments-container"><span id="14510"></span><div id="comment-14510" class="comment"><div id="post-14510-score" class="comment-score">1</div><div class="comment-text"><p>As of iOS 5 one can also use a 'Remote Virtual Interface' for packet tracing.</p><blockquote><p><code>http://developer.apple.com/library/mac/#qa/qa1176/_index.html</code></p></blockquote></div><div id="comment-14510-info" class="comment-info"><span class="comment-age">(25 Sep '12, 06:41)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-14498" class="comment-tools"></div><div class="clear"></div><div id="comment-14498-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="37321"></span>

<div id="answer-container-37321" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37321-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37321-score" class="post-score" title="current number of votes">0</div><span id="post-37321-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You run Wireshark on your Wifi access point when creating the network on your PC and connect your iOS device to that. Like described here for Linux: <a href="http://www.leaseweblabs.com/2014/10/monitor-smartphone-ubuntu-hostapd/">http://www.leaseweblabs.com/2014/10/monitor-smartphone-ubuntu-hostapd/</a> or here for Windows: <a href="http://lifehacker.com/5369381/turn-your-windows-7-pc-into-a-wireless-hotspot">http://lifehacker.com/5369381/turn-your-windows-7-pc-into-a-wireless-hotspot</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Oct '14, 14:50</strong></p><img src="https://secure.gravatar.com/avatar/dfab01f67ca4d6dd3b3a739f9770f12f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mevdschee&#39;s gravatar image" /><p><span>mevdschee</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mevdschee has no accepted answers">0%</span></p></div></div><div id="comments-container-37321" class="comments-container"></div><div id="comment-tools-37321" class="comment-tools"></div><div class="clear"></div><div id="comment-37321-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

