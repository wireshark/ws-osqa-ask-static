+++
type = "question"
title = "How SMS length is calculated when TP-DCS = 16?"
description = '''Hi All, I have a SMS packet in which TP_User_data_Length = 255 &amp;amp; TP-DCS=16 (0001 0000). Wireshark decodes it without any error. If I look into the hex dump only 84 byte of user data is there.  It&#x27;s an uncompressed message &amp;amp; not fragmented. How SMS length is calculated? I have a pcap file don...'''
date = "2017-06-13T05:10:00Z"
lastmod = "2017-06-19T13:45:00Z"
weight = 61981
keywords = [ "gsm-sms", "length" ]
aliases = [ "/questions/61981" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How SMS length is calculated when TP-DCS = 16?](/questions/61981/how-sms-length-is-calculated-when-tp-dcs-16)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61981-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61981-score" class="post-score" title="current number of votes">0</div><span id="post-61981-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>I have a SMS packet in which TP_User_data_Length = 255 &amp; TP-DCS=16 (0001 0000). Wireshark decodes it without any error. If I look into the hex dump only 84 byte of user data is there.</p><p>It's an uncompressed message &amp; not fragmented.</p><p>How SMS length is calculated? I have a pcap file don't know how to attach it?</p><p>Thanks in advance for any help.</p><p>Prithvi</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gsm-sms" rel="tag" title="see questions tagged &#39;gsm-sms&#39;">gsm-sms</span> <span class="post-tag tag-link-length" rel="tag" title="see questions tagged &#39;length&#39;">length</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jun '17, 05:10</strong></p><img src="https://secure.gravatar.com/avatar/f80796612a9bd2e5c17778ae0a41d8ba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="prithvi&#39;s gravatar image" /><p><span>prithvi</span><br />
<span class="score" title="6 reputation points">6</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="prithvi has no accepted answers">0%</span></p></div></div><div id="comments-container-61981" class="comments-container"><span id="62139"></span><div id="comment-62139" class="comment"><div id="post-62139-score" class="comment-score"></div><div class="comment-text"><p>The engine of this site doesn't allow to directly attach a capture file. You have to place it elsewhere (at Cloudshark or at any file sharing service) and edit your question with a link to it.</p></div><div id="comment-62139-info" class="comment-info"><span class="comment-age">(19 Jun '17, 13:45)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-61981" class="comment-tools"></div><div class="clear"></div><div id="comment-61981-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

