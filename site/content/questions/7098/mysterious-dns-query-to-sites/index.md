+++
type = "question"
title = "Mysterious DNS query to sites?"
description = '''I&#x27;m not sure whats going on, I was just using wireshark on my home network and I know everyones assigned local IP Address. I just wanted to see what sites everyone on my network was on, so I applied a DNS filter, and notice that My IP(192.168.1.100) was pulling in all this responses from sites I don...'''
date = "2011-10-27T08:22:00Z"
lastmod = "2011-10-27T12:52:00Z"
weight = 7098
keywords = [ "dns" ]
aliases = [ "/questions/7098" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [Mysterious DNS query to sites?](/questions/7098/mysterious-dns-query-to-sites)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7098-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7098-score" class="post-score" title="current number of votes">0</div><span id="post-7098-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm not sure whats going on, I was just using wireshark on my home network and I know everyones assigned local IP Address. I just wanted to see what sites everyone on my network was on, so I applied a DNS filter, and notice that My IP(192.168.1.100) was pulling in all this responses from sites I don't even get on. For an example: www.paypal.com, staysafeonline.org www.verizon.com.edgekey.net e2546.g.akamaiedge.net, www.softlayer.com,www.qualys.com, twitter.com, facebook,.... and I can go on, but my point is I wasn't going on these sites and some of them, I never even heard of. This was all going on with one single session. (By the way I know this is my IP for a fact, I login to my Linksys router daily).</p><p>Is this normal?</p><p>What and why is this happening?</p><p>should I worry?</p><p>What can I do to fix it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dns" rel="tag" title="see questions tagged &#39;dns&#39;">dns</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Oct '11, 08:22</strong></p><img src="https://secure.gravatar.com/avatar/3c561b8084c5071bd4ee0677446aba48?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dreaddrew&#39;s gravatar image" /><p><span>dreaddrew</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dreaddrew has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Oct '11, 08:44</strong> </span></p></div></div><div id="comments-container-7098" class="comments-container"></div><div id="comment-tools-7098" class="comment-tools"></div><div class="clear"></div><div id="comment-7098-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="7099"></span>

<div id="answer-container-7099" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7099-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7099-score" class="post-score" title="current number of votes">3</div><span id="post-7099-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="dreaddrew has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Everything looking fine - might be just ads or whatever your favorite Instant Messenger / Social Network / P*** Site is getting to show you somewhere on the page.</p><p>No honestly, are you sure you never visit a site ? It can be a reference from any website pulling a picture, an advertisement or whatever off from another domains server - thats why u might see lots of sites you "never visit"</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Oct '11, 08:33</strong></p><img src="https://secure.gravatar.com/avatar/36b41326bff63eb5ad73a0436914e05c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Landi&#39;s gravatar image" /><p><span>Landi</span><br />
<span class="score" title="2269 reputation points"><span>2.3k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="14 badges"><span class="silver">●</span><span class="badgecount">14</span></span><span title="42 badges"><span class="bronze">●</span><span class="badgecount">42</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Landi has 28 accepted answers">28%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Oct '11, 08:35</strong> </span></p></div></div><div id="comments-container-7099" class="comments-container"><span id="7100"></span><div id="comment-7100" class="comment"><div id="post-7100-score" class="comment-score"></div><div class="comment-text"><p>Even if the source is coming from my IP, and the destinations is these sites? And why is my ip sending these dns out with out my knowledge? (I'm running a Linux OS and the distro is Ubuntu)</p></div><div id="comment-7100-info" class="comment-info"><span class="comment-age">(27 Oct '11, 08:40)</span> <span class="comment-user userinfo">dreaddrew</span></div></div><span id="7104"></span><div id="comment-7104" class="comment"><div id="post-7104-score" class="comment-score"></div><div class="comment-text"><p>The <code>akamai</code> domain is an advertising endpoint --they serve ads on websites that you visit, so your computer will have to lookup that domain when a webpage contains an ad from that network. Likewise, any website with a <code>Like</code> button for Facebook will lookup Facebook (whether or not you use that service).</p><p>This behavior is perfectly normal. If you can eliminate <strong>all</strong> processes accessing the Internet (web browsers, desktop widgets, <em>Wireshark</em>), and <em>still</em> see these lookups, that could be an issue. As it stands, it appears you have nothing to worry about.</p></div><div id="comment-7104-info" class="comment-info"><span class="comment-age">(27 Oct '11, 08:49)</span> <span class="comment-user userinfo">multipleinte...</span></div></div><span id="7105"></span><div id="comment-7105" class="comment"><div id="post-7105-score" class="comment-score"></div><div class="comment-text"><p>I was thinking it was probably an Ad too, but the page I got on had no adds on it, and I never use or went on www.paypal.com or ever seen a ad for it. And this came on only on one session that I stopped after 6 minutes.</p></div><div id="comment-7105-info" class="comment-info"><span class="comment-age">(27 Oct '11, 08:51)</span> <span class="comment-user userinfo">dreaddrew</span></div></div><span id="7110"></span><div id="comment-7110" class="comment"><div id="post-7110-score" class="comment-score"></div><div class="comment-text"><p>Are you certain there were no ads --even ads that were not visible in your browser (this is the behavior of some popular ad-blocking software: the ads are still requested, but thrown away before being rendered on the page)? The request to <code>www.paypal.com</code> could have come from a <code>Donate</code> button or similar provided on a page you requested. As I said above, unless this happens and you are <strong>100% certain</strong> that your computer should not be requesting any URLs except for the ones you want (which is practically impossible and cripples your Internet experience), this is perfectly normal behavior.</p></div><div id="comment-7110-info" class="comment-info"><span class="comment-age">(27 Oct '11, 12:52)</span> <span class="comment-user userinfo">multipleinte...</span></div></div></div><div id="comment-tools-7099" class="comment-tools"></div><div class="clear"></div><div id="comment-7099-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="7101"></span>

<div id="answer-container-7101" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7101-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7101-score" class="post-score" title="current number of votes">0</div><span id="post-7101-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Are you capturing with the capture option "Enable network name resolution" turned on? If so, and your capture is picking up packets from other machines on your local network to those remote machines, then Wireshark will be causing your machine to issue DNS requests for all the IP's that it sees in the capture in an attempt to resolve them to names.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Oct '11, 08:40</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Oct '11, 08:41</strong> </span></p></div></div><div id="comments-container-7101" class="comments-container"><span id="7103"></span><div id="comment-7103" class="comment"><div id="post-7103-score" class="comment-score"></div><div class="comment-text"><p>No, I have that option turned off.</p></div><div id="comment-7103-info" class="comment-info"><span class="comment-age">(27 Oct '11, 08:43)</span> <span class="comment-user userinfo">dreaddrew</span></div></div></div><div id="comment-tools-7101" class="comment-tools"></div><div class="clear"></div><div id="comment-7101-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

