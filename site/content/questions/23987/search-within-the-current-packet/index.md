+++
type = "question"
title = "Search Within the Current Packet"
description = '''How do I search for a string or byte sequence within the scope of the current packet? '''
date = "2013-08-23T15:56:00Z"
lastmod = "2013-08-24T01:11:00Z"
weight = 23987
keywords = [ "scope", "search", "find", "packet" ]
aliases = [ "/questions/23987" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Search Within the Current Packet](/questions/23987/search-within-the-current-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23987-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23987-score" class="post-score" title="current number of votes">0</div><span id="post-23987-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I search for a string or byte sequence within the scope of the current packet?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-scope" rel="tag" title="see questions tagged &#39;scope&#39;">scope</span> <span class="post-tag tag-link-search" rel="tag" title="see questions tagged &#39;search&#39;">search</span> <span class="post-tag tag-link-find" rel="tag" title="see questions tagged &#39;find&#39;">find</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Aug '13, 15:56</strong></p><img src="https://secure.gravatar.com/avatar/807f9d3dd7b288a186f00400adfa0ef7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="araybold&#39;s gravatar image" /><p><span>araybold</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="araybold has no accepted answers">0%</span></p></div></div><div id="comments-container-23987" class="comments-container"></div><div id="comment-tools-23987" class="comment-tools"></div><div class="clear"></div><div id="comment-23987-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="23996"></span>

<div id="answer-container-23996" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23996-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23996-score" class="post-score" title="current number of votes">0</div><span id="post-23996-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="araybold has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>AFAIK you can't. The search will highlight the search string in the hex pane for the first match in the packet, but a new search will look for the next packet in which the search string appears, not the next occurrence in the same packet.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Aug '13, 01:11</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-23996" class="comments-container"></div><div id="comment-tools-23996" class="comment-tools"></div><div class="clear"></div><div id="comment-23996-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

