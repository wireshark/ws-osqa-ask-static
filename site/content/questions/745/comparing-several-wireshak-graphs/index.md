+++
type = "question"
title = "Comparing several wireshak graphs"
description = '''Hi What is the best way to view the difference between several captured graphs? Wireshark only display one graph at a time – how can I compare? Thanks'''
date = "2010-10-30T08:38:00Z"
lastmod = "2010-11-01T15:34:00Z"
weight = 745
keywords = [ "comparing", "graphs", "captures" ]
aliases = [ "/questions/745" ]
osqa_answers = 4
osqa_accepted = false
+++

<div class="headNormal">

# [Comparing several wireshak graphs](/questions/745/comparing-several-wireshak-graphs)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-745-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-745-score" class="post-score" title="current number of votes">0</div><span id="post-745-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi What is the best way to view the difference between several captured graphs? Wireshark only display one graph at a time – how can I compare? Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-comparing" rel="tag" title="see questions tagged &#39;comparing&#39;">comparing</span> <span class="post-tag tag-link-graphs" rel="tag" title="see questions tagged &#39;graphs&#39;">graphs</span> <span class="post-tag tag-link-captures" rel="tag" title="see questions tagged &#39;captures&#39;">captures</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Oct '10, 08:38</strong></p><img src="https://secure.gravatar.com/avatar/1b0c65360c0d10e57c0d059429e4efa8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jonesKon&#39;s gravatar image" /><p><span>jonesKon</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jonesKon has no accepted answers">0%</span></p></div></div><div id="comments-container-745" class="comments-container"></div><div id="comment-tools-745" class="comment-tools"></div><div class="clear"></div><div id="comment-745-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

4 Answers:

</div>

</div>

<span id="770"></span>

<div id="answer-container-770" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-770-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-770-score" class="post-score" title="current number of votes">1</div><span id="post-770-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Way back when, I used to use tcptrace and jplot. It took a little work to get it going but it fit my needs at the time. I also played around with "http://www.packetbone.com/Applications/Excel.htm" a while back. It wasn't too bad, but I found that Wireshark gave me most of what I need.<br />
</p><p>When built in (graphing) capabilities are exceeded, I usually turn to</p><p>1) Cace's Pilot. AWESOME AWESOME tool!<br />
2) Opnet's ACE or IT-Guru. Expensive, but quite nice if you know how to use it properly. The danger is that it spits out official answers and it can lull the user into thinking "I got the answer!"<br />
</p><p>Good luck.</p><p>Hansang</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Nov '10, 15:34</strong></p><img src="https://secure.gravatar.com/avatar/63805f079ac429902641cad9d7cd69e8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hansangb&#39;s gravatar image" /><p><span>hansangb</span><br />
<span class="score" title="791 reputation points">791</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="19 badges"><span class="bronze">●</span><span class="badgecount">19</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hansangb has 7 accepted answers">12%</span> </br></br></p></div></div><div id="comments-container-770" class="comments-container"></div><div id="comment-tools-770" class="comment-tools"></div><div class="clear"></div><div id="comment-770-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="746"></span>

<div id="answer-container-746" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-746-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-746-score" class="post-score" title="current number of votes">0</div><span id="post-746-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Which graphs do you want to compare?</p><p>You can (at least on a Windows host) open multiple graphs simultaneously - e.g., open the IO graph and then toggle back to Wireshark to open the TCP Time-Sequence graph. By toggling you can place them side-by-side.</p><p>If you want to compare IO or TCP Time-Sequence graphs of two different trace files, open two instances of Wireshark and graph in each instance.</p><p>If, however, you want to compare two traffic elements, such as all HTTP traffic to FTP traffic, you can add graph lines to a single IO or advanced IO graph.</p><p>If you are interested in the TCP Time-Sequence graph of two conversations in a single trace file, you'll need to separate them into separate trace files - conversation filter them each out into separate files and open in separate Wireshark instances.<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Oct '10, 10:54</strong></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchappell has 6 accepted answers">8%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Oct '10, 10:55</strong> </span></p></div></div><div id="comments-container-746" class="comments-container"></div><div id="comment-tools-746" class="comment-tools"></div><div class="clear"></div><div id="comment-746-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="757"></span>

<div id="answer-container-757" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-757-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-757-score" class="post-score" title="current number of votes">0</div><span id="post-757-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>hi You can have a look at this answer:</p><p><a href="http://www.mail-archive.com/wireshark-dev@wireshark.org/msg12342.html">http://www.mail-archive.com/<span class="__cf_email__" data-cfemail="02756b7067716a6370692f66677442756b7067716a6370692c6d7065">[email protected]</span>/msg12342.html</a></p><p>Or use the following application which does this also:</p><p><a href="http://www.softpedia.com/get/Network-Tools/Network-Testing/CapsGraph.shtml">http://www.softpedia.com/get/Network-Tools/Network-Testing/CapsGraph.shtml</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Oct '10, 03:50</strong></p><img src="https://secure.gravatar.com/avatar/6b587f5fd952e8151edcad9d1256ce8c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ron&#39;s gravatar image" /><p><span>ron</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ron has no accepted answers">0%</span></p></div></div><div id="comments-container-757" class="comments-container"></div><div id="comment-tools-757" class="comment-tools"></div><div class="clear"></div><div id="comment-757-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="767"></span>

<div id="answer-container-767" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-767-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-767-score" class="post-score" title="current number of votes">0</div><span id="post-767-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wouldn’t two monitors and two instances of wireshark be the best way to go?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Nov '10, 13:12</strong></p><img src="https://secure.gravatar.com/avatar/bcfdf26904f3a8a9fb69c7ca0dc5e7b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="net_tech&#39;s gravatar image" /><p><span>net_tech</span><br />
<span class="score" title="116 reputation points">116</span><span title="30 badges"><span class="badge1">●</span><span class="badgecount">30</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="net_tech has 2 accepted answers">13%</span></p></div></div><div id="comments-container-767" class="comments-container"></div><div id="comment-tools-767" class="comment-tools"></div><div class="clear"></div><div id="comment-767-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

