+++
type = "question"
title = "what&#x27;s the difference between entries of the downloads?"
description = '''Hi, In the nightly builds at http://www.wireshark.org/download/automated, say for 64 bit Mac, you have these different entries: Wireshark 1.11.3-1927-g2a92943 Intel 64.dmg 12-Mar-2014 10:18 35M Wireshark 1.11.3-1928-g1ab950c Intel 64.dmg 12-Mar-2014 12:41 35M Wireshark 1.11.3-1929-g7e7bf82 Intel 64....'''
date = "2014-03-12T08:33:00Z"
lastmod = "2014-03-12T10:19:00Z"
weight = 30733
keywords = [ "nightly", "download", "difference", "build", "entries" ]
aliases = [ "/questions/30733" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [what's the difference between entries of the downloads?](/questions/30733/whats-the-difference-between-entries-of-the-downloads)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30733-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30733-score" class="post-score" title="current number of votes">0</div><span id="post-30733-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>In the nightly builds at <a href="http://www.wireshark.org/download/automated,">http://www.wireshark.org/download/automated,</a> say for 64 bit Mac, you have these different entries:</p><p>Wireshark 1.11.3-1927-g2a92943 Intel 64.dmg 12-Mar-2014 10:18 35M</p><p>Wireshark 1.11.3-1928-g1ab950c Intel 64.dmg 12-Mar-2014 12:41 35M</p><p>Wireshark 1.11.3-1929-g7e7bf82 Intel 64.dmg 12-Mar-2014 13:41 35M<br />
</p><p>Wireshark 1.11.3-1930-gd89195d Intel 64.dmg 12-Mar-2014 14:50 35M<br />
</p><p>What's the difference among them? Does it matter which one we choose, or should we simply choose the latest one?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nightly" rel="tag" title="see questions tagged &#39;nightly&#39;">nightly</span> <span class="post-tag tag-link-download" rel="tag" title="see questions tagged &#39;download&#39;">download</span> <span class="post-tag tag-link-difference" rel="tag" title="see questions tagged &#39;difference&#39;">difference</span> <span class="post-tag tag-link-build" rel="tag" title="see questions tagged &#39;build&#39;">build</span> <span class="post-tag tag-link-entries" rel="tag" title="see questions tagged &#39;entries&#39;">entries</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Mar '14, 08:33</strong></p><img src="https://secure.gravatar.com/avatar/b18cada3e3589f311e24f5ffbd1737bc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="YXI&#39;s gravatar image" /><p><span>YXI</span><br />
<span class="score" title="21 reputation points">21</span><span title="18 badges"><span class="badge1">●</span><span class="badgecount">18</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="23 badges"><span class="bronze">●</span><span class="badgecount">23</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="YXI has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-30733" class="comments-container"></div><div id="comment-tools-30733" class="comment-tools"></div><div class="clear"></div><div id="comment-30733-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="30734"></span>

<div id="answer-container-30734" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30734-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30734-score" class="post-score" title="current number of votes">1</div><span id="post-30734-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I think whenever a developer checks in a code change the buildbots automatically build a new binary. So I usually chose the latest timestamp, which is the latest revision.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Mar '14, 08:42</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-30734" class="comments-container"><span id="30737"></span><div id="comment-30737" class="comment"><div id="post-30737-score" class="comment-score"></div><div class="comment-text"><p>There is an ongoing discussion on the <a href="http://www.wireshark.org/lists/wireshark-dev/201403/msg00077.html">dev list</a> about adding meaning to the automated build subversion numbers after the changeover to git to allow users to determine if any particular automated build includes a specified bug fix. This was simple in the svn era (as was much else) as a bug fix was committed as r12345, so any build with a revision greater than 12345 had the fix.</p><p>This hasn't (yet) been sorted out for git though.</p></div><div id="comment-30737-info" class="comment-info"><span class="comment-age">(12 Mar '14, 09:52)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-30734" class="comment-tools"></div><div class="clear"></div><div id="comment-30734-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="30738"></span>

<div id="answer-container-30738" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30738-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30738-score" class="post-score" title="current number of votes">0</div><span id="post-30738-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Apparently the 4 digits after the version number - i.e., the "<code>1930</code>" in "<code>Wireshark 1.11.3-1930-gd89195d Intel 64.dmg</code>" - represents the number of commits (ie, changes) for that version. So the higher that number is, within a version, the newer it is. (of course the date would tell you that too on that downloads page)</p><p>Note though that those 4 digits are only meaningful within the same version. "<code>1930</code>" in 1.11.3 is not the same as "<code>1930</code>" in 1.10.7 or 1.8.14.</p><p>It's like a build-version-number you sometimes see in other apps.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Mar '14, 10:19</strong></p><img src="https://secure.gravatar.com/avatar/d02f20c18a7742ec73a666f1974bf6dc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hadriel&#39;s gravatar image" /><p><span>Hadriel</span><br />
<span class="score" title="2652 reputation points"><span>2.7k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="39 badges"><span class="bronze">●</span><span class="badgecount">39</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hadriel has 30 accepted answers">18%</span></p></div></div><div id="comments-container-30738" class="comments-container"></div><div id="comment-tools-30738" class="comment-tools"></div><div class="clear"></div><div id="comment-30738-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

