+++
type = "question"
title = "Wirehark doesn&#x27;t start on OSX"
description = '''I have Wireshark as an application in my Applications folder, but when I launched it, I get a &quot;Choose Application&quot; dialog window that lists all of the other installed applications I should consider in order to open Wireshark.app /Applications/Wireshark.app/contents is the only directory and while I ...'''
date = "2013-10-02T09:10:00Z"
lastmod = "2013-10-02T11:18:00Z"
weight = 25533
keywords = [ "osx", "gui", "wireshark" ]
aliases = [ "/questions/25533" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wirehark doesn't start on OSX](/questions/25533/wirehark-doesnt-start-on-osx)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25533-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25533-score" class="post-score" title="current number of votes">0</div><span id="post-25533-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have Wireshark as an application in my Applications folder, but when I launched it, I get a "Choose Application" dialog window that lists all of the other installed applications I should consider in order to open Wireshark.app</p><p>/Applications/Wireshark.app/contents is the only directory and while I am expecting a GUI interface, I am now wondering if I only have the command-line version installed.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-gui" rel="tag" title="see questions tagged &#39;gui&#39;">gui</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Oct '13, 09:10</strong></p><img src="https://secure.gravatar.com/avatar/7ccedb3d8dabdda0a1f450e235c23f3c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jerald&#39;s gravatar image" /><p><span>Jerald</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jerald has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Oct '13, 11:16</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-25533" class="comments-container"></div><div id="comment-tools-25533" class="comment-tools"></div><div class="clear"></div><div id="comment-25533-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="25543"></span>

<div id="answer-container-25543" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25543-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25543-score" class="post-score" title="current number of votes">0</div><span id="post-25543-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There's no option to install <em>only</em> the command-line version on OS X with the official wireshark.org dmg; there's an option to install the command-line tools, but the GUI app is installed regardless of whether the command-line tools are installed.</p><p>You <em>do</em> need to install the bundled X11 server if this is Lion or earlier or install Xquartz if this is Mountain Lion or later, and then select the X11 server from that list.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Oct '13, 11:18</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Oct '13, 15:07</strong> </span></p></div></div><div id="comments-container-25543" class="comments-container"></div><div id="comment-tools-25543" class="comment-tools"></div><div class="clear"></div><div id="comment-25543-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

