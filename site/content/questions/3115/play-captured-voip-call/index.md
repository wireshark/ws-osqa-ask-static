+++
type = "question"
title = "Play captured VoIP call"
description = '''Hi, When I capture a voip call and go to &quot;Telephony -&amp;gt; VoIP Calls -&amp;gt; Player -&amp;gt; Decode&quot; and select both directions, only the one selected first can be played.. either one. However, if I check both streams to play the complete call - and only one direction can be heard, the timeline follows b...'''
date = "2011-03-25T04:57:00Z"
lastmod = "2011-03-27T22:13:00Z"
weight = 3115
keywords = [ "directions", "capture", "voip", "both" ]
aliases = [ "/questions/3115" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Play captured VoIP call](/questions/3115/play-captured-voip-call)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3115-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3115-score" class="post-score" title="current number of votes">0</div><span id="post-3115-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>When I capture a voip call and go to "Telephony -&gt; VoIP Calls -&gt; Player -&gt; Decode" and select both directions, only the one selected first can be played.. either one.</p><p>However, if I check both streams to play the complete call - and only one direction can be heard, the timeline follows both directions.</p><p>Weird thing is; if I "Save payload", select "both" directions and format .au, then I can hear both directions when I play that saved .au file.</p><p>Any solution? I am using WinXP SP3, latest Wireshark v1.4.4.</p><p>Thank you :-)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-directions" rel="tag" title="see questions tagged &#39;directions&#39;">directions</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span> <span class="post-tag tag-link-both" rel="tag" title="see questions tagged &#39;both&#39;">both</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Mar '11, 04:57</strong></p><img src="https://secure.gravatar.com/avatar/ffca73b75304384bff86259d4ab14f60?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ronnij&#39;s gravatar image" /><p><span>ronnij</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ronnij has no accepted answers">0%</span></p></div></div><div id="comments-container-3115" class="comments-container"><span id="3169"></span><div id="comment-3169" class="comment"><div id="post-3169-score" class="comment-score"></div><div class="comment-text"><p>No ideas? :-/</p></div><div id="comment-3169-info" class="comment-info"><span class="comment-age">(27 Mar '11, 22:13)</span> <span class="comment-user userinfo">ronnij</span></div></div></div><div id="comment-tools-3115" class="comment-tools"></div><div class="clear"></div><div id="comment-3115-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

