+++
type = "question"
title = "A problem about  libGeoIP.so.1 on sol10-sparc after installing wireshark"
description = '''When I try to run wireshark-1.6.1-sol10-sparc-local, I met a problem that: &quot;ld.so.1: wireshark: fatal: libGeoIP.so.1: open failed: No such file or directory&quot; The command ldd wireshark showed (file not found) where will I get the package? I didn&#x27;t find it on sunfreeware'''
date = "2011-08-25T01:07:00Z"
lastmod = "2011-08-25T01:26:00Z"
weight = 5862
keywords = [ "sparc", "libgeoip.so.1" ]
aliases = [ "/questions/5862" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [A problem about libGeoIP.so.1 on sol10-sparc after installing wireshark](/questions/5862/a-problem-about-libgeoipso1-on-sol10-sparc-after-installing-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5862-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5862-score" class="post-score" title="current number of votes">0</div><span id="post-5862-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I try to run wireshark-1.6.1-sol10-sparc-local, I met a problem that:</p><p>"ld.so.1: wireshark: fatal: libGeoIP.so.1: open failed: No such file or directory"</p><p>The command <strong>ldd wireshark</strong> showed (file not found)</p><p>where will I get the package? I didn't find it on sunfreeware</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sparc" rel="tag" title="see questions tagged &#39;sparc&#39;">sparc</span> <span class="post-tag tag-link-libgeoip.so.1" rel="tag" title="see questions tagged &#39;libgeoip.so.1&#39;">libgeoip.so.1</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Aug '11, 01:07</strong></p><img src="https://secure.gravatar.com/avatar/1660c11c44b3cd1292a80672a7146993?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="firobaccano&#39;s gravatar image" /><p><span>firobaccano</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="firobaccano has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Aug '11, 01:17</strong> </span></p></div></div><div id="comments-container-5862" class="comments-container"></div><div id="comment-tools-5862" class="comment-tools"></div><div class="clear"></div><div id="comment-5862-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5863"></span>

<div id="answer-container-5863" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5863-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5863-score" class="post-score" title="current number of votes">0</div><span id="post-5863-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I have found it on sunfreeware</p><p>geoip-1.4.5-sol10-sparc-local.gz</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Aug '11, 01:26</strong></p><img src="https://secure.gravatar.com/avatar/1660c11c44b3cd1292a80672a7146993?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="firobaccano&#39;s gravatar image" /><p><span>firobaccano</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="firobaccano has no accepted answers">0%</span></p></div></div><div id="comments-container-5863" class="comments-container"></div><div id="comment-tools-5863" class="comment-tools"></div><div class="clear"></div><div id="comment-5863-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

