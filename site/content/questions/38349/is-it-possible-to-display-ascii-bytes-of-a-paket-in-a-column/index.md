+++
type = "question"
title = "Is it possible to display ASCII bytes of a paket in a column?"
description = '''Hello, it&#x27;s difficult to describe but i drawed a picture to specify what i need to know. Is it possible to move the data of the packet as ASCII or printable Text in a column?  Thank you in advance. Kind regards, Norman'''
date = "2014-12-05T00:58:00Z"
lastmod = "2014-12-05T00:58:00Z"
weight = 38349
keywords = [ "column", "raw", "setting", "data", "whireshark" ]
aliases = [ "/questions/38349" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Is it possible to display ASCII bytes of a paket in a column?](/questions/38349/is-it-possible-to-display-ascii-bytes-of-a-paket-in-a-column)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38349-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38349-score" class="post-score" title="current number of votes">0</div><span id="post-38349-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>it's difficult to describe but i drawed a picture to specify what i need to know. Is it possible to move the data of the packet as ASCII or printable Text in a column?</p><p><img src="http://i62.tinypic.com/21eb8le.png" alt="alt text" /></p><p>Thank you in advance.</p><p>Kind regards, Norman</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-column" rel="tag" title="see questions tagged &#39;column&#39;">column</span> <span class="post-tag tag-link-raw" rel="tag" title="see questions tagged &#39;raw&#39;">raw</span> <span class="post-tag tag-link-setting" rel="tag" title="see questions tagged &#39;setting&#39;">setting</span> <span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span> <span class="post-tag tag-link-whireshark" rel="tag" title="see questions tagged &#39;whireshark&#39;">whireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Dec '14, 00:58</strong></p><img src="https://secure.gravatar.com/avatar/ef948e08afdb2fb4a80927879e8cc331?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Thorgas&#39;s gravatar image" /><p><span>Thorgas</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Thorgas has no accepted answers">0%</span></p></img></div></div><div id="comments-container-38349" class="comments-container"></div><div id="comment-tools-38349" class="comment-tools"></div><div class="clear"></div><div id="comment-38349-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

