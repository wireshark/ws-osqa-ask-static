+++
type = "question"
title = "addressing somebody with @? and a wireshark meta?"
description = '''Is there a meta wireshark? like meta.stackoverflow or meta.superuser? And how do you address somebody with @ so they get a notification? I just went to https://ask.wireshark.org/questions/27662/how-to-understand-out-of-order-tcp-segments And I tried adding a comment addressing somebody with @ and I ...'''
date = "2015-04-07T08:58:00Z"
lastmod = "2015-04-07T20:28:00Z"
weight = 41253
keywords = [ "discussion" ]
aliases = [ "/questions/41253" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [addressing somebody with @? and a wireshark meta?](/questions/41253/addressing-somebody-with-and-a-wireshark-meta)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41253-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41253-score" class="post-score" title="current number of votes">0</div><span id="post-41253-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a meta wireshark? like meta.stackoverflow or meta.superuser?</p><p>And how do you address somebody with @ so they get a notification?</p><p>I just went to <a href="https://ask.wireshark.org/questions/27662/how-to-understand-out-of-order-tcp-segments">https://ask.wireshark.org/questions/27662/how-to-understand-out-of-order-tcp-segments</a></p><p>And I tried adding a comment addressing somebody with @ and I didn't get a popup with autocomplete as in stackoverflow or superuser so I guess it doesn't work?<br />
</p><p>Is there any way to address somebody so they get a notification?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-discussion" rel="tag" title="see questions tagged &#39;discussion&#39;">discussion</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Apr '15, 08:58</strong></p><img src="https://secure.gravatar.com/avatar/4ca1e8011745413142c9056875b8d39c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="barlop&#39;s gravatar image" /><p><span>barlop</span><br />
<span class="score" title="16 reputation points">16</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="barlop has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-41253" class="comments-container"></div><div id="comment-tools-41253" class="comment-tools"></div><div class="clear"></div><div id="comment-41253-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="41255"></span>

<div id="answer-container-41255" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41255-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41255-score" class="post-score" title="current number of votes">1</div><span id="post-41255-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="barlop has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The @ notation should work with a valid username, e.g. <span><span>@grahamb</span></span>.</p><p>I've never seen autocomplete for usernames on this site, so I would guess that it doesn't exist.</p><p>Finally, a user will only get a notification of user of their name with @ if they have enabled such notifications in their profile, and according to the settings, only if the @name notation is used in a reply to one of their comments.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Apr '15, 09:42</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-41255" class="comments-container"><span id="41258"></span><div id="comment-41258" class="comment"><div id="post-41258-score" class="comment-score"></div><div class="comment-text"><p>I notice that once a comment is posted it comes up as a link and one sees whether it works / that it worked. so e.g. for joe blogs, hovering over his name showed his profile as joe-blogs doing <span></span><span>@joe</span> went to the wrong profile but <span></span><span>@joe</span> bloggs then posting the comment, and the profile came up as joe-blogs which is the right one</p></div><div id="comment-41258-info" class="comment-info"><span class="comment-age">(07 Apr '15, 10:10)</span> <span class="comment-user userinfo">barlop</span></div></div></div><div id="comment-tools-41255" class="comment-tools"></div><div class="clear"></div><div id="comment-41255-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="41268"></span>

<div id="answer-container-41268" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41268-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41268-score" class="post-score" title="current number of votes">0</div><span id="post-41268-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Is there a meta wireshark? like meta.stackoverflow or meta.superuser?</p></blockquote><p>No. There's only ask.wireshark.org, not a family of sites like the StackExchange sites and their corresponding "meta." sites.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Apr '15, 20:28</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-41268" class="comments-container"></div><div id="comment-tools-41268" class="comment-tools"></div><div class="clear"></div><div id="comment-41268-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

