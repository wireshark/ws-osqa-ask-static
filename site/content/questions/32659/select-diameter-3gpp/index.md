+++
type = "question"
title = "Select Diameter 3GPP"
description = '''How do I select Diameter 3GPP? trying to troubleshoot GX captures I can see in the diameter dictionary it has a reference to the application but I do not see the AVPs for Gx. I added them manually to it but is not decoding. I can not change the version of wireshark to a newer one I am using one for ...'''
date = "2014-05-09T02:00:00Z"
lastmod = "2014-05-09T03:36:00Z"
weight = 32659
keywords = [ "diameter", "3gpp", "gx" ]
aliases = [ "/questions/32659" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Select Diameter 3GPP](/questions/32659/select-diameter-3gpp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32659-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32659-score" class="post-score" title="current number of votes">0</div><span id="post-32659-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I select Diameter 3GPP? trying to troubleshoot GX captures I can see in the diameter dictionary it has a reference to the application but I do not see the AVPs for Gx. I added them manually to it but is not decoding. I can not change the version of wireshark to a newer one I am using one for Citrix NS. Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-diameter" rel="tag" title="see questions tagged &#39;diameter&#39;">diameter</span> <span class="post-tag tag-link-3gpp" rel="tag" title="see questions tagged &#39;3gpp&#39;">3gpp</span> <span class="post-tag tag-link-gx" rel="tag" title="see questions tagged &#39;gx&#39;">gx</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 May '14, 02:00</strong></p><img src="https://secure.gravatar.com/avatar/983fcbd298b94608ad7cd57d14b40cb3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="manolofuentes&#39;s gravatar image" /><p><span>manolofuentes</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="manolofuentes has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 May '14, 11:16</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-32659" class="comments-container"><span id="32661"></span><div id="comment-32661" class="comment"><div id="post-32661-score" class="comment-score"></div><div class="comment-text"><p>What version are you using? It should work to add them manually if your version supports AVPs in .xml. I think there may be problems if the syntax of the .xml is wrong. Can you give the AVP number if one undecoded AVP? Wireshark dioes not care about the application id. Adding a new .xml file requires a few changes to dictionary.xml but editing one of the existing files should work.</p></div><div id="comment-32661-info" class="comment-info"><span class="comment-age">(09 May '14, 03:36)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-32659" class="comment-tools"></div><div class="clear"></div><div id="comment-32659-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

