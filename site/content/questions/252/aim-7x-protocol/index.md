+++
type = "question"
title = "AIM 7.x protocol"
description = '''Are there any plans afoot to support the new AIM 7.x protocol? I have the client setup to send without SSL and I can see the traffic, but there is still some encryption there.'''
date = "2010-09-21T11:51:00Z"
lastmod = "2010-09-22T14:44:00Z"
weight = 252
keywords = [ "aim", "7.x" ]
aliases = [ "/questions/252" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [AIM 7.x protocol](/questions/252/aim-7x-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-252-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-252-score" class="post-score" title="current number of votes">0</div><span id="post-252-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Are there any plans afoot to support the new AIM 7.x protocol? I have the client setup to send without SSL and I can see the traffic, but there is still some encryption there.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-aim" rel="tag" title="see questions tagged &#39;aim&#39;">aim</span> <span class="post-tag tag-link-7.x" rel="tag" title="see questions tagged &#39;7.x&#39;">7.x</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Sep '10, 11:51</strong></p><img src="https://secure.gravatar.com/avatar/bba81dbc3a5d5b78cb191df7ee74cde0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="phil&#39;s gravatar image" /><p><span>phil</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="phil has no accepted answers">0%</span></p></div></div><div id="comments-container-252" class="comments-container"></div><div id="comment-tools-252" class="comment-tools"></div><div class="clear"></div><div id="comment-252-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="271"></span>

<div id="answer-container-271" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-271-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-271-score" class="post-score" title="current number of votes">0</div><span id="post-271-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That question is probably better put on the developers mailing list. In generall protocol support are added by people needing it. Regards Anders</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Sep '10, 13:19</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-271" class="comments-container"><span id="275"></span><div id="comment-275" class="comment"><div id="post-275-score" class="comment-score"></div><div class="comment-text"><p>Thanks for the tip. Unfortunately, they referred me to this FAQ: http://www.wireshark.org/faq.html#q1.11 Which leaves me still wondering if anyone out there is working on it...</p></div><div id="comment-275-info" class="comment-info"><span class="comment-age">(22 Sep '10, 14:21)</span> <span class="comment-user userinfo">phil</span></div></div><span id="277"></span><div id="comment-277" class="comment"><div id="post-277-score" class="comment-score"></div><div class="comment-text"><p>Well if some one was activly developing it they are hopfully on the developers list :-)</p></div><div id="comment-277-info" class="comment-info"><span class="comment-age">(22 Sep '10, 14:44)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-271" class="comment-tools"></div><div class="clear"></div><div id="comment-271-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

