+++
type = "question"
title = "statistics in Wireshark"
description = '''Hi, Is there a way to perform statistic analysis for MS IP Addresses taken from GTP layer? BR, Diana.'''
date = "2013-12-04T06:34:00Z"
lastmod = "2013-12-05T05:52:00Z"
weight = 27765
keywords = [ "statistics" ]
aliases = [ "/questions/27765" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [statistics in Wireshark](/questions/27765/statistics-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27765-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27765-score" class="post-score" title="current number of votes">0</div><span id="post-27765-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Is there a way to perform statistic analysis for MS IP Addresses taken from GTP layer?</p><p>BR, Diana.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Dec '13, 06:34</strong></p><img src="https://secure.gravatar.com/avatar/900044aef60dc6223168781e5d576bfb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dianalab9&#39;s gravatar image" /><p><span>Dianalab9</span><br />
<span class="score" title="26 reputation points">26</span><span title="16 badges"><span class="badge1">●</span><span class="badgecount">16</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="20 badges"><span class="bronze">●</span><span class="badgecount">20</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dianalab9 has no accepted answers">0%</span></p></div></div><div id="comments-container-27765" class="comments-container"><span id="27766"></span><div id="comment-27766" class="comment"><div id="post-27766-score" class="comment-score"></div><div class="comment-text"><p>I mean in the same way Wireshark does statistics for SRC DST IP</p></div><div id="comment-27766-info" class="comment-info"><span class="comment-age">(04 Dec '13, 07:11)</span> <span class="comment-user userinfo">Dianalab9</span></div></div><span id="27799"></span><div id="comment-27799" class="comment"><div id="post-27799-score" class="comment-score"></div><div class="comment-text"><p>maybe it simply cannot be done?</p></div><div id="comment-27799-info" class="comment-info"><span class="comment-age">(05 Dec '13, 02:01)</span> <span class="comment-user userinfo">Dianalab9</span></div></div><span id="27807"></span><div id="comment-27807" class="comment"><div id="post-27807-score" class="comment-score"></div><div class="comment-text"><p>what kind of '<strong>statistic</strong> analysis'?</p></div><div id="comment-27807-info" class="comment-info"><span class="comment-age">(05 Dec '13, 05:50)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="27808"></span><div id="comment-27808" class="comment"><div id="post-27808-score" class="comment-score"></div><div class="comment-text"><p>a list of all available MS IPs and the number of frames it appears in. similar to the statistics of source and dest IP with the number of frames</p></div><div id="comment-27808-info" class="comment-info"><span class="comment-age">(05 Dec '13, 05:52)</span> <span class="comment-user userinfo">Dianalab9</span></div></div></div><div id="comment-tools-27765" class="comment-tools"></div><div class="clear"></div><div id="comment-27765-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

