+++
type = "question"
title = "what exactly does playback do"
description = '''Hi I have some experience with ethereal/wireshark but Ive no experience with playback techniques. I am interested to understand if it is possible to use playback in a manner that would allow the stream to actually be viewed in either its original application or any compatible alternative. I have som...'''
date = "2017-01-14T04:29:00Z"
lastmod = "2017-01-16T03:01:00Z"
weight = 58760
keywords = [ "playback" ]
aliases = [ "/questions/58760" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [what exactly does playback do](/questions/58760/what-exactly-does-playback-do)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58760-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58760-score" class="post-score" title="current number of votes">0</div><span id="post-58760-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I have some experience with ethereal/wireshark but Ive no experience with playback techniques. I am interested to understand if it is possible to use playback in a manner that would allow the stream to actually be viewed in either its original application or any compatible alternative. I have some captured packets of a video stream that i am interested in viewing again. This would probably involve playback and review on same original machine. Initially i am interested in skype but also could the principle be applied to other formats/applications.</p><p>Thanks SRatTest</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-playback" rel="tag" title="see questions tagged &#39;playback&#39;">playback</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jan '17, 04:29</strong></p><img src="https://secure.gravatar.com/avatar/8acdab119eea511b44537b9bcb2e367d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SRatTesting&#39;s gravatar image" /><p><span>SRatTesting</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SRatTesting has no accepted answers">0%</span></p></div></div><div id="comments-container-58760" class="comments-container"></div><div id="comment-tools-58760" class="comment-tools"></div><div class="clear"></div><div id="comment-58760-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58788"></span>

<div id="answer-container-58788" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58788-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58788-score" class="post-score" title="current number of votes">0</div><span id="post-58788-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I think ive probably found the answer to this now. Which is no it will not allow play back to an application.</p><p>If my information is correct I require TCPliveplay.</p><p>The follow up question is now, where can I get this.</p><p>Thanks steve</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jan '17, 15:03</strong></p><img src="https://secure.gravatar.com/avatar/8acdab119eea511b44537b9bcb2e367d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SRatTesting&#39;s gravatar image" /><p><span>SRatTesting</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SRatTesting has no accepted answers">0%</span></p></div></div><div id="comments-container-58788" class="comments-container"><span id="58803"></span><div id="comment-58803" class="comment"><div id="post-58803-score" class="comment-score"></div><div class="comment-text"><p>Use <a href="https://wiki.wireshark.org/Tools">this</a> as your reference to a nice collection of tools related to packet captures.</p></div><div id="comment-58803-info" class="comment-info"><span class="comment-age">(16 Jan '17, 03:01)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-58788" class="comment-tools"></div><div class="clear"></div><div id="comment-58788-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

