+++
type = "question"
title = "How to turn off RPCAP traffic in 1.8.2"
description = '''I am trying to capture remote traffic with v1.8.2. I have done this in previous versions and there is a setting in previous versions that states &quot;Do not capture own RPCAP traffic&quot;. I cannot find this same setting in v1.8.2 (maybe it is default?). Anyhow, I can get the remote capture started but all ...'''
date = "2012-08-22T12:11:00Z"
lastmod = "2012-08-22T12:11:00Z"
weight = 13825
keywords = [ "remote" ]
aliases = [ "/questions/13825" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to turn off RPCAP traffic in 1.8.2](/questions/13825/how-to-turn-off-rpcap-traffic-in-182)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13825-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13825-score" class="post-score" title="current number of votes">0</div><span id="post-13825-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to capture remote traffic with v1.8.2. I have done this in previous versions and there is a setting in previous versions that states "Do not capture own RPCAP traffic". I cannot find this same setting in v1.8.2 (maybe it is default?). Anyhow, I can get the remote capture started but all I see is the connection packets between my machine and the remote machine.</p><p>In v1.8.2, I have tried looking at Capture--&gt;Options--&gt;Manage Interfaces, but cannot find this setting.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-remote" rel="tag" title="see questions tagged &#39;remote&#39;">remote</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Aug '12, 12:11</strong></p><img src="https://secure.gravatar.com/avatar/c992464053c5fc5979601f33ba4ae26f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bmazzocco&#39;s gravatar image" /><p><span>bmazzocco</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bmazzocco has no accepted answers">0%</span></p></div></div><div id="comments-container-13825" class="comments-container"></div><div id="comment-tools-13825" class="comment-tools"></div><div class="clear"></div><div id="comment-13825-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

