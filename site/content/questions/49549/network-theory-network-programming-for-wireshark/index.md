+++
type = "question"
title = "Network Theory &amp; Network Programming for Wireshark"
description = '''Hello, I am trying to learn to learn a packet sniffer and have started with libpcap tutorial. Is learning networking OSI model ...with every layer &amp;amp; protocol in detail (theoretically) be advise able at this point or should I learn network theory and related protocols on the fly (as &amp;amp; when th...'''
date = "2016-01-27T06:50:00Z"
lastmod = "2016-01-28T00:29:00Z"
weight = 49549
keywords = [ "sniffer", "networking", "protocols", "packet", "wireshark" ]
aliases = [ "/questions/49549" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Network Theory & Network Programming for Wireshark](/questions/49549/network-theory-network-programming-for-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49549-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49549-score" class="post-score" title="current number of votes">0</div><span id="post-49549-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I am trying to learn to learn a packet sniffer and have started with libpcap tutorial.</p><p>Is learning networking OSI model ...with every layer &amp; protocol in detail (theoretically) be advise able at this point or should I learn network theory and related protocols on the fly (as &amp; when the requirement comes up) through books &amp; RFCs.</p><p>Secondly, I also dont know network programming (Richard Stevens). Will it be advisable to learn that beforehand as well.</p><p>can someone help guide me with the appropriate path &amp; approach that I should follow.</p><p>Regards,</p><p>Mohit</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffer" rel="tag" title="see questions tagged &#39;sniffer&#39;">sniffer</span> <span class="post-tag tag-link-networking" rel="tag" title="see questions tagged &#39;networking&#39;">networking</span> <span class="post-tag tag-link-protocols" rel="tag" title="see questions tagged &#39;protocols&#39;">protocols</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jan '16, 06:50</strong></p><img src="https://secure.gravatar.com/avatar/e74d8ecace804379f67c14c9af33695d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Monu&#39;s gravatar image" /><p><span>Monu</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Monu has no accepted answers">0%</span></p></div></div><div id="comments-container-49549" class="comments-container"><span id="49550"></span><div id="comment-49550" class="comment"><div id="post-49550-score" class="comment-score"></div><div class="comment-text"><p>what is your goal, learning to analyze packets/networks, or code a network analyzer?</p></div><div id="comment-49550-info" class="comment-info"><span class="comment-age">(27 Jan '16, 07:10)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="49570"></span><div id="comment-49570" class="comment"><div id="post-49570-score" class="comment-score"></div><div class="comment-text"><p>I want to learn to code a network analyzer</p></div><div id="comment-49570-info" class="comment-info"><span class="comment-age">(28 Jan '16, 00:29)</span> <span class="comment-user userinfo">Monu</span></div></div></div><div id="comment-tools-49549" class="comment-tools"></div><div class="clear"></div><div id="comment-49549-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

