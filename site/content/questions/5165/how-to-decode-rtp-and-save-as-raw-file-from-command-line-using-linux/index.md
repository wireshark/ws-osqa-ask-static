+++
type = "question"
title = "How to decode RTP and save as .raw file from command line using linux?"
description = '''Hi, I managed to capture some RTP packets from VoIP conversation. Does anyone know how do I execute the &quot;Decode as&quot; Wireshark command from command line and make it save as .raw file? I know how to execute these commands from the GUI, but not from a command line. I&#x27;m trying to incorporate it into my ...'''
date = "2011-07-21T22:30:00Z"
lastmod = "2012-04-26T08:00:00Z"
weight = 5165
keywords = [ "decode", "rtp" ]
aliases = [ "/questions/5165" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to decode RTP and save as .raw file from command line using linux?](/questions/5165/how-to-decode-rtp-and-save-as-raw-file-from-command-line-using-linux)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5165-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5165-score" class="post-score" title="current number of votes">1</div><span id="post-5165-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I managed to capture some RTP packets from VoIP conversation. Does anyone know how do I execute the "Decode as" Wireshark command from command line and make it save as .raw file? I know how to execute these commands from the GUI, but not from a command line. I'm trying to incorporate it into my script. Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jul '11, 22:30</strong></p><img src="https://secure.gravatar.com/avatar/4f80927e89808aac5d942eb09d48a40f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="testsmiley&#39;s gravatar image" /><p><span>testsmiley</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="testsmiley has no accepted answers">0%</span></p></div></div><div id="comments-container-5165" class="comments-container"><span id="10460"></span><div id="comment-10460" class="comment"><div id="post-10460-score" class="comment-score"></div><div class="comment-text"><p>I have the same issue... did you get the answer finally ??</p></div><div id="comment-10460-info" class="comment-info"><span class="comment-age">(26 Apr '12, 08:00)</span> <span class="comment-user userinfo">manfree</span></div></div></div><div id="comment-tools-5165" class="comment-tools"></div><div class="clear"></div><div id="comment-5165-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

