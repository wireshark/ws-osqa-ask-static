+++
type = "question"
title = "[closed] Remote Packet Capture : Linux to Linux"
description = '''Hello guys, I have a question. I have tried the Remote Packet Capture from Local Windows to a Remote Linux machine. But i can&#x27;t do the same from a Local Linux machine. I have found the facts, like  In the help page of dumpcap ( i.e. the output of ./dumpcap -h) there is no option for Remote Capture (...'''
date = "2012-08-03T07:44:00Z"
lastmod = "2012-08-04T07:07:00Z"
weight = 13352
keywords = [ "remote", "linux" ]
aliases = [ "/questions/13352" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Remote Packet Capture : Linux to Linux](/questions/13352/remote-packet-capture-linux-to-linux)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13352-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13352-score" class="post-score" title="current number of votes">0</div><span id="post-13352-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello guys, I have a question. I have tried the Remote Packet Capture from Local Windows to a Remote Linux machine. But i can't do the same from a Local Linux machine. I have found the facts, like</p><ol><li>In the help page of dumpcap ( i.e. the output of ./dumpcap -h) there is no option for Remote Capture ( like -A for authentication ).</li><li>If I give the interface of the Remote machine directly ( i.e. by simply writing "rpcap://ip_address/interface_name" ) in the Local Linux machine's Wireshark GUI, it gives error.</li></ol><p>I want to know that is Remote capture POSSIBLE from a Local Linux to Remote Linux ?</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-remote" rel="tag" title="see questions tagged &#39;remote&#39;">remote</span> <span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Aug '12, 07:44</strong></p><img src="https://secure.gravatar.com/avatar/e82780891a1e938f0bf3a529adc858a5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="baila&#39;s gravatar image" /><p><span>baila</span><br />
<span class="score" title="21 reputation points">21</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="baila has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>03 Aug '12, 08:54</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-13352" class="comments-container"><span id="13354"></span><div id="comment-13354" class="comment"><div id="post-13354-score" class="comment-score"></div><div class="comment-text"><p>This is currently being worked on in your other question <a href="http://ask.wireshark.org/questions/13217/remote-packet-capture-on-remote-linux-machine">here</a></p></div><div id="comment-13354-info" class="comment-info"><span class="comment-age">(03 Aug '12, 08:54)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="13355"></span><div id="comment-13355" class="comment"><div id="post-13355-score" class="comment-score"></div><div class="comment-text"><p>"This is currently being worked on in your other question here"</p><p>-- does it mean that this question is under process or being discussed on the mentioned link?</p></div><div id="comment-13355-info" class="comment-info"><span class="comment-age">(03 Aug '12, 09:07)</span> <span class="comment-user userinfo">baila</span></div></div><span id="13364"></span><div id="comment-13364" class="comment"><div id="post-13364-score" class="comment-score"></div><div class="comment-text"><p>I mean that this seems to be a very close duplicate of your other question and help is still being provided by others for that question.</p><p>Having duplicates, or very similar questions isn't helpful for other users looking for answers.</p></div><div id="comment-13364-info" class="comment-info"><span class="comment-age">(04 Aug '12, 07:07)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-13352" class="comment-tools"></div><div class="clear"></div><div id="comment-13352-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by grahamb 03 Aug '12, 08:54

</div>

</div>

</div>

