+++
type = "question"
title = "TSU Notification"
description = '''Dear Wireshark Community, I have a question regarding EAR compliance for wireshark community mentioned at http://www.wireshark.org/export.html. Am I correct in assuming that TSU notification(s) have been submitted to crypt@bis.doc.gov and to enc@nsa.gov so that Wireshark&#x27;s encryption source code cla...'''
date = "2013-10-15T01:01:00Z"
lastmod = "2013-10-16T03:03:00Z"
weight = 25987
keywords = [ "export-control" ]
aliases = [ "/questions/25987" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [TSU Notification](/questions/25987/tsu-notification)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25987-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25987-score" class="post-score" title="current number of votes">0</div><span id="post-25987-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear Wireshark Community,</p><p>I have a question regarding EAR compliance for wireshark community mentioned at <a href="http://www.wireshark.org/export.html">http://www.wireshark.org/export.html</a>. Am I correct in assuming that TSU notification(s) have been submitted to <span class="__cf_email__" data-cfemail="ef8c9d969f9baf8d869cc18b808cc1888099">[email protected]</span> and to <span class="__cf_email__" data-cfemail="55303b36153b26347b323a23">[email protected]</span> so that Wireshark's encryption source code classified under ECCN 5D002 can be eligible for license exception TSU?</p><p>Regards, Makoto</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-export-control" rel="tag" title="see questions tagged &#39;export-control&#39;">export-control</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Oct '13, 01:01</strong></p><img src="https://secure.gravatar.com/avatar/8e0109c15e1890801eeb96276ea39cab?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="arf193nm&#39;s gravatar image" /><p><span>arf193nm</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="arf193nm has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Oct '13, 05:11</strong> </span></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span></p></div></div><div id="comments-container-25987" class="comments-container"></div><div id="comment-tools-25987" class="comment-tools"></div><div class="clear"></div><div id="comment-25987-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="26010"></span>

<div id="answer-container-26010" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26010-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26010-score" class="post-score" title="current number of votes">2</div><span id="post-26010-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Kurt Knochner has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The notification was sent to <span class="__cf_email__" data-cfemail="3457464d444074565d471a505b571a535b42">[email protected]</span> and <span class="__cf_email__" data-cfemail="3c59525f7c524f5d125b534a">[email protected]</span> in 2006: <a href="http://www.wireshark.org/lists/wireshark-dev/200607/msg00303.html">http://www.wireshark.org/lists/wireshark-dev/200607/msg00303.html</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Oct '13, 08:26</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Oct '13, 05:12</strong> </span></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span></p></div></div><div id="comments-container-26010" class="comments-container"><span id="26033"></span><div id="comment-26033" class="comment"><div id="post-26033-score" class="comment-score"></div><div class="comment-text"><p>Thank you for telling me the information. I appreciate your support. If the internet location of the subversion repository remains unchanged, your one-time notification would cover subsequent updates/modifications of the encryption source code.</p><p>Makoto</p></div><div id="comment-26033-info" class="comment-info"><span class="comment-age">(15 Oct '13, 17:35)</span> <span class="comment-user userinfo">arf193nm</span></div></div></div><div id="comment-tools-26010" class="comment-tools"></div><div class="clear"></div><div id="comment-26010-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="25994"></span>

<div id="answer-container-25994" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25994-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25994-score" class="post-score" title="current number of votes">0</div><span id="post-25994-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As it's mentioned on the export page</p><blockquote><p><strong>To the best of our knowledge</strong>, Wireshark falls under ECCN 5D002 and qualifies for license exemption TSU under Section 734.3(b)(3) of the EAR.</p></blockquote><p>So, I don't think that anybody really submitted code to the mentioned addresses. However, the <a href="http://hecker.org/mozilla/eccn">ECCN explanation for Mozilla</a> mentions this</p><blockquote><p>However <strong>as an open source product</strong> Mozilla (including NSS) <strong>is governed by section 740.13(e) of the EAR</strong>,</p></blockquote><p>As Wireshark is also an open source product the same section can be applied (my interpretation!!)</p><p>In the <a href="http://hecker.org/mozilla/eccn">ECCN explanation for Mozilla</a>, it's mentioned</p><blockquote><p>UPDATE: On his Export Control Blog Scott Gearity makes the point <strong>that ANYONE could request BIS review for NLR export</strong> of Mozilla-related binaries if they wanted to. This might be relevant for U.S. corporations bundling Firefox, Thunderbird, etc., with their software and/or hardware.</p></blockquote><p>So, if you think you need a review, go ahead and send the code and/or the binaries to the addresses you mentioned and ask for a review. Please update here (for the benefit of others) if you ever get an answer.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Oct '13, 06:12</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-25994" class="comments-container"><span id="26039"></span><div id="comment-26039" class="comment"><div id="post-26039-score" class="comment-score"></div><div class="comment-text"><p>Thank you for your reply. My question was resolved.</p><p>FYU, I recommend you to refer to EAR Part 740.13(e)(3) (at page 34 of the document updated on July 16, 2013).</p></div><div id="comment-26039-info" class="comment-info"><span class="comment-age">(15 Oct '13, 20:40)</span> <span class="comment-user userinfo">arf193nm</span></div></div><span id="26046"></span><div id="comment-26046" class="comment"><div id="post-26046-score" class="comment-score"></div><div class="comment-text"><p>good. I accepted the answer of <span>@Gerald Combs</span> for you.</p></div><div id="comment-26046-info" class="comment-info"><span class="comment-age">(16 Oct '13, 03:03)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-25994" class="comment-tools"></div><div class="clear"></div><div id="comment-25994-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

