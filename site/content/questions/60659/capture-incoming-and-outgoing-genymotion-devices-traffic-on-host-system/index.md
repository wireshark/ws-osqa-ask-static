+++
type = "question"
title = "Capture incoming and outgoing Genymotion devices traffic on host system"
description = '''How can I capture Genymotion traffic on host (Ubuntu 16.04) using Wireshark? I have 10 android devices simulated using Genymotion which run on top off Virtualbox, what is best configuration for devices, host and Wireshark to capture both incoming and outgoing traffic of all android device?  Or if yo...'''
date = "2017-04-07T10:50:00Z"
lastmod = "2017-04-07T10:50:00Z"
weight = 60659
keywords = [ "capture", "virtualbox", "ubuntu" ]
aliases = [ "/questions/60659" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capture incoming and outgoing Genymotion devices traffic on host system](/questions/60659/capture-incoming-and-outgoing-genymotion-devices-traffic-on-host-system)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60659-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60659-score" class="post-score" title="current number of votes">0</div><span id="post-60659-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can I capture Genymotion traffic on host (Ubuntu 16.04) using Wireshark?</p><p>I have 10 android devices simulated using Genymotion which run on top off Virtualbox, what is best configuration for devices, host and Wireshark to capture both incoming and outgoing traffic of all android device?</p><p>Or if you suggest any other tools for traffic capturing, please let me know it and related configuration.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-virtualbox" rel="tag" title="see questions tagged &#39;virtualbox&#39;">virtualbox</span> <span class="post-tag tag-link-ubuntu" rel="tag" title="see questions tagged &#39;ubuntu&#39;">ubuntu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Apr '17, 10:50</strong></p><img src="https://secure.gravatar.com/avatar/1595a24111dff7d0376d456e91895399?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Zahra&#39;s gravatar image" /><p><span>Zahra</span><br />
<span class="score" title="31 reputation points">31</span><span title="8 badges"><span class="badge1">●</span><span class="badgecount">8</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Zahra has no accepted answers">0%</span></p></div></div><div id="comments-container-60659" class="comments-container"></div><div id="comment-tools-60659" class="comment-tools"></div><div class="clear"></div><div id="comment-60659-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

