+++
type = "question"
title = "only bluetooth"
description = '''why i have only bluetooth , I connect to WiFi but nothing appears . I use ubuntu 14.04'''
date = "2015-11-15T07:53:00Z"
lastmod = "2015-11-15T08:59:00Z"
weight = 47616
keywords = [ "bluetooth" ]
aliases = [ "/questions/47616" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [only bluetooth](/questions/47616/only-bluetooth)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47616-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47616-score" class="post-score" title="current number of votes">0</div><span id="post-47616-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/Screenshot_from_2015-11-15_16:48:36.png" alt="alt text" />why i have only bluetooth , I connect to WiFi but nothing appears . I use ubuntu 14.04</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bluetooth" rel="tag" title="see questions tagged &#39;bluetooth&#39;">bluetooth</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Nov '15, 07:53</strong></p><img src="https://secure.gravatar.com/avatar/ea6d1979b5ae81313bca7ae38e304329?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="%D0%9C%D0%B0%D1%80%D0%BA%D0%BE%20%D0%9E%D1%82%D0%BB%D0%BE%D0%BA%D0%B0%D0%BD&#39;s gravatar image" /><p><span>Марко Отлокан</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Марко Отлокан has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Nov '15, 07:54</strong> </span></p></div></div><div id="comments-container-47616" class="comments-container"></div><div id="comment-tools-47616" class="comment-tools"></div><div class="clear"></div><div id="comment-47616-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="47618"></span>

<div id="answer-container-47618" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47618-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47618-score" class="post-score" title="current number of votes">0</div><span id="post-47618-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's (most certainly) a permission problem. Your non-root user does not have the required access rights to open the other interfaces in a way Wireshark needs it.</p><p>See 'setcap' in the answers to the following question</p><blockquote><p><a href="https://ask.wireshark.org/questions/7523/ubuntu-machine-no-interfaces-listed">https://ask.wireshark.org/questions/7523/ubuntu-machine-no-interfaces-listed</a><br />
<a href="https://ask.wireshark.org/questions/37061/no-interface-for-capture-to-be-done-on-ubuntu-server-1404">https://ask.wireshark.org/questions/37061/no-interface-for-capture-to-be-done-on-ubuntu-server-1404</a><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Nov '15, 08:59</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-47618" class="comments-container"></div><div id="comment-tools-47618" class="comment-tools"></div><div class="clear"></div><div id="comment-47618-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

