+++
type = "question"
title = "How to capture traffic on port 8080?"
description = '''Hello,  can anyone help me how to capture traffic on port 8080 using wireshark? I use wifi connection and I type tcp.port == 8080 but there are no packets Thanks'''
date = "2015-08-22T09:41:00Z"
lastmod = "2015-08-24T12:48:00Z"
weight = 45314
keywords = [ "capture", "capture-filter" ]
aliases = [ "/questions/45314" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture traffic on port 8080?](/questions/45314/how-to-capture-traffic-on-port-8080)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45314-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45314-score" class="post-score" title="current number of votes">0</div><span id="post-45314-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, can anyone help me how to capture traffic on port 8080 using wireshark? I use wifi connection and I type tcp.port == 8080 but there are no packets Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Aug '15, 09:41</strong></p><img src="https://secure.gravatar.com/avatar/c1d07897a6fbac5d6e244e3cfa8ea347?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Djordje%20Novakovic&#39;s gravatar image" /><p><span>Djordje Nova...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Djordje Novakovic has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Aug '15, 11:39</strong> </span></p></div></div><div id="comments-container-45314" class="comments-container"></div><div id="comment-tools-45314" class="comment-tools"></div><div class="clear"></div><div id="comment-45314-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="45325"></span>

<div id="answer-container-45325" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45325-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45325-score" class="post-score" title="current number of votes">1</div><span id="post-45325-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Apart from the capture and display filter syntax difficulties, is the problem due to your WiFi network running in an encrypted mode? Are you trying to capture traffic from the device you're capturing on, or other devices on the same WiFi network?</p><p>If you leave the capture\display filters empty, what do you see?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Aug '15, 12:48</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-45325" class="comments-container"></div><div id="comment-tools-45325" class="comment-tools"></div><div class="clear"></div><div id="comment-45325-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="45321"></span>

<div id="answer-container-45321" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45321-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45321-score" class="post-score" title="current number of votes">0</div><span id="post-45321-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You have to decide whether to use a /capture/ filter or a /display/ filter - the syntax is different between those two filter types. <strong>tcp port 8080</strong> is /capture/ filter, but <strong>tcp.port == 8080</strong> is /display/ filter.</p><p>First thing I would confirm is that I am using the right interface. To do this quickly and simply, I would click Capture &gt; Interfaces and confirm which interface is receiving packets. Then select that interface and click the Start button.</p><p>Once the trace has started, then you should be able to use type your filter (the /display/ filter) into the filter toolbar in the Wireshark interface. Then you should /only/ see packets with a source or destination port 8080.</p><p>If you have confirmed you are tracing with the right interface and you have correctly typed your display filter, and you /still/ not seeing any packets, then the only thing to conclude is that those packets were never sent or received on your network interface. Then the picture changes and you need to reassess the situation.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Aug '15, 11:33</strong></p><img src="https://secure.gravatar.com/avatar/32272e9efae0156b7a71e9b634428d14?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smp&#39;s gravatar image" /><p><span>smp</span><br />
<span class="score" title="39 reputation points">39</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smp has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Aug '15, 11:35</strong> </span></p></div></div><div id="comments-container-45321" class="comments-container"></div><div id="comment-tools-45321" class="comment-tools"></div><div class="clear"></div><div id="comment-45321-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

