+++
type = "question"
title = "child dumpcap process died : Exception"
description = '''Hi When I open Wireshark to capture the traffic, I am not able to see the network interfaces/adapters. And I am getting seeing the message &quot;child dumpcap process died: Exception 0xc0000022&quot;. But I am able to open the existing traces. I have tried uninstalling and installing the newer versions of Wir...'''
date = "2011-03-01T18:50:00Z"
lastmod = "2011-03-21T20:04:00Z"
weight = 2626
keywords = [ "dumpcap" ]
aliases = [ "/questions/2626" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [child dumpcap process died : Exception](/questions/2626/child-dumpcap-process-died-exception)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2626-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2626-score" class="post-score" title="current number of votes">1</div><span id="post-2626-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>When I open Wireshark to capture the traffic, I am not able to see the network interfaces/adapters. And I am getting seeing the message "child dumpcap process died: Exception 0xc0000022".</p><p>But I am able to open the existing traces.</p><p>I have tried uninstalling and installing the newer versions of Wireshark (1.4.4) and Winpcap(4.1.2). Also rebooted my laptop.</p><p>Please let me know what is this error message about and how I can troubleshoot/resolve this issue?</p><p>Your help is highly appreciated.</p><p>Thanks &amp; Regards, Vasu.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dumpcap" rel="tag" title="see questions tagged &#39;dumpcap&#39;">dumpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Mar '11, 18:50</strong></p><img src="https://secure.gravatar.com/avatar/f56706fcb441f1beed029aee8adb1adf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vasu&#39;s gravatar image" /><p><span>vasu</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vasu has no accepted answers">0%</span></p></div></div><div id="comments-container-2626" class="comments-container"><span id="2997"></span><div id="comment-2997" class="comment"><div id="post-2997-score" class="comment-score"></div><div class="comment-text"><p>What version of Windows are you running on? Do you have sufficient privileges to perform packet capturing?</p></div><div id="comment-2997-info" class="comment-info"><span class="comment-age">(21 Mar '11, 20:04)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-2626" class="comment-tools"></div><div class="clear"></div><div id="comment-2626-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

