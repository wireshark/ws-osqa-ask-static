+++
type = "question"
title = "Capture HL7 router validation setting errors in a custom table?"
description = '''Capture HL7 router validation setting errors in a custom table?'''
date = "2014-02-20T23:14:00Z"
lastmod = "2014-02-20T23:14:00Z"
weight = 30074
keywords = [ "hl7", "router" ]
aliases = [ "/questions/30074" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capture HL7 router validation setting errors in a custom table?](/questions/30074/capture-hl7-router-validation-setting-errors-in-a-custom-table)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30074-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30074-score" class="post-score" title="current number of votes">0</div><span id="post-30074-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Capture HL7 router validation setting errors in a custom table?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hl7" rel="tag" title="see questions tagged &#39;hl7&#39;">hl7</span> <span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Feb '14, 23:14</strong></p><img src="https://secure.gravatar.com/avatar/6446ee50f12377c768b5a3a99454da48?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bhgetsingh&#39;s gravatar image" /><p><span class="suspended-user">bhgetsingh</span><br />
(suspended)<br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bhgetsingh has no accepted answers">0%</span></p></div></div><div id="comments-container-30074" class="comments-container"></div><div id="comment-tools-30074" class="comment-tools"></div><div class="clear"></div><div id="comment-30074-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

