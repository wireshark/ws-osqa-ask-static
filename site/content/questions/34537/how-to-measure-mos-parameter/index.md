+++
type = "question"
title = "How to measure MOS parameter"
description = '''Hello everybody, I&#x27;m using this program to analyze some call voip and I&#x27;d like to calculate mos value but I don&#x27;t know where I can find it....can you help me?? thank you '''
date = "2014-07-10T00:46:00Z"
lastmod = "2014-07-10T02:11:00Z"
weight = 34537
keywords = [ "voipcalls", "mos", "rfactor", "parameterofquality" ]
aliases = [ "/questions/34537" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to measure MOS parameter](/questions/34537/how-to-measure-mos-parameter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34537-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34537-score" class="post-score" title="current number of votes">0</div><span id="post-34537-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello everybody, I'm using this program to analyze some call voip and I'd like to calculate mos value but I don't know where I can find it....can you help me?? thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voipcalls" rel="tag" title="see questions tagged &#39;voipcalls&#39;">voipcalls</span> <span class="post-tag tag-link-mos" rel="tag" title="see questions tagged &#39;mos&#39;">mos</span> <span class="post-tag tag-link-rfactor" rel="tag" title="see questions tagged &#39;rfactor&#39;">rfactor</span> <span class="post-tag tag-link-parameterofquality" rel="tag" title="see questions tagged &#39;parameterofquality&#39;">parameterofquality</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jul '14, 00:46</strong></p><img src="https://secure.gravatar.com/avatar/e778b32221e09e52585a198f1fc32592?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Francesco%20Rovito&#39;s gravatar image" /><p><span>Francesco Ro...</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Francesco Rovito has no accepted answers">0%</span></p></div></div><div id="comments-container-34537" class="comments-container"></div><div id="comment-tools-34537" class="comment-tools"></div><div class="clear"></div><div id="comment-34537-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34544"></span>

<div id="answer-container-34544" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34544-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34544-score" class="post-score" title="current number of votes">0</div><span id="post-34544-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>From this article <a href="http://en.wikipedia.org/wiki/Mean_opinion_score">http://en.wikipedia.org/wiki/Mean_opinion_score</a> it's a subjective value obtained by having pople listening to call samples.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Jul '14, 01:59</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-34544" class="comments-container"><span id="34546"></span><div id="comment-34546" class="comment"><div id="post-34546-score" class="comment-score"></div><div class="comment-text"><p>That's all right...I'd like to see also a graph,for example,where I can see the quality of voice and from this obtain the mos.....Do you think it's impossible something similar in wireshark???how can I write in a project?? I must answer at "analyze packets SIP and RTP exchange and with their content valutate mos parameter and codec quality"</p></div><div id="comment-34546-info" class="comment-info"><span class="comment-age">(10 Jul '14, 02:11)</span> <span class="comment-user userinfo">Francesco Ro...</span></div></div></div><div id="comment-tools-34544" class="comment-tools"></div><div class="clear"></div><div id="comment-34544-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

