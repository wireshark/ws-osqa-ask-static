+++
type = "question"
title = "capturing (and decoding) open wireless network packets"
description = '''hello, i am trying to capture my open wireless network&#x27;s (no password) packets. I am able to capture wpa2 traffic and decrypt the packets with it&#x27;s password. but, when I capture open wifi traffic, i cannot see any readable packet in the capture file. I am using: -backtrack 5 -AWUS036H card (Realtek ...'''
date = "2013-07-19T15:00:00Z"
lastmod = "2013-07-20T14:14:00Z"
weight = 23182
keywords = [ "decode", "decryption", "wifi", "open" ]
aliases = [ "/questions/23182" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [capturing (and decoding) open wireless network packets](/questions/23182/capturing-and-decoding-open-wireless-network-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23182-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23182-score" class="post-score" title="current number of votes">0</div><span id="post-23182-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello,</p><p>i am trying to capture my open wireless network's (no password) packets.</p><p>I am able to capture wpa2 traffic and decrypt the packets with it's password. but, when I capture open wifi traffic, i cannot see any readable packet in the capture file.</p><p>I am using: -backtrack 5 -AWUS036H card (Realtek RTL8187L chipset)</p><p>i tried: "airdecap-ng -b &lt;ap_mac&gt; &lt;capture_file.cap&gt;"</p><p>but it didn't work.</p><p>How can i decode/decrypt/decipher the "open wifi" packets?</p><p>Thank you,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span> <span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-open" rel="tag" title="see questions tagged &#39;open&#39;">open</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jul '13, 15:00</strong></p><img src="https://secure.gravatar.com/avatar/b2c7227c4e4a2e6da3127fd50fd71f3b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmdturk&#39;s gravatar image" /><p><span>cmdturk</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmdturk has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Jul '13, 15:07</strong> </span></p></div></div><div id="comments-container-23182" class="comments-container"></div><div id="comment-tools-23182" class="comment-tools"></div><div class="clear"></div><div id="comment-23182-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="23200"></span>

<div id="answer-container-23200" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23200-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23200-score" class="post-score" title="current number of votes">0</div><span id="post-23200-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>sorry, problem has been solved.</p><p>my capture adapted is compatible with 801.11 b/g but my open wifi network is 802.11 "n"</p><p>so i couldn't capture any traffic with the card.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jul '13, 14:14</strong></p><img src="https://secure.gravatar.com/avatar/b2c7227c4e4a2e6da3127fd50fd71f3b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmdturk&#39;s gravatar image" /><p><span>cmdturk</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmdturk has no accepted answers">0%</span></p></div></div><div id="comments-container-23200" class="comments-container"></div><div id="comment-tools-23200" class="comment-tools"></div><div class="clear"></div><div id="comment-23200-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

