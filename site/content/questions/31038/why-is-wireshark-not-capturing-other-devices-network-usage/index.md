+++
type = "question"
title = "Why is wireshark not capturing other device&#x27;s network usage?"
description = '''Wireshark is not capturing other device&#x27;s network usage. It is only capturing my computer&#x27;s usage whereas I want to see other device&#x27;s network usage also (through wifi). Please help.'''
date = "2014-03-20T22:45:00Z"
lastmod = "2014-03-20T23:14:00Z"
weight = 31038
keywords = [ "device", "usage", "network" ]
aliases = [ "/questions/31038" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why is wireshark not capturing other device's network usage?](/questions/31038/why-is-wireshark-not-capturing-other-devices-network-usage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31038-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31038-score" class="post-score" title="current number of votes">0</div><span id="post-31038-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark is not capturing other device's network usage. It is only capturing my computer's usage whereas I want to see other device's network usage also (through wifi). Please help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-device" rel="tag" title="see questions tagged &#39;device&#39;">device</span> <span class="post-tag tag-link-usage" rel="tag" title="see questions tagged &#39;usage&#39;">usage</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Mar '14, 22:45</strong></p><img src="https://secure.gravatar.com/avatar/6e9e3fa1243a0402df6702d2e7c02e0f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Coolboy&#39;s gravatar image" /><p><span>Coolboy</span><br />
<span class="score" title="21 reputation points">21</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Coolboy has no accepted answers">0%</span></p></div></div><div id="comments-container-31038" class="comments-container"></div><div id="comment-tools-31038" class="comment-tools"></div><div class="clear"></div><div id="comment-31038-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31040"></span>

<div id="answer-container-31040" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31040-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31040-score" class="post-score" title="current number of votes">1</div><span id="post-31040-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You need to enable monitor mode in your WLAN interface.</p><blockquote><p><a href="http://wiki.wireshark.org/CaptureSetup/WLAN">http://wiki.wireshark.org/CaptureSetup/WLAN</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Mar '14, 23:14</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Mar '14, 23:15</strong> </span></p></div></div><div id="comments-container-31040" class="comments-container"></div><div id="comment-tools-31040" class="comment-tools"></div><div class="clear"></div><div id="comment-31040-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

