+++
type = "question"
title = "how to target user on local network?"
description = '''I have just started using wireshark. I&#x27;m using it so see how good is Internet security. But i have a problem targeting local mac, ip o computer of my choice. In aircrack, aireplay, and all others you can target computer and start capturing packets. But i haven&#x27;t found same option in wireshark.  How ...'''
date = "2011-04-18T10:33:00Z"
lastmod = "2011-04-18T10:33:00Z"
weight = 3577
keywords = [ "wifi" ]
aliases = [ "/questions/3577" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to target user on local network?](/questions/3577/how-to-target-user-on-local-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3577-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3577-score" class="post-score" title="current number of votes">0</div><span id="post-3577-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have just started using wireshark. I'm using it so see how good is Internet security. But i have a problem targeting local mac, ip o computer of my choice. In aircrack, aireplay, and all others you can target computer and start capturing packets. But i haven't found same option in wireshark.</p><p>How can i capture packages, and do i need to put my wlan0 to mon0 mode? How can i target or hack or sniff computer of my choice? I have wireless connection with my router. How to see all users on it? And how to pick one out to capture it's packages?</p><p>thx</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Apr '11, 10:33</strong></p><img src="https://secure.gravatar.com/avatar/a0493b2d3720b2be3cc21143b17c67bb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="calmpitbull&#39;s gravatar image" /><p><span>calmpitbull</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="calmpitbull has no accepted answers">0%</span></p></div></div><div id="comments-container-3577" class="comments-container"></div><div id="comment-tools-3577" class="comment-tools"></div><div class="clear"></div><div id="comment-3577-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

