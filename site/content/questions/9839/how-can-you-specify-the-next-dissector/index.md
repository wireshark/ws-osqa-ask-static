+++
type = "question"
title = "How can you specify the next dissector"
description = '''Hi!  I created a dissector that dissects EtherNet/IP data payload. I also want to create a dissector that will be used as a &quot;link layer&quot; dissector that will read packets created by a vendor specific protocol. In some cases, I want wireshark to dissect from self created pcap files. In those, I have a...'''
date = "2012-03-29T08:39:00Z"
lastmod = "2012-03-29T08:39:00Z"
weight = 9839
keywords = [ "transfer", "pass", "specify", "next" ]
aliases = [ "/questions/9839" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can you specify the next dissector](/questions/9839/how-can-you-specify-the-next-dissector)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9839-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9839-score" class="post-score" title="current number of votes">0</div><span id="post-9839-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi!</p><p>I created a dissector that dissects EtherNet/IP data payload. I also want to create a dissector that will be used as a "link layer" dissector that will read packets created by a vendor specific protocol.</p><p>In some cases, I want wireshark to dissect from self created pcap files. In those, I have a different protocol that has the same payload type that otherwise goes through EtherNet/IP. So I would like to reuse the same dissector that I use for dissecting the EtherNet/IP packets.</p><p>So, I can create a dissector that understands the protocol on my own files, but how can I then make it so that my first dissector then dissects the payload?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-transfer" rel="tag" title="see questions tagged &#39;transfer&#39;">transfer</span> <span class="post-tag tag-link-pass" rel="tag" title="see questions tagged &#39;pass&#39;">pass</span> <span class="post-tag tag-link-specify" rel="tag" title="see questions tagged &#39;specify&#39;">specify</span> <span class="post-tag tag-link-next" rel="tag" title="see questions tagged &#39;next&#39;">next</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Mar '12, 08:39</strong></p><img src="https://secure.gravatar.com/avatar/90140f74d53512910ba33db4ef7d30de?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Martino&#39;s gravatar image" /><p><span>Martino</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Martino has no accepted answers">0%</span></p></div></div><div id="comments-container-9839" class="comments-container"></div><div id="comment-tools-9839" class="comment-tools"></div><div class="clear"></div><div id="comment-9839-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

