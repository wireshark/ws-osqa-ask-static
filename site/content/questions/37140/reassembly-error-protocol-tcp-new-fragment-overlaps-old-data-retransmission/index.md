+++
type = "question"
title = "Reassembly error, protocol TCP: new fragment overlaps old data (retransmission?)"
description = '''I am troubleshooting intermittent file transfer problems during automated VM deployment and software installation. Sometimes the files do not copy over in its entirety causing the install to fail. I set up a test where wireshark runs while the software copies from the server to the VM. Even though I...'''
date = "2014-10-17T14:22:00Z"
lastmod = "2014-10-17T14:22:00Z"
weight = 37140
keywords = [ "reassembly", "error" ]
aliases = [ "/questions/37140" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Reassembly error, protocol TCP: new fragment overlaps old data (retransmission?)](/questions/37140/reassembly-error-protocol-tcp-new-fragment-overlaps-old-data-retransmission)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37140-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37140-score" class="post-score" title="current number of votes">0</div><span id="post-37140-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am troubleshooting intermittent file transfer problems during automated VM deployment and software installation. Sometimes the files do not copy over in its entirety causing the install to fail. I set up a test where wireshark runs while the software copies from the server to the VM. Even though I have not been able to reproduce the problem, I do see a ton of reassembly errors on a successful file transfer. Is this indicative of a problem? The server and the provisioned VM are on two different VLANS going through a Cisco VSS switch that is heavily utilized so I am wondering if this could be causing this error.<br />
</p><p>Once again the specific error is: Reassembly error, protocol TCP: new fragment overlaps old data (retransmission?)</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-reassembly" rel="tag" title="see questions tagged &#39;reassembly&#39;">reassembly</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Oct '14, 14:22</strong></p><img src="https://secure.gravatar.com/avatar/38c82974a7f8cbed4cf932573911797f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sewell18&#39;s gravatar image" /><p><span>sewell18</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sewell18 has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-37140" class="comments-container"></div><div id="comment-tools-37140" class="comment-tools"></div><div class="clear"></div><div id="comment-37140-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

