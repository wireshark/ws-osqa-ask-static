+++
type = "question"
title = "how to decode traffic from g729 codec"
description = '''i have capture one wireshark trace, it is using g.729 codec, How to decode it in wireshark?'''
date = "2013-08-12T23:36:00Z"
lastmod = "2013-08-13T00:49:00Z"
weight = 23731
keywords = [ "codec" ]
aliases = [ "/questions/23731" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to decode traffic from g729 codec](/questions/23731/how-to-decode-traffic-from-g729-codec)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23731-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23731-score" class="post-score" title="current number of votes">0</div><span id="post-23731-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i have capture one wireshark trace, it is using g.729 codec, How to decode it in wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-codec" rel="tag" title="see questions tagged &#39;codec&#39;">codec</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Aug '13, 23:36</strong></p><img src="https://secure.gravatar.com/avatar/e12d9d0c390836527ae1e83372d959a2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="archu&#39;s gravatar image" /><p><span>archu</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="archu has no accepted answers">0%</span></p></div></div><div id="comments-container-23731" class="comments-container"></div><div id="comment-tools-23731" class="comment-tools"></div><div class="clear"></div><div id="comment-23731-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="23735"></span>

<div id="answer-container-23735" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23735-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23735-score" class="post-score" title="current number of votes">1</div><span id="post-23735-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As detailed on the cunningly named Wireshark Wiki page: <a href="http://wiki.wireshark.org/HowToDecodeG729">HowToDecodeG729</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Aug '13, 00:49</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-23735" class="comments-container"></div><div id="comment-tools-23735" class="comment-tools"></div><div class="clear"></div><div id="comment-23735-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

