+++
type = "question"
title = "How can you be aware the source of the slow connection problem?"
description = '''Hi guys, this is not really a question. I want to know how can you be aware the source of the problem when a slow connection problem appearing. If you have any good idea, trick or tips, please show me and every member of this page know that.  Longing for more. Thanks.'''
date = "2013-06-20T11:05:00Z"
lastmod = "2013-06-20T14:30:00Z"
weight = 22206
keywords = [ "connection", "slow", "slowly" ]
aliases = [ "/questions/22206" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can you be aware the source of the slow connection problem?](/questions/22206/how-can-you-be-aware-the-source-of-the-slow-connection-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22206-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22206-score" class="post-score" title="current number of votes">0</div><span id="post-22206-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi guys, this is not really a question. I want to know how can you be aware the source of the problem when a slow connection problem appearing. If you have any good idea, trick or tips, please show me and every member of this page know that.</p><p>Longing for more. Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-connection" rel="tag" title="see questions tagged &#39;connection&#39;">connection</span> <span class="post-tag tag-link-slow" rel="tag" title="see questions tagged &#39;slow&#39;">slow</span> <span class="post-tag tag-link-slowly" rel="tag" title="see questions tagged &#39;slowly&#39;">slowly</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jun '13, 11:05</strong></p><img src="https://secure.gravatar.com/avatar/6403b6eed27c3e6a469e7bb75cb2bc2f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="quang20082008&#39;s gravatar image" /><p><span>quang20082008</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="quang20082008 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Jun '13, 11:13</strong> </span></p></div></div><div id="comments-container-22206" class="comments-container"><span id="22214"></span><div id="comment-22214" class="comment"><div id="post-22214-score" class="comment-score"></div><div class="comment-text"><p>Well, there is a million possible root causes out there so without a trace example at <a href="http://www.cloudshark.org">http://www.cloudshark.org</a> for us to look at, all you can expect is reading a crystal ball... You could possibly use editcap -s 100 to hide the real data if it is confidential ...</p></div><div id="comment-22214-info" class="comment-info"><span class="comment-age">(20 Jun '13, 14:30)</span> <span class="comment-user userinfo">mrEEde2</span></div></div></div><div id="comment-tools-22206" class="comment-tools"></div><div class="clear"></div><div id="comment-22206-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

