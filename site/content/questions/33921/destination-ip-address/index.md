+++
type = "question"
title = "Destination ip address"
description = '''I am a Newbie. I have a small business running through an Apple Extreme. Employees seem to be surfing more than working and I would like to know what their surfing on my equipment and dollar. The only destination ip address that appears is: 244.0.0.251 Why is that?'''
date = "2014-06-18T04:29:00Z"
lastmod = "2014-06-18T05:17:00Z"
weight = 33921
keywords = [ "destination" ]
aliases = [ "/questions/33921" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Destination ip address](/questions/33921/destination-ip-address)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33921-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33921-score" class="post-score" title="current number of votes">0</div><span id="post-33921-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am a Newbie. I have a small business running through an Apple Extreme. Employees seem to be surfing more than working and I would like to know what their surfing on my equipment and dollar. The only destination ip address that appears is:</p><p>244.0.0.251 Why is that?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-destination" rel="tag" title="see questions tagged &#39;destination&#39;">destination</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jun '14, 04:29</strong></p><img src="https://secure.gravatar.com/avatar/63ccc367e03178a1ddc775ebf18d47b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Willy&#39;s gravatar image" /><p><span>Willy</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Willy has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Jun '14, 07:42</strong> </span></p></div></div><div id="comments-container-33921" class="comments-container"><span id="33924"></span><div id="comment-33924" class="comment"><div id="post-33924-score" class="comment-score"></div><div class="comment-text"><p>Each post should have a clear, specific question in the title field. Please rephrase the title as a proper question.</p></div><div id="comment-33924-info" class="comment-info"><span class="comment-age">(18 Jun '14, 05:17)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-33921" class="comment-tools"></div><div class="clear"></div><div id="comment-33921-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

