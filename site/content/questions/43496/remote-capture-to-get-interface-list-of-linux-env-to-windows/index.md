+++
type = "question"
title = "Remote Capture to get Interface list of Linux Env to Windows"
description = '''Hi As observed as link-type as shown as &quot;unknow&quot; in wireshark if i run &quot;rpcapd&quot; with only the bind address option. But there is no error popping out with unknown link type at the time of capturing but we can&#x27;t use filter option to filter specific protocols. RPCAD: http://www.winpcap.org/install/bin/...'''
date = "2015-06-24T03:02:00Z"
lastmod = "2015-06-24T03:02:00Z"
weight = 43496
keywords = [ "rpcapd", "remote-capture", "link-type", "libpcap", "wireshark" ]
aliases = [ "/questions/43496" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Remote Capture to get Interface list of Linux Env to Windows](/questions/43496/remote-capture-to-get-interface-list-of-linux-env-to-windows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43496-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43496-score" class="post-score" title="current number of votes">0</div><span id="post-43496-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>As observed as link-type as shown as "unknow" in wireshark if i run "rpcapd" with only the bind address option. But there is no error popping out with unknown link type at the time of capturing but we can't use filter option to filter specific protocols.</p><p>RPCAD: <a href="http://www.winpcap.org/install/bin/WpcapSrc_4_1_3.zip">http://www.winpcap.org/install/bin/WpcapSrc_4_1_3.zip</a></p><p>Wireshark: Version 1.12.4</p><p>Run 1: &lt;link type="" as="" "unknown"=""&gt; - RPCAPD run options [Linux RHEL 6.0]</p><p><img src="https://osqa-ask.wireshark.org/upfiles/1_K9YAMuq.jpg" alt="alt text" /></p><ul><li>Wireshark remote capture [Windows 7 64 bit] -&gt; Strart-Wireshark.exe - Capture -&gt; options -&gt; interface -&gt; manage Interface -&gt; remote Interface -&gt; Add : IP addrss, User: <strong><em>&amp; Password:</em></strong> ***</li></ul><p><img src="https://osqa-ask.wireshark.org/upfiles/2.jpg" alt="alt text" /></p><p>Request some one to help me out why it is showing as "unknown".</p><p>Note: If I run same "rpcapd" with NULL authentication interface are shown as Ethernet.</p><ul><li>RPCAD with NULL authentication <img src="https://osqa-ask.wireshark.org/upfiles/3.jpg" alt="alt text" /></li></ul><p>Thank you</p><p>Regards</p><p>Dinesh Sadu</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rpcapd" rel="tag" title="see questions tagged &#39;rpcapd&#39;">rpcapd</span> <span class="post-tag tag-link-remote-capture" rel="tag" title="see questions tagged &#39;remote-capture&#39;">remote-capture</span> <span class="post-tag tag-link-link-type" rel="tag" title="see questions tagged &#39;link-type&#39;">link-type</span> <span class="post-tag tag-link-libpcap" rel="tag" title="see questions tagged &#39;libpcap&#39;">libpcap</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jun '15, 03:02</strong></p><img src="https://secure.gravatar.com/avatar/04334c27cb629065a13d61a61b611038?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dinesh%20Babu%20Sadu&#39;s gravatar image" /><p><span>Dinesh Babu ...</span><br />
<span class="score" title="16 reputation points">16</span><span title="13 badges"><span class="badge1">●</span><span class="badgecount">13</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="17 badges"><span class="bronze">●</span><span class="badgecount">17</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dinesh Babu Sadu has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Jun '15, 06:02</strong> </span></p></div></div><div id="comments-container-43496" class="comments-container"></div><div id="comment-tools-43496" class="comment-tools"></div><div class="clear"></div><div id="comment-43496-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

