+++
type = "question"
title = "[closed] Feature request: flip the &quot;Packet Details&quot; window...."
description = '''One little feature I would love in Wireshark; a simple graphical change. In the “packet details” window, I would like to flip the order upside down, so it matches the way we typically view the OSI and TCP/IP protocol stack; Frame on the bottom, then packet, then segment, then application headers. As...'''
date = "2014-05-13T16:53:00Z"
lastmod = "2014-05-13T20:28:00Z"
weight = 32771
keywords = [ "dispay", "osi", "model", "details", "packet" ]
aliases = [ "/questions/32771" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Feature request: flip the "Packet Details" window....](/questions/32771/feature-request-flip-the-packet-details-window)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32771-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32771-score" class="post-score" title="current number of votes">0</div><span id="post-32771-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>One little feature I would love in Wireshark; a simple graphical change. In the “packet details” window, I would like to flip the order upside down, so it matches the way we typically view the OSI and TCP/IP protocol stack; Frame on the bottom, then packet, then segment, then application headers. As a teacher, I believe it could help my students correlate the models to the real world we see when we sniff.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dispay" rel="tag" title="see questions tagged &#39;dispay&#39;">dispay</span> <span class="post-tag tag-link-osi" rel="tag" title="see questions tagged &#39;osi&#39;">osi</span> <span class="post-tag tag-link-model" rel="tag" title="see questions tagged &#39;model&#39;">model</span> <span class="post-tag tag-link-details" rel="tag" title="see questions tagged &#39;details&#39;">details</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 May '14, 16:53</strong></p><img src="https://secure.gravatar.com/avatar/7b051ca7259e901917469ba2bb5c57bb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Max%20Quasar&#39;s gravatar image" /><p><span>Max Quasar</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Max Quasar has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>14 May '14, 02:07</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-32771" class="comments-container"></div><div id="comment-tools-32771" class="comment-tools"></div><div class="clear"></div><div id="comment-32771-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by grahamb 14 May '14, 02:07

</div>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32774"></span>

<div id="answer-container-32774" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32774-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32774-score" class="post-score" title="current number of votes">1</div><span id="post-32774-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Enhancement requests can be entered at the Wireshark <a href="https://bugs.wireshark.org/bugzilla/">bugzilla</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 May '14, 19:30</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-32774" class="comments-container"><span id="32775"></span><div id="comment-32775" class="comment"><div id="post-32775-score" class="comment-score"></div><div class="comment-text"><p>I just registered and I only see a way to report a bug, not request a feature.</p></div><div id="comment-32775-info" class="comment-info"><span class="comment-age">(13 May '14, 19:44)</span> <span class="comment-user userinfo">Max Quasar</span></div></div><span id="32776"></span><div id="comment-32776" class="comment"><div id="post-32776-score" class="comment-score">1</div><div class="comment-text"><p>Proceed as if you're reporting a bug, and then from the "Severity" drop-down, select "Enhancement."</p></div><div id="comment-32776-info" class="comment-info"><span class="comment-age">(13 May '14, 20:28)</span> <span class="comment-user userinfo">Jim Aragon</span></div></div></div><div id="comment-tools-32774" class="comment-tools"></div><div class="clear"></div><div id="comment-32774-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

