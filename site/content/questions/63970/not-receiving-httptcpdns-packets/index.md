+++
type = "question"
title = "not receiving HTTP,TCP,DNS packets"
description = '''To monitor the traffic of a lab of my department , i unplugged a connection(that connection is of a lab) from my department&#x27;s main distribution internet switch and connected it to hub , then from i gave two connection from hub, one to that unplugged connection and other to my laptop(on which wiresha...'''
date = "2017-10-17T09:38:00Z"
lastmod = "2017-10-18T03:00:00Z"
weight = 63970
keywords = [ "ans1" ]
aliases = [ "/questions/63970" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [not receiving HTTP,TCP,DNS packets](/questions/63970/not-receiving-httptcpdns-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63970-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63970-score" class="post-score" title="current number of votes">0</div><span id="post-63970-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>To monitor the traffic of a lab of my department , i unplugged a connection(that connection is of a lab) from my department's main distribution internet switch and connected it to hub , then from i gave two connection from hub, one to that unplugged connection and other to my laptop(on which wireshark is installed) but problem is that i am not receiving http, tcp and dns packet from my lab traffic , i am getting mostly arp and ssdp</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ans1" rel="tag" title="see questions tagged &#39;ans1&#39;">ans1</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Oct '17, 09:38</strong></p><img src="https://secure.gravatar.com/avatar/6e43b916018fd86345dba5fb2681801b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jai17&#39;s gravatar image" /><p><span>jai17</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jai17 has no accepted answers">0%</span></p></div></div><div id="comments-container-63970" class="comments-container"><span id="63972"></span><div id="comment-63972" class="comment"><div id="post-63972-score" class="comment-score"></div><div class="comment-text"><p>Are you sure it's really a hub? What's the model number of the item?</p></div><div id="comment-63972-info" class="comment-info"><span class="comment-age">(17 Oct '17, 09:50)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="63979"></span><div id="comment-63979" class="comment"><div id="post-63979-score" class="comment-score"></div><div class="comment-text"><p>i didn't remembered the hub model but it was 100mb hub</p></div><div id="comment-63979-info" class="comment-info"><span class="comment-age">(17 Oct '17, 11:42)</span> <span class="comment-user userinfo">jai17</span></div></div><span id="63993"></span><div id="comment-63993" class="comment"><div id="post-63993-score" class="comment-score"></div><div class="comment-text"><p>I suspect it's not a hub. Providing the model number would allow folks to check.</p></div><div id="comment-63993-info" class="comment-info"><span class="comment-age">(18 Oct '17, 03:00)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-63970" class="comment-tools"></div><div class="clear"></div><div id="comment-63970-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

