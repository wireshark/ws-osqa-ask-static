+++
type = "question"
title = "capturing mac addresses"
description = '''Can you just tell me how can i capture mac addresses using wireshark. I am new to it when i use ether host &quot;mac address&quot; to get the mac addresses, the software is capturing packets but it is not displaying it.'''
date = "2014-09-30T18:29:00Z"
lastmod = "2014-10-02T11:38:00Z"
weight = 36736
keywords = [ "mac", "mac-address" ]
aliases = [ "/questions/36736" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [capturing mac addresses](/questions/36736/capturing-mac-addresses)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36736-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36736-score" class="post-score" title="current number of votes">0</div><span id="post-36736-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can you just tell me how can i capture mac addresses using wireshark. I am new to it</p><p>when i use ether host "mac address" to get the mac addresses, the software is capturing packets but it is not displaying it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-mac-address" rel="tag" title="see questions tagged &#39;mac-address&#39;">mac-address</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Sep '14, 18:29</strong></p><img src="https://secure.gravatar.com/avatar/e35c18b4fe32a6eb0aa4b21c55dff6d5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Harish%20Vaibhav&#39;s gravatar image" /><p><span>Harish Vaibhav</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Harish Vaibhav has no accepted answers">0%</span></p></div></div><div id="comments-container-36736" class="comments-container"></div><div id="comment-tools-36736" class="comment-tools"></div><div class="clear"></div><div id="comment-36736-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36750"></span>

<div id="answer-container-36750" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36750-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36750-score" class="post-score" title="current number of votes">0</div><span id="post-36750-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The mac address should be visible in the Ethernet (assuming that's the media you are using) dissection in the packet details pane. By default mac addresses are not shown in the columns in the packet list, but you can add columns to show source and destination mac addresses, see <a href="https://ask.wireshark.org/questions/15824/display-mac-address-in-the-packet-list">this</a> question.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Oct '14, 02:01</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-36750" class="comments-container"><span id="36802"></span><div id="comment-36802" class="comment"><div id="post-36802-score" class="comment-score"></div><div class="comment-text"><p>i am using wifi as my media. how can i add columns in wireshark to capture mac ids?</p></div><div id="comment-36802-info" class="comment-info"><span class="comment-age">(02 Oct '14, 11:31)</span> <span class="comment-user userinfo">Harish Vaibhav</span></div></div><span id="36803"></span><div id="comment-36803" class="comment"><div id="post-36803-score" class="comment-score"></div><div class="comment-text"><p>we are doing a project in which we need to determine the number of wifi links to a laptop or mobile phone by capturing the mac ids and counting them. i want to know how this can be implemented using wireshark</p></div><div id="comment-36803-info" class="comment-info"><span class="comment-age">(02 Oct '14, 11:38)</span> <span class="comment-user userinfo">Harish Vaibhav</span></div></div></div><div id="comment-tools-36750" class="comment-tools"></div><div class="clear"></div><div id="comment-36750-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

