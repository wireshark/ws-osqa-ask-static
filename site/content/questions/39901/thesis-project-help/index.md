+++
type = "question"
title = "Thesis / Project help"
description = '''I want to make my graduate project in Wireshark tell me some special topic in which i can do my project in Wireshark, also make some description that how i will complete my project.'''
date = "2015-02-16T22:38:00Z"
lastmod = "2015-02-17T01:57:00Z"
weight = 39901
keywords = [ "ahmed" ]
aliases = [ "/questions/39901" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Thesis / Project help](/questions/39901/thesis-project-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39901-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39901-score" class="post-score" title="current number of votes">0</div><span id="post-39901-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to make my graduate project in Wireshark tell me some special topic in which i can do my project in Wireshark, also make some description that how i will complete my project.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ahmed" rel="tag" title="see questions tagged &#39;ahmed&#39;">ahmed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Feb '15, 22:38</strong></p><img src="https://secure.gravatar.com/avatar/400972e8813c27cd68d925fb8517dd20?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Naveed&#39;s gravatar image" /><p><span>Naveed</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Naveed has no accepted answers">0%</span></p></div></div><div id="comments-container-39901" class="comments-container"><span id="39906"></span><div id="comment-39906" class="comment"><div id="post-39906-score" class="comment-score"></div><div class="comment-text"><p>You need to give more detail, do you want to add dissection of a new protocol, enhance the GUI, add statistics functions or work on the core functions trying to make Wireshark more efficient or? Any specific ptotocol area etc. See also <a href="http://wiki.wireshark.org/Development">http://wiki.wireshark.org/Development</a> <a href="http://wiki.wireshark.org/GSoC2014?highlight=%28summer%29">http://wiki.wireshark.org/GSoC2014?highlight=%28summer%29</a> <a href="http://wiki.wireshark.org/GSoC2013?highlight=%28summer%29">http://wiki.wireshark.org/GSoC2013?highlight=%28summer%29</a></p></div><div id="comment-39906-info" class="comment-info"><span class="comment-age">(17 Feb '15, 01:57)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-39901" class="comment-tools"></div><div class="clear"></div><div id="comment-39901-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

