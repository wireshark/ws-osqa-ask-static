+++
type = "question"
title = "identifying the ip"
description = '''Identifying the ip of the service host'''
date = "2015-04-20T20:38:00Z"
lastmod = "2015-04-20T22:50:00Z"
weight = 41609
keywords = [ "id" ]
aliases = [ "/questions/41609" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [identifying the ip](/questions/41609/identifying-the-ip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41609-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41609-score" class="post-score" title="current number of votes">0</div><span id="post-41609-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Identifying the ip of the service host</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-id" rel="tag" title="see questions tagged &#39;id&#39;">id</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Apr '15, 20:38</strong></p><img src="https://secure.gravatar.com/avatar/cd7389192b041581356254853759a532?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="leonikaris&#39;s gravatar image" /><p><span>leonikaris</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="leonikaris has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Apr '15, 20:41</strong> </span></p></div></div><div id="comments-container-41609" class="comments-container"><span id="41612"></span><div id="comment-41612" class="comment"><div id="post-41612-score" class="comment-score"></div><div class="comment-text"><p>Each post should have a clear, specific question in the title field. Please rephrase the title as a proper question.</p></div><div id="comment-41612-info" class="comment-info"><span class="comment-age">(20 Apr '15, 22:50)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-41609" class="comment-tools"></div><div class="clear"></div><div id="comment-41609-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

