+++
type = "question"
title = "IEEE 1722a, which draft version supported?"
description = '''Hi, which draft version of the IEEE 1722a protocol is supported by Wireshark? Messages using the draft version 14 are shown as &quot;Malformed packet&quot; by Wireshark, whereas draft version 6 messages are displayed correctly. Thanks'''
date = "2015-12-18T06:23:00Z"
lastmod = "2015-12-18T06:38:00Z"
weight = 48626
keywords = [ "ieee1722" ]
aliases = [ "/questions/48626" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [IEEE 1722a, which draft version supported?](/questions/48626/ieee-1722a-which-draft-version-supported)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48626-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48626-score" class="post-score" title="current number of votes">0</div><span id="post-48626-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>which draft version of the IEEE 1722a protocol is supported by Wireshark? Messages using the draft version 14 are shown as "Malformed packet" by Wireshark, whereas draft version 6 messages are displayed correctly.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ieee1722" rel="tag" title="see questions tagged &#39;ieee1722&#39;">ieee1722</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Dec '15, 06:23</strong></p><img src="https://secure.gravatar.com/avatar/adff8d731ffe044e74b218776bff2c64?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lumi&#39;s gravatar image" /><p><span>Lumi</span><br />
<span class="score" title="16 reputation points">16</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lumi has no accepted answers">0%</span></p></div></div><div id="comments-container-48626" class="comments-container"></div><div id="comment-tools-48626" class="comment-tools"></div><div class="clear"></div><div id="comment-48626-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48627"></span>

<div id="answer-container-48627" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48627-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48627-score" class="post-score" title="current number of votes">2</div><span id="post-48627-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Lumi has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Per the header of the source file found <a href="https://code.wireshark.org/review/gitweb?p=wireshark.git;a=blob;f=epan/dissectors/packet-ieee1722a.c;h=df56f2962fdf09605ef671ed99377038f6182800;hb=refs/heads/master">here</a>, it seems to be draft 7.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Dec '15, 06:27</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-48627" class="comments-container"><span id="48628"></span><div id="comment-48628" class="comment"><div id="post-48628-score" class="comment-score"></div><div class="comment-text"><p>Great, thanks!</p></div><div id="comment-48628-info" class="comment-info"><span class="comment-age">(18 Dec '15, 06:38)</span> <span class="comment-user userinfo">Lumi</span></div></div></div><div id="comment-tools-48627" class="comment-tools"></div><div class="clear"></div><div id="comment-48627-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

