+++
type = "question"
title = "retransmittion timer"
description = '''Hello everyone, Yesterday my colleagues bring one issue of slow database response,there were 2 windows application servers and one of them was working fine and one had a problem.after taking capture from both of them i found that server which had no problem was responding in 23 sec and problematic s...'''
date = "2014-02-22T04:41:00Z"
lastmod = "2014-02-24T10:30:00Z"
weight = 30097
keywords = [ "rto", "timer" ]
aliases = [ "/questions/30097" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [retransmittion timer](/questions/30097/retransmittion-timer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30097-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30097-score" class="post-score" title="current number of votes">0</div><span id="post-30097-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello everyone, Yesterday my colleagues bring one issue of slow database response,there were 2 windows application servers and one of them was working fine and one had a problem.after taking capture from both of them i found that server which had no problem was responding in 23 sec and problematic servers was taking 43 seconds,after further drill i found that both server had few retransmittion but in working server retransmittion was happening in 2 sec(constant) whereas in problematic server it was above 4 seconds and 2 of them were above 9 seconds.both server are in same vlan.why this retransmittion timer varies in both of them.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rto" rel="tag" title="see questions tagged &#39;rto&#39;">rto</span> <span class="post-tag tag-link-timer" rel="tag" title="see questions tagged &#39;timer&#39;">timer</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Feb '14, 04:41</strong></p><img src="https://secure.gravatar.com/avatar/6f9cdab5081b4272d1abf703a2689372?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kishan%20pandey&#39;s gravatar image" /><p><span>kishan pandey</span><br />
<span class="score" title="221 reputation points">221</span><span title="28 badges"><span class="badge1">●</span><span class="badgecount">28</span></span><span title="29 badges"><span class="silver">●</span><span class="badgecount">29</span></span><span title="36 badges"><span class="bronze">●</span><span class="badgecount">36</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kishan pandey has 2 accepted answers">28%</span></p></div></div><div id="comments-container-30097" class="comments-container"><span id="30140"></span><div id="comment-30140" class="comment"><div id="post-30140-score" class="comment-score"></div><div class="comment-text"><p>did you compare the CPU load and I/O load on both systems?</p></div><div id="comment-30140-info" class="comment-info"><span class="comment-age">(24 Feb '14, 10:30)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-30097" class="comment-tools"></div><div class="clear"></div><div id="comment-30097-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

