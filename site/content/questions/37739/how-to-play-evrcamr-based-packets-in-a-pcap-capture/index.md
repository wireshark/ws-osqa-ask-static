+++
type = "question"
title = "How to Play EVRC/AMR based packets in a pcap capture"
description = '''Hello, I have a pcap capture which has EVRC and AMR RTP packets. I want to be able to play those packets. One of the ways i was thinking this could be possible is by: Extracting AMR/EVRC packets from the pcap. Converting it to PCMU and than playing it using a audio player. Can someone please suggest...'''
date = "2014-11-10T17:11:00Z"
lastmod = "2014-11-11T08:16:00Z"
weight = 37739
keywords = [ "amr", "evrc", "playback", "decode_rtp" ]
aliases = [ "/questions/37739" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to Play EVRC/AMR based packets in a pcap capture](/questions/37739/how-to-play-evrcamr-based-packets-in-a-pcap-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37739-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37739-score" class="post-score" title="current number of votes">0</div><span id="post-37739-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I have a pcap capture which has EVRC and AMR RTP packets. I want to be able to play those packets.</p><p>One of the ways i was thinking this could be possible is by: Extracting AMR/EVRC packets from the pcap. Converting it to PCMU and than playing it using a audio player. Can someone please suggest me the possible ways? and steps to accomplish so?</p><p>Thanks for considering my request and i really appreciate your feedback.</p><p>Thanks!!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-amr" rel="tag" title="see questions tagged &#39;amr&#39;">amr</span> <span class="post-tag tag-link-evrc" rel="tag" title="see questions tagged &#39;evrc&#39;">evrc</span> <span class="post-tag tag-link-playback" rel="tag" title="see questions tagged &#39;playback&#39;">playback</span> <span class="post-tag tag-link-decode_rtp" rel="tag" title="see questions tagged &#39;decode_rtp&#39;">decode_rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Nov '14, 17:11</strong></p><img src="https://secure.gravatar.com/avatar/290a11ae874b8b8bbd0eb5921d109955?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="learnday&#39;s gravatar image" /><p><span>learnday</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="learnday has no accepted answers">0%</span></p></div></div><div id="comments-container-37739" class="comments-container"><span id="37756"></span><div id="comment-37756" class="comment"><div id="post-37756-score" class="comment-score"></div><div class="comment-text"><p>I have been able to play AMR RTP packets in the RTP player using the opencore-amr library. That library is distributed under the apache licence, so I assume that code can't be committed to the public sources.</p></div><div id="comment-37756-info" class="comment-info"><span class="comment-age">(11 Nov '14, 08:16)</span> <span class="comment-user userinfo">MartinM</span></div></div></div><div id="comment-tools-37739" class="comment-tools"></div><div class="clear"></div><div id="comment-37739-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

