+++
type = "question"
title = "cannot decode RTP and SKINNY VOIP capture"
description = '''Hi all, I hope someone can help, I have a Cisco uc540 system with a Ciscosf300 24 POE switch. I have interface gig 4 setup as a span port to SPAN vlan 100 (the voice VLAN). I can see plenty of rtp and SKINNY traffic, but when i go to telephony and voip, I can see the call, the from and to numbers, t...'''
date = "2012-12-13T04:03:00Z"
lastmod = "2012-12-13T05:12:00Z"
weight = 16831
keywords = [ "rtp", "voip", "decoding" ]
aliases = [ "/questions/16831" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [cannot decode RTP and SKINNY VOIP capture](/questions/16831/cannot-decode-rtp-and-skinny-voip-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16831-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16831-score" class="post-score" title="current number of votes">0</div><span id="post-16831-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>I hope someone can help, I have a Cisco uc540 system with a Ciscosf300 24 POE switch. I have interface gig 4 setup as a span port to SPAN vlan 100 (the voice VLAN).</p><p>I can see plenty of rtp and SKINNY traffic, but when i go to telephony and voip, I can see the call, the from and to numbers, the call will not decode. Have tried going to edit, preferences, protocols, rtp and capture packets outside of conversation.</p><p>Also i have tried setting a completely seperate system, a Cisco 2800 router and a catylyst switch and i still cannot decode calls.</p><p>All calls i am trying to decode are internal as this system is pre deployment.</p><p>I have also tried using a hub, still cannot decode.</p><p>Any help would be much appreciated.</p><p>I can provide captures if anyone was willing to look.</p><p>Regards</p><p>Owen</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span> <span class="post-tag tag-link-decoding" rel="tag" title="see questions tagged &#39;decoding&#39;">decoding</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Dec '12, 04:03</strong></p><img src="https://secure.gravatar.com/avatar/96438e751204008dce96e0588444d2b8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="owenjkirwan&#39;s gravatar image" /><p><span>owenjkirwan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="owenjkirwan has no accepted answers">0%</span></p></div></div><div id="comments-container-16831" class="comments-container"><span id="16833"></span><div id="comment-16833" class="comment"><div id="post-16833-score" class="comment-score"></div><div class="comment-text"><p>Put a capture on CloudShark if you can.</p></div><div id="comment-16833-info" class="comment-info"><span class="comment-age">(13 Dec '12, 05:12)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-16831" class="comment-tools"></div><div class="clear"></div><div id="comment-16831-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

