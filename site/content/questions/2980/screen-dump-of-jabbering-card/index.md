+++
type = "question"
title = "Screen dump of Jabbering card"
description = '''Hi all, I&#x27;m trying to find a screendump of a wiresharks output for a jabbering card. For a course I&#x27;m writing, has anyone got one? Any help would be much apprechiated Thanks Julian'''
date = "2011-03-21T14:48:00Z"
lastmod = "2011-03-21T16:30:00Z"
weight = 2980
keywords = [ "jabbering" ]
aliases = [ "/questions/2980" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Screen dump of Jabbering card](/questions/2980/screen-dump-of-jabbering-card)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2980-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2980-score" class="post-score" title="current number of votes">0</div><span id="post-2980-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>I'm trying to find a screendump of a wiresharks output for a jabbering card. For a course I'm writing, has anyone got one?</p><p>Any help would be much apprechiated</p><p>Thanks</p><p>Julian</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-jabbering" rel="tag" title="see questions tagged &#39;jabbering&#39;">jabbering</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Mar '11, 14:48</strong></p><img src="https://secure.gravatar.com/avatar/97e01f86ca254ba6243063079e09b7a2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Julianh&#39;s gravatar image" /><p><span>Julianh</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Julianh has no accepted answers">0%</span></p></div></div><div id="comments-container-2980" class="comments-container"></div><div id="comment-tools-2980" class="comment-tools"></div><div class="clear"></div><div id="comment-2980-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2992"></span>

<div id="answer-container-2992" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2992-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2992-score" class="post-score" title="current number of votes">0</div><span id="post-2992-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Is this kind of what you're looking for?</p><p><a href="http://www.synerity.com/images/jabber.png" title="jabber.png">jabber.png</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Mar '11, 16:30</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Mar '11, 16:33</strong> </span></p></div></div><div id="comments-container-2992" class="comments-container"></div><div id="comment-tools-2992" class="comment-tools"></div><div class="clear"></div><div id="comment-2992-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

