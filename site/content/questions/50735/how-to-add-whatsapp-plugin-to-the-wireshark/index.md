+++
type = "question"
title = "How to add  whatsapp plugin to the wireshark??"
description = '''Hi all,  I downloaded the whatsapp dissector plugin into the github repo.After that they said to make .so file to add wireshark repo.But they didn&#x27;t provide any detailed information about the whatsapp plugin make.If anyone else knows about this,Please post me the solution. I am doing research in wir...'''
date = "2016-03-06T23:09:00Z"
lastmod = "2016-04-03T11:18:00Z"
weight = 50735
keywords = [ "libwireshark", "whatsapp", "plugins", "dissector", "wireshark" ]
aliases = [ "/questions/50735" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [How to add whatsapp plugin to the wireshark??](/questions/50735/how-to-add-whatsapp-plugin-to-the-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50735-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50735-score" class="post-score" title="current number of votes">1</div><span id="post-50735-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all, I downloaded the whatsapp dissector plugin into the github repo.After that they said to make .so file to add wireshark repo.But they didn't provide any detailed information about the whatsapp plugin make.If anyone else knows about this,Please post me the solution. I am doing research in wireshark.</p><p>Thank you,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-libwireshark" rel="tag" title="see questions tagged &#39;libwireshark&#39;">libwireshark</span> <span class="post-tag tag-link-whatsapp" rel="tag" title="see questions tagged &#39;whatsapp&#39;">whatsapp</span> <span class="post-tag tag-link-plugins" rel="tag" title="see questions tagged &#39;plugins&#39;">plugins</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Mar '16, 23:09</strong></p><img src="https://secure.gravatar.com/avatar/8a669421eea30a71c4677fff8b0c5734?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rathnaTech&#39;s gravatar image" /><p><span>rathnaTech</span><br />
<span class="score" title="31 reputation points">31</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rathnaTech has no accepted answers">0%</span></p></div></div><div id="comments-container-50735" class="comments-container"></div><div id="comment-tools-50735" class="comment-tools"></div><div class="clear"></div><div id="comment-50735-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50749"></span>

<div id="answer-container-50749" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50749-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50749-score" class="post-score" title="current number of votes">1</div><span id="post-50749-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="rathnaTech has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Did you read the <a href="https://github.com/davidgfnet/wireshark-whatsapp/blob/master/INSTALL">INSTALL file</a>? It seems that it explains all the steps and even installs the library for you. (The README has Windows instructions.)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Mar '16, 06:31</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-50749" class="comments-container"><span id="50761"></span><div id="comment-50761" class="comment"><div id="post-50761-score" class="comment-score">1</div><div class="comment-text"><p>Thank you jeffMorriss, i red but i didn't get any idea how to configure.They said,just create a build repo and make 'cmake' command.I tried a lot.But there is no progress.please provide the clear steps as how can i add a whatsapp plugin to the wireshark.</p></div><div id="comment-50761-info" class="comment-info"><span class="comment-age">(07 Mar '16, 23:01)</span> <span class="comment-user userinfo">rathnaTech</span></div></div><span id="50787"></span><div id="comment-50787" class="comment"><div id="post-50787-score" class="comment-score">1</div><div class="comment-text"><p>What do you mean by configure? What do you mean there's no progress if you use cmake? The build fails? It never finishes? Keep in mind it takes quite a while to compile Wireshark.</p></div><div id="comment-50787-info" class="comment-info"><span class="comment-age">(09 Mar '16, 10:16)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div><span id="51343"></span><div id="comment-51343" class="comment"><div id="post-51343-score" class="comment-score"></div><div class="comment-text"><p>Hi,i use mac os x and i build with success the plugin whatsapp.so... but when i put it in the plugin folders the app crash at startup.. it's the same for you?</p></div><div id="comment-51343-info" class="comment-info"><span class="comment-age">(01 Apr '16, 06:19)</span> <span class="comment-user userinfo">cicciovo</span></div></div><span id="51386"></span><div id="comment-51386" class="comment"><div id="post-51386-score" class="comment-score"></div><div class="comment-text"><p><span>@rathnaTech</span> do u have build whatsapp.so? if not i can help you</p></div><div id="comment-51386-info" class="comment-info"><span class="comment-age">(03 Apr '16, 11:18)</span> <span class="comment-user userinfo">cicciovo</span></div></div></div><div id="comment-tools-50749" class="comment-tools"></div><div class="clear"></div><div id="comment-50749-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

