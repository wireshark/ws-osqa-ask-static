+++
type = "question"
title = "Exporting sniffed packets"
description = '''Hi every one. I have a WLAN captured packets that i know it contains deauthentication attacks. I would like to cluster the packets to see if i can have the attacks in one cluster and the normal packets in another cluster. I want to use the inbuild k-means in SPSS. How can i export these packets to t...'''
date = "2014-07-19T14:42:00Z"
lastmod = "2014-07-19T15:25:00Z"
weight = 34780
keywords = [ "thanks" ]
aliases = [ "/questions/34780" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Exporting sniffed packets](/questions/34780/exporting-sniffed-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34780-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34780-score" class="post-score" title="current number of votes">0</div><span id="post-34780-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi every one. I have a WLAN captured packets that i know it contains deauthentication attacks. I would like to cluster the packets to see if i can have the attacks in one cluster and the normal packets in another cluster. I want to use the inbuild k-means in SPSS. How can i export these packets to that environment to perform clustering on them? Is there any other alternative of seperating(quantifying) the attacks from the normal packets?</p><p>Thanks. Maigana.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-thanks" rel="tag" title="see questions tagged &#39;thanks&#39;">thanks</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jul '14, 14:42</strong></p><img src="https://secure.gravatar.com/avatar/e3778ae8a621767b52cdf5a8052a93c4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Maigana&#39;s gravatar image" /><p><span>Maigana</span><br />
<span class="score" title="11 reputation points">11</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Maigana has no accepted answers">0%</span></p></div></div><div id="comments-container-34780" class="comments-container"><span id="34782"></span><div id="comment-34782" class="comment"><div id="post-34782-score" class="comment-score"></div><div class="comment-text"><p>Presumably you don't want the raw packet data from each packet, you want particular fields (addresses, packet types, time stamps, etc.) from each of the packets, right?</p></div><div id="comment-34782-info" class="comment-info"><span class="comment-age">(19 Jul '14, 15:25)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-34780" class="comment-tools"></div><div class="clear"></div><div id="comment-34780-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

