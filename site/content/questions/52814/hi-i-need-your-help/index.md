+++
type = "question"
title = "[closed] hi i need your help"
description = '''when i want to add an account the xps viewer said to me that the xps viewer can not activate any rights management accounts on this machine'''
date = "2016-05-21T07:17:00Z"
lastmod = "2016-05-21T07:17:00Z"
weight = 52814
keywords = [ "89898" ]
aliases = [ "/questions/52814" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] hi i need your help](/questions/52814/hi-i-need-your-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52814-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52814-score" class="post-score" title="current number of votes">0</div><span id="post-52814-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>when i want to add an account the xps viewer said to me that the xps viewer can not activate any rights management accounts on this machine</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-89898" rel="tag" title="see questions tagged &#39;89898&#39;">89898</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 May '16, 07:17</strong></p><img src="https://secure.gravatar.com/avatar/138f5b604bf1ee2b32fac8007f0b9440?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mod89&#39;s gravatar image" /><p><span>mod89</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mod89 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>21 May '16, 12:22</strong> </span></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span></p></div></div><div id="comments-container-52814" class="comments-container"></div><div id="comment-tools-52814" class="comment-tools"></div><div class="clear"></div><div id="comment-52814-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant Sorry, this is a Q&A site for Wireshark, not other products." by JeffMorriss 21 May '16, 12:22

</div>

</div>

</div>

