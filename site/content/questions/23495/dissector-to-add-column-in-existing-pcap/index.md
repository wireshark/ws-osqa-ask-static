+++
type = "question"
title = "Dissector to add column in existing pcap"
description = '''how to insert extra column in existing pcap to show some information into that'''
date = "2013-08-01T04:02:00Z"
lastmod = "2013-08-01T07:33:00Z"
weight = 23495
keywords = [ "dissector" ]
aliases = [ "/questions/23495" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Dissector to add column in existing pcap](/questions/23495/dissector-to-add-column-in-existing-pcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23495-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23495-score" class="post-score" title="current number of votes">1</div><span id="post-23495-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how to insert extra column in existing pcap to show some information into that</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Aug '13, 04:02</strong></p><img src="https://secure.gravatar.com/avatar/bcee72ced4763fe4b835a94e861d5115?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gst&#39;s gravatar image" /><p><span>gst</span><br />
<span class="score" title="26 reputation points">26</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gst has no accepted answers">0%</span></p></div></div><div id="comments-container-23495" class="comments-container"></div><div id="comment-tools-23495" class="comment-tools"></div><div class="clear"></div><div id="comment-23495-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="23503"></span>

<div id="answer-container-23503" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23503-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23503-score" class="post-score" title="current number of votes">2</div><span id="post-23503-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Dissectors can't add columns. They are (are supposed to be) (mostly) UI-agnostic.</p><p>But users can. And users can add a "custom column" which contains any field that a dissector has created (that is, anything that was added with an hf_ variable).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Aug '13, 07:33</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-23503" class="comments-container"></div><div id="comment-tools-23503" class="comment-tools"></div><div class="clear"></div><div id="comment-23503-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

