+++
type = "question"
title = "[closed] Master (by Research) involving wireshark and packet and ipv6 and security topics"
description = '''Hi, I would like to do Master (by Research) involving all these topics wireshark, packet analysis, IPv6, security. Right now I&#x27;m trying to build my conceptual domain model and all ideas and opinions from you guys are really appreciate. :)))'''
date = "2015-05-21T11:00:00Z"
lastmod = "2015-05-21T11:37:00Z"
weight = 42605
keywords = [ "security", "ipv6", "research" ]
aliases = [ "/questions/42605" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Master (by Research) involving wireshark and packet and ipv6 and security topics](/questions/42605/master-by-research-involving-wireshark-and-packet-and-ipv6-and-security-topics)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42605-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42605-score" class="post-score" title="current number of votes">0</div><span id="post-42605-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I would like to do Master (by Research) involving all these topics wireshark, packet analysis, IPv6, security. Right now I'm trying to build my conceptual domain model and all ideas and opinions from you guys are really appreciate. :)))</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-security" rel="tag" title="see questions tagged &#39;security&#39;">security</span> <span class="post-tag tag-link-ipv6" rel="tag" title="see questions tagged &#39;ipv6&#39;">ipv6</span> <span class="post-tag tag-link-research" rel="tag" title="see questions tagged &#39;research&#39;">research</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 May '15, 11:00</strong></p><img src="https://secure.gravatar.com/avatar/0a3865cbc361c1abb797fdc4062f3dd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="awi216&#39;s gravatar image" /><p><span>awi216</span><br />
<span class="score" title="10 reputation points">10</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="awi216 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>21 May '15, 11:38</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-42605" class="comments-container"><span id="42606"></span><div id="comment-42606" class="comment"><div id="post-42606-score" class="comment-score">1</div><div class="comment-text"><p>To quote the very first item of the <a href="https://ask.wireshark.org/faq/">FAQ</a>:</p><p><strong>This is a discussion forum, right?</strong><br />
No. This is a Q&amp;A site.</p><p>You might want to discuss such matters on mailing lists, message boards or other such places, but this is certainly not the right forum.</p></div><div id="comment-42606-info" class="comment-info"><span class="comment-age">(21 May '15, 11:37)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-42605" class="comment-tools"></div><div class="clear"></div><div id="comment-42605-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "There is no actual question here, but merely a request for comments and thus does not fit with the format of this site." by cmaynard 21 May '15, 11:38

</div>

</div>

</div>

