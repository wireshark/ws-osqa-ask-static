+++
type = "question"
title = "RSSI and Injection rate"
description = '''Good day everyone. I&#x27;m currently working onsome pcap files and want to add a column for RSSI and injection rate in wireshark. How can i do it? Thanks in anticipation.'''
date = "2014-08-22T07:53:00Z"
lastmod = "2014-08-22T08:16:00Z"
weight = 35675
keywords = [ "column" ]
aliases = [ "/questions/35675" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [RSSI and Injection rate](/questions/35675/rssi-and-injection-rate)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35675-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35675-score" class="post-score" title="current number of votes">0</div><span id="post-35675-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Good day everyone. I'm currently working onsome pcap files and want to add a column for RSSI and injection rate in wireshark. How can i do it? Thanks in anticipation.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-column" rel="tag" title="see questions tagged &#39;column&#39;">column</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Aug '14, 07:53</strong></p><img src="https://secure.gravatar.com/avatar/e3778ae8a621767b52cdf5a8052a93c4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Maigana&#39;s gravatar image" /><p><span>Maigana</span><br />
<span class="score" title="11 reputation points">11</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Maigana has no accepted answers">0%</span></p></div></div><div id="comments-container-35675" class="comments-container"></div><div id="comment-tools-35675" class="comment-tools"></div><div class="clear"></div><div id="comment-35675-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35677"></span>

<div id="answer-container-35677" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35677-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35677-score" class="post-score" title="current number of votes">1</div><span id="post-35677-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If it's something you can find as a value in the decode you can right click on that field and select "Apply as Column".</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Aug '14, 08:16</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-35677" class="comments-container"></div><div id="comment-tools-35677" class="comment-tools"></div><div class="clear"></div><div id="comment-35677-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

