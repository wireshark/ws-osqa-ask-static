+++
type = "question"
title = "How to resolve point code to a name"
description = '''I&#x27;m using Wireshark Version 1.12.0 and is there a possibility to resolve point code to a name like we can do for IP addresses?'''
date = "2014-08-20T05:04:00Z"
lastmod = "2014-08-20T07:16:00Z"
weight = 35621
keywords = [ "pointcode", "ss7", "isup", "nameresolution" ]
aliases = [ "/questions/35621" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to resolve point code to a name](/questions/35621/how-to-resolve-point-code-to-a-name)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35621-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35621-score" class="post-score" title="current number of votes">0</div><span id="post-35621-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm using Wireshark Version 1.12.0 and is there a possibility to resolve point code to a name like we can do for IP addresses?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pointcode" rel="tag" title="see questions tagged &#39;pointcode&#39;">pointcode</span> <span class="post-tag tag-link-ss7" rel="tag" title="see questions tagged &#39;ss7&#39;">ss7</span> <span class="post-tag tag-link-isup" rel="tag" title="see questions tagged &#39;isup&#39;">isup</span> <span class="post-tag tag-link-nameresolution" rel="tag" title="see questions tagged &#39;nameresolution&#39;">nameresolution</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Aug '14, 05:04</strong></p><img src="https://secure.gravatar.com/avatar/ae1c8541a8ceea7f431af220f39fd5f6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="swarup_network_rookie&#39;s gravatar image" /><p><span>swarup_netwo...</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="swarup_network_rookie has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> retagged <strong>21 Aug '14, 13:52</strong> </span></p><img src="https://secure.gravatar.com/avatar/57dca282828fcb7b6086c0a77af93ca5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Edmond&#39;s gravatar image" /><p><span>Edmond</span><br />
<span class="score" title="181 reputation points">181</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span></p></div></div><div id="comments-container-35621" class="comments-container"></div><div id="comment-tools-35621" class="comment-tools"></div><div class="clear"></div><div id="comment-35621-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35628"></span>

<div id="answer-container-35628" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35628-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35628-score" class="post-score" title="current number of votes">2</div><span id="post-35628-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, although there is an <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=7592">enhancement request out there</a> to request the functionality. No one has worked on it yet though (as far as I know).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Aug '14, 07:16</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-35628" class="comments-container"></div><div id="comment-tools-35628" class="comment-tools"></div><div class="clear"></div><div id="comment-35628-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

