+++
type = "question"
title = "How to highlight the &quot;Data&quot; segment of packets?"
description = '''Hey there! To be able to investigate only differences in data of TCP packets, but the bytes before of other protocols like Internet Protocol make the packets difficult to read. Can I filter out just the bytes of all other protocols and leave the data bytes to read alone? When I activate the column C...'''
date = "2011-05-31T09:02:00Z"
lastmod = "2011-05-31T09:02:00Z"
weight = 4295
keywords = [ "data", "protocol", "packets", "packet", "wireshark" ]
aliases = [ "/questions/4295" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to highlight the "Data" segment of packets?](/questions/4295/how-to-highlight-the-data-segment-of-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4295-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4295-score" class="post-score" title="current number of votes">0</div><span id="post-4295-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey there!</p><p>To be able to investigate only differences in data of TCP packets, but the bytes before of other protocols like Internet Protocol make the packets difficult to read.</p><p>Can I filter out just the bytes of all other protocols and leave the data bytes to read alone?</p><p>When I activate the column Custom() = data.data, then it cuts the bytes after 30 or so bytes. It is shown like "75b6022eb9460eb9060875ba1438fb11bcc15464b27d59d0...". Can I fix this?</p><p>Or can I just automatically highlight the data bytes, so without clicking on the "Data (xx bytes)" entry of each packet?</p><p>Greets</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span> <span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 May '11, 09:02</strong></p><img src="https://secure.gravatar.com/avatar/42dba7472f1f6575664b96ec0fbed2ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hawk&#39;s gravatar image" /><p><span>Hawk</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hawk has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>31 May '11, 09:02</strong> </span></p></div></div><div id="comments-container-4295" class="comments-container"></div><div id="comment-tools-4295" class="comment-tools"></div><div class="clear"></div><div id="comment-4295-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

