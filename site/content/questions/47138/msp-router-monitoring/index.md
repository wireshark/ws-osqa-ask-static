+++
type = "question"
title = "MSP Router Monitoring"
description = '''Hi First post to be gentle :) I work for a telecoms reseller and we are looking to provide a service in which we can monitor clients routers/lines to see if they are down. I was looking for advice to see if there is a suitable product out there to allow me to do this ? I was hoping wireshark could. ...'''
date = "2015-11-02T02:04:00Z"
lastmod = "2015-11-02T04:37:00Z"
weight = 47138
keywords = [ "remote-monitoring" ]
aliases = [ "/questions/47138" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [MSP Router Monitoring](/questions/47138/msp-router-monitoring)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47138-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47138-score" class="post-score" title="current number of votes">0</div><span id="post-47138-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi First post to be gentle :)</p><p>I work for a telecoms reseller and we are looking to provide a service in which we can monitor clients routers/lines to see if they are down. I was looking for advice to see if there is a suitable product out there to allow me to do this ? I was hoping wireshark could.</p><p>The only equipment onsite which we can access is a router and again these can be 800 series cisco drayteks netgear etc.</p><p>Advice is appreciated</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-remote-monitoring" rel="tag" title="see questions tagged &#39;remote-monitoring&#39;">remote-monitoring</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Nov '15, 02:04</strong></p><img src="https://secure.gravatar.com/avatar/09c7fb631b95549834a1d90f12b54eeb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="CommsFM&#39;s gravatar image" /><p><span>CommsFM</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="CommsFM has no accepted answers">0%</span></p></div></div><div id="comments-container-47138" class="comments-container"></div><div id="comment-tools-47138" class="comment-tools"></div><div class="clear"></div><div id="comment-47138-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="47152"></span>

<div id="answer-container-47152" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47152-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47152-score" class="post-score" title="current number of votes">0</div><span id="post-47152-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p><strong>provide a service</strong> in which we can <strong>monitor clients routers/lines</strong> to see <strong>if they are down</strong><br />
I was hoping wireshark could.</p></blockquote><p>no it can't. Wireshark is <strong>not</strong> a network monitoring tool, it's a network analysis and troubleshooting tool.</p><p>So, what you are really looking for is something like:</p><blockquote><p><a href="https://www.icinga.org/">https://www.icinga.org/</a><br />
<a href="http://www.cacti.net/">http://www.cacti.net/</a><br />
<a href="http://oss.oetiker.ch/smokeping/">http://oss.oetiker.ch/smokeping/</a><br />
</p></blockquote><p>or any similar tool. There are tons and tons of these out there. Just google for:</p><blockquote><p><a href="https://www.google.com/#q=network+monitoring+tools">https://www.google.com/#q=network+monitoring+tools</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Nov '15, 04:37</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Nov '15, 04:37</strong> </span></p></div></div><div id="comments-container-47152" class="comments-container"></div><div id="comment-tools-47152" class="comment-tools"></div><div class="clear"></div><div id="comment-47152-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

