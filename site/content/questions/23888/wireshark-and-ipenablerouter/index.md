+++
type = "question"
title = "Wireshark and IPEnableRouter"
description = '''I&#x27;m developing a ARP spoofing tool for Windows and during my tests I turned on the IPEnableRouter option (which is basically the Windows version of the linux IP Forward option).  It worked out great (the victim could access the internet), but I realized that if I started a capture in Wireshark the v...'''
date = "2013-08-20T18:07:00Z"
lastmod = "2013-08-21T03:27:00Z"
weight = 23888
keywords = [ "ip", "windows7", "forwarding", "arpspoofing" ]
aliases = [ "/questions/23888" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark and IPEnableRouter](/questions/23888/wireshark-and-ipenablerouter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23888-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23888-score" class="post-score" title="current number of votes">0</div><span id="post-23888-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm developing a ARP spoofing tool for Windows and during my tests I turned on the IPEnableRouter option (which is basically the Windows version of the linux IP Forward option).</p><p>It worked out great (the victim could access the internet), but I realized that if I started a capture in Wireshark the victim of the ARP spoofing would loose it's connection to the Internet. Repeated the whole thing a few times and I got the same result on all of them.</p><p>Does anybody know what could be happening and why?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-forwarding" rel="tag" title="see questions tagged &#39;forwarding&#39;">forwarding</span> <span class="post-tag tag-link-arpspoofing" rel="tag" title="see questions tagged &#39;arpspoofing&#39;">arpspoofing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Aug '13, 18:07</strong></p><img src="https://secure.gravatar.com/avatar/99d0d825bd23c5dda4b75085f5e2cc9e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Andr%C3%A9%20Louren%C3%A7o&#39;s gravatar image" /><p><span>André Lourenço</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="André Lourenço has no accepted answers">0%</span></p></div></div><div id="comments-container-23888" class="comments-container"><span id="23900"></span><div id="comment-23900" class="comment"><div id="post-23900-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I'm developing a ARP spoofing tool for Windows</p></blockquote><p>What do you spoof? The MAC address of the Internet router?</p><blockquote><p>during my tests I turned on the IPEnableRouter option</p></blockquote><p>How many interfaces are connected to the network on your spoofing system?</p><blockquote><p>but I realized that if I started a capture in Wireshark the victim of the ARP spoofing would loose it's connection to the Internet.</p></blockquote><p>In that case, what do you see in the capture file?</p></div><div id="comment-23900-info" class="comment-info"><span class="comment-age">(21 Aug '13, 03:27)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-23888" class="comment-tools"></div><div class="clear"></div><div id="comment-23888-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

