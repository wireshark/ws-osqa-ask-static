+++
type = "question"
title = "I/O Graph Auto-Scroll"
description = '''In the past, I/O graphs used to auto-scroll. How do I make this happen with Wireshark 1.8.4? Setting the main packet list to auto-scroll has no effect.'''
date = "2013-01-11T08:14:00Z"
lastmod = "2013-01-11T09:07:00Z"
weight = 17605
keywords = [ "graph" ]
aliases = [ "/questions/17605" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [I/O Graph Auto-Scroll](/questions/17605/io-graph-auto-scroll)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17605-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17605-score" class="post-score" title="current number of votes">0</div><span id="post-17605-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In the past, I/O graphs used to auto-scroll. How do I make this happen with Wireshark 1.8.4? Setting the main packet list to auto-scroll has no effect.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jan '13, 08:14</strong></p><img src="https://secure.gravatar.com/avatar/41d461566d4749dd44ab0205e7f4ab73?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dpward&#39;s gravatar image" /><p><span>dpward</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dpward has no accepted answers">0%</span></p></div></div><div id="comments-container-17605" class="comments-container"></div><div id="comment-tools-17605" class="comment-tools"></div><div class="clear"></div><div id="comment-17605-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17607"></span>

<div id="answer-container-17607" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17607-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17607-score" class="post-score" title="current number of votes">0</div><span id="post-17607-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Currently, I/O graphs do not auto-scroll. This is a known bug, reported in July 2012. If you want to encourage someone to tackle the fix, you might want to go <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=7442">here</a> and vote for it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jan '13, 09:07</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-17607" class="comments-container"></div><div id="comment-tools-17607" class="comment-tools"></div><div class="clear"></div><div id="comment-17607-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

