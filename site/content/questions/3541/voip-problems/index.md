+++
type = "question"
title = "VoIP problems..."
description = '''Dears, I&#x27;ve been using VoIP phone for long time wwithout problems. However, lately (since 2 months) the other side almost can&#x27;t hear me at all. I troublshooted everything without sucess, till I finally tried a different internet connection, and my VoIP worked fine there. I started using Wireshark to...'''
date = "2011-04-17T16:58:00Z"
lastmod = "2011-04-18T13:24:00Z"
weight = 3541
keywords = [ "rtp", "voip" ]
aliases = [ "/questions/3541" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [VoIP problems...](/questions/3541/voip-problems)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3541-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3541-score" class="post-score" title="current number of votes">0</div><span id="post-3541-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dears,</p><p>I've been using VoIP phone for long time wwithout problems. However, lately (since 2 months) the other side almost can't hear me at all. I troublshooted everything without sucess, till I finally tried a different internet connection, and my VoIP worked fine there.</p><p>I started using Wireshark to diagnose my internet problem and found some really "bad" stuff in the upstreaming RTP. However, I still don't know what exactly the problem is.</p><p>Please take a look and tell me what you think. This is a test call.</p><p>http://tinypic.com/r/d5iea/7</p><p>Many thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Apr '11, 16:58</strong></p><img src="https://secure.gravatar.com/avatar/e037ed9e692bb2f16671255a3fa470b7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Salloum&#39;s gravatar image" /><p><span>Salloum</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Salloum has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>17 Apr '11, 17:19</strong> </span></p></div></div><div id="comments-container-3541" class="comments-container"><span id="3583"></span><div id="comment-3583" class="comment"><div id="post-3583-score" class="comment-score"></div><div class="comment-text"><p>All dups, could be your mirror setup.</p></div><div id="comment-3583-info" class="comment-info"><span class="comment-age">(18 Apr '11, 13:24)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-3541" class="comment-tools"></div><div class="clear"></div><div id="comment-3541-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

