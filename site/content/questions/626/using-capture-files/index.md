+++
type = "question"
title = "Using Capture File(s)"
description = '''When the &quot;use multiple files&quot; box is check how do you tell WireShark the different names of the files you want to save?'''
date = "2010-10-25T12:56:00Z"
lastmod = "2010-10-25T13:49:00Z"
weight = 626
keywords = [ "multiple-files", "capture-file" ]
aliases = [ "/questions/626" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Using Capture File(s)](/questions/626/using-capture-files)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-626-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-626-score" class="post-score" title="current number of votes">0</div><span id="post-626-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When the "use multiple files" box is check how do you tell WireShark the different names of the files you want to save?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-multiple-files" rel="tag" title="see questions tagged &#39;multiple-files&#39;">multiple-files</span> <span class="post-tag tag-link-capture-file" rel="tag" title="see questions tagged &#39;capture-file&#39;">capture-file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Oct '10, 12:56</strong></p><img src="https://secure.gravatar.com/avatar/99b52aba1b4c99c567a89ada178dba99?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AMCCSupport&#39;s gravatar image" /><p><span>AMCCSupport</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AMCCSupport has no accepted answers">0%</span></p></div></div><div id="comments-container-626" class="comments-container"></div><div id="comment-tools-626" class="comment-tools"></div><div class="clear"></div><div id="comment-626-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="629"></span>

<div id="answer-container-629" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-629-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-629-score" class="post-score" title="current number of votes">1</div><span id="post-629-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>When you select Use multiple files, you have to specify a file name, otherwise you get an error message.<br />
<br />
You can add a file name just above "Use multiple files"<br />
File: Add filename like MultipleFiles.pcap<br />
<br />
Every output file gets a number and a timestamp.<br />
The output files will look like:<br />
First file:<br />
MultipleFiles_00001_20101025221518.pcap<br />
Second file:<br />
MultipleFiles_00002_20101025221618.pcap<br />
Third file:<br />
MultipleFiles_00003_20101025221718.pcap<br />
etc..<br />
<br />
You can find more information in the Wireshark User's guide:<br />
<a href="http://www.wireshark.org/docs/wsug_html_chunked/ChCapCaptureOptions.html">Capture Options</a><br />
<a href="http://www.wireshark.org/docs/wsug_html_chunked/ChCapCaptureFiles.html">Capture files and file modes</a><br />
<a href="http://www.wireshark.org/docs/wsug_html_chunked/ChIOFileSetSection.html">File Sets</a><br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Oct '10, 13:49</strong></p><img src="https://secure.gravatar.com/avatar/fac200552b0c24be2bc93a740bd54d0d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joke&#39;s gravatar image" /><p><span>joke</span><br />
<span class="score" title="1278 reputation points"><span>1.3k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joke has 6 accepted answers">9%</span> </br></br></p></div></div><div id="comments-container-629" class="comments-container"></div><div id="comment-tools-629" class="comment-tools"></div><div class="clear"></div><div id="comment-629-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

