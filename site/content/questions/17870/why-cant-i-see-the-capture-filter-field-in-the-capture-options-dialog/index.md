+++
type = "question"
title = "Why can&#x27;t I see the capture filter field in the capture options dialog?"
description = '''I&#x27;m attempting to apply a capture filter to only capture packets to/from a particular IP: HOST 201.3.72.34 I&#x27;ve used the capture filters dialog to add a new filter. When I open the capture options dialog there is no field to select or apply capture filters. The user guide contains a screenshot of th...'''
date = "2013-01-22T14:28:00Z"
lastmod = "2013-01-22T14:57:00Z"
weight = 17870
keywords = [ "capture-filter" ]
aliases = [ "/questions/17870" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why can't I see the capture filter field in the capture options dialog?](/questions/17870/why-cant-i-see-the-capture-filter-field-in-the-capture-options-dialog)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17870-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17870-score" class="post-score" title="current number of votes">0</div><span id="post-17870-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm attempting to apply a capture filter to only capture packets to/from a particular IP:</p><p>HOST 201.3.72.34</p><p>I've used the capture filters dialog to add a new filter. When I open the capture options dialog there is no field to select or apply capture filters. The user guide contains a screenshot of the field but it's not in my version: <a href="http://www.wireshark.org/docs/wsug_html_chunked/ChCapCaptureOptions.html#ChCapCaptureOptionsDialog">http://www.wireshark.org/docs/wsug_html_chunked/ChCapCaptureOptions.html#ChCapCaptureOptionsDialog</a></p><p>I'm using Wireshark Version 1.8.4 (SVN Rev 46250 from /trunk-1.8).</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jan '13, 14:28</strong></p><img src="https://secure.gravatar.com/avatar/4a9aa51cd6a651718560c42f2a61ea75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Daniel%20Wright&#39;s gravatar image" /><p><span>Daniel Wright</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Daniel Wright has no accepted answers">0%</span></p></div></div><div id="comments-container-17870" class="comments-container"></div><div id="comment-tools-17870" class="comment-tools"></div><div class="clear"></div><div id="comment-17870-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17872"></span>

<div id="answer-container-17872" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17872-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17872-score" class="post-score" title="current number of votes">0</div><span id="post-17872-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It has moved, see: <a href="http://ask.wireshark.org/questions/12452/where-are-the-capture-filter-options-in-wireshark-180">http://ask.wireshark.org/questions/12452/where-are-the-capture-filter-options-in-wireshark-180</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Jan '13, 14:57</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-17872" class="comments-container"></div><div id="comment-tools-17872" class="comment-tools"></div><div class="clear"></div><div id="comment-17872-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

