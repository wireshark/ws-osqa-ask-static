+++
type = "question"
title = "[closed] error sending message to whatsapp"
description = '''I am a member of a whatsapp group. My name is present in participants list. But when I send any message I get the error can&#x27;t send messages to this group not a member. Friends also say message has not arrived. Having no clue.'''
date = "2016-04-01T12:07:00Z"
lastmod = "2016-04-01T12:07:00Z"
weight = 51355
keywords = [ "whatsapp" ]
aliases = [ "/questions/51355" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] error sending message to whatsapp](/questions/51355/error-sending-message-to-whatsapp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51355-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51355-score" class="post-score" title="current number of votes">0</div><span id="post-51355-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am a member of a whatsapp group. My name is present in participants list. But when I send any message I get the error can't send messages to this group not a member. Friends also say message has not arrived. Having no clue.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-whatsapp" rel="tag" title="see questions tagged &#39;whatsapp&#39;">whatsapp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Apr '16, 12:07</strong></p><img src="https://secure.gravatar.com/avatar/499fdc7ae0131e457531c84a47a403e8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Saptak%20Das&#39;s gravatar image" /><p><span>Saptak Das</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Saptak Das has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Jul '16, 15:51</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-51355" class="comments-container"></div><div id="comment-tools-51355" class="comment-tools"></div><div class="clear"></div><div id="comment-51355-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jasper 01 Apr '16, 12:57

</div>

</div>

</div>

