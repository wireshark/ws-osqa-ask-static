+++
type = "question"
title = "Start making commands for tshark"
description = '''My PC is running windows 7 and I opened the tshark.exe from the wireshark installation file in my computer folder. The program kept recurring messages. I am a beginner in using tshark and i would like to start making commands. How do i start making commands on tshark?'''
date = "2012-04-16T01:05:00Z"
lastmod = "2012-04-16T03:07:00Z"
weight = 10172
keywords = [ "commands", "tshark" ]
aliases = [ "/questions/10172" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Start making commands for tshark](/questions/10172/start-making-commands-for-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10172-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10172-score" class="post-score" title="current number of votes">0</div><span id="post-10172-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My PC is running windows 7 and I opened the tshark.exe from the wireshark installation file in my computer folder. The program kept recurring messages. I am a beginner in using tshark and i would like to start making commands. How do i start making commands on tshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-commands" rel="tag" title="see questions tagged &#39;commands&#39;">commands</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Apr '12, 01:05</strong></p><img src="https://secure.gravatar.com/avatar/94990dfa38fcf1b33157bef842da0291?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="misteryuku&#39;s gravatar image" /><p><span>misteryuku</span><br />
<span class="score" title="20 reputation points">20</span><span title="24 badges"><span class="badge1">●</span><span class="badgecount">24</span></span><span title="26 badges"><span class="silver">●</span><span class="badgecount">26</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="misteryuku has no accepted answers">0%</span></p></div></div><div id="comments-container-10172" class="comments-container"><span id="10180"></span><div id="comment-10180" class="comment"><div id="post-10180-score" class="comment-score">1</div><div class="comment-text"><p>MisterYuku, do me a favor and STOP creating new questions that are in the same topic as one you have already started. And do not answer your own questions with answers if it is in fact a comment (I just converted your answer to a comment).</p><p>People here will help you, but on their own time. Pushing out new question will not speed up things - more likely it will annoy the others who then will not answer at all. Patience, young padawan.</p></div><div id="comment-10180-info" class="comment-info"><span class="comment-age">(16 Apr '12, 03:02)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-10172" class="comment-tools"></div><div class="clear"></div><div id="comment-10172-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10181"></span>

<div id="answer-container-10181" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10181-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10181-score" class="post-score" title="current number of votes">2</div><span id="post-10181-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You should open a command prompt first: Press the Window Key and R, and enter "cmd" (enter).</p><p>Then change to the Wireshark directory by entering <code>cd \\program files\\wireshark</code>. There, you can now enter "<code>tshark</code>" and work with the additional parameters that usually need to be put behind the command, for example <code>"tshark -r capture.pcap"</code>. Entering <code>"tshark -h"</code> will print the command help.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Apr '12, 03:07</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Apr '12, 08:51</strong> </span></p></div></div><div id="comments-container-10181" class="comments-container"></div><div id="comment-tools-10181" class="comment-tools"></div><div class="clear"></div><div id="comment-10181-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

