+++
type = "question"
title = "Wave Short Message P1609.3"
description = '''Hi, i am wondering about implementation in wireshark. By starting wireshark(1.6) on channel 178 we take 2 codha boxes. On captured traffic i see: malform Packet: [WSMP] ... It would be helpful to get implementation with more details. e.g MsgCut,ID,SecMark,Lat,Long,Elev and so on. Best Regards, carhs...'''
date = "2011-07-21T07:50:00Z"
lastmod = "2011-07-21T07:50:00Z"
weight = 5155
keywords = [ "car2x", "security", "sniffer" ]
aliases = [ "/questions/5155" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wave Short Message P1609.3](/questions/5155/wave-short-message-p16093)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5155-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5155-score" class="post-score" title="current number of votes">0</div><span id="post-5155-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, i am wondering about implementation in wireshark. By starting wireshark(1.6) on channel 178 we take 2 codha boxes. On captured traffic i see: malform Packet: [WSMP] ... It would be helpful to get implementation with more details. e.g MsgCut,ID,SecMark,Lat,Long,Elev and so on.</p><p>Best Regards,</p><p>carhs.communication GmbH</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-car2x" rel="tag" title="see questions tagged &#39;car2x&#39;">car2x</span> <span class="post-tag tag-link-security" rel="tag" title="see questions tagged &#39;security&#39;">security</span> <span class="post-tag tag-link-sniffer" rel="tag" title="see questions tagged &#39;sniffer&#39;">sniffer</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jul '11, 07:50</strong></p><img src="https://secure.gravatar.com/avatar/29651699f158165ec2b628123b9481db?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lordofdarkness&#39;s gravatar image" /><p><span>lordofdarkness</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lordofdarkness has no accepted answers">0%</span></p></div></div><div id="comments-container-5155" class="comments-container"></div><div id="comment-tools-5155" class="comment-tools"></div><div class="clear"></div><div id="comment-5155-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

