+++
type = "question"
title = "Wireshark Cannot See Neighbor Packet In Same Network With Netgear Nighthawk Router"
description = '''I have a very basic and common question. I have 2 laptops connected to a netgear nighthawk, both in the same subnet (192.168.1.x /24) When I capture packet, I cannot see what websites the 2nd laptop has visited. It only shows the ipv6 and no source ipaddress of the 2nd laptop. Could it be my netgear...'''
date = "2016-01-23T16:26:00Z"
lastmod = "2016-01-23T17:38:00Z"
weight = 49486
keywords = [ "subnet", "capture", "packet", "neighbor", "same" ]
aliases = [ "/questions/49486" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark Cannot See Neighbor Packet In Same Network With Netgear Nighthawk Router](/questions/49486/wireshark-cannot-see-neighbor-packet-in-same-network-with-netgear-nighthawk-router)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49486-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49486-score" class="post-score" title="current number of votes">0</div><span id="post-49486-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a very basic and common question. I have 2 laptops connected to a netgear nighthawk, both in the same subnet (192.168.1.x /24) When I capture packet, I cannot see what websites the 2nd laptop has visited. It only shows the ipv6 and no source ipaddress of the 2nd laptop. Could it be my netgear nighthawk wireless router does not support, or do I need a high end network equipment like Cisco??</p><p>jason chang</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-subnet" rel="tag" title="see questions tagged &#39;subnet&#39;">subnet</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-neighbor" rel="tag" title="see questions tagged &#39;neighbor&#39;">neighbor</span> <span class="post-tag tag-link-same" rel="tag" title="see questions tagged &#39;same&#39;">same</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jan '16, 16:26</strong></p><img src="https://secure.gravatar.com/avatar/49405b72256e704da4c9aabf06718853?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jason%20chang&#39;s gravatar image" /><p><span>jason chang</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jason chang has no accepted answers">0%</span></p></div></div><div id="comments-container-49486" class="comments-container"><span id="49487"></span><div id="comment-49487" class="comment"><div id="post-49487-score" class="comment-score"></div><div class="comment-text"><p>Have you read the wireshark wiki about WLAN capture setup, especially the section about the monitor mode? <a href="https://wiki.wireshark.org/CaptureSetup/WLAN">https://wiki.wireshark.org/CaptureSetup/WLAN</a></p></div><div id="comment-49487-info" class="comment-info"><span class="comment-age">(23 Jan '16, 17:38)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-49486" class="comment-tools"></div><div class="clear"></div><div id="comment-49486-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

