+++
type = "question"
title = "rtsp...sdp, question concerning streaming radio"
description = '''My phone (Samsung Solstice II) can only stream music from the internet that is in the following format rtsp://64.202.98.91:554/gs64.sdp (this is Soma Groove Salad). I have used Wireshark to track down the address of different internet radio stations, but I can&#x27;t figure what to put in for the object ...'''
date = "2011-10-29T08:57:00Z"
lastmod = "2011-10-29T11:40:00Z"
weight = 7144
keywords = [ "streaming", "rtsp", "sdp" ]
aliases = [ "/questions/7144" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [rtsp...sdp, question concerning streaming radio](/questions/7144/rtspsdp-question-concerning-streaming-radio)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7144-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7144-score" class="post-score" title="current number of votes">0</div><span id="post-7144-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My phone (Samsung Solstice II) can only stream music from the internet that is in the following format rtsp://64.202.98.91:554/gs64.sdp (this is Soma Groove Salad). I have used Wireshark to track down the address of different internet radio stations, but I can't figure what to put in for the object (like the "gs64" for groove salad). Do all radio stations broadcast in this manner, and it's just up to me to figure it out? Or do only some radio stations broadcast this way? (like soma, and radio paradise).</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-streaming" rel="tag" title="see questions tagged &#39;streaming&#39;">streaming</span> <span class="post-tag tag-link-rtsp" rel="tag" title="see questions tagged &#39;rtsp&#39;">rtsp</span> <span class="post-tag tag-link-sdp" rel="tag" title="see questions tagged &#39;sdp&#39;">sdp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Oct '11, 08:57</strong></p><img src="https://secure.gravatar.com/avatar/f3608237363a77718c6582a2f95ee601?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="boogsey&#39;s gravatar image" /><p><span>boogsey</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="boogsey has no accepted answers">0%</span></p></div></div><div id="comments-container-7144" class="comments-container"></div><div id="comment-tools-7144" class="comment-tools"></div><div class="clear"></div><div id="comment-7144-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7147"></span>

<div id="answer-container-7147" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7147-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7147-score" class="post-score" title="current number of votes">0</div><span id="post-7147-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's the complete URL that defines the media stream, not just the address. So, by definition, there is no default name for the SDP object. It has to come from some external source.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Oct '11, 11:40</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-7147" class="comments-container"></div><div id="comment-tools-7147" class="comment-tools"></div><div class="clear"></div><div id="comment-7147-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

