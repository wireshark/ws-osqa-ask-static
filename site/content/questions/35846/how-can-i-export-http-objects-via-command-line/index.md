+++
type = "question"
title = "How can I export HTTP Objects via command line?"
description = '''I routinely have large pcap files that I need to export the HTTP objects from. How can I do this via command line? Using the GUI is a very slow process with large files.'''
date = "2014-08-28T10:41:00Z"
lastmod = "2016-12-15T16:52:00Z"
weight = 35846
keywords = [ "export-http", "command-line" ]
aliases = [ "/questions/35846" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [How can I export HTTP Objects via command line?](/questions/35846/how-can-i-export-http-objects-via-command-line)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35846-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35846-score" class="post-score" title="current number of votes">0</div><span id="post-35846-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I routinely have large pcap files that I need to export the HTTP objects from. How can I do this via command line? Using the GUI is a very slow process with large files.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-export-http" rel="tag" title="see questions tagged &#39;export-http&#39;">export-http</span> <span class="post-tag tag-link-command-line" rel="tag" title="see questions tagged &#39;command-line&#39;">command-line</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Aug '14, 10:41</strong></p><img src="https://secure.gravatar.com/avatar/8f18b0151cb67cf196ab006ef68afba1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="stom&#39;s gravatar image" /><p><span>stom</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="stom has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Aug '14, 10:41</strong> </span></p></div></div><div id="comments-container-35846" class="comments-container"><span id="35848"></span><div id="comment-35848" class="comment"><div id="post-35848-score" class="comment-score"></div><div class="comment-text"><p>see this: <a href="https://ask.wireshark.org/questions/35689/export-http-packet-from-pcap-file">https://ask.wireshark.org/questions/35689/export-http-packet-from-pcap-file</a></p></div><div id="comment-35848-info" class="comment-info"><span class="comment-age">(28 Aug '14, 12:22)</span> <span class="comment-user userinfo">Babyy</span></div></div><span id="35871"></span><div id="comment-35871" class="comment"><div id="post-35871-score" class="comment-score"></div><div class="comment-text"><p>I'm actually looking to export the object themselves to a folder, not just have them placed into another PCAP that I'll then still have to open and manually export them from. Any way to do this? The proposed solution might make the files smaller and easier to deal with but still isn't the automation I was looking for.</p></div><div id="comment-35871-info" class="comment-info"><span class="comment-age">(29 Aug '14, 07:54)</span> <span class="comment-user userinfo">stom</span></div></div></div><div id="comment-tools-35846" class="comment-tools"></div><div class="clear"></div><div id="comment-35846-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="35896"></span>

<div id="answer-container-35896" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35896-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35896-score" class="post-score" title="current number of votes">0</div><span id="post-35896-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please see my answers to similar questions:</p><blockquote><p><a href="https://ask.wireshark.org/questions/26959/if-tshark-can-support-export-objects-like-wireshark-for-gui">https://ask.wireshark.org/questions/26959/if-tshark-can-support-export-objects-like-wireshark-for-gui</a><br />
<a href="https://ask.wireshark.org/questions/15560/headless-automate-export-object-when-capturing-packeting/15564">https://ask.wireshark.org/questions/15560/headless-automate-export-object-when-capturing-packeting/15564</a><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Aug '14, 04:28</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-35896" class="comments-container"></div><div id="comment-tools-35896" class="comment-tools"></div><div class="clear"></div><div id="comment-35896-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="58157"></span>

<div id="answer-container-58157" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58157-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58157-score" class="post-score" title="current number of votes">0</div><span id="post-58157-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As of Wireshark 2.3.0, you can export HTTP objects with tshark. (Wireshark 2.3.0 hasn't been released yet, so you can grab a daily build from <a href="https://www.wireshark.org/download/automated/win32/">here</a>.)</p><p>To extract HTTP objects from the command-line, run the following command:</p><pre><code>tshark -r mypcap.pcap --export-objects &quot;http,destdir&quot;</code></pre></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Dec '16, 16:52</strong></p><img src="https://secure.gravatar.com/avatar/8df259c952186aa93179732461b8d1e7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="moshe&#39;s gravatar image" /><p><span>moshe</span><br />
<span class="score" title="21 reputation points">21</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="moshe has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-58157" class="comments-container"></div><div id="comment-tools-58157" class="comment-tools"></div><div class="clear"></div><div id="comment-58157-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

