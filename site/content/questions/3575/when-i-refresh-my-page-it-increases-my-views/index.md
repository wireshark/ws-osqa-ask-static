+++
type = "question"
title = "When I refresh my page it increases my views!!"
description = '''Please look into this flaw.'''
date = "2011-04-18T09:39:00Z"
lastmod = "2011-04-18T18:32:00Z"
weight = 3575
keywords = [ "meta" ]
aliases = [ "/questions/3575" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [When I refresh my page it increases my views!!](/questions/3575/when-i-refresh-my-page-it-increases-my-views)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3575-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3575-score" class="post-score" title="current number of votes">0</div><span id="post-3575-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Please look into this flaw.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-meta" rel="tag" title="see questions tagged &#39;meta&#39;">meta</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Apr '11, 09:39</strong></p><img src="https://secure.gravatar.com/avatar/e26c7ebb23eae3f6b8a22c85915807f0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bruce&#39;s gravatar image" /><p><span>Bruce</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bruce has no accepted answers">0%</span></p></div></div><div id="comments-container-3575" class="comments-container"><span id="3584"></span><div id="comment-3584" class="comment"><div id="post-3584-score" class="comment-score">1</div><div class="comment-text"><p>What flaw?</p></div><div id="comment-3584-info" class="comment-info"><span class="comment-age">(18 Apr '11, 13:31)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="3593"></span><div id="comment-3593" class="comment"><div id="post-3593-score" class="comment-score"></div><div class="comment-text"><p>Presumably he means "if I go to a page for a question, every time I refresh it, it counts as another view of the question".</p></div><div id="comment-3593-info" class="comment-info"><span class="comment-age">(18 Apr '11, 18:32)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-3575" class="comment-tools"></div><div class="clear"></div><div id="comment-3575-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

