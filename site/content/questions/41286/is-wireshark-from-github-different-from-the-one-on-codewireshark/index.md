+++
type = "question"
title = "Is Wireshark from GitHub different from the one on code.wireshark?"
description = '''Is there any difference between a cloned copy of Wireshark from GitHub or from code.wireshark? I guess they both could be same but I need to get this confirmed because I&#x27;m still new to the world of CVS and open source as well. I&#x27;ve had already a look at the documentation and I found no thing mention...'''
date = "2015-04-08T07:16:00Z"
lastmod = "2015-04-08T08:47:00Z"
weight = 41286
keywords = [ "git", "clone", "github", "wireshark" ]
aliases = [ "/questions/41286" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Is Wireshark from GitHub different from the one on code.wireshark?](/questions/41286/is-wireshark-from-github-different-from-the-one-on-codewireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41286-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41286-score" class="post-score" title="current number of votes">0</div><span id="post-41286-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there any difference between a cloned copy of Wireshark from <a href="https://github.com/wireshark/wireshark">GitHub</a> or from <a href="https://code.wireshark.org/review/gitweb?p=wireshark.git;a=tree">code.wireshark</a>?</p><p>I guess they both could be same but I need to get this confirmed because I'm still new to the world of CVS and open source as well. I've had already a look at the <a href="https://www.wireshark.org/docs/wsdg_html_chunked/ChSrcGitRepository.html">documentation</a> and I found no thing mentioned about Wireshark's Github.</p><p>Context: I've a repository with GitHub and I want to clone/fork a copy from Wireshark. I'm working on a dissector which I'm planing to commit, so I want to keep my private source tree updated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-git" rel="tag" title="see questions tagged &#39;git&#39;">git</span> <span class="post-tag tag-link-clone" rel="tag" title="see questions tagged &#39;clone&#39;">clone</span> <span class="post-tag tag-link-github" rel="tag" title="see questions tagged &#39;github&#39;">github</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Apr '15, 07:16</strong></p><img src="https://secure.gravatar.com/avatar/5642d9fe33d29ee47043f7e5796e67aa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="flora&#39;s gravatar image" /><p><span>flora</span><br />
<span class="score" title="156 reputation points">156</span><span title="31 badges"><span class="badge1">●</span><span class="badgecount">31</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="38 badges"><span class="bronze">●</span><span class="badgecount">38</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="flora has 2 accepted answers">100%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Apr '15, 07:35</strong> </span></p></div></div><div id="comments-container-41286" class="comments-container"></div><div id="comment-tools-41286" class="comment-tools"></div><div class="clear"></div><div id="comment-41286-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41289"></span>

<div id="answer-container-41289" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41289-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41289-score" class="post-score" title="current number of votes">1</div><span id="post-41289-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="flora has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Github repository is a read only clone of the one found on code.wireshark.org. You can fork the one on GitHub if you want but keep in mind (as indicated on Github) that pull requests will be ignored and that only patch sets submitted to code.wireshark.org will be accepted.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Apr '15, 08:15</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-41289" class="comments-container"><span id="41293"></span><div id="comment-41293" class="comment"><div id="post-41293-score" class="comment-score"></div><div class="comment-text"><p>...so you might want to setup at code.wireshark.org, using an OpenID, and clone from there.</p></div><div id="comment-41293-info" class="comment-info"><span class="comment-age">(08 Apr '15, 08:47)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-41289" class="comment-tools"></div><div class="clear"></div><div id="comment-41289-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

