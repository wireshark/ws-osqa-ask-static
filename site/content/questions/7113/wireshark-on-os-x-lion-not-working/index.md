+++
type = "question"
title = "Wireshark on OS X Lion, not working"
description = '''I have OS X Lion, up-to-date with patches, on a 32-bit Mac Pro. It installs fine, but does not launch. Does anyone know what the problem is? Thanks.'''
date = "2011-10-27T16:39:00Z"
lastmod = "2011-10-27T18:29:00Z"
weight = 7113
keywords = [ "osx", "lion", "32-bit", "install" ]
aliases = [ "/questions/7113" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark on OS X Lion, not working](/questions/7113/wireshark-on-os-x-lion-not-working)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7113-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7113-score" class="post-score" title="current number of votes">0</div><span id="post-7113-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have OS X Lion, up-to-date with patches, on a 32-bit Mac Pro. It installs fine, but does not launch.</p><p>Does anyone know what the problem is?</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-lion" rel="tag" title="see questions tagged &#39;lion&#39;">lion</span> <span class="post-tag tag-link-32-bit" rel="tag" title="see questions tagged &#39;32-bit&#39;">32-bit</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Oct '11, 16:39</strong></p><img src="https://secure.gravatar.com/avatar/d73db50356f554982fa4549e4c70a936?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="forrie&#39;s gravatar image" /><p><span>forrie</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="forrie has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Feb '12, 20:03</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-7113" class="comments-container"><span id="7119"></span><div id="comment-7119" class="comment"><div id="post-7119-score" class="comment-score"></div><div class="comment-text"><p>ask.wireshark.org is not a traditional forum or chat. Please refrain from asking the same question multiple times (see <a href="faq">http://ask.wireshark.org/faq/</a>). You will not get an answer faster by asking multiple times very quickly.</p></div><div id="comment-7119-info" class="comment-info"><span class="comment-age">(27 Oct '11, 16:58)</span> <span class="comment-user userinfo">multipleinte...</span></div></div><span id="7120"></span><div id="comment-7120" class="comment"><div id="post-7120-score" class="comment-score"></div><div class="comment-text"><p>There is no such thing as "a 32-bit Mac Pro" - the Mac Pro was the last Mac to switch to Intel, and that happened after Apple switched from the 32-bit Intel Core processors to the 64-bit Intel Core 2 processors. (And, in any case, Lion doesn't support 32-bit processors, at least not without some hacking.)</p><p>Did you install the 32-bit Intel version of Wireshark, as built for and on Leopard? If so, what happens if you install the 64-bit Intel version?</p></div><div id="comment-7120-info" class="comment-info"><span class="comment-age">(27 Oct '11, 18:29)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-7113" class="comment-tools"></div><div class="clear"></div><div id="comment-7113-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

