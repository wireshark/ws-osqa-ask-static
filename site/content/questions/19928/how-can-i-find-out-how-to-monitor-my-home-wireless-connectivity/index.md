+++
type = "question"
title = "How can I FIND OUT HOW TO monitor my home wireless connectivity?"
description = '''No-one has answered my question (below) in the four days since I posted it, maybe because it&#x27;s not DIRECTLY relevant to Wireshark. If that&#x27;s the case, I&#x27;m sorry - is there another more general website where I might get an answer? My home wireless connection seems to fail every few minutes for a few ...'''
date = "2013-03-29T07:04:00Z"
lastmod = "2013-04-02T03:39:00Z"
weight = 19928
keywords = [ "wireless", "connectivity", "monitor", "router" ]
aliases = [ "/questions/19928" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [How can I FIND OUT HOW TO monitor my home wireless connectivity?](/questions/19928/how-can-i-find-out-how-to-monitor-my-home-wireless-connectivity)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19928-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19928-score" class="post-score" title="current number of votes">0</div><span id="post-19928-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>No-one has answered my question (below) in the four days since I posted it, maybe because it's not DIRECTLY relevant to Wireshark. If that's the case, I'm sorry - is there another more general website where I might get an answer?</p><p>My home wireless connection seems to fail every few minutes for a few seconds. Mostly it reconnects without my intervention, but sometimes I have to go to the Wireless Network Connection dialog box and click "Connect" or "Repair". It's very annoying, especially when it happens during on-line chat or shopping. I changed my wireless router but the problem remains. What can I do? My ISP hasn't been able to help. My PC is running Windows XP (Professional).</p><p>If you're kind enough to reply, please bear in mind that what I know about computer and network technology could be written on the back of a postage stamp. No jargon please!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-connectivity" rel="tag" title="see questions tagged &#39;connectivity&#39;">connectivity</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span> <span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Mar '13, 07:04</strong></p><img src="https://secure.gravatar.com/avatar/162c8d3005ec782cf5938abc98b986f3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AntwerpSmerle&#39;s gravatar image" /><p><span>AntwerpSmerle</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AntwerpSmerle has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Apr '13, 23:02</strong> </span></p></div></div><div id="comments-container-19928" class="comments-container"></div><div id="comment-tools-19928" class="comment-tools"></div><div class="clear"></div><div id="comment-19928-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="20005"></span>

<div id="answer-container-20005" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20005-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20005-score" class="post-score" title="current number of votes">0</div><span id="post-20005-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If your connection fails that often even after replacing your router you could still have a problem with your WiFi equipment of your PC, so you could try to see if other devices have the same problem when connected to your access point.</p><p>On the other hand there are other reasons why you lose connectivity, and sometimes they're not that easy to find, but what you could do is to use a WLAN detection tool like <a href="http://www.metageek.net/blog/2012/12/inssider3-preview/">InSSIDer</a> to find out how many wireless networks are active on the same channel as yours. If there are too many different access points sending on the same frequency you'll have a hard time staying connected. Same goes for other devices that operate on the same frequency bands as WiFi, e.g. microwave ovens and other non-IT devices. See <a href="http://en.wikipedia.org/wiki/Electromagnetic_interference_at_2.4_GHz">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Apr '13, 00:43</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-20005" class="comments-container"></div><div id="comment-tools-20005" class="comment-tools"></div><div class="clear"></div><div id="comment-20005-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="20012"></span>

<div id="answer-container-20012" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20012-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20012-score" class="post-score" title="current number of votes">0</div><span id="post-20012-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You might want to use Event Viewer to see if there are any logged events to indicate the problem, or possibly view logs on your access point. Your best bet to isolate the problem is to either use your laptop at another known good site - an internet cafe or a friends home and see if the problem travels with your laptop. Alternatively invite a friend over to your place with a laptop or smartphone and watch youtube over coffee for a few hours and see if they have the same problem as you. I know this doesn't seem technical but it is straightforward method of problem determination. Then you have narrowed where to look for the issue.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Apr '13, 03:39</strong></p><img src="https://secure.gravatar.com/avatar/57fbbe2a1e14ccc2a681a28886e5a484?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="martyvis&#39;s gravatar image" /><p><span>martyvis</span><br />
<span class="score" title="891 reputation points">891</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="martyvis has 5 accepted answers">7%</span></p></div></div><div id="comments-container-20012" class="comments-container"></div><div id="comment-tools-20012" class="comment-tools"></div><div class="clear"></div><div id="comment-20012-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

