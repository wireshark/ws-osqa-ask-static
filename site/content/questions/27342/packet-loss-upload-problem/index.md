+++
type = "question"
title = "Packet Loss? Upload problem?"
description = '''Hi guys, I am not a expert in the subject but tomorrow I will meet up with the technicians of the internet provider where I live... There are more than 300 apartments in the same building over here and we used wired connection. The problem is the instability of our internet, it keeps going down ever...'''
date = "2013-11-25T06:03:00Z"
lastmod = "2013-11-25T07:19:00Z"
weight = 27342
keywords = [ "loss", "problem", "upload", "packet", "internet" ]
aliases = [ "/questions/27342" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Packet Loss? Upload problem?](/questions/27342/packet-loss-upload-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27342-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27342-score" class="post-score" title="current number of votes">0</div><span id="post-27342-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi guys, I am not a expert in the subject but tomorrow I will meet up with the technicians of the internet provider where I live... There are more than 300 apartments in the same building over here and we used wired connection. The problem is the instability of our internet, it keeps going down every 5-10 min , it's terrible!! We complained about that and they said that it was because we are using Wi-Fi routers, which I believe it's not the case, so, everybody removed the routers and the Internet still terrible!</p><p>As far I noticed, when I'm using whatsapp messenger, I can receive messages from my friends, but I can't send message, they keep saying "are you there?", I can receive/read their messages but I can't send mine! It's the same when I'm on skype, in a video calling and the internet "goes down", I'm talking with them, and I can hear them, I can see them, but they can not listen or see me and after a few minutes the call goes down, they keep saying "are you there?" and moving in front of the camera, I can see and hear everything, but they can't. Is it a problem with the upload? Since I can't send packet (they can't hear or see me) but I can receive packets (I can hear them or read them).</p><p>What do you guys think? What should I do? It's terrible over here, we lost connectivity many times, so I need a point to argue with the Internet provider tomorrow</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-loss" rel="tag" title="see questions tagged &#39;loss&#39;">loss</span> <span class="post-tag tag-link-problem" rel="tag" title="see questions tagged &#39;problem&#39;">problem</span> <span class="post-tag tag-link-upload" rel="tag" title="see questions tagged &#39;upload&#39;">upload</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-internet" rel="tag" title="see questions tagged &#39;internet&#39;">internet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Nov '13, 06:03</strong></p><img src="https://secure.gravatar.com/avatar/fd07d02b3af66745b21cca093bca7334?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Thiago%20Fernandes&#39;s gravatar image" /><p><span>Thiago Ferna...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Thiago Fernandes has no accepted answers">0%</span></p></div></div><div id="comments-container-27342" class="comments-container"><span id="27344"></span><div id="comment-27344" class="comment"><div id="post-27344-score" class="comment-score"></div><div class="comment-text"><p>how is your skyscraper connected to the internet via that provider? Is there only one provider link for the whole building, or are there several links to different providers.</p><p>Please provide as much <strong>technical</strong> information as possible.</p></div><div id="comment-27344-info" class="comment-info"><span class="comment-age">(25 Nov '13, 06:08)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="27349"></span><div id="comment-27349" class="comment"><div id="post-27349-score" class="comment-score"></div><div class="comment-text"><p>Thank you for your help Kurt... Yes, there is only one provider link for the whole building.</p><p>But I don't know how my skyscraper is connected to the internet, is there a way to know that? Sorry, I'm not pro as you're.</p><p>If you need more information, please ask me.</p><p>Thank you for your help.</p></div><div id="comment-27349-info" class="comment-info"><span class="comment-age">(25 Nov '13, 06:36)</span> <span class="comment-user userinfo">Thiago Ferna...</span></div></div></div><div id="comment-tools-27342" class="comment-tools"></div><div class="clear"></div><div id="comment-27342-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27353"></span>

<div id="answer-container-27353" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27353-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27353-score" class="post-score" title="current number of votes">0</div><span id="post-27353-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>But I don't know how my skyscraper is connected to the internet, is there a way to know that? Sorry, I'm not pro as you're.</p></blockquote><p>I think you can't literally 'follow' the cables, so the only way is to ask the provider.</p><p>BTW: If you (or any other person in the building) have not enough technical knowledge, you should try to find a person <strong>with</strong> that knowledge to assist you in the meeting with the provider, otherwise they will tell you whatever they want and you won't be able to prove if it is right or wrong.</p><blockquote><p>What should I do? It's terrible over here, we lost connectivity many times, so I need a point to argue with the Internet provider tomorrow</p></blockquote><p>The answer to your question is: find a technically skilled person that will assist you in the meeting with the provider.</p><p>Additionally you can ask the provider to prove that the link is stable, by attaching a test system (PC or whatever) directly to the provider router and to test internet connections through that.</p><p><strong>Please consider</strong> if you really have 300 apartments, using the same internet link, you'll need a BIG FAT pipe to the internet to get a decent surfing experience, especially if there are primarily private households in the building. People tend to stream radio and video at home without any thought. With only 30-50 households doing that, they would be able to overload the internet link. However, that's a structural problem that is hard to solve, especially if the provider sold you all an unlimited flat rate, because then he cannot enable QoS (traffic shaping) to distribute the available bandwidth in a fair way amongst all households. Then it's more like a "first come, first serve" strategy and all the rest: www == world wide wait ;-)</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Nov '13, 07:19</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Nov '13, 07:19</strong> </span></p></div></div><div id="comments-container-27353" class="comments-container"></div><div id="comment-tools-27353" class="comment-tools"></div><div class="clear"></div><div id="comment-27353-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

