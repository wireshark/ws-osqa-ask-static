+++
type = "question"
title = "List of servers that send amf packet"
description = '''I have written a dissector for amf packets. I want to capture it using wireshark. Can any one provide me with the list of servers that send amf packets???????'''
date = "2012-10-23T03:35:00Z"
lastmod = "2012-10-23T05:52:00Z"
weight = 15183
keywords = [ "amf" ]
aliases = [ "/questions/15183" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [List of servers that send amf packet](/questions/15183/list-of-servers-that-send-amf-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15183-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15183-score" class="post-score" title="current number of votes">0</div><span id="post-15183-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have written a dissector for amf packets. I want to capture it using wireshark. Can any one provide me with the list of servers that send amf packets???????</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-amf" rel="tag" title="see questions tagged &#39;amf&#39;">amf</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Oct '12, 03:35</strong></p><img src="https://secure.gravatar.com/avatar/b0ed262c234b0aa9fae2e5b2d51b14c2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Akhil&#39;s gravatar image" /><p><span>Akhil</span><br />
<span class="score" title="53 reputation points">53</span><span title="27 badges"><span class="badge1">●</span><span class="badgecount">27</span></span><span title="28 badges"><span class="silver">●</span><span class="badgecount">28</span></span><span title="31 badges"><span class="bronze">●</span><span class="badgecount">31</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Akhil has no accepted answers">0%</span></p></div></div><div id="comments-container-15183" class="comments-container"></div><div id="comment-tools-15183" class="comment-tools"></div><div class="clear"></div><div id="comment-15183-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15191"></span>

<div id="answer-container-15191" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15191-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15191-score" class="post-score" title="current number of votes">0</div><span id="post-15191-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>google is your friend:</p><blockquote><p><code>http://bit.ly/S02oUI</code><br />
</p></blockquote><p>leads to <a href="http://media.blackhat.com/bh-us-12/Briefings/Carettoni/BH_US_12_Carettoni_AMF_Testing_WP.pdf">AMF testing made easy</a>, if you mean Action Message Format (AMF).</p><p>Furthermore:</p><blockquote><p><code>http://bit.ly/S03a44</code><br />
</p></blockquote><p>leads to <a href="http://pcapr.net/browse?user=58aa0001a38f45222878d629508f3429">a sample capture file</a>.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Oct '12, 05:52</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-15191" class="comments-container"></div><div id="comment-tools-15191" class="comment-tools"></div><div class="clear"></div><div id="comment-15191-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

