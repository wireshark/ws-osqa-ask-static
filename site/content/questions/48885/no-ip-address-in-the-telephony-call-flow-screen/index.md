+++
type = "question"
title = "No IP address in the Telephony call flow screen"
description = '''When selecting VoIP and then Telephony I see the port numbers but I do not get the IP addresses at the top of the screen like it did in previous versions. Where did the IP address go in the Telephony VoIP flow sequence screen '''
date = "2016-01-05T13:38:00Z"
lastmod = "2016-01-05T14:49:00Z"
weight = 48885
keywords = [ "flow", "voip" ]
aliases = [ "/questions/48885" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [No IP address in the Telephony call flow screen](/questions/48885/no-ip-address-in-the-telephony-call-flow-screen)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48885-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48885-score" class="post-score" title="current number of votes">0</div><span id="post-48885-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When selecting VoIP and then Telephony I see the port numbers but I do not get the IP addresses at the top of the screen like it did in previous versions.</p><p>Where did the IP address go in the Telephony VoIP flow sequence screen</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-flow" rel="tag" title="see questions tagged &#39;flow&#39;">flow</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jan '16, 13:38</strong></p><img src="https://secure.gravatar.com/avatar/5a73a6caa6ab3a0cca195511cf0c8a1d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tadiranjohn&#39;s gravatar image" /><p><span>tadiranjohn</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tadiranjohn has no accepted answers">0%</span></p></div></div><div id="comments-container-48885" class="comments-container"></div><div id="comment-tools-48885" class="comment-tools"></div><div class="clear"></div><div id="comment-48885-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="48886"></span>

<div id="answer-container-48886" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48886-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48886-score" class="post-score" title="current number of votes">0</div><span id="post-48886-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="https://ask.wireshark.org/questions/48657/wireshark-200-doesnt-show-the-node-ips-on-the-sip-call-flow-graph">https://ask.wireshark.org/questions/48657/wireshark-200-doesnt-show-the-node-ips-on-the-sip-call-flow-graph</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Jan '16, 14:33</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-48886" class="comments-container"></div><div id="comment-tools-48886" class="comment-tools"></div><div class="clear"></div><div id="comment-48886-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="48889"></span>

<div id="answer-container-48889" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48889-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48889-score" class="post-score" title="current number of votes">0</div><span id="post-48889-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>From FAQ: Before posting a question, please check the site for similar ones. This one falls to that category, almost the same question has been asked less than two weeks ago. The answer is that the flow diagram in Qt (aka new GUI) version is still WiP and the IP addresses should be back soon.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Jan '16, 14:49</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-48889" class="comments-container"></div><div id="comment-tools-48889" class="comment-tools"></div><div class="clear"></div><div id="comment-48889-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

