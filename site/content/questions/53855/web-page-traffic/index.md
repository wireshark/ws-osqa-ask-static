+++
type = "question"
title = "Web page traffic"
description = '''Hallo, how is it possible to see which web pages are used in the NW from wich PC. Thanks in advance dgross'''
date = "2016-07-06T04:00:00Z"
lastmod = "2016-07-06T04:00:00Z"
weight = 53855
keywords = [ "network" ]
aliases = [ "/questions/53855" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Web page traffic](/questions/53855/web-page-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53855-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53855-score" class="post-score" title="current number of votes">0</div><span id="post-53855-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hallo,</p><p>how is it possible to see which web pages are used in the NW from wich PC.</p><p>Thanks in advance</p><p>dgross</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jul '16, 04:00</strong></p><img src="https://secure.gravatar.com/avatar/976495efc4e7144324eeaa4bd5f7916b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dgross&#39;s gravatar image" /><p><span>dgross</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dgross has no accepted answers">0%</span></p></div></div><div id="comments-container-53855" class="comments-container"></div><div id="comment-tools-53855" class="comment-tools"></div><div class="clear"></div><div id="comment-53855-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

