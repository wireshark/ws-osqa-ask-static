+++
type = "question"
title = "capture file on web http-google101.pcapng not similar to the one in Wireshark101 essentials book"
description = '''It looks like the http-google101.pcapng capture does not look similar to the one in the Wireshark 101 essentials book. Please look at Frames 10 and 11. The book shows as getting valid response but the capture file has lot of TCP segmented PDU information FYI&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;g...'''
date = "2013-04-08T16:07:00Z"
lastmod = "2013-04-08T20:25:00Z"
weight = 20214
keywords = [ "good" ]
aliases = [ "/questions/20214" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [capture file on web http-google101.pcapng not similar to the one in Wireshark101 essentials book](/questions/20214/capture-file-on-web-http-google101pcapng-not-similar-to-the-one-in-wireshark101-essentials-book)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20214-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20214-score" class="post-score" title="current number of votes">0</div><span id="post-20214-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>It looks like the http-google101.pcapng capture does not look similar to the one in the Wireshark 101 essentials book. Please look at Frames 10 and 11. The book shows as getting valid response but the capture file has lot of TCP segmented PDU information</p><p>FYI&gt;&gt;&gt;&gt;&gt;&gt;&gt;&gt;&gt;&gt;&gt;</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-good" rel="tag" title="see questions tagged &#39;good&#39;">good</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Apr '13, 16:07</strong></p><img src="https://secure.gravatar.com/avatar/e4189f27221db61a00a77aba2233a6e0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rikshaw&#39;s gravatar image" /><p><span>rikshaw</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rikshaw has no accepted answers">0%</span></p></div></div><div id="comments-container-20214" class="comments-container"><span id="20215"></span><div id="comment-20215" class="comment"><div id="post-20215-score" class="comment-score"></div><div class="comment-text"><p>never mind!!!!!!!</p></div><div id="comment-20215-info" class="comment-info"><span class="comment-age">(08 Apr '13, 16:08)</span> <span class="comment-user userinfo">rikshaw</span></div></div></div><div id="comment-tools-20214" class="comment-tools"></div><div class="clear"></div><div id="comment-20214-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="20231"></span>

<div id="answer-container-20231" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20231-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20231-score" class="post-score" title="current number of votes">2</div><span id="post-20231-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Go to Edit &gt; Preferences &gt; Protocols &gt; TCP and un-check "Allow subdissector to reassemble TCP streams."</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Apr '13, 20:25</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-20231" class="comments-container"></div><div id="comment-tools-20231" class="comment-tools"></div><div class="clear"></div><div id="comment-20231-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="20216"></span>

<div id="answer-container-20216" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20216-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20216-score" class="post-score" title="current number of votes">1</div><span id="post-20216-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I suggest to ask the author of the book ;-)</p><blockquote><p><code>http://wiresharkbook.com/contactus.html</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Apr '13, 16:10</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-20216" class="comments-container"></div><div id="comment-tools-20216" class="comment-tools"></div><div class="clear"></div><div id="comment-20216-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

