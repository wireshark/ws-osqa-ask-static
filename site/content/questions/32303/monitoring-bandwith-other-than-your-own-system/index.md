+++
type = "question"
title = "Monitoring Bandwith other than your own system?"
description = '''Hi, I&#x27;m trying to find a way to monitor the bandwidth usage on my network using Wireshark. I&#x27;m capturing my LAN traffic however it seems to only capture my own system traffic rather than the traffic of the network. Looking at other forums, this isn&#x27;t outside of what Wireshark&#x27;s capabilities are, is ...'''
date = "2014-04-29T15:17:00Z"
lastmod = "2014-04-29T15:19:00Z"
weight = 32303
keywords = [ "bandwidth", "monitor" ]
aliases = [ "/questions/32303" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Monitoring Bandwith other than your own system?](/questions/32303/monitoring-bandwith-other-than-your-own-system)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32303-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32303-score" class="post-score" title="current number of votes">1</div><span id="post-32303-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I'm trying to find a way to monitor the bandwidth usage on my network using Wireshark. I'm capturing my LAN traffic however it seems to only capture my own system traffic rather than the traffic of the network. Looking at other forums, this isn't outside of what Wireshark's capabilities are, is there soemthing I'm missing?</p><p>-Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bandwidth" rel="tag" title="see questions tagged &#39;bandwidth&#39;">bandwidth</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Apr '14, 15:17</strong></p><img src="https://secure.gravatar.com/avatar/47948efc5fb39b21c2c7b4d24898914b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bubblewhip&#39;s gravatar image" /><p><span>bubblewhip</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bubblewhip has no accepted answers">0%</span></p></div></div><div id="comments-container-32303" class="comments-container"></div><div id="comment-tools-32303" class="comment-tools"></div><div class="clear"></div><div id="comment-32303-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32304"></span>

<div id="answer-container-32304" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32304-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32304-score" class="post-score" title="current number of votes">3</div><span id="post-32304-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I think you need to take a look at this: <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">http://wiki.wireshark.org/CaptureSetup/Ethernet</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Apr '14, 15:19</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-32304" class="comments-container"></div><div id="comment-tools-32304" class="comment-tools"></div><div class="clear"></div><div id="comment-32304-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

