+++
type = "question"
title = "Interpret IEC61850 MMS Message"
description = '''Hi all, Does anybody know how to interpret IEC 61850 MMS packets by adding dissector in Wireshark v2.0.4?  Here is the traffic I captured thru SEL RTAC communication monitor. They are not shown in the MMS protocol as I wanted. I had some hard time interpret them into ISO/IEC MMS protocol. Hope I can...'''
date = "2016-07-20T09:34:00Z"
lastmod = "2016-07-20T09:34:00Z"
weight = 54192
keywords = [ "mms" ]
aliases = [ "/questions/54192" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Interpret IEC61850 MMS Message](/questions/54192/interpret-iec61850-mms-message)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54192-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54192-score" class="post-score" title="current number of votes">0</div><span id="post-54192-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>Does anybody know how to interpret IEC 61850 MMS packets by adding dissector in Wireshark v2.0.4?</p><p>Here is the <a href="https://www.cloudshark.org/captures/6078f26a0d2a">traffic</a> I captured thru SEL RTAC communication monitor. They are not shown in the MMS protocol as I wanted.</p><p>I had some hard time interpret them into ISO/IEC MMS protocol. Hope I can get rescue here from some experts!</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mms" rel="tag" title="see questions tagged &#39;mms&#39;">mms</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jul '16, 09:34</strong></p><img src="https://secure.gravatar.com/avatar/6ae8a8d97d75bb45a23fd939daab36ff?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jli63&#39;s gravatar image" /><p><span>jli63</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jli63 has no accepted answers">0%</span></p></div></div><div id="comments-container-54192" class="comments-container"></div><div id="comment-tools-54192" class="comment-tools"></div><div class="clear"></div><div id="comment-54192-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

