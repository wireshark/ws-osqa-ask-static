+++
type = "question"
title = "No interface can be used for capturing in this system with current configuration? error message received"
description = '''Does anyone know how to correct this. Ive had Wireshark my whole semester and suddenly it stopped working. Luckily the class is over but I still want to use it. Thanks for any help.'''
date = "2014-08-05T15:45:00Z"
lastmod = "2014-08-19T10:35:00Z"
weight = 35234
keywords = [ "interface", "configuration" ]
aliases = [ "/questions/35234" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [No interface can be used for capturing in this system with current configuration? error message received](/questions/35234/no-interface-can-be-used-for-capturing-in-this-system-with-current-configuration-error-message-received)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35234-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35234-score" class="post-score" title="current number of votes">0</div><span id="post-35234-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does anyone know how to correct this. Ive had Wireshark my whole semester and suddenly it stopped working. Luckily the class is over but I still want to use it. Thanks for any help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-configuration" rel="tag" title="see questions tagged &#39;configuration&#39;">configuration</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Aug '14, 15:45</strong></p><img src="https://secure.gravatar.com/avatar/5d0bdd778a054eb4e309f69fed2ff07b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="IMMAShark&#39;s gravatar image" /><p><span>IMMAShark</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="IMMAShark has no accepted answers">0%</span></p></div></div><div id="comments-container-35234" class="comments-container"><span id="35253"></span><div id="comment-35253" class="comment"><div id="post-35253-score" class="comment-score"></div><div class="comment-text"><p>what is your:</p><ul><li>OS and OS version</li><li>Wireshark version</li></ul></div><div id="comment-35253-info" class="comment-info"><span class="comment-age">(06 Aug '14, 01:34)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="35586"></span><div id="comment-35586" class="comment"><div id="post-35586-score" class="comment-score"></div><div class="comment-text"><p>OS X 10.9.4 with Wireshark 1.10.8 Edit I just opened this for the first time since end of semester and I see a update available for X11 I will try the update and see where that leads. I recently downloaded but didn't install tcpdump as an alternative. Don't know much about it.</p><p>Thanks for any help</p></div><div id="comment-35586-info" class="comment-info"><span class="comment-age">(19 Aug '14, 10:35)</span> <span class="comment-user userinfo">IMMAShark</span></div></div></div><div id="comment-tools-35234" class="comment-tools"></div><div class="clear"></div><div id="comment-35234-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

