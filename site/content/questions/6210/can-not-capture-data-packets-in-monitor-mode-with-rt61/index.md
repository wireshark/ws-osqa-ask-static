+++
type = "question"
title = "can not capture data packets in monitor mode with rt61"
description = '''Hello, My adapter is ralink rt61 (pci) and I’m working on centos6. According to CaptureSetup/WLAN, i made a monitor interface and then capture on it but only beacon and probe packets are captured. I have following questions but failed to get answer from Google. Is there anybody can help? Thanks. 1, ...'''
date = "2011-09-08T06:53:00Z"
lastmod = "2011-09-08T06:53:00Z"
weight = 6210
keywords = [ "802.11" ]
aliases = [ "/questions/6210" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [can not capture data packets in monitor mode with rt61](/questions/6210/can-not-capture-data-packets-in-monitor-mode-with-rt61)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6210-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6210-score" class="post-score" title="current number of votes">0</div><span id="post-6210-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>My adapter is ralink rt61 (pci) and I’m working on centos6. According to CaptureSetup/WLAN, i made a monitor interface and then capture on it but only beacon and probe packets are captured. I have following questions but failed to get answer from Google. Is there anybody can help? Thanks.</p><p>1, the wlan0 interface does not support 802.11 link-layer head, is that true? 2, the mon0 interface support IEEE802_11_RADIO not IEEE802_11 3, why data packets can not be captured?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Sep '11, 06:53</strong></p><img src="https://secure.gravatar.com/avatar/435a685c80db753b367bbca1a7481e61?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="metalzong&#39;s gravatar image" /><p><span>metalzong</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="metalzong has no accepted answers">0%</span></p></div></div><div id="comments-container-6210" class="comments-container"></div><div id="comment-tools-6210" class="comment-tools"></div><div class="clear"></div><div id="comment-6210-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

