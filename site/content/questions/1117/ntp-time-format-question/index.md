+++
type = "question"
title = "ntp time format question"
description = '''In wireshark you get back a Transmit &amp;amp; receive timestamp in the format Jul 14, 2008 14:20:52.3856 UTC almost everything is self explanatory but what does the .3856 represent ?  Thanks Barry'''
date = "2010-11-25T02:02:00Z"
lastmod = "2010-11-25T03:33:00Z"
weight = 1117
keywords = [ "ntp" ]
aliases = [ "/questions/1117" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [ntp time format question](/questions/1117/ntp-time-format-question)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1117-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1117-score" class="post-score" title="current number of votes">0</div><span id="post-1117-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In wireshark you get back a Transmit &amp; receive timestamp in the format Jul 14, 2008 14:20:52.3856 UTC almost everything is self explanatory but what does the .3856 represent ? Thanks Barry</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ntp" rel="tag" title="see questions tagged &#39;ntp&#39;">ntp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Nov '10, 02:02</strong></p><img src="https://secure.gravatar.com/avatar/2e3847cf0f87110e857be26e61c8cff3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Barry&#39;s gravatar image" /><p><span>Barry</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Barry has no accepted answers">0%</span></p></div></div><div id="comments-container-1117" class="comments-container"></div><div id="comment-tools-1117" class="comment-tools"></div><div class="clear"></div><div id="comment-1117-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1120"></span>

<div id="answer-container-1120" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1120-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1120-score" class="post-score" title="current number of votes">0</div><span id="post-1120-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The .3856 represent 0.3856 seconds to enhance the resolution of the time stamp. If the resolution was only 1 second, then the clocks would not get synchronized very precisely. Now the clocks can be synchronized with 100 microsecond precision (although the accuracy will be a little less than).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Nov '10, 03:31</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-1120" class="comments-container"><span id="1121"></span><div id="comment-1121" class="comment"><div id="post-1121-score" class="comment-score"></div><div class="comment-text"><p>Thanks, I did wonder that but wanted to be sure</p></div><div id="comment-1121-info" class="comment-info"><span class="comment-age">(25 Nov '10, 03:33)</span> <span class="comment-user userinfo">Barry</span></div></div></div><div id="comment-tools-1120" class="comment-tools"></div><div class="clear"></div><div id="comment-1120-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

