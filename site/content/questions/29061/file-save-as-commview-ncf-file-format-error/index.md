+++
type = "question"
title = "file save as... commview ncf file format error"
description = '''Dear Gentlemen, Whenever I try to save my network Wireshark captures in Commview ncf file format, Wireshark raises an error message: &quot;The capture file appears to be damaged or corrupt. (commview: unsupported encap:15)&quot; Wireshark is version 1.10.5 The OS I&#x27;m using is WIN7 X64, but I also tried the la...'''
date = "2014-01-21T08:54:00Z"
lastmod = "2014-01-21T08:54:00Z"
weight = 29061
keywords = [ "commview", "ncf", "encap", "unsupported" ]
aliases = [ "/questions/29061" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [file save as... commview ncf file format error](/questions/29061/file-save-as-commview-ncf-file-format-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29061-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29061-score" class="post-score" title="current number of votes">0</div><span id="post-29061-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear Gentlemen,</p><p>Whenever I try to save my network Wireshark captures in Commview ncf file format, Wireshark raises an error message: "The capture file appears to be damaged or corrupt. (commview: unsupported encap:15)"</p><p>Wireshark is version 1.10.5</p><p>The OS I'm using is WIN7 X64, but I also tried the latest UBUNTU release, X64 version too, and got the same error.</p><p>Any help fixing this would be much appreciated. Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-commview" rel="tag" title="see questions tagged &#39;commview&#39;">commview</span> <span class="post-tag tag-link-ncf" rel="tag" title="see questions tagged &#39;ncf&#39;">ncf</span> <span class="post-tag tag-link-encap" rel="tag" title="see questions tagged &#39;encap&#39;">encap</span> <span class="post-tag tag-link-unsupported" rel="tag" title="see questions tagged &#39;unsupported&#39;">unsupported</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jan '14, 08:54</strong></p><img src="https://secure.gravatar.com/avatar/2cb1326a87f9165f07f162e63163a004?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="xaimitices&#39;s gravatar image" /><p><span>xaimitices</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="xaimitices has no accepted answers">0%</span></p></div></div><div id="comments-container-29061" class="comments-container"></div><div id="comment-tools-29061" class="comment-tools"></div><div class="clear"></div><div id="comment-29061-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

