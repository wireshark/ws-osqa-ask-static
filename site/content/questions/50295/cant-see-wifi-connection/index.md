+++
type = "question"
title = "can&#x27;t see wifi connection?!"
description = '''Got wireshark installed on my laptop but it can&#x27;t find my wifi, can only see &quot;USBPcap1&quot;, &quot;USBPcap2&quot; and &quot;USBPcap3&quot;. The laptop is connected to my router before I start wireshark and have tested to capture everything on all the USBPcap but aint no traffic over those. windows 10 64bit Wiresshark 2.0.1...'''
date = "2016-02-17T23:44:00Z"
lastmod = "2016-02-17T23:44:00Z"
weight = 50295
keywords = [ "wifi" ]
aliases = [ "/questions/50295" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [can't see wifi connection?!](/questions/50295/cant-see-wifi-connection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50295-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50295-score" class="post-score" title="current number of votes">0</div><span id="post-50295-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Got wireshark installed on my laptop but it can't find my wifi, can only see "USBPcap1", "USBPcap2" and "USBPcap3". The laptop is connected to my router before I start wireshark and have tested to capture everything on all the USBPcap but aint no traffic over those.</p><p>windows 10 64bit Wiresshark 2.0.1</p><p>Really need it to work asap, got homework in networking I need to get done. Have asked my Professor but he couldn't help me.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Feb '16, 23:44</strong></p><img src="https://secure.gravatar.com/avatar/1399814b3f65d301217b1f27a635e04b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="K_D&#39;s gravatar image" /><p><span>K_D</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="K_D has no accepted answers">0%</span></p></div></div><div id="comments-container-50295" class="comments-container"></div><div id="comment-tools-50295" class="comment-tools"></div><div class="clear"></div><div id="comment-50295-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

