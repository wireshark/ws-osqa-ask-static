+++
type = "question"
title = "IOgraph y field"
description = '''Hello, In ws 1.x, it was possible to enter y axis value filter with predictive input (you type tcp.w and and ws completes wth tcp.windows and so on). Now, with ws 2.x we must enter the right filter, without any help. Is that right or I miss something ? Alain'''
date = "2017-05-23T09:55:00Z"
lastmod = "2017-05-23T12:37:00Z"
weight = 61576
keywords = [ "filter", "iograph" ]
aliases = [ "/questions/61576" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [IOgraph y field](/questions/61576/iograph-y-field)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61576-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61576-score" class="post-score" title="current number of votes">0</div><span id="post-61576-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, In ws 1.x, it was possible to enter y axis value filter with predictive input (you type tcp.w and and ws completes wth tcp.windows and so on). Now, with ws 2.x we must enter the right filter, without any help. Is that right or I miss something ? Alain</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-iograph" rel="tag" title="see questions tagged &#39;iograph&#39;">iograph</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 May '17, 09:55</strong></p><img src="https://secure.gravatar.com/avatar/82c6307a500128f138b202023db14283?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="abron&#39;s gravatar image" /><p><span>abron</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="abron has one accepted answer">100%</span></p></div></div><div id="comments-container-61576" class="comments-container"><span id="61579"></span><div id="comment-61579" class="comment"><div id="post-61579-score" class="comment-score">1</div><div class="comment-text"><p>What version (exactly) are you using--and on what platform?</p><p>If I enter either a display filter or a Y field in Wireshark 2.2.6 on Windows (64-bit) then I get offered auto-completions of the field.</p></div><div id="comment-61579-info" class="comment-info"><span class="comment-age">(23 May '17, 11:35)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div><span id="61580"></span><div id="comment-61580" class="comment"><div id="post-61580-score" class="comment-score"></div><div class="comment-text"><p>Hallo, I'm using v2.0.3-0-geed34f0 from master-2.0 on Windows 10 (64 bits).</p><p>Strange, if I check update, it said I'm up to date ! I will do a manual update and try again IO graph.</p><p>Thanks</p></div><div id="comment-61580-info" class="comment-info"><span class="comment-age">(23 May '17, 12:30)</span> <span class="comment-user userinfo">abron</span></div></div></div><div id="comment-tools-61576" class="comment-tools"></div><div class="clear"></div><div id="comment-61576-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61581"></span>

<div id="answer-container-61581" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61581-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61581-score" class="post-score" title="current number of votes">0</div><span id="post-61581-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="abron has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>ho, you're right: the auot-completion works fine with the last version (2.2.6) ! It was a stupid question ... Many thanks for your quickly respons. Best regards Alain</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 May '17, 12:37</strong></p><img src="https://secure.gravatar.com/avatar/82c6307a500128f138b202023db14283?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="abron&#39;s gravatar image" /><p><span>abron</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="abron has one accepted answer">100%</span></p></div></div><div id="comments-container-61581" class="comments-container"></div><div id="comment-tools-61581" class="comment-tools"></div><div class="clear"></div><div id="comment-61581-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

