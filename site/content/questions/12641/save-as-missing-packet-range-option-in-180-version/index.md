+++
type = "question"
title = "Save As Missing Packet Range Option in 1.8.0 version"
description = '''I downloaded the current stable release today and went to save a capture with only the filtered / displayed packets, and this option does not appear in the Save As dialog box.  I downloaded the 64 Bit Windows version (Version 1.8.0 (SVN Rev 43431 from /trunk-1.8)) and while the User&#x27;s Guide still sh...'''
date = "2012-07-11T16:59:00Z"
lastmod = "2012-07-11T18:57:00Z"
weight = 12641
keywords = [ "saveas" ]
aliases = [ "/questions/12641" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Save As Missing Packet Range Option in 1.8.0 version](/questions/12641/save-as-missing-packet-range-option-in-180-version)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12641-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12641-score" class="post-score" title="current number of votes">0</div><span id="post-12641-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I downloaded the current stable release today and went to save a capture with only the filtered / displayed packets, and this option does not appear in the Save As dialog box.<br />
</p><p>I downloaded the 64 Bit Windows version (Version 1.8.0 (SVN Rev 43431 from /trunk-1.8)) and while the User's Guide still shows these options the dialog I am getting only shows options to change folders, file name, or save as type.<br />
</p><p>Could there be a setting I am missing for this or something?<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-saveas" rel="tag" title="see questions tagged &#39;saveas&#39;">saveas</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jul '12, 16:59</strong></p><img src="https://secure.gravatar.com/avatar/a07d15b8c9fbd035b2008f18a48485df?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="PaulB&#39;s gravatar image" /><p><span>PaulB</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="PaulB has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-12641" class="comments-container"></div><div id="comment-tools-12641" class="comment-tools"></div><div class="clear"></div><div id="comment-12641-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12642"></span>

<div id="answer-container-12642" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12642-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12642-score" class="post-score" title="current number of votes">3</div><span id="post-12642-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Beginning with version 1.8.0, these options have now been moved to File &gt; Export Specified Packets.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jul '12, 18:57</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span> </br></p></div></div><div id="comments-container-12642" class="comments-container"></div><div id="comment-tools-12642" class="comment-tools"></div><div class="clear"></div><div id="comment-12642-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

