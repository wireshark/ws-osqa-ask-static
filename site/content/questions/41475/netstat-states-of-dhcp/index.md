+++
type = "question"
title = "[closed] Netstat states of DHCP"
description = '''With the help of netstat the way i can see tcp-socket connection like fin_sent, established, time_wait, close_wait etc...   in similar manner &quot;netstat -un | grep :67&quot; command i am not giving states like bind, renew, rebind states... WHY??????????????? Can u share some idea on this?????? Pl. refer ht...'''
date = "2015-04-15T23:48:00Z"
lastmod = "2015-04-16T12:04:00Z"
weight = 41475
keywords = [ "dhcp", "netstat" ]
aliases = [ "/questions/41475" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Netstat states of DHCP](/questions/41475/netstat-states-of-dhcp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41475-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41475-score" class="post-score" title="current number of votes">0</div><span id="post-41475-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>With the help of netstat the way i can see tcp-socket connection like fin_sent, established, time_wait, close_wait etc...<br />
</p><p>in similar manner "netstat -un | grep :67" command i am not giving states like bind, renew, rebind states... WHY??????????????? Can u share some idea on this??????</p><p>Pl. refer <a href="http://www.tcpipguide.com/free/t_DHCPGeneralOperationandClientFiniteStateMachine.htm">http://www.tcpipguide.com/free/t_DHCPGeneralOperationandClientFiniteStateMachine.htm</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dhcp" rel="tag" title="see questions tagged &#39;dhcp&#39;">dhcp</span> <span class="post-tag tag-link-netstat" rel="tag" title="see questions tagged &#39;netstat&#39;">netstat</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Apr '15, 23:48</strong></p><img src="https://secure.gravatar.com/avatar/ce1843f92a1c18db26bc79b3afa9bd50?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="srinu_bel&#39;s gravatar image" /><p><span>srinu_bel</span><br />
<span class="score" title="20 reputation points">20</span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="20 badges"><span class="bronze">●</span><span class="badgecount">20</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="srinu_bel has no accepted answers">0%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>16 Apr '15, 12:03</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-41475" class="comments-container"><span id="41497"></span><div id="comment-41497" class="comment"><div id="post-41497-score" class="comment-score"></div><div class="comment-text"><blockquote><p>WHY???????????????</p></blockquote><p>Becauuuuuuuuuuuuuuuse, <strong>nestat -un</strong> shows information about UDP sockets on the system and it is TOTALLY unrelated to DHCP!!!!!!!!!!!!!!!!!!!!!!!!!!!</p><p>;-)</p><p>BTW: This question is off topic and thus I closed it.</p></div><div id="comment-41497-info" class="comment-info"><span class="comment-age">(16 Apr '15, 12:04)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-41475" class="comment-tools"></div><div class="clear"></div><div id="comment-41475-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Kurt Knochner 16 Apr '15, 12:03

</div>

</div>

</div>

