+++
type = "question"
title = "what is NBSTAT 00 00 00 00"
description = '''Hi, i tried to check my network security be monitoring packet on my computer (with win10), but i found that my computer exchange nbns packet with every website that i open facebook, google ...  this a screenshot from wireshark session  my computer is clean from malicious software, also i tried with ...'''
date = "2017-10-24T12:44:00Z"
lastmod = "2017-10-25T12:11:00Z"
weight = 64166
keywords = [ "nbstat" ]
aliases = [ "/questions/64166" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [what is NBSTAT 00 00 00 00](/questions/64166/what-is-nbstat-00-00-00-00)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64166-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64166-score" class="post-score" title="current number of votes">0</div><span id="post-64166-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>i tried to check my network security be monitoring packet on my computer (with win10), but i found that my computer exchange nbns packet with every website that i open facebook, google ...</p><p>this a screenshot from wireshark session</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Sans_titre.png" alt="alt text" /></p><p>my computer is clean from malicious software, also i tried with a clean vm with win10 and i found the same packets. with my computer in work i found the same packets exchange with any website that i browse.</p><p>just wanna know what is the story of these packets.</p><p>thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nbstat" rel="tag" title="see questions tagged &#39;nbstat&#39;">nbstat</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Oct '17, 12:44</strong></p><img src="https://secure.gravatar.com/avatar/c0dd74ed646c5b171b679b2de0537257?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Huolsam&#39;s gravatar image" /><p><span>Huolsam</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Huolsam has no accepted answers">0%</span></p></img></div></div><div id="comments-container-64166" class="comments-container"><span id="64167"></span><div id="comment-64167" class="comment"><div id="post-64167-score" class="comment-score"></div><div class="comment-text"><p>Not quite an answer but I'm just seeing exactly the same, apart from IP addresses but they are all public as well:</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Capture_FvyvDgc.PNG" width="640" /></p></div><div id="comment-64167-info" class="comment-info"><span class="comment-age">(24 Oct '17, 13:38)</span> <span class="comment-user userinfo">patkszen</span></div></div><span id="64202"></span><div id="comment-64202" class="comment"><div id="post-64202-score" class="comment-score"></div><div class="comment-text"><p>yeah it's the same, for example in ur case the ip 66.110.49.38 is belong to kaspersky</p><p><a href="https://www.whois.com/whois/66.110.49.38">https://www.whois.com/whois/66.110.49.38</a></p></div><div id="comment-64202-info" class="comment-info"><span class="comment-age">(25 Oct '17, 12:11)</span> <span class="comment-user userinfo">Huolsam</span></div></div></div><div id="comment-tools-64166" class="comment-tools"></div><div class="clear"></div><div id="comment-64166-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="64193"></span>

<div id="answer-container-64193" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64193-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64193-score" class="post-score" title="current number of votes">0</div><span id="post-64193-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I would disable NetBios altogether unless you have a specific need for it.</p><p>Check a previous answer: <a href="https://ask.wireshark.org/questions/2824/unexplained-netbios-traffic">https://ask.wireshark.org/questions/2824/unexplained-netbios-traffic</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Oct '17, 08:48</strong></p><img src="https://secure.gravatar.com/avatar/9591804f3aac21bac1d826cac0cd1109?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Papa%20Packet&#39;s gravatar image" /><p><span>Papa Packet</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Papa Packet has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Oct '17, 08:49</strong> </span></p></div></div><div id="comments-container-64193" class="comments-container"></div><div id="comment-tools-64193" class="comment-tools"></div><div class="clear"></div><div id="comment-64193-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

