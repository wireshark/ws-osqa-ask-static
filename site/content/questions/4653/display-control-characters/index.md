+++
type = "question"
title = "display control characters"
description = '''Is there a way to display ascii control characters in the Hex or Binary view where text is displayed to the right? I am looking for symbology for things like SOH STX ETX EOT. This is useful when reading an embedded or wrapped protocol. Especially serial ascii based ones. '''
date = "2011-06-21T14:24:00Z"
lastmod = "2011-06-23T10:51:00Z"
weight = 4653
keywords = [ "text", "ascii", "display" ]
aliases = [ "/questions/4653" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [display control characters](/questions/4653/display-control-characters)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4653-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4653-score" class="post-score" title="current number of votes">0</div><span id="post-4653-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a way to display ascii control characters in the Hex or Binary view where text is displayed to the right? I am looking for symbology for things like SOH STX ETX EOT. This is useful when reading an embedded or wrapped protocol. Especially serial ascii based ones.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-text" rel="tag" title="see questions tagged &#39;text&#39;">text</span> <span class="post-tag tag-link-ascii" rel="tag" title="see questions tagged &#39;ascii&#39;">ascii</span> <span class="post-tag tag-link-display" rel="tag" title="see questions tagged &#39;display&#39;">display</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jun '11, 14:24</strong></p><img src="https://secure.gravatar.com/avatar/a68c510af7908480b708f0ef309d994d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wickedgeek&#39;s gravatar image" /><p><span>wickedgeek</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wickedgeek has no accepted answers">0%</span></p></div></div><div id="comments-container-4653" class="comments-container"></div><div id="comment-tools-4653" class="comment-tools"></div><div class="clear"></div><div id="comment-4653-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4705"></span>

<div id="answer-container-4705" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4705-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4705-score" class="post-score" title="current number of votes">0</div><span id="post-4705-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Currently, there isn't. I <em>think</em> Unicode has symbols that can be used to display control characters, with the abbreviation for the control character spelled out in small letters, which could be used for this, but that should probably be an option, as those characters aren't as unobtrusive as "."s, and might make the display harder to read if somebody <em>doesn't</em> care about the control characters.</p><p>I'd suggest filing an enhancement request on <a href="http://bugs.wireshark.org/">the Wireshark Bugzilla</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jun '11, 10:51</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-4705" class="comments-container"></div><div id="comment-tools-4705" class="comment-tools"></div><div class="clear"></div><div id="comment-4705-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

