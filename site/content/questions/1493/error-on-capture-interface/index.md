+++
type = "question"
title = "error on capture interface"
description = '''after i click start a error &quot;Please check that &quot;DeviceNPF_{FC0CF46C-B871-42C2-91F2-C82463030CC1}&quot; is the proper interface.&quot;always apear.'''
date = "2010-12-28T00:06:00Z"
lastmod = "2011-01-01T10:23:00Z"
weight = 1493
keywords = [ "capture", "error" ]
aliases = [ "/questions/1493" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [error on capture interface](/questions/1493/error-on-capture-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1493-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1493-score" class="post-score" title="current number of votes">0</div><span id="post-1493-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>after i click start a error "Please check that "DeviceNPF_{FC0CF46C-B871-42C2-91F2-C82463030CC1}" is the proper interface."always apear.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Dec '10, 00:06</strong></p><img src="https://secure.gravatar.com/avatar/c9e5b52b8de1000922ce7f1c4869656c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Satriyo%20Tiyok&#39;s gravatar image" /><p><span>Satriyo Tiyok</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Satriyo Tiyok has no accepted answers">0%</span></p></div></div><div id="comments-container-1493" class="comments-container"><span id="1578"></span><div id="comment-1578" class="comment"><div id="post-1578-score" class="comment-score"></div><div class="comment-text"><p>Does the error say anything more than that? That looks like the second part of an error message, and the first part would probably say why it couldn't open that interface.</p></div><div id="comment-1578-info" class="comment-info"><span class="comment-age">(01 Jan '11, 10:23)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-1493" class="comment-tools"></div><div class="clear"></div><div id="comment-1493-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

