+++
type = "question"
title = "linux non-root question"
description = '''how do you configure a linux install to allow non-root users priviledges'''
date = "2012-11-16T10:24:00Z"
lastmod = "2012-11-16T12:10:00Z"
weight = 15974
keywords = [ "linux-support" ]
aliases = [ "/questions/15974" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [linux non-root question](/questions/15974/linux-non-root-question)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15974-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15974-score" class="post-score" title="current number of votes">0</div><span id="post-15974-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how do you configure a linux install to allow non-root users priviledges</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-linux-support" rel="tag" title="see questions tagged &#39;linux-support&#39;">linux-support</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Nov '12, 10:24</strong></p><img src="https://secure.gravatar.com/avatar/ebf571a260a6a2fa1eda548e4d367d35?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="newton3&#39;s gravatar image" /><p><span>newton3</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="newton3 has no accepted answers">0%</span></p></div></div><div id="comments-container-15974" class="comments-container"></div><div id="comment-tools-15974" class="comment-tools"></div><div class="clear"></div><div id="comment-15974-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15981"></span>

<div id="answer-container-15981" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15981-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15981-score" class="post-score" title="current number of votes">0</div><span id="post-15981-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="https://blog.wireshark.org/2010/02/running-wireshark-as-you/">Running Wireshark as you</a> is a nice start.</p><p>For Debian users, look <a href="http://anonscm.debian.org/viewvc/collab-maint/ext-maint/wireshark/trunk/debian/README.Debian?view=markup">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Nov '12, 12:10</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-15981" class="comments-container"></div><div id="comment-tools-15981" class="comment-tools"></div><div class="clear"></div><div id="comment-15981-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

